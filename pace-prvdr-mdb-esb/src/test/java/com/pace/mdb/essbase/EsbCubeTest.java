/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Properties;

import junit.framework.TestCase;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.essbase.api.base.EssException;
import com.essbase.api.base.IEssIterator;
import com.essbase.api.base.IEssValueAny;
import com.essbase.api.datasource.IEssCube;
import com.essbase.api.datasource.IEssOlapFileObject;
import com.essbase.api.datasource.IEssOlapServer;
import com.essbase.api.metadata.IEssDimension;
import com.pace.base.PafException;
import com.pace.base.mdb.testCommonParms;

public class EsbCubeTest extends TestCase {

	private static final Properties connectionProps = testCommonParms.getConnectionProps();
	private static final Logger logger = Logger.getLogger(EsbCubeTest.class);
	private static final String filePath = "." + File.separator;
	private static final String dataSourceID = "Titan";
	
	public void testGetCubeProperties(){
		boolean isSuccess = true;
		EsbCube esbCube = null;
		IEssCube cube = null;
		IEssOlapServer olapServer = null;
		
		final int[] esbProps = new int[] {IEssCube.PROP_ACTUAL_BLOCK_SIZE,
				IEssCube.PROP_ACTUAL_MAX_BLOCKS,
				IEssCube.PROP_APPLICATION_NAME,
				IEssCube.PROP_BLOCK_DENSITY,
				IEssCube.PROP_CACHE_MEMORY_LOCKING,
				IEssCube.PROP_CALC_CREATE_BLOCK,
				IEssCube.PROP_COMMIT_BLOCKS,
				IEssCube.PROP_COMMIT_ROWS,
				IEssCube.PROP_COMPRESSION_RATIO,
				IEssCube.PROP_COUNT_OF_DIMENSIONS,
				IEssCube.PROP_COUNT_OF_INMEM_COMPRESSED_BLOCKS,
				IEssCube.PROP_COUNT_OF_LOCKS,
				IEssCube.PROP_COUNT_OF_NON_MISSING_NON_LEAF_BLOCKS,
				IEssCube.PROP_COUNT_OF_USERS_CONNECTED,
				IEssCube.PROP_CUBE_ACCESS,
				IEssCube.PROP_CUBE_LOAD_STATUS,
				IEssCube.PROP_CUBE_TYPE,
				IEssCube.PROP_CURRENCY_CATEGORY_MEMBER,
				IEssCube.PROP_CURRENCY_CONV_TYPE,
				IEssCube.PROP_CURRENCY_CONV_TYPE_MEMBER_NAME,
				IEssCube.PROP_CURRENCY_COUNTRY_MEMBER,
				IEssCube.PROP_CURRENCY_CUBE_NAME,
				IEssCube.PROP_CURRENCY_PARTITION_MEMBER,
				IEssCube.PROP_CURRENCY_TIME_MEMBER,
				IEssCube.PROP_CURRENCY_TYPE_MEMBER,
				IEssCube.PROP_DATA_CACHE_SETTING,
				IEssCube.PROP_DATA_CACHE_SIZE,
				IEssCube.PROP_DATA_COMPRESS,
				IEssCube.PROP_DATA_COMPRESS_TYPE,
				IEssCube.PROP_DATA_LOAD_STATUS,
				IEssCube.PROP_DATAFILE_CACHE_SETTING,
				IEssCube.PROP_DATAFILE_CACHE_SIZE,
				IEssCube.PROP_DECLARED_BLOCK_SIZE,
				IEssCube.PROP_DECLARED_MAX_BLOCKS,
				IEssCube.PROP_DESCRIPTION,
				IEssCube.PROP_ELAPSED_CUBE_TIME,
				IEssCube.PROP_EXCLUSIVE_LOCK_COUNT,
				IEssCube.PROP_INDEX_CACHE_SETTING,
				IEssCube.PROP_INDEX_CACHE_SIZE,
				IEssCube.PROP_INDEX_PAGE_SETTING,
				IEssCube.PROP_INDEX_PAGE_SIZE,
				IEssCube.PROP_IS_AUTOLOAD,
				IEssCube.PROP_IS_CALC_NO_AGG_MISSING,
				IEssCube.PROP_IS_CALC_NO_AVG_MISSING,
				IEssCube.PROP_IS_CALC_TWO_PASS,
				IEssCube.PROP_IS_LODABLE,
				IEssCube.PROP_ISOLATION_LEVEL,
				IEssCube.PROP_MAX_MEM_NON_COMP_BLOCKS,
				IEssCube.PROP_NAME,
				IEssCube.PROP_NON_EXCLUSIVE_LOCK_COUNT,
				IEssCube.PROP_NON_MISSING_LEAF_BLOCKS,
				IEssCube.PROP_PAGED_IN_BLOCKS,
				IEssCube.PROP_PAGED_OUT_BLOCKS,
				IEssCube.PROP_PRE_EMAGE,
				IEssCube.PROP_RETRIEVAL_BUFFER,
				IEssCube.PROP_RETRIEVAL_SORT_BUFFER,
				IEssCube.PROP_SPARSE_DENSITY,
				IEssCube.PROP_TOTAL_BLOCKS,
				IEssCube.PROP_TOTAL_MEMORY_ALL_BLOCKS,
				IEssCube.PROP_TOTAL_MEMORY_FOR_INDEX,
				IEssCube.PROP_TOTAL_MEMORY_FOR_INMEM_COMPRESSED_BLOCKS,
				IEssCube.PROP_TOTAL_MEMORY_PAGED_IN_BLOCKS};
				
		logger.info("***************************************************");
		logger.info(this.getName() +  " - Test Started");
		try {
			
			// Open the Essbase cube
			logger.info("Opening cube: [" + dataSourceID + "]");
			logger.info("Connection Properties: [" + connectionProps.toString() + "]");
			esbCube = new EsbCube(connectionProps);				
			cube = esbCube.getEssCube();
			olapServer = esbCube.getOlapServer();
			
			//for(int p  : esbProps){
				//logger.info("property: " + p + ", value: " + getSetting(cube, p).toString());
			//}
			
			IEssIterator itr = cube.getDimensions();
			for(int i = 0; i < itr.getCount(); i++){
				printDimension((IEssDimension) itr.getAt(i));
			}
			
			//====General Tab====
			//Buffer Sizes (match exactly)
			assertEquals(getIntSetting(cube, IEssCube.PROP_RETRIEVAL_BUFFER ,true), 200);
			assertEquals(getIntSetting(cube, IEssCube.PROP_RETRIEVAL_SORT_BUFFER ,true), 20);
			
			//====Stats Tab=====
			assertEquals(getDoubleSetting(cube, IEssCube.PROP_TOTAL_BLOCKS, 0), 30468.0);
			//Missing Block Size
			assertEquals(getDoubleSetting(cube, IEssCube.PROP_ACTUAL_MAX_BLOCKS, 0), 4606272.0);
			assertEquals(getDoubleSetting(cube, IEssCube.PROP_NON_MISSING_LEAF_BLOCKS, 0), 17309.0);
			assertEquals(getDoubleSetting(cube, IEssCube.PROP_COUNT_OF_NON_MISSING_NON_LEAF_BLOCKS, 0), 13159.0);
			assertEquals(getDoubleSetting(cube, IEssCube.PROP_BLOCK_DENSITY, 2), 2.95);
			assertEquals(getDoubleSetting(cube, IEssCube.PROP_SPARSE_DENSITY, 2), .66);
			assertEquals(getDoubleSetting(cube, IEssCube.PROP_COMPRESSION_RATIO, 2), .04);

			
			//====Caches Tab====
			//Cache Sizes
			assertEquals(getIntSetting(cube, IEssCube.PROP_DATA_CACHE_SETTING ,true), 3067);
			assertEquals(getIntSetting(cube, IEssCube.PROP_DATA_CACHE_SIZE ,true), 3067);
			
			//Cache File Sizes (these values don't see to match EAS)
			assertEquals(getIntSetting(cube, IEssCube.PROP_DATAFILE_CACHE_SETTING ,true), 0);
			assertEquals(getLongSetting(cube, IEssCube.PROP_DATAFILE_CACHE_SIZE ,true), 0);
			
			//Index Cache Sizes (these values don't see to match EAS)
			assertEquals(getIntSetting(cube, IEssCube.PROP_INDEX_CACHE_SETTING ,true), 1024);
			assertEquals(getIntSetting(cube, IEssCube.PROP_INDEX_CACHE_SIZE ,true), 1024);
			
			//List<String> fileObjects = EsbUtility.copyCubeObjectsToFile(cube, IEssOlapFileObject.TYPE_CALCSCRIPT);
			
			File tempFile = EsbUtility.copyOutlineToFile(olapServer, cube, "/home/kmoos/Downloads");
			logger.info(tempFile.getAbsolutePath());
			FileUtils.copyFile(tempFile, new File("/home/kmoos/Downloads/titan.otl"));
			
				
		} catch (PafException pfe) {
			logger.error(pfe.getMessage());	
			fail(pfe.getMessage());
			isSuccess = false;
		} catch (Exception e) {
			logger.error("Java Exception: " + e.getMessage());	
			fail(e.getMessage());
			isSuccess = false;
		} finally {
			System.out.print(this.getName());
			if (isSuccess) {
				logger.info(this.getName() + " - Successful");
				logger.info("***************************************************\n");
			} else {
				logger.info(this.getName() + " - Failed");			
				logger.info("***************************************************\n");
			}
			assertTrue(isSuccess);
		}
	}
	
	private void printDimension(IEssDimension dimension) throws EssException{
		logger.info(dimension.getName());
		logger.info(dimension.getStorageType().stringValue());
		logger.info(dimension.getDeclaredSize());
		logger.info(dimension.getActualSize());
		
	}
	
	private double getDoubleSetting(IEssCube cube, int setting, int decPlaces) throws EssException{

		return BigDecimal.valueOf(getSetting(cube, setting).getDouble()).setScale(decPlaces, BigDecimal.ROUND_HALF_UP).doubleValue();
		
	}
	
	
	private int getIntSetting(IEssCube cube, int setting, boolean convertToKb) throws EssException{
		
		int value = getSetting(cube, setting).getInt();

		//convert to KB
		if(convertToKb){
			return (value / 1024);
		} 
		
		return value;
		
	}
	
	private long getLongSetting(IEssCube cube, int setting, boolean convertToKb) throws EssException{
		
		long value = getSetting(cube, setting).getLong();

		//convert to KB
		if(convertToKb){
			return (value / 1024);
		} 
		
		return value;
		
	}
	
	private IEssValueAny getSetting(IEssCube cube, int setting) throws EssException{

		return cube.getPropertyValueAny(setting);
	}
	
}
