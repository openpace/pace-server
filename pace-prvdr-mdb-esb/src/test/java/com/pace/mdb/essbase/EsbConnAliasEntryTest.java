/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;


import org.apache.log4j.Logger;

import junit.framework.TestCase;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public class EsbConnAliasEntryTest extends TestCase {

	private static Logger logger = Logger.getLogger(EsbConnAliasEntryTest.class);

	/*
	 * Test method for 'com.pace.base.mdb.essbase.EsbConnAliasEntry.EsbConnAliasEntry(String)'
	 */
	public void testEsbConnAliasEntry() {

		boolean isSuccess = true;
		EsbConnAliasEntry aliasEntry =	null;
		String connAliases[] = {"TitanServer", "TitanCube", "TitanCube2"};
		
		logger.info("***************************************************");
		logger.info(this.getName() +  " - Test Started");
		try {
			// Loop through each connection alias and display it's info
			for (int i = 0; i < connAliases.length; i++) {
				aliasEntry = new EsbConnAliasEntry(connAliases[i]);
				logger.info("\n-- Creating new EsbConnAliasEntry object for " + aliasEntry.getEsbConnAlias());
				logger.info("-- Connection Alias: " + aliasEntry.getEsbConnAlias());
				logger.info("-- Connection Type: " + aliasEntry.getEsbConnType().toString());
				logger.info("-- Server: " + aliasEntry.getEsbServer());
				logger.info("-- User: " + aliasEntry.getEsbUser());
				logger.info("-- Password: " + aliasEntry.getEsbPswd());
				logger.info("-- App: " + aliasEntry.getEsbApp());
				logger.info("-- Db: " + aliasEntry.getEsbDb());
			}
		} catch (Exception e) {
			isSuccess = false;
			logger.info("Java Exception: " + e.getMessage());			
		} finally {
			try {
				assertTrue(isSuccess);
			} finally {
				logger.info("\n" + this.getName());
				if (isSuccess) {
					logger.info(" - Successful");
				}
				else {
					logger.info(" - Failed");			
				}
				logger.info("***************************************************");
				logger.info(""); 
			}
		}

	}

}
