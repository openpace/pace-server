/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import junit.framework.TestCase;


/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public class EsbConnParmsTest extends TestCase {

	
	/*
	 * Test method for 'com.pace.base.mdb.essbase.EsbConnType.EsbConnParms.EsbConnParms(String)'
	 */
	public void testEsbConnParms() {
		
		boolean isSuccess = true;
		EsbConnParms connParms =	null;
		String connAliases[] = {"TitanServer", "TitanCube", "TitanCube2"};
		
		System.out.println("***************************************************");
		System.out.println(this.getName() +  " - Test Started");
		try {
			// Loop through each connection alias and display it's info
			for (int i = 0; i < connAliases.length; i++) {
				connParms = new EsbConnParms(connAliases[i]);
				System.out.println("\n-- Creating new EsbConnParms object for " + connAliases[i]);
				System.out.println("-- Connection Type: " + connParms.getEsbConnType().toString());
				System.out.println("-- Server: " + connParms.getEsbServer());
				System.out.println("-- User: " + connParms.getEsbUser());
				System.out.println("-- Password: " + connParms.getEsbPswd());
				System.out.println("-- App: " + connParms.getEsbApp());
				System.out.println("-- Db: " + connParms.getEsbDb());
			}
		} catch (Exception e) {
			isSuccess = false;
			System.out.println("Java Exception: " + e.getMessage());			
		} finally {
			try {
				assertTrue(isSuccess);
			} finally {
				System.out.print("\n" + this.getName());
				if (isSuccess) {
					System.out.println(" - Successful");
				}
				else {
					System.out.println(" - Failed");			
				}
				System.out.println("***************************************************");
				System.out.println(""); 
			}
		}

	}

}
