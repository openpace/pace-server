/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import java.util.Properties;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.mdb.testCommonParms;

/**
 * EsbCalculate Unit Tests
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public class EsbCalculateTest extends TestCase {        
	
	private static Properties connectionProps = testCommonParms.getConnectionProps();
	private static Logger logger = Logger.getLogger(EsbCalculateTest.class);
	
	/*
	 * Test method for 'com.pace.base.mdb.essbase.EsbCalcCmd.execute()'
	 */
	public void testExecute() {
		
		boolean isSuccess = true;
		final String dataSourceID = "Titan", calcScript = "zClcTst";
		String esbErrorMsg = null;
				
		logger.info("***************************************************");
		logger.info(this.getName() +  " - Test Started");
		try {
			
			// Create Essbase data object
			logger.info("Creating Essbase data object");
			EsbData esbData = new EsbData(connectionProps, dataSourceID);
			
			// Execute custom Essbase calc process
			logger.info("Running custom Essbase calc [" + calcScript + "]");
			esbErrorMsg = esbData.runCalcScript(calcScript);
			if (esbErrorMsg != null) {
				String errMsg = "Calc script error: " + esbErrorMsg;
				throw new PafException(errMsg, PafErrSeverity.Fatal);			
			}
			
		} catch (PafException pfe) {
			logger.error(pfe.getMessage());	
			isSuccess = false;
		} catch (Exception e) {
			logger.error("Java Exception: " + e.getMessage());	
			isSuccess = false;
		} finally {
			System.out.print(this.getName());
			if (isSuccess) {
				logger.info(this.getName() + " - Successful");
				logger.info("***************************************************\n");
			} else {
				logger.info(this.getName() + " - Failed");			
				logger.info("***************************************************\n");
			}
			assertTrue(isSuccess);
		}
	}
}