/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import java.util.Properties;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.pace.base.mdb.testCommonParms;

/**
 * Test the PafBaseTree object
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafBaseTreeTest1 extends TestCase {

	@SuppressWarnings("unused")
	private Properties titanProps = testCommonParms.getConnectionProps();
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger("PafBaseTreeTest");

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getTreeType()}.
	 */
	public void testGetTreeType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#PafBaseTree()}.
	 */
	public void testPafBaseTree() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#PafBaseTree(com.pace.base.mdb.PafBaseMember, java.lang.String[])}.
	 */
	public void testPafBaseTreePafBaseMemberStringArray() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#PafBaseTree(com.pace.base.mdb.PafBaseMember, java.lang.String[], int)}.
	 */
	public void testPafBaseTreePafBaseMemberStringArrayInt() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#PafBaseTree(com.pace.base.mdb.PafBaseMember, java.lang.String[], int, float)}.
	 */
	public void testPafBaseTreePafBaseMemberStringArrayIntFloat() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getMember(java.lang.String)}.
	 */
	public void testGetMemberString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getRootNode()}.
	 */
	public void testGetRootNode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#setRootNode(com.pace.base.mdb.PafBaseMember)}.
	 */
	public void testSetRootNodePafBaseMember() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#addChildByName(java.lang.String, java.lang.String)}.
	 */
	public void testAddChildByName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#setAttributeDimInfo(java.util.Map)}.
	 */
	public void testSetAttributeDimInfo() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getAttributeDimNames()}.
	 */
	public void testGetAttributeDimNames() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getAttributeMappingLevel(java.lang.String)}.
	 */
	public void testGetAttributeMappingLevel() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getAttributeValues(java.lang.String, java.lang.String)}.
	 */
	public void testGetAttributeMembers() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getSubTreeCopy(java.lang.String)}.
	 */
	public void testGetSubTreeCopyString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getSubTreeCopy(java.lang.String, int)}.
	 */
	public void testGetSubTreeCopyStringInt() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getSubTreeCopyByGen(java.lang.String, int)}.
	 */
	public void testGetSubTreeCopyByGenStringInt() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getSimpleVersion()}.
	 */
	public void testGetSimpleVersion() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafBaseTree#getMemberValues()}.
	 */
	public void testGetMemberValues() {
		fail("Not yet implemented");
	}

}
