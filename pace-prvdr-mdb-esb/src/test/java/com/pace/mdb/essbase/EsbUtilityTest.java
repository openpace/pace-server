/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import java.io.File;
import java.util.Properties;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.essbase.api.datasource.IEssCube;
import com.essbase.api.datasource.IEssOlapServer;
import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.mdb.IMdbCubeInfo;
import com.pace.base.mdb.testCommonParms;

/**
 * EsbUtility Unit Tests
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class EsbUtilityTest extends TestCase {

	private static final Properties connectionProps = testCommonParms.getConnectionProps();
	private static final Logger logger = Logger.getLogger(EsbUtilityTest.class);
	private static final String filePath = "." + File.separator;
	private static final String calcScript = "zClcTst";
	private static final String dataSourceID = "Titan";

	/*
	 * Test method for 'com.pace.base.mdb.essbase.EsbUtility.copyCalcScriptToFile()'
	 */
	public void testCopyCalcScriptToFile() {
		
		boolean isSuccess = true;
		EsbCube esbCube = null;
		IEssCube cube = null;
		IEssOlapServer olapServer = null;
		
				
		logger.info("***************************************************");
		logger.info(this.getName() +  " - Test Started");
		try {
			
			// Open the Essbase cube
			logger.info("Opening cube: [" + dataSourceID + "]"); 
			esbCube = new EsbCube(connectionProps);				
			cube = esbCube.getEssCube();
			olapServer = esbCube.getOlapServer();
				
			// Copy calc script to file
			logger.info("Copying calc script [" + calcScript + "] to file");
			File calcScriptFile = EsbUtility.copyCalcScriptToFile(olapServer, cube, calcScript, filePath);
			logger.info("Temp file: " + calcScriptFile.getPath());
			
				
		} catch (PafException pfe) {
			logger.error(pfe.getMessage());	
			isSuccess = false;
		} catch (Exception e) {
			logger.error("Java Exception: " + e.getMessage());	
			isSuccess = false;
		} finally {
			System.out.print(this.getName());
			if (isSuccess) {
				logger.info(this.getName() + " - Successful");
				logger.info("***************************************************\n");
			} else {
				logger.info(this.getName() + " - Failed");			
				logger.info("***************************************************\n");
			}
			assertTrue(isSuccess);
		}
	}

	
	/*
	 * Test method for 'com.pace.base.mdb.essbase.EsbUtility.copyCalcScriptToServer()'
	 */
	public void testCopyCalcScriptToServer() {
		
		boolean isSuccess = true;
		String calcScriptFileName = null, calcScriptName = null;
		File calcScriptFile = null;
		EsbCube esbCube = null;
		IEssCube cube = null;
		IEssOlapServer olapServer = null;
		
				
		logger.info("***************************************************");
		logger.info(this.getName() +  " - Test Started");
		try {
			
			// Open the Essbase cube
			logger.info("Opening cube: [" + dataSourceID + "]"); 
			esbCube = new EsbCube(connectionProps);				
			cube = esbCube.getEssCube();
			olapServer = esbCube.getOlapServer();
			
			// Copy calc script to file
			logger.info("Copying calc script [" + calcScript + "] to file");
			calcScriptFile = EsbUtility.copyCalcScriptToFile(olapServer, cube, calcScript, filePath);
			logger.info("Temp file: " + calcScriptFile.getPath());
					
			// Copy calc script file to server
			calcScriptFileName = calcScriptFile.getPath();
			calcScriptName = calcScriptFile.getName().substring(0,8);
			logger.info("Copying calc script file[" + calcScriptFileName + "] to server as " + calcScriptName);
			calcScriptName = EsbUtility.copyCalcScriptToServer(olapServer, cube, calcScriptFile, calcScriptName);
			logger.info("CalcScriptName: [" + calcScriptName + "]");
			
				
		} catch (PafException pfe) {
			logger.error(pfe.getMessage());	
			isSuccess = false;
		} catch (Exception e) {
			logger.error("Java Exception: " + e.getMessage());	
			isSuccess = false;
		} finally {
			System.out.print(this.getName());
			if (isSuccess) {
				logger.info(this.getName() + " - Successful");
				logger.info("***************************************************\n");
			} else {
				logger.info(this.getName() + " - Failed");			
				logger.info("***************************************************\n");
			}
			assertTrue(isSuccess);
		}
	}

	/*
	 * Test method for 'com.pace.base.mdb.essbase.EsbUtility.()'
	 */
	public void testEsbInfoObjects() {
		
		boolean isSuccess = false;
		EsbCube esbCube = null;
		IEssCube cube = null;
		IMdbCubeInfo cubeInfo = null;
				
		logger.info("***************************************************");
		logger.info(this.getName() +  " - Test Started");
		try {
			
			// Open the Essbase cube
			logger.info("Opening cube: [" + dataSourceID + "]"); 
			esbCube = new EsbCube(connectionProps);				
			cube = esbCube.getEssCube();
			
			//Get the cube object
			cubeInfo = EsbUtility.getCubeInfo(cube);
			//Assert the cube info object
			assertTrue(cubeInfo instanceof EsbCubeInfo);
			EsbCubeInfo esbCubeInfo = (EsbCubeInfo)cubeInfo;
			assertEquals(connectionProps.getProperty(PafBaseConstants.CONN_PROPERTY_ESB_DB), esbCubeInfo.getName());
			assertTrue(esbCubeInfo.getCalcScriptNames() != null && esbCubeInfo.getCalcScriptNames().length > 0);
			
			//Validate the server info object
			assertNotNull(esbCubeInfo.getEsbServerInfo());
			assertEquals(connectionProps.getProperty(PafBaseConstants.CONN_PROPERTY_ESB_SERVER), esbCubeInfo.getEsbServerInfo().getName());
			assertEquals(String.format("EsbServerInfo [name=%s, version=11.1.2]", connectionProps.getProperty(PafBaseConstants.CONN_PROPERTY_ESB_SERVER)), esbCubeInfo.getEsbServerInfo().toString());
			
			//Validate the app info object
			assertNotNull(esbCubeInfo.getEsbAppInfo());
			assertEquals(connectionProps.getProperty(PafBaseConstants.CONN_PROPERTY_ESB_APP), esbCubeInfo.getEsbAppInfo().getName());
			assertEquals(connectionProps.getProperty(PafBaseConstants.CONN_PROPERTY_ESB_SERVER), esbCubeInfo.getEsbAppInfo().getServerName());
			
			isSuccess = true;
			
		}catch (PafException pfe) {
			logger.error(pfe.getMessage());	
		} catch (Exception e) {
			logger.error("Java Exception: " + e.getMessage());	
		} finally {
			System.out.print(this.getName());
			if (isSuccess) {
				logger.info(this.getName() + " - Successful");
				logger.info("***************************************************\n");
			} else {
				logger.info(this.getName() + " - Failed");			
				logger.info("***************************************************\n");
			}
			assertTrue(isSuccess);
		}
	}
}
