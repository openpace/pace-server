/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.pace.base.mdb.PafBaseMember;
import com.pace.base.mdb.PafSimpleBaseMember;

/**
 * Test PafBaseMember
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public class PafBaseMemberTest extends TestCase {
	
	private static Logger logger = Logger.getLogger(PafBaseMemberTest.class);
	
	/*
	 * Test method for 'com.pace.base.mdb.PafBaseMember.getSimpleVersion()'
	 */
	public void testGetSimpleVersion() {
		boolean isSuccess = true;
		
		String validationErrors = "";
		final String key = "Test Member";
		
		PafBaseMember member = null;
		PafSimpleBaseMember simple = null;
		
		
		logger.info("***************************************************");
		logger.info(this.getName() +  " - Test Started");
		try {
			// Create and populate a representative PafBaseMember
			member = new PafBaseMember(key);
			
			// Get simple version of PafBaseMember
			simple = member.getSimpleVersion();

		
			// Validate member key
			if (!simple.getKey().equals(member.getKey())) {
				validationErrors += "Member key doesn't match\n";
			}
			
		} catch (Exception e) {
			logger.info("Java Exception: " + e.getMessage());	
			isSuccess = false;
		} finally {
			try {
				if (validationErrors.length() > 0) {
					isSuccess = false;
					logger.info("Validation Error(s) Encountered:\r" + validationErrors);
				}
				assertTrue(isSuccess);
			} finally {
				System.out.print(this.getName());
				if (isSuccess) {
					logger.info(this.getName() + " - Successful");
					logger.info("***************************************************\n");
				}
				else {
					logger.info(this.getName() + " - Failed");			
					logger.info("***************************************************\n");
				}
			}
		}
	
	}
		
}