/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

/**
 * Class to hold information about an Essbase Server.
 * @author themoosman
 *
 */
public class EsbServerInfo{

	private String name;
	private String version;
	
	public EsbServerInfo(){
		this(null, null);
	}

	public EsbServerInfo(String name, String version) {
		super();
		this.name = name;
		this.version = version;
	}

	/**
	 * Gets the name of the Essbase server
	 * @return
	 */
	public String getName() {
		return name;
	}
	/**
	 * Gets the Essbase server version
	 * @return
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * Sets the name of the Essbase server
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Sets the Essbase server version
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EsbServerInfo other = (EsbServerInfo) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EsbServerInfo [name=" + name + ", version=" + version + "]";
	}

}
