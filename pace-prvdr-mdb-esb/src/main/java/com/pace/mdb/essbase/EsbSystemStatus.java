/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import java.io.File;
import java.util.Arrays;

import com.pace.base.mdb.IMdbCubeInfo;
import com.pace.base.mdb.IMdbSystemStatus;

/**
 * Class to hold status information about the underlying Essbase MDB structure.
 * @author themoosman
 *
 */
public class EsbSystemStatus implements IMdbSystemStatus {

	private IMdbCubeInfo mdbServerInfo;
	private String testResults;
	private boolean canConnect;
	private File outline;
	private File[] reportScripts;
	private File[] calcScripts;
	
	/**
	 * Default constructor.
	 */
	public EsbSystemStatus() {
		this(null, false);
	}
	
	/**
	 * 
	 * @param testResults String representation of test results.
	 * @param canConnect was the connection test successful.
	 */
	public EsbSystemStatus(String testResults, boolean canConnect) {
		this(testResults, canConnect, new EsbCubeInfo());
	}
	
	/**
	 * 
	 * @param testResults
	 * @param canConnect
	 * @param cubeInfo
	 */
	public EsbSystemStatus(String testResults, boolean canConnect, EsbCubeInfo cubeInfo) {
		super();
		this.mdbServerInfo = cubeInfo;
	}

	@Override
	public IMdbCubeInfo getMdbCubeInfo() {
		return mdbServerInfo;
	}

	@Override
	public void setMdbCubeInfo(IMdbCubeInfo mdbServerInfo) {
		this.mdbServerInfo = mdbServerInfo;
		
	}
	
	@Override
	public String getTestResults() {
		return testResults;
	}
	
	@Override
	public void setTestResults(String testResults) {
		this.testResults = testResults;
	}
	
	@Override
	public boolean isCanConnect() {
		return canConnect;
	}
	
	@Override
	public void setCanConnect(boolean canConnect) {
		this.canConnect = canConnect;
	}

	@Override
	public File getOutline() {
		return outline;
	}

	@Override
	public void setOutline(File outline) {
		this.outline = outline;
		
	}

	@Override
	public File[] getReportScripts() {
		return reportScripts;
	}

	@Override
	public void setReportScripts(File[] reportScripts) {
		this.reportScripts = reportScripts;
	}

	@Override
	public File[] getCalcScripts() {
		return calcScripts;
	}

	@Override
	public void setCalcScripts(File[] calcScripts) {
		this.calcScripts = calcScripts;
	}

	@Override
	public String toString() {
		return "EsbSystemStatus [mdbServerInfo=" + mdbServerInfo
				+ ", testResults=" + testResults + ", canConnect=" + canConnect
				+ ", outline=" + outline + ", reportScripts="
				+ Arrays.toString(reportScripts) + ", calcScripts="
				+ Arrays.toString(calcScripts) + "]";
	}
}
