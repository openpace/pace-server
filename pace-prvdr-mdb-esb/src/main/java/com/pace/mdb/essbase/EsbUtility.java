/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Properties;

import java.util.Set;


import org.apache.log4j.Logger;

import com.essbase.api.base.EssException;
import com.essbase.api.base.IEssBaseObject;
import com.essbase.api.base.IEssIterator;
import com.essbase.api.base.IEssValueAny;
import com.essbase.api.dataquery.IEssCubeView;
import com.essbase.api.dataquery.IEssMdDataSet;
import com.essbase.api.dataquery.IEssOpMdxQuery;
import com.essbase.api.dataquery.IEssOpMdxQuery.EEssMemberIdentifierType;
import com.essbase.api.datasource.EssCube;
import com.essbase.api.datasource.EssOlapApplication;
import com.essbase.api.datasource.EssOlapServer;
import com.essbase.api.datasource.IEssCube;
import com.essbase.api.datasource.IEssOlapApplication;
import com.essbase.api.datasource.IEssOlapFileObject;
import com.essbase.api.datasource.IEssOlapServer;
import com.essbase.api.metadata.IEssCubeOutline;
import com.essbase.api.metadata.IEssDimension;
import com.essbase.api.metadata.IEssMember;
import com.essbase.api.metadata.IEssMember.EEssConsolidationType;
import com.essbase.api.metadata.IEssMember.EEssShareOption;
import com.essbase.api.metadata.IEssMember.EEssTimeBalanceOption;
import com.essbase.api.metadata.IEssMemberSelection;
import com.pace.base.PafBaseConstants;
import com.pace.base.utility.FileUtils;
import com.pace.base.utility.StringUtils;

/**
 * Essbase utility functions
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public abstract class EsbUtility {
    
	private final static Logger logger = Logger.getLogger(EsbUtility.class);
	
	private static final String ESS_CALC_SCRIPT_SUFFIX = ".csc";
	private static final String ESS_LOAD_RULE_SUFFIX = ".rul";
	private static final String ESS_REPORT_SCRIPT_SUFFIX = ".rep";
	private static final String ESS_TEMP_FILE_PREFIX = "esb";
	private static final String ESS_TEXT_FILE_SUFFIX = ".txt";
	private static final String ESS_OUTLINE_SUFFIX = ".otl";
	private static final String LINE_TERM = PafBaseConstants.LINE_TERM;
	private static final int ESS_FILE_OJBECT_NAME_MAX_LEN = 8;
	private static final Set<Integer> ESS_CONNECT_TIMEOUT_ERRCODE_SET = 
			new HashSet<Integer>(Arrays.asList(1042006,1290001));		// TTN-2428
	private static final Set<String> ESS_CONNECT_TIMEOUT_ERRMSG_SET = 
			new HashSet<String>(Arrays.asList("client timed out"));		// TTN-2428
	private static final int SUB_VAR_ROW_NDX_VAR_NAME = 0; 
	private static final int SUB_VAR_ROW_NDX_VAR_VALUE = 1;
	private static final int SUB_VAR_ROW_NDX_APP_NAME = 2;
	private static final int SUB_VAR_ROW_NDX_CUBE_NAME = 3;
	private static final int SUB_VAR_ROW_NDX_SERVER_NAME = 4;
	private static final int PROP_RETRIEVAL_BUFFER = 54;
	private static final int PROP_RETRIEVAL_SORT_BUFFER = 55;
	private static final int PROP_TOTAL_BLOCKS = 37;
	private static final int PROP_INDEX_CACHE_SETTING = 52;
	private static final int PROP_DATA_CACHE_SETTING = 49;

//	/**
//	 *	Copy Essbase calc script to temp file
//	 *
//	 * @param olapServer Essbase olap server
//	 * @param cube Essbase cube
//	 * @param calcScript Name of Essbase calc script 
//	 * 
//	 * @return File object
//	 * @throws IOException 
//	 * @throws EssException 
//	 */
//	public static File copyCalcScriptToFile(IEssOlapServer olapServer, IEssCube cube, String calcScript) throws IOException, EssException {
//		return copyServerObjectToFile(olapServer, cube, calcScript, IEssOlapFileObject.TYPE_CALCSCRIPT, ESS_CALC_SCRIPT_SUFFIX, null);
//	}
	
	/**
	 *	Copy Essbase calc script to temp file
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param calcScript Name of Essbase calc script 
	 * @param tempFilePath Temp file path
	 * 
	 * @return File object
	 * @throws IOException 
	 * @throws EssException 
	 */
	public static File copyCalcScriptToFile(IEssOlapServer olapServer, IEssCube cube, String calcScript, String tempFilePath) throws IOException, EssException {
		return copyServerObjectToFile(olapServer, cube, calcScript, IEssOlapFileObject.TYPE_CALCSCRIPT, ESS_CALC_SCRIPT_SUFFIX, tempFilePath);
	}
	

//	/**
//	 *	Copy Essbase report script to temp file
//	 *
//	 * @param olapServer Essbase olap server
//	 * @param cube Essbase cube
//	 * @param reportScript Name of Essbase report script 
//	 * 
//	 * @return File object
//	 * @throws IOException 
//	 * @throws EssException 
//	 */
//	public static File copyReportScriptToFile(IEssOlapServer olapServer, IEssCube cube, String calcScript) throws IOException, EssException {
//		return copyServerObjectToFile(olapServer, cube, calcScript, IEssOlapFileObject.TYPE_REPORT, ESS_REPORT_SCRIPT_SUFFIX, null);
//	}
	
	/**
	 *	Copy Essbase report script to temp file
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param reportScript Name of Essbase report script 
	 * @param tempFilePath Temp file path
	 * 
	 * @return File object
	 * @throws IOException 
	 * @throws EssException 
	 */
	public static File copyReportScriptToFile(IEssOlapServer olapServer, IEssCube cube, String calcScript, String tempFilePath) throws IOException, EssException {
		return copyServerObjectToFile(olapServer, cube, calcScript, IEssOlapFileObject.TYPE_REPORT, ESS_REPORT_SCRIPT_SUFFIX, tempFilePath);
	}
	
	/**
     * Copies Essbase Outline to a temporary file
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param tempFilePath Temp file path
	 * @return File object
	 * @throws IOException
	 * @throws EssException
	 */
	public static File copyOutlineToFile(IEssOlapServer olapServer, IEssCube cube, String tempFilePath) throws IOException, EssException {
		File tempFile =  copyServerObjectToFile(olapServer, cube, cube.getName(), IEssOlapFileObject.TYPE_OUTLINE, ESS_OUTLINE_SUFFIX, tempFilePath);
		File outline = new File(tempFilePath + File.separator + cube.getName() + ESS_OUTLINE_SUFFIX);
		FileUtils.copy(tempFile, outline);

		return outline;
	}
	
	/**
	 *	Copy Essbase server object to temp file
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param objectName Name of server object
	 * @param objectType Server object type
	 * @param fileSuffix Temp file suffix
	 * @param tempFilePath Temp file path
	 * 
	 * @return File object
	 * @throws IOException
	 * @throws EssException 
	 */
	public static File[] copyServerObjectsToFile(IEssOlapServer olapServer, IEssCube cube, String[] objectNames, int objectType, String fileSuffix, String tempFilePath) throws IOException, EssException {
		File[] files = new File[objectNames.length];
		
		for(int i = 0; i < files.length; i++){
			files[i] = copyServerObjectToFile(olapServer, cube, objectNames[i], objectType, fileSuffix, tempFilePath);
		}
		
		return files;
	}
	
	
	/**
	 *	Copy Essbase server object to temp file
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param objectName Name of server object
	 * @param objectType Server object type
	 * @param fileSuffix Temp file suffix
	 * @param tempFilePath Temp file path
	 * 
	 * @return File object
	 * @throws IOException
	 * @throws EssException 
	 */
	public static File copyServerObjectToFile(IEssOlapServer olapServer, IEssCube cube, String objectName, int objectType, String fileSuffix, String tempFilePath) throws IOException, EssException {
		
		// Create temporary file. If the temporary file directory does not exist, create it.
		File fileObject = new File(tempFilePath); 
		fileObject.mkdir();
		if (!fileObject.exists()) {
			StringBuffer errMsg = new StringBuffer("Can't copy Essbase object to temporary file, the required file path does not exist and could not be created [");
			errMsg.append(fileObject.getAbsolutePath());
			errMsg.append("]. Essbase process aborted.");
			logger.error(errMsg);
			throw new IOException(errMsg.toString());			
		}
		File esbFile = File.createTempFile(ESS_TEMP_FILE_PREFIX, fileSuffix, fileObject);
		String esbFileName = esbFile.getPath();
		esbFile.deleteOnExit();
	
		// Copy Essbase server object to file
		try {
			// Essbase 11 compliance - it was necessary to modify the API call below, by calling the
			// copyOlapFileObjectToSever method on the Olap Server object instead of the Cube object.
			// A change in API functionality caused API to look for the file on the HPS server, instead
			// of the local client machine.
			olapServer.copyOlapFileObjectFromServer(cube.getApplicationName(), cube.getName(), objectType, objectName, esbFileName, false);
		} catch (EssException esx) {
			throw esx;
		}
		
		// Return essbae file
		return esbFile;
	}
	
	/**
	 * Gets the name of all the calc scripts from a cube
	 * @param cube Essbase cube
	 * @return Array of calc script names
	 * @throws EssException
	 */
	public static String[] essGetCubeCalcScriptNames(IEssCube cube) throws EssException {
		return essGetCubeFileObjectsName(cube, IEssOlapFileObject.TYPE_CALCSCRIPT);
	}
	
	/**
	 * Gets the name of all the report scripts from a cube
	 * @param cube Essbase cube
	 * @return Array of report script names
	 * @throws EssException
	 */
	public static String[] essGetCubeReportScriptNames(IEssCube cube) throws EssException {
		return essGetCubeFileObjectsName(cube, IEssOlapFileObject.TYPE_REPORT);
	}
	
	/**
	 * Gets all the calc script from a cube.
	 * @param cube Essbase cube
	 * @param tempFilePath Temp file path
	 * @return Array of calc script files
	 * @throws EssException
	 * @throws IOException
	 */
	public static File[] essGetCubeCalcScripts(IEssCube cube, String tempFilePath) throws EssException, IOException {
		String[] fileNames = essGetCubeFileObjectsName(cube, IEssOlapFileObject.TYPE_CALCSCRIPT);
		File[] files = null;
		if(fileNames != null && fileNames.length > 0){
			files = new File[fileNames.length];
			for(int i = 0; i < files.length; i++){
				files[i] = copyCalcScriptToFile(cube.getApplication().getOlapServer(), cube, fileNames[i], tempFilePath);
			}
		}
		return files;
	}
	

	/**
	 * Gets all the report scripts from a cube.
	 * @param cube Essbase cube
	 * @param tempFilePath Temp file path
	 * @return Array of report script files
	 * @throws EssException
	 * @throws IOException
	 */
	public static File[] essGetCubeReportScripts(IEssCube cube, String tempFilePath) throws EssException, IOException {
		String[] fileNames = essGetCubeFileObjectsName(cube, IEssOlapFileObject.TYPE_REPORT);
		File[] files = null;
		if(fileNames != null && fileNames.length > 0){
			files = new File[fileNames.length];
			for(int i = 0; i < files.length; i++){
				files[i] = copyReportScriptToFile(cube.getApplication().getOlapServer(), cube, fileNames[i], tempFilePath);
			}
		}
		return files;
	}
	
	/**
	 * Gets an array of cube object (file) names
	 * @param cube Essbase cube
	 * @param objectType cube object type
	 * @return Array of cube file names.
	 * @throws EssException
	 */
	private static String[] essGetCubeFileObjectsName(IEssCube cube, int objectType) throws EssException {

		String[] fileObjects = null;
		try {
			//Get an IEssIterator of the cube file object
			IEssIterator itr = cube.getOlapFileObjects(objectType);
			if(itr != null && itr.getCount() > 0){
				fileObjects = new String[itr.getCount()];
				for(int i = 0; i < itr.getCount(); i++){
					fileObjects[i] = ((IEssOlapFileObject) itr.getAt(i)).getName();
				}
			}
		} catch (EssException esx) {
			throw esx;
		}
		

		return fileObjects;
	}
	


	/**
	 *	Copy calc script file to Essbase server using the file name as the 
	 *  server object name.
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param calcScriptFile Calc script file
	 * 
	 * @return Name of created Essbase calc script
	 * @throws EssException 
	 */
	public static String copyCalcScriptToServer(IEssOlapServer olapServer, IEssCube cube, File calcScriptFile) throws EssException {
		return copyCalcScriptToServer(olapServer, cube, calcScriptFile, null);
	}

	/**
	 *	Copy calc script file to Essbase server using the specified calc script name
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param calcScriptFile Calc script file
	 * @param calcScriptName Name to use for Essbase calc script
	 * 
	 * @return Name of created Essbase calc script
	 * @throws EssException 
	 */
	public static String copyCalcScriptToServer(IEssOlapServer olapServer, IEssCube cube, File calcScriptFile, String calcScriptName) throws EssException {
		return copyFileObjectToServer(olapServer, cube, calcScriptFile, IEssOlapFileObject.TYPE_CALCSCRIPT, calcScriptName, ESS_CALC_SCRIPT_SUFFIX.length());
	}
	

	/**
	 *	Copy load rule file to Essbase server using the file name as the 
	 *  server object name.
	 *  
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param loadRuleFile Load rule file
	 * 
	 * @return Name of created Essbase load rule
	 * @throws EssException 
	 */
	public static String copyLoadRuleToServer(IEssOlapServer olapServer, IEssCube cube, File loadRuleFile) throws EssException {
		return copyLoadRuleToServer(olapServer, cube, loadRuleFile, null);
	}

	/**
	 *	Copy load rule file to Essbase server using the specified load rule name
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param loadRuleFile Load rule file
	 * @param loadRuleName Name to use for Essbase load rule
	 * 
	 * @return Name of created Essbase load rule
	 * @throws EssException 
	 */
	public static String copyLoadRuleToServer(IEssOlapServer olapServer, IEssCube cube, File loadRuleFile, String loadRuleName) throws EssException {
		return copyFileObjectToServer(olapServer, cube, loadRuleFile, IEssOlapFileObject.TYPE_RULES, loadRuleName, ESS_LOAD_RULE_SUFFIX.length());
	}

	
	/**
	 *	Copy report script file to Essbase server using the file name as the
	 *  server object name.
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param reportScriptFile Report script file
	 * 
	 * @return Name of created Essbase calc script
	 * @throws EssException 
	 */
	public static String copyReportScriptToServer(IEssOlapServer olapServer, IEssCube cube, File reportScriptFile) throws EssException {
		return copyReportScriptToServer(olapServer, cube, reportScriptFile, null);
	}

	/**
	 *	Copy report script file to Essbase server using the specified report script name
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param reportScriptFile Report script file
	 * @param reportScriptName Name to use for Essbase report script
	 * 
	 * @return Name of created Essbase calc script
	 * @throws EssException 
	 */
	public static String copyReportScriptToServer(IEssOlapServer olapServer, IEssCube cube, File reportScriptFile, String reportScriptName) throws EssException {
		return copyFileObjectToServer(olapServer, cube, reportScriptFile, IEssOlapFileObject.TYPE_REPORT, reportScriptName, ESS_REPORT_SCRIPT_SUFFIX.length());
	}


	/**
	 *	Copy text file to Essbase server
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param textFile Text file (usually data load file)
	 * 
	 * @return Name of created Essbase text object
	 * @throws EssException 
	 */
	public static String copyTextFileToServer(IEssOlapServer olapServer, IEssCube cube, File textFile) throws EssException {
		return copyTextFileToServer(olapServer, cube, textFile, null);
	}

	/**
	 *	Copy text file to Essbase server using the specified object name
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param textFile Text file
	 * @param objectName Name to use for text object
	 * 
	 * @return Name of created Essbase text object
	 * @throws EssException 
	 */
	public static String copyTextFileToServer(IEssOlapServer olapServer, IEssCube cube, File textFile, String objectName) throws EssException {
		return copyFileObjectToServer(olapServer, cube, textFile, IEssOlapFileObject.TYPE_TEXT, objectName, ESS_TEXT_FILE_SUFFIX.length());
	}
	
	/**
	 *	Copy file object to Essbase server
	 *
	 * @param olapServer Essbase olap server
	 * @param cube Essbase cube
	 * @param esbFile Essbase object file
	 * @param objectType Essbase object type
	 * @param objectName Essbase object name
	 * @param fileExtLength Length of file name extension 
	 * 
	 * @return Name of created Essbase server object
	 * @throws EssException 
	 */
	public static String copyFileObjectToServer(IEssOlapServer olapServer, IEssCube cube, File esbFile, int objectType, String objectName, int fileExtLength) throws EssException {

		// If no object name is supplied, then derive object name from name by removing file suffix.
		// If necessary, truncate the name further to adhere to Essbase object name constraints.
		if (objectName == null) {
			objectName = esbFile.getName().substring(0, esbFile.getName().length() - fileExtLength);	
			if (objectName.length() > ESS_FILE_OJBECT_NAME_MAX_LEN) {
				objectName = objectName.substring(0, ESS_FILE_OJBECT_NAME_MAX_LEN);
			}
		}

		// Copy Essbase file object to server.
		try {
			// Essbase 11 compliance - it was necessary to modify the API call below, by calling the
			// copyOlapFileObjectToSever method on the Olap Server object instead of the Cube object.
			// A change in API functionality caused API to look for the file on the HPS server, instead
			// of the local client machine.
			olapServer.copyOlapFileObjectToServer(cube.getApplicationName(), cube.getName(), objectType, objectName, esbFile.getPath(), false);
		} catch (EssException esx) {
			throw esx;
		}

		// Return essbase object name
		return objectName;
	}

	
	/**
	 *	Resolve all the token values on the specified tokenized Essbase script
	 *
	 * @param tokenizedScript Tokenized Essbase script file
	 * @param tokenCatalog Collection of client state token key/value pairs
	 * @param tempFilePath Location where temporary calc script will be created
	 * 
	 * @return Resolved calc script file
	 * 
	 * @throws IllegalArgumentException 
	 * @throws IOException 
	 */
	public static File resolveEssbaseScriptTokens(File tokenizedScript, Properties tokenCatalog, String tempFilePath) throws IllegalArgumentException, IOException {

		String scriptName = tokenizedScript.getName();
		String errMsg = "Error encountered resolving tokens on Essbase script [" + scriptName + "] - ";
		String scriptNameSuffix = "";

		File resolvedScript = null;
		FileReader scriptReader = null;
		FileWriter scriptWriter = null;

		// Check for null tokens
    	if (tokenCatalog == null || tokenCatalog.isEmpty()) {
    		errMsg += " Token catalog is null or empty";
    		logger.error(errMsg);
    		throw (new IllegalArgumentException(errMsg));
    	}
    	
		// Create temporary output file for resolved script. 
		File fileObject = new File(tempFilePath);
		int pos = scriptName.indexOf(".");
		if (pos > 0) {
			scriptNameSuffix = scriptName.substring(pos);
		}
		resolvedScript = File.createTempFile(ESS_TEMP_FILE_PREFIX, scriptNameSuffix, fileObject);
		
		// Open tokenized script for input and resolved script for output
	    scriptReader = new FileReader(tokenizedScript.getPath());
		scriptWriter = new FileWriter(resolvedScript.getPath());
    
		// Resolve the tokens on each tokenized script line and write out to the resolved script file
	    boolean eof = false;
	    BufferedReader inputBuffer = new BufferedReader(scriptReader);
	    while (!eof) {
	    	// Read next line
	    	String line = inputBuffer.readLine();

    		// Exit loop upon EOF
	    	if (line == null) {
	    		eof = true;
	    		continue;
	    	} 
	    	
	    	// Replace all tokens on line (ignoring case)
	    	for (Enumeration <?> enumerator = tokenCatalog.propertyNames(); enumerator.hasMoreElements();) {
	    		String key = (String) enumerator.nextElement();
	    		String value = tokenCatalog.getProperty(key).toUpperCase();
	    		if (value != null) {
	    			line = StringUtils.replaceAllIgnoreCase(line, key, value);
	    		}
	    	}
	    	
	    	// Write out line to resolved calc script
			scriptWriter.append(line + LINE_TERM);
	    }
	     
	    // Close all files
	    inputBuffer.close();
	    scriptReader.close();
	    scriptWriter.close();
		
	    // Return Essbase script with fully resolve tokens
	    return resolvedScript;
	}

	/**
	 *	Delete calc script from Essbase server
	 *
	 * @param cube Essbase cube
	 * @param calcScript Name of Essbase calc script
	 */
	public static void deleteServerCalcScript(IEssCube cube, String calcScript) {
		deleteServerObject(cube, calcScript, IEssOlapFileObject.TYPE_CALCSCRIPT);		
	}

	/**
	 *	Delete load rule from Essbase server
	 *
	 * @param cube Essbase cube
	 * @param loadRule Name of Essbase load rule
	 */
	public static void deleteServerLoadRule(IEssCube cube, String loadRule) {
		deleteServerObject(cube, loadRule, IEssOlapFileObject.TYPE_RULES);		
	}

	/**
	 *	Delete text file from Essbase server
	 *
	 * @param cube Essbase cube
	 * @param textFileName Name of Essbase text file
	 */
	public static void deleteServerTextFile(IEssCube cube, String textFileName) {
		deleteServerObject(cube, textFileName, IEssOlapFileObject.TYPE_TEXT);		
	}

	/**
	 *	Delete object from Essbase server
	 *
	 * @param cube Essbase cube
	 * @param objectName Name of Essbase server object
	 * @param objectType Server object type
	 */
	public static void deleteServerObject(IEssCube cube, String objectName, int objectType) {

		try {
			cube.unlockOlapFileObject(objectType, objectName);
		} catch (EssException esx) {
			// Unable to unlock data file - log warning message
			String errMsg = esx.getMessage();
			logger.warn(errMsg);
		} finally {
			try {
				cube.deleteOlapFileObject(objectType, objectName);	
			} catch (EssException esx) {
				// Unable to delete data file - log warning message
				String errMsg = esx.getMessage();
				logger.warn(errMsg);
			}
		}
	}
	
	/**
	 * Performs the actual call to get the value of the substitute variable
	 * 
	 * @param esbObj Essbase cube, application, or server
	 * @param varName name of the variable to check for
	 * @param ndxToTestAgainst index in the row to check against
	 * @return
	 * @throws EssException
	 */
	public static String getSubsVarValue(IEssBaseObject esbObj, String varName) throws EssException {
		try{
			String[][] subVarRows = null;
			String nameToTestAgainst = null;
			int ndxToTestAgainst = -1;
			if(esbObj.getClass() == IEssCube.class || esbObj.getClass() == EssCube.class){
				subVarRows = ((IEssCube)esbObj).getSubstitutionVariables();
				nameToTestAgainst = ((IEssCube)esbObj).getName();
				ndxToTestAgainst = EsbUtility.SUB_VAR_ROW_NDX_CUBE_NAME;
			} else if(esbObj.getClass() == IEssOlapApplication.class || esbObj.getClass() == EssOlapApplication.class){
				subVarRows = ((IEssOlapApplication)esbObj).getSubstitutionVariables();
				nameToTestAgainst = ((IEssOlapApplication)esbObj).getName();
				ndxToTestAgainst = EsbUtility.SUB_VAR_ROW_NDX_APP_NAME;
			} else if(esbObj.getClass() == IEssOlapServer.class || esbObj.getClass() == EssOlapServer.class){
				subVarRows = ((IEssOlapServer)esbObj).getSubstitutionVariables();
				nameToTestAgainst = ((IEssOlapServer)esbObj).getName();
				ndxToTestAgainst = EsbUtility.SUB_VAR_ROW_NDX_SERVER_NAME;
			}
			
			if(ndxToTestAgainst == -1) return null;
			
			for(String[] row : subVarRows){
				String name = row[EsbUtility.SUB_VAR_ROW_NDX_VAR_NAME];
				String cubeName = row[ndxToTestAgainst];
				if(cubeName != null && cubeName.equals(nameToTestAgainst) && name.equalsIgnoreCase(varName)){
					return row[EsbUtility.SUB_VAR_ROW_NDX_VAR_VALUE];
				}
			}
		} catch(EssException esx){
			String errMsg = esx.getMessage();
			logger.warn(errMsg);
		}
		
		return null;
	}

	/**
	 *  Close Essbase member selection object
	 *
	 * @param mbrSel Essbase member selection object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @throws EssException 
	 */
	public static void essCloseMemberSelection(IEssMemberSelection mbrSel, int essNetTimeOut) throws EssException {

		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		mbrSel.close();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

	}

	/**
	 * Determine if the supplied exception corresponds to an Essbase TCP/IP
	 * connection timeout error.
	 * 
	 * @param esx Essbase exception
	 * @return True if an essbase connection timeout error was detected
	 */
	private static boolean isEsbConnectTimeoutError(EssException esx) {
	
		boolean isConnectErr = false;

		// Check error code against list of know essbase connection
		// timeout error codes.
		if (ESS_CONNECT_TIMEOUT_ERRCODE_SET.contains(esx.getNativeCode())) {
			isConnectErr = true;
		} else {
			// If no match then check for specific keywords in the
			/// error message description.
			String errMsg = esx.getMessage();
			for (String connectErrText : ESS_CONNECT_TIMEOUT_ERRMSG_SET) {
				if (errMsg.contains(connectErrText)) {
					isConnectErr = true;
					break;
				}
			}
		}

		return isConnectErr;
	}


	/**
	 *  Close essbase cube outline
	 *
	 * @param essCubeOutline Essbase cube outline
	 * 
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * @throws EssException 
	 */
	public static void essCloseOutline(IEssCubeOutline essCubeOutline, int essNetTimeOut) throws EssException {

		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		essCubeOutline.close();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

	}

	/**
	 *  Find member in Essbase outline
	 *
	 * @param essCubeOutline Essbase outline
	 * @param branch Branch to seek
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return Essbase member
	 * @throws EssException 
	 */
	public static IEssMember essFindOtlMember(IEssCubeOutline essCubeOutline, String branch, int essNetTimeOut) throws EssException {

		IEssMember essMember = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		essMember = essCubeOutline.findMember(branch);
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return Essbase member
		return essMember;
	}

	/**
	 *  Return Essbase alias table names
	 *  
	 * @param essCubeOutline Essbase cube outline
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return String[]
	 * @throws EssException 
	 */
	public static String[] essGetAliasTableNames(IEssCubeOutline essCubeOutline, int essNetTimeOut) throws EssException {

		String[] tableNames = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		tableNames = essCubeOutline.getAliasTableNames();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return alias table names
		return tableNames;
	}

	/**
	 *  Get indexed object from Essbase iterator
	 *
	 * @param iterator Essbase iterator
	 * @param i Iterator index
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return IEssbaseObject
	 * @throws EssException 
	 */
	public static IEssBaseObject essGetAt(IEssIterator iterator, int i, int essNetTimeOut) throws EssException {

		IEssBaseObject essObject = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		essObject = iterator.getAt(i);
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return Essbase object
		return essObject;
	}

	/**
	 *  Get children of specified Essbase member
	 *
	 * @param essMember Essbase member object
	 * @param essNetTimeOut 
	 * 
	 * @return IEssIterator
	 * @throws EssException 
	 */
	public static IEssIterator essGetChildMembers(IEssMember essMember, int essNetTimeOut) throws EssException {
		return essGetChildMembers(essMember, false, essNetTimeOut);
	}
	
	/**
	 *  Get children of specified Essbase member
	 *
	 * @param essMember Essbase member object
	 * @param withAllProps If true, all properties are member properties are returned
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return IEssIterator
	 * @throws EssException 
	 */
	public static IEssIterator essGetChildMembers(IEssMember essMember, boolean withAllProps, int essNetTimeOut) throws EssException {

		IEssIterator childMembers = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		childMembers = essMember.getChildMembers(withAllProps);
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return children
		return childMembers;
		
	}

	/**
	 *  Return essbase member consolidation type
	 *  
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 *
	 * @return EEssConsolidationType
	 * @throws EssException 
	 */
	public static EEssConsolidationType essGetConsolidationType(IEssMember essMember, int essNetTimeOut) throws EssException {
		
		EEssConsolidationType consolType = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		consolType = essMember.getConsolidationType();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return consolidation type
		return consolType;
	}

	/**
	 *  Create Essbase Mdx Query in Cube View
	 *
	 * @param essCubeView Essbase cube view object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return Essbase mdx query object
	 * @throws EssException 
	 */
	public static IEssOpMdxQuery essCreateIEssOpMdxQuery(IEssCubeView essCubeView, int essNetTimeOut) throws EssException {
		
		IEssOpMdxQuery mdxQuery = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		mdxQuery = essCubeView.createIEssOpMdxQuery();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return cube outline
		return mdxQuery;
	}

	/**
	 *  Execute Essbase member query
	 *
	 * @param mbrSelQry Essbase member selection query
	 * @param fieldSelectionSpec Member properties to return in query
	 * @param mbrSelectionSpec Members to return
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @throws EssException 
	 */
	public static void essExecuteQuery(IEssMemberSelection mbrSelQry, String fieldSelectionSpec, String mbrSelectionSpec, int essNetTimeOut) throws EssException {

		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
    			mbrSelQry.executeQuery(fieldSelectionSpec, mbrSelectionSpec);
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

	}

	/**
	 *  Execute Essbase member query
	 *
	 * @param essCubeOutline Essbase Cube
	 * @param fieldSelectionSpec Member properties to return in query
	 * @param mbrSelectionSpec Members to return
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return IEssIterator
	 * @throws EssException 
	 */
	public static IEssIterator essExecuteQuery(IEssCubeOutline essCubeOutline, String fieldSelectionSpec, String mbrSelectionSpec, int essNetTimeOut) throws EssException {

		IEssIterator members = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
    			members = essCubeOutline.executeQuery(fieldSelectionSpec, mbrSelectionSpec);
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return query results
		return members;
	}

	/**
	 *  Return Essbase member generation number
	 *  
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 *
	 * @return Essbase member generation
	 * @throws EssException 
	 */
	public static int essGetGenerationNumber(IEssMember essMember, int essNetTimeOut) throws EssException {

		int genNumber = 0;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		genNumber = essMember.getGenerationNumber();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return generation number
		return genNumber;
	}

	/**
	 *  Return Essbase member level
	 *
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return Member level
	 * @throws EssException 
	 */
	public static int essGetLevelNumber(IEssMember essMember, int essNetTimeOut) throws EssException {

		int levelNumber = 0;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		levelNumber = essMember.getLevelNumber();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return level number
		return levelNumber;
	}

	/**
	 *  Get multi-dimensional data set from Essbase cube view
	 *
	 * @param essCubeView Essbase cube view object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return IEssMdDataSet
	 * @throws EssException 
	 */
	public static IEssMdDataSet essGetMdDataSet(IEssCubeView essCubeView, int essNetTimeOut) throws EssException {

		IEssMdDataSet essMdDataSet = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		essMdDataSet = essCubeView.getMdDataSet();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return md data set
		return essMdDataSet;
	}
	
	//api call that uses scope parameter to tell it where to look at
	
	
	//api call that goes through the scope options until find one
	

	/**
	 *  Get Essbase member alias
	 *
	 * @param essMember Essbase member object
	 * @param aliasTable Essbase alias table name
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return Member alias
	 * @throws EssException 
	 */
	public static String essGetMemberAlias(IEssMember essMember, String aliasTable, int essNetTimeOut) throws EssException {
		
		String memberAlias = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		memberAlias = essMember.getAlias(aliasTable);
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return member alias
		return memberAlias;
	}

	/**
	 *  Get essbase member name
	 *  
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return Member name
	 * @throws EssException 
	 */
	public static String essGetMemberName(IEssMember essMember, int essNetTimeOut) throws EssException {
		
		String memberName = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		memberName = essMember.getName();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return member alias
		return memberName;
	}

	/**
	 *  Return Essbase member number
	 *  
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 *
	 * @return Essbase member number
	 * @throws EssException 
	 */
	public static int essGetMemberNumber(IEssMember essMember, int essNetTimeOut) throws EssException {

		int mbrNumber = 0;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		mbrNumber = essMember.getMemberNumber();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return generation number
		return mbrNumber;
	}

	/**
	 *  Get essbase member parent
	 *  
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return Member name
	 * @throws EssException 
	 */
	public static String essGetParentMemberName(IEssMember essMember, int essNetTimeOut) throws EssException {
		
		String parentMemberName = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		parentMemberName = essMember.getParentMemberName();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return member alias
		return parentMemberName;
	}

	/**
	 *  Get Essbase share option
	 *
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return Essbase member share option
	 * @throws EssException 
	 */
	public static EEssShareOption essGetShareOption(IEssMember essMember, int essNetTimeOut) throws EssException {

		EEssShareOption shareOption = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		shareOption = essMember.getShareOption();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return share option
		return shareOption;
	}

	/**
	 *  Get Essbase time balance option
	 *
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return Essbase time balance option
	 * @throws EssException 
	 */
	public static EEssTimeBalanceOption essGetTimeBalanceOption(IEssMember essMember, int essNetTimeOut) throws EssException {

		EEssTimeBalanceOption timeBalanceOption = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		timeBalanceOption = essMember.getTimeBalanceOption();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return time balance option
		return timeBalanceOption;
	}

	/**
	 *  Get Essbase isExpenseMember property
	 *
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return boolean
	 * @throws EssException 
	 */
	public static boolean essIsExpenseMember(IEssMember essMember, int essNetTimeOut) throws EssException {

		boolean isExpense = false;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		isExpense = essMember.isExpenseMember();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return property value
		return isExpense;
	}

	/**
	 *  Get Essbase isTwoPassMember property
	 *
	 * @param essMember Essbase member object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return boolean
	 * @throws EssException 
	 */
	public static boolean essIsTwoPassCalculationMember(IEssMember essMember, int essNetTimeOut) throws EssException {

		boolean isTwoPass = false;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		isTwoPass = essMember.isTwoPassCalculationMember();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return property value
		return isTwoPass;
	}

	/**
	 *  Open Essbase member selection query
	 *
	 * @param essCube Essbase Cube
	 * @param mbrSelName Member selection name
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return IEssMemberSelection
	 * @throws EssException 
	 */
	public static IEssMemberSelection essOpenMemberSelection(IEssCube essCube, String mbrSelName, int essNetTimeOut) throws EssException {

		IEssMemberSelection mbrSelQry = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		mbrSelQry = essCube.openMemberSelection(mbrSelName);
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return member selection
		return mbrSelQry;
	}

	/**
	 *  Open Essbase cube outline in read-only mode
	 *  
	 * @param essCube Essbase cube object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 *
	 * @return IEssCubeOutline
	 * @throws EssException 
	 */
	public static IEssCubeOutline essOpenOutline(IEssCube essCube, int essNetTimeOut) throws EssException {

		IEssCubeOutline cubeOtl = null;
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		cubeOtl = essCube.openOutline();
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}

		// Return cube outline
		return cubeOtl;
	}

	/**
	 *  Perform Essbase cube view operation of running mdx query
	 *
	 * @param essCubeView Essbase cube view object
	 * @param essOpMdxQuery Mdx query object
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @throws EssException 
	 */
	public static void essPerformOperation(IEssCubeView essCubeView, IEssOpMdxQuery essOpMdxQuery, int essNetTimeOut) throws EssException {

		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		essCubeView.performOperation(essOpMdxQuery);
         		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}
		
	}

	/**
	 *  Set mdx query object in cube view
	 *
	 * @param essOpMdxQuery Essbase mdx query object
	 * @param dataLess Indicates if query returns no data
	 * @param mdxQuery Mdx query string
	 * @param needCellAttributes Indicates if cell attributes are needed
	 * @param id Query id type
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @throws EssException 
	 */
	public static void essSetQuery(IEssOpMdxQuery essOpMdxQuery, boolean dataLess, String mdxQuery, boolean needCellAttributes, EEssMemberIdentifierType name, int essNetTimeOut) throws EssException {
		
		boolean exit = false, isConnectErr = false;
        long startTime = System.currentTimeMillis();
        long maxTime = essNetTimeOut;
        long elapsedTime = 0;
        do {
        	try {
        		essOpMdxQuery.setQuery(dataLess, mdxQuery, needCellAttributes,IEssOpMdxQuery.EEssMemberIdentifierType.NAME);
        		exit = true;
        	} catch (EssException esx) {
        		// Check for Essbase connection issue
        		if (isEsbConnectTimeoutError(esx)) {		// TTN-2428
        			isConnectErr = true;
        			elapsedTime = System.currentTimeMillis() - startTime;
        			if (elapsedTime > maxTime) {
        				String errMsg = "Essbase time out - waited " + elapsedTime + "ms for an Essbase connection";
        				logger.error(errMsg);
        				throw esx;
        			}
        		} else {
        			throw esx;
        		}
        	}
        } while (!exit);	
		
		// Display appropriate message if connection error encountered
		if (isConnectErr) {
			String errMsg = "Waited " + elapsedTime + "ms for an Essbase connection";
			logger.warn(errMsg);
		}
		
	}
	
	/**
	 * Creates a EsbCubeInfo object from an IEssCube object
	 * @param cube IEssCube
	 * @return EsbCubeInfo EsbCubeInfo object
	 * @throws EssException
	 */
	public static EsbCubeInfo getCubeInfo(IEssCube cube) throws EssException{
		EsbCubeInfo result = new EsbCubeInfo();
		
		result.setName(cube.getName());
		result.setRetrievalBufferSize(essGetCubeSetting(cube, PROP_RETRIEVAL_BUFFER).getInt());
		result.setRetrievalSortBuffer(essGetCubeSetting(cube, PROP_RETRIEVAL_SORT_BUFFER).getInt());
		result.setExistingBlocks(essGetCubeSetting(cube, PROP_TOTAL_BLOCKS).getDouble());
		result.setIdxCacheSetting(essGetCubeSetting(cube, PROP_INDEX_CACHE_SETTING).getInt());
		result.setDataCacheSetting(essGetCubeSetting(cube, PROP_DATA_CACHE_SETTING).getInt());
		
		 
		IEssIterator itr = cube.getDimensions();
		String[] dims = new String[itr.getCount()];
		for(int i = 0; i < itr.getCount(); i++){
			dims[i] = ((IEssDimension) itr.getAt(i)).getName();
		}
		result.setDimensionNames(dims);
		
		result.setCalcScriptNames(essGetCubeCalcScriptNames(cube));
		result.setReportScriptNames(essGetCubeReportScriptNames(cube));
		
		result.setEsbServerInfo(getServerInfo(cube.getApplication().getOlapServer()));
		result.setEsbAppInfo(getAppInfo(cube.getApplication()));

		
		return result;
	}
	
	/**
	 * Creates a EsbServerInfo object from an IEssOlapServer object
	 * @param olapServer
	 * @return
	 * @throws EssException
	 */
	public static EsbServerInfo getServerInfo(IEssOlapServer olapServer) throws EssException{
		return new EsbServerInfo(olapServer.getName(), olapServer.getOlapServerVersion());
	}
	
	/**
	 * Creates a EsbAppInfo object from an IEssOlapApplication object
	 * @param olapApplication IEssOlapApplication
	 * @return
	 * @throws EssException
	 */
	public static EsbAppInfo getAppInfo(IEssOlapApplication olapApplication) throws EssException{
		return new EsbAppInfo(olapApplication.getName(), olapApplication.getServerName(), olapApplication.getType(), olapApplication.getCountUsersConnected());
	}
	
	/**
	 * Retrieves a setting from the IEssCube object
	 * @param cube IEssCube
	 * @param setting com.essbase.api.datasource.IEssCube property setting.
	 * @return IEssValueAny
	 * @throws EssException
	 */
	private static IEssValueAny essGetCubeSetting(IEssCube cube, int setting) throws EssException{

		return cube.getPropertyValueAny(setting);
	}
	

//	/**
//	 *  Return Essbase time-out setting (milliseconds)
//	 *
//	 * @return long
//	 */
//	private static long getEssNetTimeOut() {
//		return 180000;
//	}

}
