/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;


import org.apache.log4j.Logger;

import com.essbase.api.base.EssException;
import com.essbase.api.domain.IEssDomain;
import com.essbase.api.session.IEssbase;
import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;

/**
 * @author Alan Farkas
 *
 */
public class EsbDomain {
	
	private IEssbase essbase = null;
	private IEssDomain essDomain = null;
	private static Logger logger = Logger.getLogger(EsbDomain.class);
	
	/**
	 * @param esbDomainName
	 * @param esbDomainUrl
	 * @param esbDomainUser
	 * @param esbDomainPswd
	 * @throws PafException 
	 */
	public EsbDomain(String esbDomainName, String esbDomainUrl, String esbDomainUser, String esbDomainPswd) throws PafException {
		
		// Sign On to the EDS domain.
		try {			
			// Create Essbase JAPI instance.
			essbase = IEssbase.Home.create(IEssbase.JAPI_VERSION);
			essbase.setAssertionsEnabled(false);			// Setting assertions to false should help performance
			
			// Sign On to EDS (using the correct signon method) 
//			String japiVersion = IEssbase.JAPI_VERSION;
//			if (japiVersion.compareTo("9.5.0") >= 0) {
//				// JAPI 9.3.0 and higher
//				essDomain = essbase.signOn(esbDomainUser, esbDomainPswd, false, null, esbDomainUrl);
//			} else {
				// All earlier JAPI versions
				essDomain = essbase.signOn(esbDomainUser, esbDomainPswd, esbDomainName, esbDomainUrl);		
//			}
		} catch (EssException esx) {
			// throw Paf Exception
			String errMsg = "Essbase Exception: " + esx.getMessage();
			logger.error(errMsg);
			PafException pfe = new PafException(errMsg, PafErrSeverity.Error, esx);	
			throw pfe;
		}
	}
	/**
	 *	Method_description_goes_here
	 *
	 */
	protected void disconnect()  {
		try {
			// Disconnect from domain
			if (essbase!= null && (essbase.isSignedOn()))
				essbase.signOff();
		} catch (EssException ee) {
			//throw pafException;
		} finally {
			// Clean-up unneeded objects
			essDomain = null;
			essbase = null;
		}
		
	}
	
	/**
	 * @return Returns the essDomain.
	 */
	protected IEssDomain getEssDomain() {
		return essDomain;
	}
	
	
	
	
}
