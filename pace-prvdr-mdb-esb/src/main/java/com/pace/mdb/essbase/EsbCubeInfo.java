/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import java.util.Arrays;

import com.pace.base.mdb.IMdbCubeInfo;

/**
 * Class to hold information about an Essbase Cube.
 * @author themoosman
 *
 */
public class EsbCubeInfo implements IMdbCubeInfo {

	private String name;
	private int retrievalBufferSize;
	private int retrievalSortBuffer;
	private double existingBlocks;
	private int idxCacheSetting;
	private int dataCacheSetting;
	private String[] dimensionNames;
	private EsbAppInfo esbAppInfo;
	private EsbServerInfo esbServerInfo;
	private String[] calcScriptNames;
	private String[] reportScriptNames;
	
	public EsbCubeInfo(){
		super();
	}

	public EsbCubeInfo(String name, int retrievalBufferSize,
			int retrievalSortBuffer, double existingBlocks,
			int idxCacheSetting, int dataCacheSetting, String[] dimensionNames,
			EsbAppInfo esbAppInfo, EsbServerInfo esbServerInfo,
			String[] calcScriptNames, String[] reportScriptNames) {
		super();
		this.name = name;
		this.retrievalBufferSize = retrievalBufferSize;
		this.retrievalSortBuffer = retrievalSortBuffer;
		this.existingBlocks = existingBlocks;
		this.idxCacheSetting = idxCacheSetting;
		this.dataCacheSetting = dataCacheSetting;
		this.dimensionNames = dimensionNames;
		this.esbAppInfo = esbAppInfo;
		this.esbServerInfo = esbServerInfo;
		this.calcScriptNames = calcScriptNames;
		this.reportScriptNames = reportScriptNames;
	}
	
	@Override
	public String getName() {
		return name;
	}
	@Override
	public int getRetrievalBufferSize() {
		return retrievalBufferSize;
	}
	@Override
	public int getRetrievalSortBuffer() {
		return retrievalSortBuffer;
	}
	@Override
	public double getExistingBlocks() {
		return existingBlocks;
	}
	@Override
	public int getIdxCacheSetting() {
		return idxCacheSetting;
	}
	@Override
	public int getDataCacheSetting() {
		return dataCacheSetting;
	}
	@Override
	public String[] getDimensionNames() {
		return dimensionNames;
	}
	@Override
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public void setRetrievalBufferSize(int retrievalBufferSize) {
		this.retrievalBufferSize = retrievalBufferSize;
	}
	@Override
	public void setRetrievalSortBuffer(int retrievalSortBuffer) {
		this.retrievalSortBuffer = retrievalSortBuffer;
	}
	@Override
	public void setExistingBlocks(double existingBlocks) {
		this.existingBlocks = existingBlocks;
	}
	@Override
	public void setIdxCacheSetting(int idxCacheSetting) {
		this.idxCacheSetting = idxCacheSetting;
	}
	@Override
	public void setDataCacheSetting(int dataCacheSetting) {
		this.dataCacheSetting = dataCacheSetting;
	}
	@Override
	public void setDimensionNames(String[] dimensionNames) {
		this.dimensionNames = dimensionNames;
	}
	@Override
	public String[] getCalcScriptNames(){
		return calcScriptNames;
	}
	@Override
	public String[] getReportScriptNames(){
		return reportScriptNames;
	}
	@Override
	public void setCalcScriptNames(String[] calcScripts) {
		this.calcScriptNames = calcScripts;
	}

	@Override
	public void setReportScriptNames(String[] reportScripts) {
		this.reportScriptNames = reportScripts;
	}

	
	public EsbAppInfo getEsbAppInfo() {
		return esbAppInfo;
	}
	public EsbServerInfo getEsbServerInfo() {
		return esbServerInfo;
	}
	public void setEsbAppInfo(EsbAppInfo esbAppInfo) {
		this.esbAppInfo = esbAppInfo;
	}
	public void setEsbServerInfo(EsbServerInfo esbServerInfo) {
		this.esbServerInfo = esbServerInfo;
	}

	@Override
	public String toString() {
		return "EsbCubeInfo ["
				+ "\r\nname=" + name
				+ "\r\nretrievalBufferSize=" + retrievalBufferSize 
				+ "\r\nretrievalSortBuffer=" + retrievalSortBuffer  
				+ "\r\nexistingBlocks=" + existingBlocks
				+ "\r\nidxCacheSetting=" + idxCacheSetting
				+ "\r\ndataCacheSetting=" + dataCacheSetting
				+ "\r\ndimensionNames=" + Arrays.toString(dimensionNames)
				+ "\r\nreportScriptNames=" + Arrays.toString(reportScriptNames)
				+ "\r\ncalcScriptNames=" + Arrays.toString(calcScriptNames)
				+ "\r\nesbAppInfo=" + esbAppInfo 
				+ "\r\nesbServerInfo=" + esbServerInfo 
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(calcScriptNames);
		result = prime * result + dataCacheSetting;
		result = prime * result + Arrays.hashCode(dimensionNames);
		result = prime * result
				+ ((esbAppInfo == null) ? 0 : esbAppInfo.hashCode());
		result = prime * result
				+ ((esbServerInfo == null) ? 0 : esbServerInfo.hashCode());
		long temp;
		temp = Double.doubleToLongBits(existingBlocks);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + idxCacheSetting;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Arrays.hashCode(reportScriptNames);
		result = prime * result + retrievalBufferSize;
		result = prime * result + retrievalSortBuffer;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EsbCubeInfo other = (EsbCubeInfo) obj;
		if (!Arrays.equals(calcScriptNames, other.calcScriptNames))
			return false;
		if (dataCacheSetting != other.dataCacheSetting)
			return false;
		if (!Arrays.equals(dimensionNames, other.dimensionNames))
			return false;
		if (esbAppInfo == null) {
			if (other.esbAppInfo != null)
				return false;
		} else if (!esbAppInfo.equals(other.esbAppInfo))
			return false;
		if (esbServerInfo == null) {
			if (other.esbServerInfo != null)
				return false;
		} else if (!esbServerInfo.equals(other.esbServerInfo))
			return false;
		if (Double.doubleToLongBits(existingBlocks) != Double
				.doubleToLongBits(other.existingBlocks))
			return false;
		if (idxCacheSetting != other.idxCacheSetting)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (!Arrays.equals(reportScriptNames, other.reportScriptNames))
			return false;
		if (retrievalBufferSize != other.retrievalBufferSize)
			return false;
		if (retrievalSortBuffer != other.retrievalSortBuffer)
			return false;
		return true;
	}	
}
