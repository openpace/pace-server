/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.essbase.api.base.EssException;
import com.essbase.api.datasource.IEssCube;
import com.essbase.api.datasource.IEssOlapServer;
import com.essbase.api.domain.IEssDomain;
import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.mdb.IPafConnection;


/**
 * Essbase cube connection wrapper
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public class EsbCubeConn  implements IPafConnection {
	
	private EsbDomain esbDomain = null;
	private IEssOlapServer essOlapServer = null;
	private IEssCube essCube = null;
	private static Logger logger = Logger.getLogger(EsbCubeConn.class);
	

	/**
	 * @param esbDomain Essbase domain object
	 * @param props Essbase connection properties
	 * @throws PafException 
	 */
	public EsbCubeConn(EsbDomain esbDomain, Properties props) throws PafException {
		
		String esbApp = null;
		String esbDb = null;
		String esbServer = null;
		
		logger.debug("Creating instance of EsbCubeConn");

		// Get a connection to the Essbase domain
		this.esbDomain = esbDomain;		
		IEssDomain essDomain = esbDomain.getEssDomain();
       
		// Open connection with Essbase server using any available connection pool
		esbServer = props.getProperty("SERVER");
		esbApp = props.getProperty("APPLICATION");
		esbDb = props.getProperty("DATABASE");
        try {
			essOlapServer = essDomain.getOlapServer(esbServer);
		    essOlapServer.connect();
	        essCube = essOlapServer.getApplication(esbApp).getCube(esbDb);
		} catch (EssException esx) {
			// throw Paf Exception
			String errMsg = "Essbase Exception: " + esx.getMessage();
			logger.error(errMsg);
			PafException pfe = new PafException(errMsg, PafErrSeverity.Error, esx);	
			throw pfe;
		}
	}

	/**
	 *	Return the Essbase domain object
	 *
	 * @return Returns the Essbase domain object.
	 */
	public EsbDomain getEsbDomain() {
		return esbDomain;
	}

	/**
	 *	Return the Essbase cube instance
	 *
	 * @return Returns the Essbase cube instance.
	 */
	public IEssCube getEssCube() {
		return essCube;
	}	
	
	/**
	 *	Return the Essbase olap server instance
	 *
	 * @return Returns the olap server instance.
	 */
	public IEssOlapServer getEssOlapServer() {
		return essOlapServer;
	}

	
	/**
	 *	Disconnect from Essbase server
	 *
	 * @throws PafException
	 */
	protected void disconnect() throws PafException {
		
        try {
			if (essOlapServer != null && essOlapServer.isConnected()) {
				essCube = null;
                essOlapServer.disconnect();
			}
			// Disconnect from domain
			/*if (essbase!= null && (essbase.isSignedOn() == true))
				essbase.signOff(); */
		} catch (EssException esx) {
			// throw Paf Exception
			String errMsg = "Essbase Exception: " + esx.getMessage();
			logger.error(errMsg);
			PafException pfe = new PafException(errMsg, PafErrSeverity.Error, esx);	
			throw pfe;
		} finally {
			// Clean-up unneeded objects
			essCube = null;
			essOlapServer = null;
			//essDomain = null;
			//essbase = null;
		}
	}
		
}
