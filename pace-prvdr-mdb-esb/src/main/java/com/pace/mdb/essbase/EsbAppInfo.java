/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.mdb.essbase;

/**
 * Class to hold information about an Essbase application.
 * @author themoosman
 *
 */
public class EsbAppInfo {

	private String name;
	private String serverName;
	private String type;
	private int countUsersConn;

	public EsbAppInfo(){
		this(null, null, null, 0);
	}
	
	public EsbAppInfo(String name, String serverName, String type,
			int countUsersConn) {
		super();
		this.name = name;
		this.serverName = serverName;
		this.type = type;
		this.countUsersConn = countUsersConn;
	}
	
	/**
	 * Gets the name of the application.
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the name of the server.
	 * @return
	 */
	public String getServerName() {
		return serverName;
	}
	
	/**
	 * Gets the application type.
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Gets the number of users currently connected to the application.
	 * @return
	 */
	public int getCountUsersConn() {
		return countUsersConn;
	}

	/**
	 * Sets the name of the application.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Sets the name of the server.
	 * @param serverName
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
	/**
	 * Sets the application type.
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Sets the number of users currently connected to the application.
	 * @param countUsersConn
	 */
	public void setCountUsersConn(int countUsersConn) {
		this.countUsersConn = countUsersConn;
	}
	
	@Override
	public String toString() {
		return "EsbAppInfo [name=" + name + ", serverName=" + serverName
				+ ", type=" + type + ", countUsersConn=" + countUsersConn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + countUsersConn;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((serverName == null) ? 0 : serverName.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EsbAppInfo other = (EsbAppInfo) obj;
		if (countUsersConn != other.countUsersConn)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (serverName == null) {
			if (other.serverName != null)
				return false;
		} else if (!serverName.equals(other.serverName))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
}