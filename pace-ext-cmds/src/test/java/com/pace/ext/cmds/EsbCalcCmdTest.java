/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.cmds;

import java.util.Properties;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.pace.base.CustomCommandResult;
import com.pace.base.PafBaseConstants;
import com.pace.base.comm.ClientInitRequest;
import com.pace.base.mdb.IPafConnectionProps;
import com.pace.base.mdb.testCommonParms;
import com.pace.base.server.PafMetaData;
import com.pace.base.state.IPafClientState;
import com.pace.base.state.PafClientState;

/**
 * Test Execute
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public class EsbCalcCmdTest extends TestCase {
	
	private Properties tokenCatalog = new Properties();
	private IPafConnectionProps connectionProps = testCommonParms.getPafConnectionProps();
	private static Logger logger = Logger.getLogger(EsbCalcCmdTest.class);
	
	/*
	 * Test method for 'com.pace.base.cc.EsbCalcCmd.execute()'
	 */
	public void testExecute() {
		
		boolean isSuccess = true;
		final String dataSource = "Titan", calcScript = "zClcTst";
		final String actionParmPrefix = PafBaseConstants.CC_TOKEN_PREFIX_ACTION_PARM;
		final String tokenStartChar = PafBaseConstants.CC_TOKEN_START_CHAR;
		final String tokenEndChar = PafBaseConstants.CC_TOKEN_END_CHAR;
		
		EsbCalcCmd esbCalcCmd = new EsbCalcCmd();
		CustomCommandResult result = null;
		IPafClientState clientState = null;
		
		logger.info("***************************************************");
		logger.info(this.getName() +  " - Test Started");
		try {
			// Create dummy properties
			logger.info("Creating tokenCatalog");
			tokenCatalog.setProperty(tokenStartChar + actionParmPrefix + "DATASOURCEID" + tokenEndChar, dataSource);
			tokenCatalog.setProperty(tokenStartChar + actionParmPrefix + "CALCSCRIPT" + tokenEndChar, calcScript);
		
			// Create dummy client state
			logger.info("Creating client state object");
			clientState = testCommonParms.getClientState();
			clientState.getDataSources().put("Titan", connectionProps);
			
			// Execute custom Essbase calc process
			logger.info("Running custom Essbase calc process");
			result = esbCalcCmd.execute(tokenCatalog, clientState, PafMetaData.getServerSettings(), PafMetaData.getTransferDirPath());
			
			// Display response
			logger.info("Result: " + result.getReturnMessage());
			
		} catch (Exception e) {
			logger.info("Java Exception: " + e.getMessage());	
			isSuccess = false;
		} finally {
			assertTrue(isSuccess);
			System.out.print(this.getName());
			if (isSuccess) {
				logger.info(this.getName() + " - Successful");
				logger.info("***************************************************\n");
			}
			else {
				logger.info(this.getName() + " - Failed");			
				logger.info("***************************************************\n");
			}
		}
	}
}