/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.cmds;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.pace.base.PafException;
import com.pace.base.mdb.IPafConnectionProps;
import com.pace.base.state.IPafClientState;
import com.pace.mdb.essbase.EsbData;

/**
 * Common Essbase Custom Command Properties & Methods
 *  
 * @version	x.xx
 * @author AFarkas
 *
 */
public abstract class EssbaseCmd extends AbstractCommand {

	private static Logger logger = Logger.getLogger(EssbaseCmd.class);
	private static final String PROPKEY_DATA_SOURCE_ID = "DATASOURCEID";
	public String dataSourceId = null;

	/**
	 *  Return an Essbase data connection
	 *
	 * @param tokenCatalog Collection of resolved client and application tokens
	 * @param clientState Client state object
	 * 
	 * @return EsbData Essbase data object
	 * @throws PafException
	 */
	public EsbData getEsbDataConnection (Properties tokenCatalog, IPafClientState clientState) throws PafException {

		// Get data source ID. If DEFAULT VALUE is specified then default to the current application's
		// data source ID (TTN-1387).
		String dataSourceId = this.getActionParm(PROPKEY_DATA_SOURCE_ID, tokenCatalog, false);	
		if (dataSourceId.equalsIgnoreCase(defaultCCParm)) {
			dataSourceId = clientState.getApp().getMdbDef().getDataSourceId();
		}

		// Get connection information
		IPafConnectionProps connectionProps = clientState.getDataSources().get(dataSourceId);
		if (connectionProps == null) {
			errMsgBase += "Unable to get the connection properties for the dataSourceID [" + dataSourceId + "]";
			logger.error(errMsgBase);
			throw new IllegalArgumentException(errMsgBase);
		}

		// Create Essbase data object
		EsbData esbData = new EsbData(connectionProps.getProperties(), dataSourceId);
		
		// Return results
		return esbData;
	}

}
