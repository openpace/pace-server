/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.cmds;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.pace.base.CustomCommandResult;
import com.pace.base.IPafCustomCommand;
import com.pace.base.PafBaseConstants;

/**
 * Common Custom Command Properties & Methods
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public abstract class AbstractCommand implements IPafCustomCommand {

	final String actionParmPrefix = PafBaseConstants.CC_TOKEN_PREFIX_ACTION_PARM;
	final String tokenStartChar = PafBaseConstants.CC_TOKEN_START_CHAR;
	final String tokenEndChar = PafBaseConstants.CC_TOKEN_END_CHAR;
	String defaultCCParm = tokenStartChar + PafBaseConstants.CC_PARM_DEFAULT + tokenEndChar;
	String errMsgBase = "Error in custom command [" + this.getClass().getName() + "] - ";
	String propKey = null;
	CustomCommandResult result = new CustomCommandResult();
	private static Logger logger = Logger.getLogger(AbstractCommand.class);

	
	/**
	 * Get the specified action parameter
	 * 
	 * @param parmName Action parameter name
	 * @param propCatalog Property catalog
	 * @param isRequired Indicates if parameter is required
	 * 
	 * @return String
	 */
	public String getActionParm(String parmName, Properties propCatalog, boolean isRequired) {

		String parmKey = tokenStartChar + actionParmPrefix + parmName + tokenEndChar;
		String parmVal = propCatalog.getProperty(parmKey);
		if (  (parmVal == null || parmVal.equals("")) && isRequired ) {
			String errMsgDtl = "Unable to resolve the [" + parmName + "] property";
			logger.error(errMsgBase + errMsgDtl);
			throw new IllegalArgumentException(errMsgBase + errMsgDtl);
		}
	
		return parmVal;
		
	}


}
