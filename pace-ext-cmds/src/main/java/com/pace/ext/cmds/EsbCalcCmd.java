/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.cmds;

import java.util.Properties;

import com.pace.base.CustomCommandResult;
import com.pace.base.PafException;
import com.pace.base.server.ServerSettings;
import com.pace.base.state.IPafClientState;
import com.pace.mdb.essbase.EsbData;

/**
 * "EsbCalc" Custom Command - Executes a tokenized Essbase Calculation script against the specified 
 * Essbase database.
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class EsbCalcCmd extends EssbaseCmd  {

	private static final String CALC_SCRIPT_TO_RUN = "CALCSCRIPT";
	private static final String PROPKEY_ASYNCH_MODE = "DONT.WAIT";
	protected String calcScript = null;
	protected EsbData esbData = null;

	/* (non-Javadoc)
	 * @see com.pace.base.IPafCustomCommand#execute(java.util.Properties, com.pace.base.IPafClientState)
	 */
	public CustomCommandResult execute(Properties tokenCatalog, IPafClientState clientState, 
			ServerSettings serverSettings, String transferDirPath) throws PafException {
		
		// Get calculation properties
		calcScript = this.getActionParm(CALC_SCRIPT_TO_RUN, tokenCatalog, true);
		boolean asynchMode = Boolean.parseBoolean(this.getActionParm(PROPKEY_ASYNCH_MODE, tokenCatalog, false));

		// Get Essbase data connection
		esbData = this.getEsbDataConnection(tokenCatalog, clientState);

		if (asynchMode) {
			// Asynchronous Mode - Spawn a new process to run the calc script (TTN-1817)
			Runnable threadJob = new EsbCalcCmdRunnable(tokenCatalog, clientState, calcScript, esbData, serverSettings, transferDirPath);
			Thread myThread = new Thread(threadJob);
			myThread.start();
			
		} else {
			
			// Synchronous Mode - Run the tokenized calc script in the current thread
			esbData.runTokenizedCalcScript(calcScript, tokenCatalog, clientState, serverSettings, transferDirPath);

		}
		
		// Return results
		return result;
	}

}
