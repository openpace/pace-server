/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.cmds;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.pace.base.CustomCommandResult;
import com.pace.base.IPafCustomCommand;
import com.pace.base.PafException;
import com.pace.base.server.ServerSettings;
import com.pace.base.state.IPafClientState;

public class SendEmailNotification extends EssbaseCmd implements IPafCustomCommand {

	private static Logger logger = Logger
			.getLogger(SendEmailNotification.class);

	public CustomCommandResult execute(Properties tokenCatalog, IPafClientState clientState, 
			ServerSettings serverSettings, String transferDirPath) throws PafException {

		String serverName = this.getActionParm("SMTP_SERVER", tokenCatalog,
				true);
		String uName = this.getActionParm("USERNAME", tokenCatalog, true);
		String pwd = this.getActionParm("PWD", tokenCatalog, true);

		String recepient = this.getActionParm("RECEPIENT", tokenCatalog, true);
		String subject = this.getActionParm("SUBJECT", tokenCatalog, true);

		String content = this.getActionParm("CONTENT", tokenCatalog, true);

		String sentFrom = this.getActionParm("SENTFROM", tokenCatalog, true);
		
		String smtpPort = this.getActionParm("SMTP_PORT", tokenCatalog, false);

		String[] args = { serverName, uName, pwd, recepient, subject, content,
				sentFrom ,smtpPort};
		sendEmail(args);
		return result;

	}

	public static void sendEmail(String args[]) {
		try {
			Properties properties = System.getProperties();

			// Setup mail server
			properties.setProperty("mail.smtp.host", args[0]);
			
			if(args[7] == null || args[7].equals(""))
			{
				properties.put("mail.smtp.socketFactory.port", "465");  // use default port 465 if no param available
			}
			else
			properties.put("mail.smtp.socketFactory.port", args[7]);  // else use the specified property

			properties.setProperty("mail.user", args[1]);
			properties.setProperty("mail.password", args[2]);

			Session session = Session.getDefaultInstance(properties);
			MimeMessage emailMessage = new MimeMessage(session);

			emailMessage.setFrom(new InternetAddress(args[6]));

			emailMessage.addRecipients(Message.RecipientType.TO,
					InternetAddress.parse(args[3]));

			emailMessage.setText(args[5]);

			emailMessage.setSubject(args[4]);

			Transport.send(emailMessage);

			logger.info("email Sent!!!");

		} catch (AddressException e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
		} catch (MessagingException e) {

			logger.info(e.getMessage());
			System.out.println(e.getMessage());
		}

	}
	
	

}
