/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.cmds;

import java.util.Properties;

import com.pace.base.PafException;
import com.pace.base.server.ServerSettings;
import com.pace.base.state.IPafClientState;
import com.pace.mdb.essbase.EsbData;

/**
 * Executes a tokenized Essbase Calculation script against the specified Essbase database
 * (in Asynchonous Mode).
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class EsbCalcCmdRunnable extends EsbCalcCmd implements Runnable  {


	private IPafClientState clientState;
	private Properties tokenCatalog;
	private ServerSettings serverSettings;
	private String transferDirPath;

	public EsbCalcCmdRunnable(Properties tokenCatalog, IPafClientState clientState, String calcScript, EsbData esbData, 
			ServerSettings serverSettings, String transferDirPath) {
		this.clientState = clientState;
		this.tokenCatalog = tokenCatalog;
		this.calcScript = calcScript;
		this.esbData = esbData;
		this.serverSettings = serverSettings;
		this.transferDirPath = transferDirPath;
	}

	@Override
	public void run() {
		
		// Run tokenized calc script
		try {
			esbData.runTokenizedCalcScript(calcScript, tokenCatalog, clientState, serverSettings, transferDirPath);
		} catch (PafException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}

}
