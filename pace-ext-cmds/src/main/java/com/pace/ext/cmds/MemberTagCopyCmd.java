/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.cmds;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.pace.base.CustomCommandResult;
import com.pace.base.IPafCustomCommand;
import com.pace.base.PafException;
import com.pace.base.server.ServerSettings;
import com.pace.base.state.IPafClientState;
import com.pace.mdb.essbase.EsbData;

public class MemberTagCopyCmd extends EssbaseCmd implements IPafCustomCommand {
	private static final String CALC_SCRIPT_TO_RUN = "CALCSCRIPT";
	private static final String PROPKEY_ASYNCH_MODE = "DONT.WAIT";
	protected String calcScript = null;
	protected EsbData esbData = null;
	private static Logger logger = Logger.getLogger(MemberTagCopyCmd.class);

	String[] sourceDimNames = null;
	String[] destDimNames = null;

	public CustomCommandResult execute(Properties tokenCatalog, IPafClientState clientState, 
			ServerSettings serverSettings, String transferDirPath) throws PafException {

		// Get calculation properties

		String sourceNames = this.getActionParm("SRC_DIM_NAMES", tokenCatalog,
				true);
		String destNames = this.getActionParm("DEST_DIM_NAMES", tokenCatalog,
				true);

		logger.info("Source Dim Names is:" + sourceNames);
		logger.info("Dest Dim Names is:" + destNames);

		String sourceMTName = this.getActionParm("SRC_MT_NAME", tokenCatalog,
				true);
		String destMTName = this.getActionParm("DEST_MT_NAME", tokenCatalog,
				true);

		logger.info("Source Dim Names is:" + sourceMTName);
		logger.info("Dest Dim Names is:" + destMTName);

		String sourceDbURL = this.getActionParm("SRC_URL", tokenCatalog, true);
		String destDbURL = this.getActionParm("DEST_URL", tokenCatalog, true);

		String[] args = { sourceMTName, destMTName, sourceNames, destNames,
				sourceDbURL, destDbURL };
		logger.info("In Execute method");
		main(args);
		return result;
	}

	private static Connection getSourceConnection(String srcURL) {
		Connection srcConn = null;

		try {

			logger.info("Before getting source conn");

			srcURL = srcURL + ";create=true;user=pafsys;password=pafsys123";
			// String srcDbUrl =
			// "jdbc:derby://localhost:1527/../data/default/pafclientcache;create=true;user=pafsys;password=pafsys123";

			// jdbc Connection

			Class.forName("org.apache.derby.jdbc.ClientDriver");
			// Get a connection

			logger.info("Source connection info:" + srcURL);
			srcConn = DriverManager.getConnection(srcURL);
			logger.info("After getting source conn");
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			try {

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return srcConn;
	}

	private static Connection getDestConnection(String destURL) {
		Connection destConn = null;

		try {

			logger.info("Before getting dest conn");

			destURL = destURL + ";create=true;user=pafsys;password=pafsys123";

			// String destDbUrl =
			// "jdbc:derby://localhost:1527/../data/Server2/pafclientcache;create=true;user=pafsys;password=pafsys123";

			// jdbc Connection

			logger.info("Destination URL:" + destURL);
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			// Get a connection
			destConn = DriverManager.getConnection(destURL);

			logger.info("After gettingdest conn");

		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			try {

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return destConn;
	}

	private static void fetchAndCopyMemberTags(String[] args) {

		String[] sourceDimNames = null;
		String[] destDimNames = null;

		int[] sourceDimIds = null;
		;

		int[] destDimIds = null;

		Connection sourceConn = null;

		Connection destConn = null;

		PreparedStatement stmtIDSrc = null;
		ResultSet rsIDSrc = null;

		PreparedStatement stmtDataSrc = null;
		ResultSet rsDataSrc = null;

		PreparedStatement stmtCoordsSrc = null;
		ResultSet rsCoordsSrc = null;

		PreparedStatement stmtIDDest = null;
		ResultSet rsIDDest = null;

		PreparedStatement stmtDataDest = null;
		ResultSet rsDataDest = null;

		PreparedStatement stmtCoordsDest = null;
		// ResultSet rsCoordsDest = null;

		// PreparedStatement selectStmt = null;
		// ResultSet selectRs = null;

		PreparedStatement sourceDimStmt = null;
		ResultSet sourceDimRs = null;

		PreparedStatement destDimStmt = null;
		ResultSet destDimRs = null;

		try {

			sourceConn = getSourceConnection(args[4]);
			destConn = getDestConnection(args[5]);

			// get the dimension names for source

			sourceDimNames = args[2].split(",");
			destDimNames = args[3].split(",");

			sourceDimIds = new int[sourceDimNames.length];
			destDimIds = new int[sourceDimNames.length];

			for (int i = 0; i < sourceDimNames.length; i++) {
				sourceDimStmt = sourceConn
						.prepareStatement("Select * from DIMENSIONS where NAME = ?");
				sourceDimStmt.setString(1, sourceDimNames[i]);

				sourceDimRs = sourceDimStmt.executeQuery();

				while (sourceDimRs.next()) {
					sourceDimIds[i] = sourceDimRs.getInt("ID");
				}

				sourceDimStmt = null;
				sourceDimRs = null;

			}

			for (int i = 0; i < destDimNames.length; i++) {
				destDimStmt = destConn
						.prepareStatement("Select * from DIMENSIONS where NAME = ?");
				destDimStmt.setString(1, destDimNames[i]);

				destDimRs = destDimStmt.executeQuery();

				while (destDimRs.next()) {
					destDimIds[i] = destDimRs.getInt("ID");
				}

				destDimStmt = null;
				destDimRs = null;

			}

			// Get the tag ids

			stmtIDSrc = sourceConn
					.prepareStatement("Select * from MEMBER_TAG_ID where MEMBER_TAG_NAME_TXT = ?");
			stmtIDSrc.setString(1, args[0]);

			rsIDSrc = stmtIDSrc.executeQuery();

			int destTagID = -1;
			int sourceTagID = -1;
			while (rsIDSrc.next()) {
				sourceTagID = rsIDSrc.getInt("ID_INT");
				logger.info("Source tag id is:" + sourceTagID);

				stmtIDDest = destConn
						.prepareStatement("Select * from MEMBER_TAG_ID where MEMBER_TAG_NAME_TXT = ?");
				stmtIDDest.setString(1, args[1]);
				logger.info("Dest member tag name is:"+args[1]);

				rsIDDest = stmtIDDest.executeQuery();
				while (rsIDDest.next()) {
					destTagID = rsIDDest.getInt("ID_INT");
					logger.info("000 Dest tag id is:" + destTagID);
				}

			}

			// fetch the data and copy the data

			stmtDataSrc = sourceConn
					.prepareStatement("Select * from MEMBER_TAG_DATA where MEMBER_TAG_ID_INT = ?");
			stmtDataSrc.setInt(1, sourceTagID);
			rsDataSrc = stmtDataSrc.executeQuery();

			logger.info("Executed get member tag from source");

			while (rsDataSrc.next()) {

				String[] columns = { "ID_INT" };
				stmtDataDest = destConn
						.prepareStatement(
								"insert into  MEMBER_TAG_DATA (MEMBER_TAG_VALUE_TXT, MEMBER_TAG_TYPE_CHAR, CREATOR_TXT, LAST_UPDATED_DT,MEMBER_TAG_ID_INT) VALUES (?, ?, ?, ?, ?)",
								columns);
				stmtDataDest.setString(1,
						rsDataSrc.getString("MEMBER_TAG_VALUE_TXT"));
				stmtDataDest.setString(2,
						rsDataSrc.getString("MEMBER_TAG_TYPE_CHAR"));
				stmtDataDest.setString(3, rsDataSrc.getString("CREATOR_TXT"));
				stmtDataDest.setString(4,
						rsDataSrc.getString("LAST_UPDATED_DT"));
				stmtDataDest.setInt(5, destTagID);

				logger.info("Src member tag value txt:"
						+ rsDataSrc.getString("MEMBER_TAG_VALUE_TXT"));

				int id_int = stmtDataDest.executeUpdate();
				logger.info("member tag data inserted:"+id_int);
				rsDataDest = stmtDataDest.getGeneratedKeys();

				// selectStmt = destConn.prepareStatement(sql)
				// while( rsDataDest.next())
				// logger.info("Inserting member tag info in the dest member tag data:"+rsDataDest.getInt(1));

				int sourceDataID = rsDataSrc.getInt("ID_INT");
				logger.info(" 111   Source data id is:"+sourceDataID);
				int destDataID = -1;
				while (rsDataDest.next()) {
					destDataID = rsDataDest.getInt(1);
					logger.info("222 Dest Data id:" + destDataID);
				}
				for (int j = 0; j < sourceDimIds.length; j++) {

					stmtCoordsSrc = sourceConn
							.prepareStatement("Select * from MEMBER_TAG_COORDS where MEMBER_TAG_ID_INT = ? and MEMBER_TAG_DATA_ID_INT = ? and DIMENSION_ID_INT = ?");
					stmtCoordsSrc.setInt(1, sourceTagID);
					stmtCoordsSrc.setInt(2, sourceDataID);
					stmtCoordsSrc.setInt(3, sourceDimIds[j]);

					logger.info("333 Executed get coord from source");

					rsCoordsSrc = stmtCoordsSrc.executeQuery();
					
					logger.info("444 Fetching of source coords done");

					while (rsCoordsSrc.next()) {

						logger.info("555 For each source coordinate:"+destDataID);
						stmtCoordsDest = destConn
								.prepareStatement("insert into  MEMBER_TAG_COORDS  (MEMBER_TAG_ID_INT, DIMENSION_ID_INT, MEMBER_NAME_TXT, MEMBER_TAG_DATA_ID_INT) VALUES (?,?,?,?)");
						stmtCoordsDest.setInt(1, destTagID);
						stmtCoordsDest.setInt(2, destDimIds[j]);
						stmtCoordsDest.setString(3,
								rsCoordsSrc.getString("MEMBER_NAME_TXT"));
						stmtCoordsDest.setInt(4, destDataID);

						stmtCoordsDest.execute();

						logger.info("666 Inserting member coord info the dest member tag coord");
					}

				}// for ends

			}

			/*
			 * stmtCoordsSrc = sourceConn.prepareStatement(
			 * "Select * from MEMBER_TAG_COORDS where MEMBER_TAG_ID_INT = ? ") ;
			 * stmtCoordsSrc.setInt(1, sourceTagID);
			 * 
			 * logger.info("Executed get coord from source");
			 * 
			 * rsCoordsSrc = stmtCoordsSrc.executeQuery();
			 * 
			 * while(rsCoordsSrc.next()) {
			 * 
			 * stmtCoordsDest = destConn.prepareStatement(
			 * "insert into  MEMBER_TAG_COORDS  (MEMBER_TAG_ID_INT, DIMENSION_ID_INT, MEMBER_NAME_TXT, MEMBER_TAG_DATA_ID_INT) VALUES (?,?,?,?)"
			 * ); stmtCoordsDest.setInt(1, destTagID); stmtCoordsDest.setInt(2,
			 * rsCoordsSrc.getInt("DIMENSION_ID_INT"));
			 * stmtCoordsDest.setString(3,
			 * rsCoordsSrc.getString("MEMBER_NAME_TXT"));
			 * stmtCoordsDest.setInt(4,
			 * rsCoordsSrc.getInt("MEMBER_TAG_DATA_ID_INT"));
			 * 
			 * 
			 * stmtCoordsDest.execute();
			 * 
			 * logger.info("Inserting member coord info the dest member tag coord"
			 * ); }
			 */
			/*
			 * stmt = sourceConn .prepareStatement(
			 * "select * from MEMBER_TAG_DATA where MEMBER_TAG_ID_INT = 12"); //
			 * stmt.setString(1, "11"); rs = stmt.executeQuery();
			 * 
			 * while (rs.next())
			 * System.out.println(rs.getString("MEMBER_TAG_VALUE_TXT"));
			 */
		} catch (Exception except) {
			except.printStackTrace();
		} finally {
			try {

				if (rsIDSrc != null)
					rsIDSrc.close();
				if (rsDataSrc != null)
					rsDataSrc.close();
				if (rsCoordsSrc != null)
					rsCoordsSrc.close();

				if (rsIDDest != null)
					rsIDDest.close();
				if (rsDataDest != null)
					rsDataDest.close();
				// if(rsCoordsDest!=null);
				// rsCoordsDest.close();

				if (stmtIDSrc != null)
					stmtIDSrc.close();
				if (stmtDataSrc != null)
					stmtDataSrc.close();
				if (stmtCoordsSrc != null)
					stmtCoordsSrc.close();

				if (stmtIDDest != null)
					stmtIDDest.close();
				if (stmtDataDest != null)
					stmtDataDest.close();
				if (stmtCoordsDest != null)
					stmtCoordsDest.close();

				sourceConn.close();
				destConn.close();

				System.gc();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String args[]) {
		MemberTagCopyCmd cmd = new MemberTagCopyCmd();
		cmd.fetchAndCopyMemberTags(args);

	}

}
