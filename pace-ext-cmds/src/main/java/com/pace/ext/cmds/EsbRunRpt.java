/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.cmds;

import java.io.File;
import java.util.Properties;

import com.pace.base.CustomCommandResult;
import com.pace.base.IPafCustomCommand;
import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.server.ServerSettings;
import com.pace.base.state.IPafClientState;
import com.pace.mdb.essbase.EsbData;

/**
 * "EsbRunRpt" Custom Command - Executes a tokenized Essbase Report script against the specified 
 * Essbase database.
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class EsbRunRpt extends EssbaseCmd {

	private static final String PROPKEY_REPORT_SCRIPT_TO_RUN = "REPORTSCRIPT";
	private static final String PROPKEY_OUTPUT_FILE = "OUTPUTFILE";
	private static final String PROPKEY_OUTPUT_FILE_PATTERN = "FILE.PATTERN";
	private static final String PROPKEY_OUTPUT_DIR = "OUTPUT.DIRECTORY";

	
	/* (non-Javadoc)
	 * @see com.pace.base.IPafCustomCommand#execute(java.util.Properties, com.pace.base.IPafClientState)
	 */
	public CustomCommandResult execute(Properties tokenCatalog, IPafClientState clientState,
			ServerSettings serverSettings, String transferDirPath) throws PafException {
		
		// Get calculation properties
		String reportScript = this.getActionParm(PROPKEY_REPORT_SCRIPT_TO_RUN, tokenCatalog, true);
		String outputFile = this.getActionParm(PROPKEY_OUTPUT_FILE, tokenCatalog, true);
		String filePattern = this.getActionParm(PROPKEY_OUTPUT_FILE_PATTERN, tokenCatalog, false);
		String outputDir = this.getActionParm(PROPKEY_OUTPUT_DIR, tokenCatalog, false);
		String fullPath;

		
		// Get Essbase data connection
		EsbData esbData = this.getEsbDataConnection(tokenCatalog, clientState);
		
		// Check for randomized filename
		if (outputFile.equalsIgnoreCase("[randomized]")) {
			// calculate double string
			double d = Math.random();
			String randKey = String.valueOf(d).substring(2,  10);		
			if (filePattern == null || filePattern.trim().equals("") ) {
				// just use default filename pattern
				outputFile = "pace-rpt-" + randKey;
			} else {
				outputFile = filePattern + randKey;
			}
		}
		
		if (outputDir == null || outputDir.trim().isEmpty()) {
			fullPath = outputFile;
		} else {			
			// check for path seperator
			if (outputDir.endsWith(File.separator))
				fullPath = outputDir + outputFile;
			else
				fullPath = outputDir + File.separator + outputFile;
		}


		// Run tokenized report script
		esbData.runTokenizedReportScript(reportScript, fullPath, tokenCatalog, clientState, serverSettings, transferDirPath);

		// Return results
		return result;
	}

}
