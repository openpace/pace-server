/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

import junit.framework.TestCase;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PafAxisTest extends TestCase {

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link com.pace.base.view.PafAxis#PafAxis()}.
	 */
	public void testPafAxis() {
		
		PafAxis pafAxis = new PafAxis();
		
		assertNotNull(pafAxis);
		
	}

	/**
	 * Test method for {@link com.pace.base.view.PafAxis#PafAxis(int)}.
	 */
	public void testPafAxisInt() {

		PafAxis pafAxis = new PafAxis(2);
		
		assertNotNull(pafAxis);
		assertEquals(2, pafAxis.getValue());
		
	}

	/**
	 * Test method for {@link com.pace.base.view.PafAxis#getValue()}.
	 */
	public void testGetValue() {
		
		PafAxis pafAxis = new PafAxis(0);
		
		assertNotNull(pafAxis);
		assertEquals(0, pafAxis.getValue());
		
	}

	/**
	 * Test method for {@link com.pace.base.view.PafAxis#setValue(int)}.
	 */
	public void testSetValue() {
		
		PafAxis pafAxis = new PafAxis();
		
		assertNotNull(pafAxis);
		
		pafAxis.setValue(100);
		
		assertEquals(PafAxis.ROW, pafAxis.getValue());
		
		pafAxis.setValue(PafAxis.ROW);
		
		assertEquals(PafAxis.ROW, pafAxis.getValue());
		
		pafAxis.setValue(PafAxis.COL);
		
		assertEquals(PafAxis.COL, pafAxis.getValue());
		
		pafAxis.setValue(PafAxis.PAGE);
		
		assertEquals(PafAxis.PAGE, pafAxis.getValue());
	}

	/**
	 * Test method for {@link com.pace.base.view.PafAxis#getPageAxis()}.
	 */
	public void testGetPageAxis() {
		
		PafAxis pafAxis = new PafAxis();
		
		assertEquals(PafAxis.PAGE, pafAxis.getPageAxis());
		
	}

	/**
	 * Test method for {@link com.pace.base.view.PafAxis#getColAxis()}.
	 */
	public void testGetColAxis() {

		PafAxis pafAxis = new PafAxis();
		
		assertEquals(PafAxis.COL, pafAxis.getColAxis());
	}

	/**
	 * Test method for {@link com.pace.base.view.PafAxis#getRowAxis()}.
	 */
	public void testGetRowAxis() {
		
		PafAxis pafAxis = new PafAxis();
		
		assertEquals(PafAxis.ROW, pafAxis.getRowAxis());
	}

	
	/**
	 * Test method for {@link com.pace.base.view.PafAxis#equals(java.lang.Object)}.
	 */
	public void testEqualsObject() {
	
		PafAxis rowPafAxis = new PafAxis(PafAxis.ROW);
		
		assertEquals(rowPafAxis, new PafAxis(PafAxis.ROW));
		
		PafAxis colPafAxis = new PafAxis(PafAxis.COL);
		
		assertEquals(colPafAxis, new PafAxis(PafAxis.COL));
		
		PafAxis pagePafAxis = new PafAxis(PafAxis.PAGE);
		
		assertEquals(pagePafAxis, new PafAxis(PafAxis.PAGE));
		
		assertEquals(rowPafAxis, PafAxis.ROW);
		assertEquals(colPafAxis, PafAxis.COL);
		assertEquals(pagePafAxis, PafAxis.PAGE);
		
	}

}
