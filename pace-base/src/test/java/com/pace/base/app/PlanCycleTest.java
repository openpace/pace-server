/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

import junit.framework.TestCase;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PlanCycleTest extends TestCase {

	/**
	 * @param name
	 */
	public PlanCycleTest(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link com.pace.base.app.PlanCycle#equals(java.lang.Object)}.
	 */
	public void testEqualsObject() {

		PlanCycle pc = new PlanCycle();
		assertEquals(pc, new PlanCycle());
		
		pc = new PlanCycle(null, null);
		assertEquals(pc, new PlanCycle());
		
		String label = "test1";
		
		pc = new PlanCycle(label, null);
		assertNotSame(pc, new PlanCycle());
		assertEquals(pc, new PlanCycle(label, null));
		assertNotSame(pc, new PlanCycle(null, label));
		
		String version = "WP";
		
		pc = new PlanCycle(label, version);
		assertNotSame(pc, new PlanCycle());
		assertEquals(pc, new PlanCycle(label, version));
		
		pc = new PlanCycle(null, version);
		assertNotSame(pc, new PlanCycle(null, version));		
		
	}

	/**
	 * Test method for {@link com.pace.base.app.PlanCycle#clone()}.
	 */
	public void testClone() {

		String label = "test1";
		String version = "WP";
		
		PlanCycle pc = new PlanCycle(label, version);
		
		PlanCycle pcClone = (PlanCycle) pc.clone();
					
		assertNotNull(pcClone);
		assertEquals(pc, pcClone);
		assertEquals(label, pcClone.getLabel());
		assertEquals(pc.getLabel(), pcClone.getLabel());
		
		assertEquals(version, pcClone.getVersion());
		assertEquals(pc.getVersion(), pcClone.getVersion());
		
		
		
	}

}
