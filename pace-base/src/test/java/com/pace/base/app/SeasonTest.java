/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

import junit.framework.TestCase;

public class SeasonTest extends TestCase {

	public SeasonTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testClone() {

		Season season = new Season();		
		Season clonedSeason = season.clone();		
		assertNotNull(clonedSeason);		
		assertEquals(season, clonedSeason);
		
		String id = "test id";    
		boolean isOpen = false;
		String planCycle = "test plan cycle";
		String year = "test year";
		String timePeriod = "test time period";
		PafDimSpec[] otherDims = null;
		
		season.setId(id);
		season.setOpen(isOpen);
		season.setPlanCycle(planCycle);
		season.setYears(new String[]{year});
		season.setTimePeriod(timePeriod);
		
		clonedSeason = season.clone();		
		assertNotNull(clonedSeason);		
		assertEquals(season, clonedSeason);
		
		otherDims = new PafDimSpec[3];
		
		otherDims[0] = new PafDimSpec();
		
		otherDims[1] = new PafDimSpec();
		
		otherDims[2] = new PafDimSpec();
		
		season.setOtherDims(otherDims);
		
		clonedSeason = season.clone();		
		assertNotNull(clonedSeason);		
		assertEquals(season, clonedSeason);
		assertEquals(season.getOtherDims().length, clonedSeason.getOtherDims().length);
		
		for (int i = 0; i < season.getOtherDims().length; i++ ) {
			
			assertEquals(season.getOtherDims()[i], clonedSeason.getOtherDims()[i]);
			
		}	
		
		String dim1 = "dim1";
		String dim2 = "dim2";
		String dim3 = "dim3";
		
		String exp1 = "exp1";
		String exp2 = "exp2";
		String exp3 = "exp3";
		
		otherDims[0].setDimension(dim1);
		otherDims[0].setExpressionList(new String[] { exp1 });		
		
		otherDims[1].setDimension(dim2);
		otherDims[1].setExpressionList(new String[] { exp2 });
		
		otherDims[2].setDimension(dim3);
		otherDims[2].setExpressionList(new String[] { exp3 });
		
		season.setOtherDims(otherDims);
		
		clonedSeason = season.clone();		
		assertNotNull(clonedSeason);		
		assertEquals(season, clonedSeason);
		assertEquals(season.getOtherDims().length, clonedSeason.getOtherDims().length);
		
		for (int i = 0; i < season.getOtherDims().length; i++ ) {
			
			assertEquals(season.getOtherDims()[i], clonedSeason.getOtherDims()[i]);
			
		}
		
	}

}

