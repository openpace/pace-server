/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

import junit.framework.TestCase;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PafDimSpecTest extends TestCase {

	/**
	 * @param name
	 */
	public PafDimSpecTest(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link java.lang.Object#clone()}.
	 */
	public void testClone() {
		
		PafDimSpec pafDimSpec = new PafDimSpec();
		
		PafDimSpec clonePafDimSpec = pafDimSpec.clone();
		
		assertNotNull(clonePafDimSpec);
		assertEquals(pafDimSpec, clonePafDimSpec);
		
		String dim1 = "dimension1";
		
		pafDimSpec.setDimension(dim1);
		clonePafDimSpec = pafDimSpec.clone();
		assertNotNull(clonePafDimSpec);
		assertNotNull(clonePafDimSpec.getDimension());
		assertEquals(pafDimSpec, clonePafDimSpec);
		
		String exp1 = "expression 1";
		
		pafDimSpec.setExpressionList(new String[] { exp1 });
		clonePafDimSpec = pafDimSpec.clone();
		assertNotNull(clonePafDimSpec);
		assertNotNull(clonePafDimSpec.getExpressionList());
		assertEquals(pafDimSpec.getExpressionList().length, clonePafDimSpec.getExpressionList().length);
		assertEquals(pafDimSpec, clonePafDimSpec);
		
	}

}
