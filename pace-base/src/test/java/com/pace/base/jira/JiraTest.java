/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.jira;

import java.util.Collection;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.jira.JiraIssuePriority.IssuePriority;
import com.pace.base.jira.JiraIssueType.IssueType;
import com.pace.base.utility.JsonUtil;

public class JiraTest extends TestCase {

	private static Logger logger = Logger.getLogger(JiraTest.class);
	//This URL is for running units tests on TeamCity server
	public static final String JIRA_SERVER_BASE_URL = "http://172.30.30.143:8080/jira/rest/api/latest/";
	//Use this URL to test locally.
	//public static final String JIRA_SERVER_BASE_URL = "http://ams.pace-planning.com/jira/rest/api/latest/";

	public void testQuery(){
		try {

			Collection<JiraProjectVersion> versions = JiraUtil.getJiraVersions(JIRA_SERVER_BASE_URL);
			
			assertNotNull(versions);
			
			for(JiraProjectVersion version : versions){
				logger.info(version.getName());
			}

			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	

	public void testJiraIssueCreation() {
		
		JiraIssue issue = new JiraIssue(new JiraProject(PafBaseConstants.JIRA_DEFAULT_PROJECT), 
				"pace-base unit test issue creation", 
				"Creating of an issue using project keys and issue type names using the REST API", 
				new JiraIssueType(IssueType.Bug),
				new JiraIssuePriority(IssuePriority.Critical),
				new JiraVersion[] { new JiraVersion ("2.8.6") },
				"Environment info will go here!",
				"Maverick Moos - mm@pace.local - (312.867.5309)",
				new JiraClient(PafBaseConstants.JIRA_DEFAULT_CLIENT));
		

		try {
			JiraResponse response = JiraUtil.createJiraIssue(JIRA_SERVER_BASE_URL, issue);
			
			assertNotNull(response);
			
			int id = response.getId();
			String key = response.getKey();
			
			JiraIssueResponse resp = new JiraIssueResponse(new JiraResponse(id, key, JIRA_SERVER_BASE_URL + "issue/" + id));
			String json = "";
			try {
				json = JsonUtil.toJson(resp);
			} catch (Exception e1) {
				fail(e1.getMessage());
			}	
			
			
			
			json = "{ \"issue\": {\"id\":\"" + id + "\",\"key\":\"" + key + "\",\"self\":\"" + JIRA_SERVER_BASE_URL + "issue/" + id +"\"}}"; 
			JiraIssueResponse jiraResponse = null;
			try {
				jiraResponse = JsonUtil.toObject(json, JiraIssueResponse.class);
				assertNotNull(jiraResponse);
			} catch (Exception e) {
				fail(e.getMessage());
			}
			
			json = "{\"id\":\"" + id + "\",\"key\":\"" + key + "\",\"self\":\"" + JIRA_SERVER_BASE_URL + "issue/" + id +"\"}}"; 
			JiraResponse response2 = null;
			try {
				response2 = JsonUtil.toObject(json, JiraResponse.class);
				assertNotNull(response2);
			} catch (Exception e) {
				fail(e.getMessage());
			}
			
			assertTrue(JiraUtil.deleteJiraIssue(JIRA_SERVER_BASE_URL, id));
						
			JiraUtil.addAttachmentToIssue(response.getKey(), "/home/kmoos/Downloads/jira-rest-test.7z");
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	public void testParseJiraResponse(){
		
		JiraIssueResponse resp = new JiraIssueResponse(new JiraResponse(17154, "TEST-10", "http://ams.pace-planning.com/jira/rest/api/2/issue/17154"));
		String json = "";
		try {
			json = JsonUtil.toJson(resp);
		} catch (Exception e1) {
			fail(e1.getMessage());
		}	
		
		
		
		json = "{ \"issue\": {\"id\":\"17154\",\"key\":\"TEST-10\",\"self\":\"http://ams.pace-planning.com/jira/rest/api/2/issue/17154\"} }";
		JiraIssueResponse jiraResponse = null;
		try {
			jiraResponse = JsonUtil.toObject(json, JiraIssueResponse.class);
			assertNotNull(jiraResponse);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		
		json = "{\"id\":\"17167\",\"key\":\"TEST-12\",\"self\":\"http://ams.pace-planning.com/jira/rest/api/2/issue/17167\"}";
		JiraResponse response = null;
		try {
			response = JsonUtil.toObject(json, JiraResponse.class);
			assertNotNull(response);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
}
