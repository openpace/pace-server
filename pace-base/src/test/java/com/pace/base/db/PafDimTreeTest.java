/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.pace.base.mdb.PafAttributeTree;
import com.pace.base.mdb.PafBaseTree;
import com.pace.base.mdb.PafDimTree;
import com.pace.base.mdb.PafDimTree.DimTreeType;

/**
 * PafDimTree Unit Test
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafDimTreeTest extends TestCase {

//	private static final Properties connectionProps = testCommonParms.getConnectionProps();
	private static final Logger logger = Logger.getLogger(PafDimTreeTest.class);

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
    
	/**
	 * Test method for {@link com.pace.base.mdb.PafDimTree#getTreeType()}.
	 */
	public void testGetTreeType1() {

		boolean isSuccess = false;
		PafDimTree attrTree = new PafAttributeTree();
		
		try {
			logger.info("");
			logger.info("Running unit test: testGetTreeType() against an attribute tree");
			if (attrTree.getTreeType() == DimTreeType.Attribute) {
				isSuccess = true;
			}
		} catch (Exception ex) {
			logger.error("Java Exception: " + ex.getMessage());	
		} finally {
			assertTrue(isSuccess);
		}
	}

	/**
	 * Test method for {@link com.pace.base.mdb.PafDimTree#getTreeType()}.
	 */
	public void testGetTreeType2() {

		boolean isSuccess = false;
		PafDimTree baseTree = new PafBaseTree();

		try {
			logger.info("");
			logger.info("Running unit test: testGetTreeType() against a base tree");
			if (baseTree.getTreeType() == DimTreeType.Base) {
				isSuccess = true;
			}
		} catch (Exception ex) {
			logger.error("Java Exception: " + ex.getMessage());	
		} finally {
			assertTrue(isSuccess);
		}
	}

}
