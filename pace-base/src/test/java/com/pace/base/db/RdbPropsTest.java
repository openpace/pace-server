/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.PafKeystoreEncryptionException;
import com.pace.base.migration.RdbPropsMigrationActionTest;
import com.pace.base.migration.RdbPropsMigrationActionV1;
import com.pace.base.security.AESKeystoreEncryption;

public class RdbPropsTest extends RdbPropsMigrationActionTest {
	
	private static final Logger logger = Logger.getLogger(RdbPropsTest.class);
	private static final String PASSWORD_KEY = PafBaseConstants.RDB_PASSWORD_KEY;

	protected void setUp() throws Exception {
		super.setUp(PafBaseConstants.FN_RdbClientDataSources, 0, 1);
		this.migrationAction = new RdbPropsMigrationActionV1(this.serverXmlFile);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	@SuppressWarnings("unchecked")
	public void testNotNull() {
		logger.info("Validating xml is not null");
		List<RdbProps> props = (List<RdbProps>) getObjectFromXml();
		for(RdbProps prop : props){
			assertNotNull(prop);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void testPasswordEncryption() {
		
		validateStringIsEncrypted((List<RdbProps>) getObjectFromXml(), false);
		
		this.runMigrationAction();
		
		validateStringIsEncrypted((List<RdbProps>) getObjectFromXml(), true);
	}

	@SuppressWarnings("unchecked")
	public void testPasswordDecryption() {
		
		this.runMigrationAction();
		
		List<RdbProps> rdbProps = (List<RdbProps>) getObjectFromXml();
		for(RdbProps rdbProp : rdbProps){
			Properties props = null;
			try {
				props = rdbProp.decryptHibernateProperties();
				
				for(String key : props.stringPropertyNames()) {
					String value = props.getProperty(key);
					if(key.equalsIgnoreCase(PASSWORD_KEY)){
					
						assertFalse(AESKeystoreEncryption.isStringEncrypted(value));
						break;
					}
				}
			} catch (PafKeystoreDecryptionException e) {
				fail(e.getMessage());
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void testPasswordDecryptionReturnsDefault() {
		
		this.runMigrationAction();
		
		//Get the rdb props and change the encrypted string.
		List<RdbProps> rdbProps = (List<RdbProps>) getObjectFromXml();
		for(RdbProps rdbProp : rdbProps){
			Properties props = null;
	
			props = rdbProp.getHibernateProperties();
			
			for(String key : props.stringPropertyNames()) {
				String value = props.getProperty(key);
				if(key.equalsIgnoreCase(PASSWORD_KEY)){
				
					props.setProperty(key, value.substring(0, value.length() - 2));
					
				}
			}
		}
		
		//Loop thru the props again and check for the password to get reset
		checkDefaultPasswords(rdbProps);
	}
	
	@SuppressWarnings("unchecked")
	public void testPasswordSetEncryption() {
		
		this.runMigrationAction();
		
		//Get the encryped rdb props and re set them.
		List<RdbProps> rdbProps = (List<RdbProps>) getObjectFromXml();
		for(RdbProps rdbProp : rdbProps){
			Properties props = rdbProp.getHibernateProperties();
			try {
				rdbProp.setHibernateProperties(props);
			} catch (PafKeystoreEncryptionException e) {
				fail(e.getMessage());
			}
		}
		//Now double check the props to see if they were double encrypted.
		checkDefaultPasswords(rdbProps);
		
	}
	
	public void validateStringIsEncrypted(List<RdbProps> rdbProps, boolean isEncrypted){
		for(RdbProps rdbProp : rdbProps){
			Properties props = rdbProp.getHibernateProperties();
			for(String key : props.stringPropertyNames()) {
				String value = props.getProperty(key);
				if(key.equalsIgnoreCase(PASSWORD_KEY)){
				
					assertEquals(AESKeystoreEncryption.isStringEncrypted(value), isEncrypted);
					break;
				}
			}
		}
	}
	
	/**
	 * Validates that each rdb prop object has the default password set.
	 * @param rdbProps
	 */
	private void checkDefaultPasswords(List<RdbProps> rdbProps){

		List<String> pafSysProps = Arrays.asList(PafBaseConstants.PAF_SYS_PROPS);
		List<String> pafSys123Props = Arrays.asList(PafBaseConstants.PAF_SYS_123_PROPS);
		
		//Loop thru the props again and check for the password to get reset
		for(RdbProps rdbProp : rdbProps){
			
			Properties props = null;
			try {
				props = rdbProp.decryptHibernateProperties();
				
				for(String key : props.stringPropertyNames()) {
					String value = props.getProperty(key);
					if(key.equalsIgnoreCase(PASSWORD_KEY)){
						
						String name = rdbProp.getName();
						
						if(pafSysProps.contains(name)){
	    					
							assertEquals(PafBaseConstants.PAFSYS, value);
							
							break;
	    					
	    				} else if(pafSys123Props.contains(name)){
	    					
	    					assertEquals(PafBaseConstants.PAFSYS123, value);
	    					
	    					break;
	    				}

					}
				}
				

			} catch (PafKeystoreDecryptionException e) {
				fail(String.format("The password string could not be reset: %s", e.getMessage()));
			}
		}

	}
	
	
	@Override
	public void testValidateMigrationAction() {
		logger.info("Ignoring migration test.");
	}
	
	@Override
	public void testMigrationActionNotNull(){
		logger.info("Ignoring migration test.");
	}
	
	@Override
	public void testMigrationActionOriginalStatus(){
		logger.info("Ignoring migration test.");
	}
	
	@Override
	public void runMigrationAction() {
		migrationAction.run();
	}
}
