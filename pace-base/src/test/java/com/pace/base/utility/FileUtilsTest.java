/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.base.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import junit.framework.TestCase;

/**
 * @author jmilliron
 *
 */
public class FileUtilsTest extends TestCase {

	private static final String TEST_FILES = ".\\test_files\\";
	
	private static final String TEST_FILES_PROJECT_TEMPLATE_XLSX = TEST_FILES + "project-template.xlsx";
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testIsFileLocked() {
		
		try {
		
			File file = new File(TEST_FILES_PROJECT_TEMPLATE_XLSX);
			
			assertFalse(FileUtils.isFileLocked(file));
			
			FileChannel channel= new RandomAccessFile(file, "rw").getChannel();
						
			FileLock lock = channel.lock();
			
			assertTrue(FileUtils.isFileLocked(file));
			
			lock.release();
			
			assertFalse(FileUtils.isFileLocked(file));
			
			channel.close();
			
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
	}
}
