/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;

import com.pace.base.view.PafAxis;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;

import junit.framework.TestCase;

public class PafViewSectionUtilTest extends TestCase {

	private PafViewSection pafViewSection = null;
	
	protected void setUp() throws Exception {
		super.setUp();
		
		pafViewSection = new PafViewSection();
		
		PageTuple p1 = new PageTuple("Location", null);
		PageTuple p2 = new PageTuple("Product", null);

		pafViewSection.setPageTuples(new PageTuple[] { p1, p2 });
		
		pafViewSection.setColAxisDims(new String[] { "Time", "Measures" } );
		pafViewSection.setRowAxisDims(new String[] { "Version", "Years" } );
		
		
	}

	/*
	 * Test method for 'com.pace.base.utility.PafViewSectionUtil.getDimensionAxis(PafViewSection, String)'
	 */
	public void testGetDimensionAxis() {
		
		PafAxis pafAxis = null;
		
		pafAxis = PafViewSectionUtil.getDimensionAxis(null, null);
		
		assertNull(pafAxis);

		pafAxis = PafViewSectionUtil.getDimensionAxis(pafViewSection, null);
		
		assertNull(pafAxis);
		
		pafAxis = PafViewSectionUtil.getDimensionAxis(pafViewSection, "");
		
		assertNull(pafAxis);
		
		pafAxis = PafViewSectionUtil.getDimensionAxis(null, "");
		
		assertNull(pafAxis);
		
		pafAxis = PafViewSectionUtil.getDimensionAxis(pafViewSection, "Location");
		
		assertEquals(PafAxis.PAGE, pafAxis.getValue());
				
		pafAxis = PafViewSectionUtil.getDimensionAxis(pafViewSection, "Time");
		
		assertEquals(PafAxis.COL, pafAxis.getValue());

		pafAxis = PafViewSectionUtil.getDimensionAxis(pafViewSection, "Years");
		
		assertEquals(PafAxis.ROW, pafAxis.getValue());
		
		pafAxis = PafViewSectionUtil.getDimensionAxis(pafViewSection, "InvalidDimName");
		
		assertNull(pafAxis);
		
	}

}
