/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.utility.FileFilterUtility;
import com.pace.base.view.PafViewGroup;


/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PafViewGroupsMigrationActionTest extends MigrationActionTestCase {

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {

		super.setUp("./test_files/pace1210.paf");
		
	}
	
	/**
	 * Test method for {@link com.pace.base.migration.PafViewGroupsMigrationAction#PafViewGroupsMigrationAction(com.pace.base.project.XMLPaceProject)}.
	 */
	public void testPafViewGroupsMigrationAction() {
		
		MigrationAction action = new PafViewGroupsMigrationAction(pp);
		
		assertNotNull(action);
		assertNotNull(action.getXMLPaceProject());
		assertEquals(action.getXMLPaceProject(), pp);
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafViewGroupsMigrationAction#getStatus()}.
	 */
	public void testGetStatus() {
		
		MigrationAction action = new PafViewGroupsMigrationAction(pp);
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		PafViewGroup vg1 = new PafViewGroup();
		
		vg1.setName("VG1");
	
		Map<String, PafViewGroup> viewGroupMap = new HashMap<String, PafViewGroup>();
		
		viewGroupMap.put(vg1.getName(), vg1);
				
		pp.setViewGroups(viewGroupMap);
		
		try {
			pp.save();
		} catch (ProjectSaveException e) {
			fail(e.getMessage());
		}
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		
		
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafViewGroupsMigrationAction#run()}.
	 */
	public void testRun() {
		
		MigrationAction action = new PafViewGroupsMigrationAction(pp);
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		Map<String, PafViewGroup> viewGroupMap =  pp.getViewGroups();
		
		/*try {
			for (PafViewGroup vg : viewGroupMap.values()) {
				
				System.out.println(vg.getName());
				
			}
		} catch (ClassCastException cce ) {
			fail(cce.getMessage());
		}*/
		
		File viewGroupFile = new File(tempConfDir + File.separator + PafBaseConstants.FN_ViewGroups);
		
		Set<String> filterSet = new HashSet<String>	();
		
		filterSet.add("<PafViewGroup>");
		
		try {
			assertFalse(FileFilterUtility.doFiltersExistsInFile(viewGroupFile, filterSet));
		} catch (FileNotFoundException e1) {
			fail(e1.getMessage());
		}
		
		//assertEquals(0, viewGroupMap.size());
		
		action.run();
				
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		

		try {
			assertTrue(FileFilterUtility.doFiltersExistsInFile(viewGroupFile, filterSet));
		} catch (FileNotFoundException e1) {
			fail(e1.getMessage());
		}
		
		try {
			pp.reloadData();
		} catch (PafException e) {
			fail(e.getMessage());
		}
		
		viewGroupMap =  pp.getViewGroups();
		
		assertEquals(5, viewGroupMap.size());
		
				
	}

}
