/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import java.io.File;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.utility.FileUtils;
import com.pace.base.utility.PafXStream;

/**
 * Used as a base test case that creates a temp conf_server dir with test data.  The
 * temp dir is deleted after each test case.
 *
 * @author themoosman
 * @version	2.8.6.4
 *
 */
public abstract class ServerFileMigrationActionTestCase extends TestCase {

	protected static final Logger logger = Logger.getLogger(ServerFileMigrationActionTestCase.class);
	protected File tempConfDir = getTempDirectory();
	protected static final String CONF_SERVER_DIR = "conf_server";
	protected static final String SOURCE_CONF_SERVER = "." + File.separator + "test_files" + File.separator + CONF_SERVER_DIR;
	protected String workingDirectory = tempConfDir.getAbsolutePath() + File.separator + CONF_SERVER_DIR;
	protected String serverXmlFile;
	protected MigrationAction migrationAction;
	protected int NewVersion;
	protected int OldVersion;
	
	protected void setUp(String serverXmlFile, int oldVersion, int newVersion) throws Exception {
		
		super.setUp();
		
		if (!tempConfDir.exists()) {
			assertTrue(tempConfDir.mkdir());
		}
		
		org.apache.commons.io.FileUtils.copyDirectory(new File(SOURCE_CONF_SERVER), new File(workingDirectory));
		
		this.OldVersion = oldVersion;
		this.NewVersion = newVersion;
		this.serverXmlFile = workingDirectory + File.separator +  serverXmlFile;
		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		
		org.apache.commons.io.FileUtils.deleteDirectory(tempConfDir);
	}

	private static File getTempDirectory(){
		return FileUtils.createTempDirectory();
	}
	
	@SuppressWarnings("unchecked")
	protected <T extends Object> T getObjectFromXml(){
		try {
			return (T) PafXStream.importObjectFromXml(serverXmlFile);
		} catch (PafConfigFileNotFoundException e) {
			fail(e.getMessage());
		}
		return null;
	}
	
	public void testMigrationActionNotNull(){

		assertNotNull(migrationAction);

	}
	
	public void testMigrationActionOriginalStatus(){

		assertNotNull(migrationAction);
		logAssertEquals(MigrationActionStatus.NotStarted, migrationAction.getStatus());	

	}
	
	
	public void runMigrationAction() {
		
		logAssertEquals(MigrationActionStatus.NotStarted, migrationAction.getStatus());
		
		migrationAction.run();
		
		logAssertEquals(MigrationActionStatus.Completed, migrationAction.getStatus());	
		
	}
	
	public <T extends Object> void assertNotEquals(T o1, T o2){
		logger.info(String.format("Validating '%s' does not match '%s'", o1, o2));
		assertThat(o1, not(o2));
	}
	
	public <T extends Object> void logAssertEquals(T o1, T o2){
		logger.info(String.format("Validating '%s' matches '%s'", o1, o2));
		assertEquals(o1, o2);
	}
	
	public abstract void testValidateMigrationAction();

}
