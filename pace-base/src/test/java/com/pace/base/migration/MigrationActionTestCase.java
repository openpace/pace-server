/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import com.pace.base.PafBaseConstants;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.utility.FileUtils;
import com.pace.base.utility.PafZipUtil;

import junit.framework.TestCase;

/**
 * Used as a base test case that creates a temp conf dir with test data.  The
 * temp dir is deleted after each test case.
 *
 * @author jmilliron
 * @version	2.6.0.3
 *
 */
public class MigrationActionTestCase extends TestCase {

	protected XMLPaceProject pp = null;
	
	Set<ProjectElementId> set = new HashSet<ProjectElementId>();

	//protected File tempConfDir = new File(PafBaseConstants.DN_PaceTestFldr);
	
	protected File tempConfDir = getTempDirectory();	
	
	protected void setUp(String paceArchiveFileName) throws Exception {
		
		super.setUp();
		
		if ( ! tempConfDir.exists() ) {
			
			assertTrue(tempConfDir.mkdir());
			
		}
		
		if ( paceArchiveFileName == null || paceArchiveFileName.trim().length() == 0 ) {
			
			paceArchiveFileName = "./test_files/pace.paf"; 
			
		} else {
			
			File paceArchiveFile = new File(paceArchiveFileName);
			
			assertTrue(paceArchiveFile.isFile());
			
		}
		
		PafZipUtil.unzipFile(paceArchiveFileName, tempConfDir.toString());
		
		pp = new XMLPaceProject(tempConfDir.getAbsolutePath(), set, false);
		
	}
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		
		setUp(null);
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		
		pp = null;
		
		set.clear();
		
		FileUtils.deleteDirectory(tempConfDir);
		FileUtils.deleteDirectory(tempConfDir);
		
	}

	private static File getTempDirectory(){
		return FileUtils.createTempDirectory();
	}
	
	public void testProject() {
		assertNotNull( pp );
	}

}
