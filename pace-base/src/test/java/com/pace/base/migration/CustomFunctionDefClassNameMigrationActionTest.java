/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.base.migration;

public class CustomFunctionDefClassNameMigrationActionTest extends MigrationActionTestCase {

	protected void setUp() throws Exception {
		super.setUp("./test_files/pace2600.paf");
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testCustomFunctionDefClassNameMigrationAction() {
		MigrationAction action = new CustomFunctionDefClassNameMigrationAction(pp);
		
		assertNotNull(action);
		assertNotNull(action.getXMLPaceProject());
		assertEquals(action.getXMLPaceProject(), pp);
	}

	public void testGetStatus() {
		MigrationAction action = new CustomFunctionDefClassNameMigrationAction(pp);
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
	}

	public void testRun() {
		MigrationAction action = new CustomFunctionDefClassNameMigrationAction(pp);
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		action.run();
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
	}

}
