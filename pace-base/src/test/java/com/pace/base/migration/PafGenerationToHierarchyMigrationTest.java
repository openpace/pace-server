/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.io.IOException;

import com.pace.base.PafBaseConstants;
import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.utility.FileUtils;
import com.pace.base.utility.PafZipUtil;

/**
 * Unit test for PafGenerationToHierarchyMigration
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PafGenerationToHierarchyMigrationTest extends MigrationActionTestCase {
		
	/* (non-Javadoc)
	 * @see com.pace.base.migration.MigrationActionTestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp("./test_files/pace1210.paf");
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafGenerationToHierarchyMigration#PafGenerationToHierarchyMigration(com.pace.base.project.XMLPaceProject)}.
	 */
	public void testPafGenerationToHierarchyMigrationXMLPaceProject() {
		
		MigrationAction action = new PafGenerationToHierarchyMigration(pp);
		
		assertNotNull(action);
		assertNotNull(action.getXMLPaceProject());
		assertEquals(action.getXMLPaceProject(), pp);
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafGenerationToHierarchyMigration#getStatus()}.
	 */
	public void testGetStatus() {
		
		MigrationAction action = new PafGenerationToHierarchyMigration(pp);
		
		File genFormatsFile = new File(tempConfDir, PafBaseConstants.FN_GenerationFormats);
		
		assertTrue(genFormatsFile.exists());
		
		File hierFormatsFile = new File(tempConfDir, PafBaseConstants.FN_HierarchyFormats);
		
		assertFalse(hierFormatsFile.exists());
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		FileUtils.deleteFilesInDir(tempConfDir, new String[] { PafBaseConstants.FN_GenerationFormats } );
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		File newHierFormatsFile = null;
		
		try {
			newHierFormatsFile = File.createTempFile("test", "xml");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		assertNotNull(newHierFormatsFile);
		
		assertTrue(newHierFormatsFile.isFile());
		
		assertTrue(newHierFormatsFile.renameTo(hierFormatsFile));
		
		assertFalse(newHierFormatsFile.isFile());
		
		assertTrue(hierFormatsFile.isFile());
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafGenerationToHierarchyMigration#run()}.
	 */
	public void testRun() {
		
		MigrationAction action = new PafGenerationToHierarchyMigration(pp);
		
		File genFormatsFile = new File(tempConfDir, PafBaseConstants.FN_GenerationFormats);
		
		assertTrue(genFormatsFile.exists());
		
		File hierFormatsFile = new File(tempConfDir, PafBaseConstants.FN_HierarchyFormats);
		
		assertFalse(hierFormatsFile.exists());
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		assertEquals(0, pp.getHierarchyFormats().size());
		
		action.run();
				
		assertFalse(genFormatsFile.exists());
		
		assertTrue(hierFormatsFile.exists());
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		FileUtils.deleteFilesInDir(tempConfDir, true);
		
		try {
			PafZipUtil.unzipFile("./test_files/pace1210.paf", tempConfDir.toString());
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		try {
			pp = new XMLPaceProject(tempConfDir.getAbsolutePath(), true);
		} catch (InvalidPaceProjectInputException e) {
			fail(e.getMessage());
		} catch (PaceProjectCreationException e) {
			fail(e.getMessage());
		}
		
		assertNotNull(pp);
		
		action.setXMLPaceProject(pp);
						
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		FileUtils.deleteFilesInDir(tempConfDir, true);
						
		try {
			PafZipUtil.unzipFile("./test_files/pace1210.paf", tempConfDir.toString());
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		try {
			pp = new XMLPaceProject(tempConfDir.getAbsolutePath(), true);
		} catch (InvalidPaceProjectInputException e) {
			fail(e.getMessage());
		} catch (PaceProjectCreationException e) {
			fail(e.getMessage());
		}
		
		assertNotNull(pp);
		
		action.setXMLPaceProject(pp);
						
		assertEquals(MigrationActionStatus.Completed, action.getStatus());		
		
		
	}

}
