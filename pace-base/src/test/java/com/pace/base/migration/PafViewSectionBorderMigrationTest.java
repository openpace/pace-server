/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pace.base.view.PafBorder;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.ViewTuple;


/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PafViewSectionBorderMigrationTest extends MigrationActionTestCase {

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {

		super.setUp("./test_files/pace1210.paf");
		
	}
	

	/**
	 * Test method for {@link com.pace.base.migration.PafViewSectionBorderMigration#PafViewSectionBorderMigration(com.pace.base.project.XMLPaceProject)}.
	 */
	public void testPafViewSectionBorderMigration() {
		
		MigrationAction action = new PafViewSectionBorderMigration(pp);
		
		assertNotNull(action);
		assertNotNull(action.getXMLPaceProject());
		assertEquals(action.getXMLPaceProject(), pp);
		
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafViewSectionBorderMigration#getStatus()}.
	 */
	public void testGetStatus() {
		
		MigrationAction action = new PafViewSectionBorderMigration(pp);
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		List<PafViewSection> viewSectionList = pp.getViewSections();
		
		for (PafViewSection viewSection : viewSectionList ) {
			
			if ( viewSection.getRowTuples() != null ) {
				
				for (ViewTuple vt : viewSection.getRowTuples()) {
					
					vt.setBorder(null);
					
				}
				
			}
			
			if ( viewSection.getColTuples() != null ) {
				
				for (ViewTuple vt : viewSection.getColTuples()) {
					
					vt.setBorder(null);
					
				}
				
			}
			
		}
		
		pp.setViewSections(viewSectionList);
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		
		
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafViewSectionBorderMigration#run()}.
	 */
	public void testRun() {

		MigrationAction action = new PafViewSectionBorderMigration(pp);
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		List<PafViewSection> viewSectionList = pp.getViewSections();
		
		Map<ViewTuple, PafBorder> map = new HashMap<ViewTuple, PafBorder>();
		
		for (PafViewSection viewSection : viewSectionList ) {
			
			if ( viewSection.getRowTuples() != null ) {
				
				for (ViewTuple vt : viewSection.getRowTuples()) {
					
					if ( vt.getBorder() != null ) {
						
						map.put(vt, new PafBorder(vt.getBorder().getBorder()));
						
						assertNull(vt.getDataBorder());
					}
					
					
				}
				
			}
			
			if ( viewSection.getColTuples() != null ) {
				
				for (ViewTuple vt : viewSection.getColTuples()) {
					
					if ( vt.getBorder() != null ) {
						
						map.put(vt, new PafBorder(vt.getBorder().getBorder()));
						
						assertNull(vt.getDataBorder());
					}
					
				}
				
			}
			
		}
		
		action.run();
		
		for (PafViewSection viewSection : viewSectionList ) {
			
			if ( viewSection.getRowTuples() != null ) {
				
				for (ViewTuple vt : viewSection.getRowTuples()) {
					
					assertNull(vt.getBorder());
					
					if ( map.containsKey(vt) ) {
						assertNotNull(vt.getDataBorder());
						assertEquals(map.get(vt).getBorder(), vt.getDataBorder().getBorder());
					}
					
					
				}
				
			}
			
			if ( viewSection.getColTuples() != null ) {
				
				for (ViewTuple vt : viewSection.getColTuples()) {
					
					assertNull(vt.getBorder());
					
					if ( map.containsKey(vt) ) {
						assertNotNull(vt.getDataBorder());
						assertEquals(map.get(vt).getBorder(), vt.getDataBorder().getBorder());
					}
					
				}
				
			}
			
		}
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
	}

}
