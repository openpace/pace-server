/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.pace.base.PafBaseConstants;
import com.pace.base.db.RdbProps;
import com.pace.base.utility.StringUtils;

public abstract class RdbPropsMigrationActionTest extends ServerFileMigrationActionTestCase {

	private static String PASSWORD_KEY = PafBaseConstants.RDB_PASSWORD_KEY;
	
	protected void setUp(String file) throws Exception {
		super.setUp(file, 0, 1);
		this.migrationAction = new RdbPropsMigrationActionV1(this.serverXmlFile);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	public void testValidateMigrationAction() {
		
		try {
		
			logger.info("Validating pre upgrade version number ");
			List<RdbProps> action = getObjectFromXml();
			for(RdbProps prop : action){
				logAssertEquals(prop.getVersion(), OldVersion);
			}
			
			
			runMigrationAction();
			
			//Verify the passwords have been encrypted
			action = getObjectFromXml();
			List<String> encPasswords = new ArrayList<String>(20);
			for(RdbProps prop : action){
				Properties props = prop.getHibernateProperties();
				for(String key : props.stringPropertyNames()) {
					
					String value = props.getProperty(key);
					
					if(key.equalsIgnoreCase(PASSWORD_KEY)){
						logger.info(String.format("Validating password: %s is encrypted", value));
						
						assertTrue(StringUtils.isHex(value));
						if(encPasswords.contains(value)){
							fail("Duplicate encrypted password string was found.  This should never happen, however if it does the IV was not unique.");
						}else{
							encPasswords.add(value);
						}
					}
				}
			}
			
			
			//Verify that the password can be decrypted.
			action = getObjectFromXml();
			for(RdbProps prop : action){
				logger.info("Processing: " + prop.getName());
				logAssertEquals(prop.getVersion(), NewVersion);
				
				
				Properties props = prop.decryptHibernateProperties();
				assertNotNull(prop);
				
				for(String key : props.stringPropertyNames()) {
					String value = props.getProperty(key);
					logger.info("Key: " + key);
					logger.info("Value: " + value);
					assertNotNull(key);
					assertNotNull(value);
					
					if(key.equalsIgnoreCase(PASSWORD_KEY)){
						logger.info(String.format("Validating password: %s is decrypted", value));
						assertFalse(StringUtils.isHex(value));
					}
					
				}
			}
			
		} catch(Exception e){
			fail(e.getMessage());
		}
	}
}
