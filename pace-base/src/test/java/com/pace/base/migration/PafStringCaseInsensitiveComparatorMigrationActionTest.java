/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import com.pace.base.PafBaseConstants;

public class PafStringCaseInsensitiveComparatorMigrationActionTest extends MigrationActionTestCase {


	protected void setUp() throws Exception {
		super.setUp("./test_files/pace2600.paf");
	}


	public void testPafStringCaseInsensitiveComparatorMigrationActionStringArrayString() {
		
		MigrationAction action = new PafStringCaseInsensitiveComparatorMigrationAction(new String[] {PafBaseConstants.FN_HierarchyFormats }, this.tempConfDir.getAbsolutePath());
		
		assertNotNull(action);
	}

	public void testGetStatus() {
		
		MigrationAction action = new 
			PafStringCaseInsensitiveComparatorMigrationAction(new String[] {PafBaseConstants.FN_HierarchyFormats }, this.tempConfDir.getAbsolutePath());
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
	}

	public void testRun() {
		
		MigrationAction action = new 
			PafStringCaseInsensitiveComparatorMigrationAction(new String[] {PafBaseConstants.FN_HierarchyFormats }, this.tempConfDir.getAbsolutePath());
	
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		action.run();
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
	}

}
