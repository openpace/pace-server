/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.rules.RuleSet;
import com.pace.base.utility.PafXStream;


/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PafRuleSetsSeparationMigrationActionTest extends MigrationActionTestCase {

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {

		super.setUp("./test_files/pace1210.paf");
		
	}
	
	/**
	 * Test method for {@link com.pace.base.migration.PafRuleSetsSeparationMigrationAction#PafRuleSetsSeparationMigrationAction(com.pace.base.project.XMLPaceProject)}.
	 */
	public void testPafRuleSetsSeparationMigrationActionXMLPaceProject() {
	
		MigrationAction action = new PafRuleSetsSeparationMigrationAction(pp);
		
		assertNotNull(action);
		assertNotNull(action.getXMLPaceProject());
		assertEquals(action.getXMLPaceProject(), pp);
		
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafRuleSetsSeparationMigrationAction#getStatus()}.
	 */
	public void testGetStatus() {
		
		MigrationAction action = new PafRuleSetsSeparationMigrationAction(pp);
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		File pafRuleSetsXmlFile = new File(tempConfDir + File.separator
				+ PafBaseConstants.FN_RuleSetMetaData);
		
		File newPafRuleSetsDir = new File(tempConfDir+ File.separator + PafBaseConstants.DN_RuleSetsFldr);
		
		assertTrue(pafRuleSetsXmlFile.exists());
		assertFalse(newPafRuleSetsDir.exists());
		
		assertTrue(pafRuleSetsXmlFile.delete());
		assertTrue(newPafRuleSetsDir.mkdir());
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafRuleSetsSeparationMigrationAction#run()}.
	 */
	public void testRun() {
		
		MigrationAction action = new PafRuleSetsSeparationMigrationAction(pp);
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		Map<String, RuleSet> ruleSetMap = pp.getRuleSets();
		
		assertNotNull(ruleSetMap);
		assertEquals(0, ruleSetMap.size());
		
		File pafRuleSetsXmlFile = new File(tempConfDir + File.separator
				+ PafBaseConstants.FN_RuleSetMetaData);
		
		assertTrue(pafRuleSetsXmlFile.exists());
		
		List<RuleSet> ruleSets = null;
		
		try {
			 ruleSets = (List<RuleSet>) PafXStream.importObjectFromXml(pafRuleSetsXmlFile.toString());
		} catch (PafConfigFileNotFoundException e) {
			fail(e.getMessage());
		}
		
		assertNotNull(ruleSets);
		assertEquals(2, ruleSets.size());	
		
		File newPafRuleSetsDir = new File(tempConfDir+ File.separator + PafBaseConstants.DN_RuleSetsFldr);
		
		assertFalse(newPafRuleSetsDir.exists());
		
		action.run();
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		assertFalse(pafRuleSetsXmlFile.exists());
		
		File pafRuleSetsXmlFileBak = new File(tempConfDir + File.separator
				+ PafBaseConstants.FN_RuleSetMetaData + PafBaseConstants.BAK_EXT);
		
		assertTrue(pafRuleSetsXmlFileBak.exists());
		assertTrue(newPafRuleSetsDir.exists());
		
		Map<String, RuleSet> ruleSetMap2 = pp.getRuleSets();
		
		assertNotNull(ruleSetMap2);
		assertEquals(2, ruleSetMap2.size());
		
		//2 xml and 2 backup's with all tags
		assertEquals(4, newPafRuleSetsDir.list().length);
		
		
	}

}
