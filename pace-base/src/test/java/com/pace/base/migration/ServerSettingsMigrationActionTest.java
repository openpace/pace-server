/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import com.pace.base.PafBaseConstants;
import com.pace.base.server.ServerSettings;

public class ServerSettingsMigrationActionTest extends ServerFileMigrationActionTestCase {

	public void setUp() throws Exception {
		super.setUp(PafBaseConstants.FN_ServerSettings, 0, 1);
		this.migrationAction = new ServerSettingsMigrationActionV1(this.serverXmlFile);
	}

	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	public void testValidateMigrationAction() {
		
		try {
		
			logger.info("Validating pre upgrade version number ");
			ServerSettings action = getObjectFromXml();
			logAssertEquals(action.getVersion(), OldVersion);
			
			
			runMigrationAction();
			
			action = getObjectFromXml();
			logAssertEquals(action.getVersion(), NewVersion);
			
		} catch(Exception e){
			fail(e.getMessage());
		}
	}
}
