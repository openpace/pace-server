/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import com.pace.base.PafBaseConstants;
import com.pace.base.server.PafLDAPSettings;

public class PafLDAPSettingsMigrationActionTest extends ServerFileMigrationActionTestCase {

	private static final String DEFAULT_PASSWORD = "password";
	
	protected void setUp() throws Exception {
		super.setUp(PafBaseConstants.FN_LdapSettings, 0, 1);
		this.migrationAction = new PafLDAPSettingsMigrationAction(this.serverXmlFile);
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}


	@Override
	public void testValidateMigrationAction() {
		try{
			
			PafLDAPSettings action = getObjectFromXml();
			logger.info("Validating pre upgrade version number ");
			logAssertEquals(action.getVersion(), OldVersion);
			
			runMigrationAction();
			
			action = getObjectFromXml();
			assertNotNull(action);
			assertNotEquals(DEFAULT_PASSWORD, action.getEncryptedSecurityCredentials());
			logAssertEquals(DEFAULT_PASSWORD, action.getSecurityCredentials());

		}catch(Exception e){
			fail(e.getMessage());
		}

	}

}
