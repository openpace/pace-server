/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.pace.base.PafBaseConstants;
import com.pace.base.mdb.IPafConnectionProps;
import com.pace.base.mdb.PafConnectionProps;

public class PafConnectionPropsMigrationActionTest extends ServerFileMigrationActionTestCase {

	private static final String PASSWORD_KEY = PafBaseConstants.CONN_STRING_PSWD_KEY;
	private static final String PASSWORD_VALUE = "password";
	
	protected void setUp() throws Exception {
		super.setUp(PafBaseConstants.FN_MdbDataSources, 0, 1);
		this.migrationAction = new PafConnectionPropsMigrationActionV1(this.serverXmlFile);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	public void testValidateMigrationAction() {
		
		try {
		
			logger.info("Validating pre upgrade version number ");
			List<IPafConnectionProps> action = getObjectFromXml();
			for(IPafConnectionProps prop : action){
				logAssertEquals(prop.getVersion(), OldVersion);
			}
			
			
			runMigrationAction();
			
			//Verify the passwords have been encrypted
			action = getObjectFromXml();
			List<String> encPasswords = new ArrayList<String>(20);
			for(IPafConnectionProps prop : action){
				List<String> keyValues = PafConnectionProps.splitConnStr(prop.getConnectionString());
				for(String keyValue : keyValues) {
					
					String[] kvPair = PafConnectionProps.splitConnStrKeyValue(keyValue,  false);
					String key = kvPair[0];
					
					if(key.equalsIgnoreCase(PASSWORD_KEY)){
						String value = kvPair[1];
						logger.info(String.format("Comparing default password: %s to encrypted password: %s", PASSWORD_VALUE, value));
						assertNotEquals(PASSWORD_VALUE, value);
						if(encPasswords.contains(value)){
							fail("Duplicate encrypted password string was found.  This should never happen, however if it does the IV was not unique.");
						}else{
							encPasswords.add(value);
						}
					}
				}
			}
			
			
			//Verify that the password can be decrypted.
			action = getObjectFromXml();
			for(IPafConnectionProps prop : action){
				logger.info("Processing: " + prop.getName());
				logAssertEquals(prop.getVersion(), NewVersion);
				assertNotNull(prop.getProperties());
				
				Properties props = prop.getProperties();
				for(String key : props.stringPropertyNames()) {
					String value = props.getProperty(key);
					logger.info("Key: " + key);
					logger.info("Value: " + value);
					logAssertEquals(key.toUpperCase().equals(key), true);
					assertNotNull(key);
					assertNotNull(value);
					
					if(key.equalsIgnoreCase(PASSWORD_KEY)){
						logger.info(String.format("Comparing default password: %s to decrypted password: %s", PASSWORD_VALUE, value));
						assertEquals(PASSWORD_VALUE, value);
					}
					
				}
			}
			
		} catch(Exception e){
			fail(e.getMessage());
		}
	}
}
