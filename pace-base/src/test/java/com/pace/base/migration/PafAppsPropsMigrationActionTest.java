/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

/* Class added to test if properties to paf apps are added during upgrade */
public class PafAppsPropsMigrationActionTest extends MigrationActionTestCase {

	

	public void testPafAppsPropsMigrationAction() {
		MigrationAction action = new AppSettingsMigrationAction(pp);
		
		assertNotNull(action);
		assertNotNull(action.getXMLPaceProject());
		assertEquals(action.getXMLPaceProject(), pp);
	}

	public void testGetStatus() {
		
		pp.setUpgradeProject(true);
		
		MigrationAction action = new AppSettingsMigrationAction(pp);
		// make sure upgrade action is not complete
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
	}

	public void testRun() {
		MigrationAction action = new AppSettingsMigrationAction(pp);
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		action.run();
		// check if filtered properties got added
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
	}
	
}
