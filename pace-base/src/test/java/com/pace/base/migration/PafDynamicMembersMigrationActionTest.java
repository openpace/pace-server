/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.app.DynamicMemberDef;
import com.pace.base.project.*;
import com.pace.base.utility.FileUtils;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PafDynamicMembersMigrationActionTest extends MigrationActionTestCase {

	/**
	 * Test method for {@link com.pace.base.migration.PafDynamicMembersMigrationAction#PafDynamicMembersMigrationAction(com.pace.base.project.XMLPaceProject)}.
	 */
	public void testPafDynamicMembersMigrationActionXMLPaceProject() {
		
		PafDynamicMembersMigrationAction action = new PafDynamicMembersMigrationAction(pp);
		
		assertNotNull(action);
		assertNotNull(action.getXMLPaceProject());
		assertEquals(action.getXMLPaceProject(), pp);
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafDynamicMembersMigrationAction#getStatus()}.
	 */
	public void testGetStatus() {
		
		PafDynamicMembersMigrationAction action = new PafDynamicMembersMigrationAction(pp);
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		String versionName = pp.getApplicationDefinitions().get(0).getMdbDef().getVersionDim();
		
		assertEquals(versionName, pp.getDynamicMembers().get(0).getDimension());
		
		pp.getDynamicMembers().get(0).setDimension("NOT VERSION DIM");
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		pp.getDynamicMembers().get(0).setDimension(versionName);
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		File dynamicMembersFile = new File(tempConfDir, PafBaseConstants.FN_DynamicMembers);
		
		assertTrue(dynamicMembersFile.isFile());
		
		FileUtils.deleteFilesInDir(tempConfDir, new String[] { PafBaseConstants.FN_DynamicMembers } );
		
		assertFalse(dynamicMembersFile.exists());
		
		try {
			pp.loadData(new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.DynamicMembers)));
		} catch (PafException e) {
			fail(e.getMessage());
		}
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
				
		assertFalse(dynamicMembersFile.exists());
		
		try {
			pp = new XMLPaceProject(pp.getProjectInput(), new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.DynamicMembers)), true);
		} catch (InvalidPaceProjectInputException e) {
			fail(e.getMessage());
		} catch (PaceProjectCreationException e) {
			fail(e.getMessage());
		}
		
		action.setXMLPaceProject(pp);
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		FileUtils.deleteFilesInDir(tempConfDir, new String[] { PafBaseConstants.FN_DynamicMembers } );
		
		assertFalse(dynamicMembersFile.exists());
		
		try {
			pp = new XMLPaceProject(pp.getProjectInput(), new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.DynamicMembers)), false);
		} catch (InvalidPaceProjectInputException e) {
			fail(e.getMessage());
		} catch (PaceProjectCreationException e) {
			fail(e.getMessage());
		}
		
		action.setXMLPaceProject(pp);
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
						
	}

	/**
	 * Test method for {@link com.pace.base.migration.PafDynamicMembersMigrationAction#run()}.
	 */
	public void testRun() {
		
		PafDynamicMembersMigrationAction action = new PafDynamicMembersMigrationAction(pp);
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		File dynamicMembersFile = new File(tempConfDir, PafBaseConstants.FN_DynamicMembers);
		
		assertTrue(dynamicMembersFile.isFile());
		
		FileUtils.deleteFilesInDir(tempConfDir, new String[] { PafBaseConstants.FN_DynamicMembers } );
		
		assertFalse(dynamicMembersFile.exists());
		
		try {
			pp.loadData(new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.DynamicMembers)));
		} catch (PafException e) {
			fail(e.getMessage());
		}
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		action.run();
		
		assertEquals(MigrationActionStatus.Completed, action.getStatus());
		
		//List<DynamicMemberDef> dynamicMemberList = pp.getDynamicMembers();
		
		assertEquals(1, pp.getDynamicMembers().size());
		
		assertEquals(pp.getApplicationDefinitions().get(0).getMdbDef().getVersionDim(), pp.getDynamicMembers().get(0).getDimension());
		
		pp.getDynamicMembers().get(0).setDimension("NOT VERSION DIM");
		
		assertNotSame(pp.getApplicationDefinitions().get(0).getMdbDef().getVersionDim(), pp.getDynamicMembers().get(0).getDimension());
		
		assertEquals(MigrationActionStatus.NotStarted, action.getStatus());
		
		action.run();
		
		assertEquals(1, pp.getDynamicMembers().size());
		assertEquals(pp.getApplicationDefinitions().get(0).getMdbDef().getVersionDim(), pp.getDynamicMembers().get(0).getDimension());
		assertEquals(1, pp.getDynamicMembers().get(0).getMemberSpecs().length);
		assertEquals(PafBaseConstants.PLAN_VERSION, pp.getDynamicMembers().get(0).getMemberSpecs()[0]);
		
		try {
			pp.save();
		} catch (ProjectSaveException e) {
			fail(e.getMessage());
		}
						
		assertTrue(dynamicMembersFile.isFile());
				
	}

}
