/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.pace.base.migration");
		//$JUnit-BEGIN$
		suite.addTestSuite(PafSecurityMigrationActionTest.class);
		suite.addTestSuite(PafViewsSeparationMigrationActionTest.class);
		suite.addTestSuite(PafDTDtoXSDMigrationActionTest.class);
		suite.addTestSuite(PafPlannerConfigMigrationActionTest.class);
		suite.addTestSuite(PafDynamicMembersMigrationActionTest.class);
		suite.addTestSuite(PafViewSectionsSeparationMigrationActionTest.class);
		suite.addTestSuite(PafGlobalStylesMigrationActionTest.class);
		suite.addTestSuite(PafRuleSetsSeparationMigrationActionTest.class);
		suite.addTestSuite(PafViewGroupsMigrationActionTest.class);
		suite.addTestSuite(PafViewSectionBorderMigrationTest.class);
		suite.addTestSuite(PafGenerationToHierarchyMigrationTest.class);
		suite.addTestSuite(PafAppsMigrationActionTest.class);
		//$JUnit-END$
		return suite;
	}

}
