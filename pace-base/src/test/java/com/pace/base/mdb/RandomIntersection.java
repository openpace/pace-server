/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.base.mdb;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.pace.base.data.Intersection;

/**
 * @author Alan
 *
 */
public class RandomIntersection {
	
	boolean isMaxMemberReached = false;
	List<String> generatedMembers = null;
	int memberPoolSize;
	String[] dimensions;
	int memberLength;
	
	@SuppressWarnings("unused")
	private RandomIntersection() {
		
	}
	
	public RandomIntersection(int memberPoolSize, String[] dimensions, int memberLength) {
		this.memberPoolSize = memberPoolSize;
		this.dimensions = dimensions;
		this.memberLength = memberLength;
		this.generatedMembers = new ArrayList<String>(memberPoolSize);
	}

	public Intersection getIntersection() {
		// TODO Auto-generated constructor stub

		
		int dimSize = dimensions.length;
		String[] coordinates = new String[dimSize];
		String memberName = null;
		for (int dimInx = 0; dimInx < dimSize; dimInx++) {
			if (!isMaxMemberReached) {
				memberName = UUID.randomUUID().toString().substring(0, memberLength);
				generatedMembers.add(memberName);
			} else {
				int memberIndex = (int) (Math.random()* memberPoolSize);
				memberName = generatedMembers.get(memberIndex);
			}
			coordinates[dimInx] = memberName;
		}
		if (!isMaxMemberReached && (memberPoolSize > 0) && (generatedMembers.size() >= memberPoolSize)) {
			isMaxMemberReached=true;
		}
		return new Intersection(dimensions, coordinates);
	}

}
