/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.base.mdb;
import java.util.HashMap;
import java.util.Map;

import com.pace.base.data.Intersection;

/**
 * @author Alan
 *
 */
public class BlockDC2 {
	Map<Intersection, Intersection> cellIndexMap = null;;
	Map<Intersection, double[][]> dataBlockPool;
	public void setDataBlockPool(Map<Intersection, double[][]> dataBlockPool) {
		this.dataBlockPool = dataBlockPool;
	}
	int intersectionCount = 0;

	public BlockDC2() {
		this(0);
	}
	public BlockDC2(int initSize) {
		cellIndexMap = new HashMap<Intersection, Intersection>(initSize);
		dataBlockPool = new HashMap<Intersection,double[][]>();
	}
	public Map<Intersection, double[][]> getDataBlockPool() {
		return dataBlockPool;
	}
	public void addIntersection(Intersection intersection) {
		//int[] dimIndexes = new int[]{0,1,2,3,4,5};
		//Intersection blockIndex = intersection.createSubIntersection(dimIndexes);
		cellIndexMap.put(intersection, intersection);

		int periodCount = 37;
		int measureCount = 50;
		
		// Initialize data block
		getDataBlockPool().put(intersection,new double[measureCount][periodCount]);
		
	}
}
