/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.base.mdb;
import com.pace.base.data.Intersection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alan
 *
 */
public class IntersectionDC {
	Map<Intersection, Integer> cellIndexMap = null;
	List<Double> cellArray = null;
	int intersectionCount = 0;

	public IntersectionDC() {
		this(0);
	}
	public IntersectionDC(int initSize) {
		cellIndexMap = new HashMap<Intersection, Integer>(initSize);
		cellArray = new ArrayList<Double>();
	}
	
	public List<Double> getCellArray() {
		return cellArray;
	}
	public void addIntersection(Intersection intersection) {
		// Add index entry for new cell (index is auto-incremented)
		cellIndexMap.put(intersection, intersectionCount++);

		cellArray.add(0.0);
		// Intialize data block
//		getDataBlockArray().add(new double[measureCount][periodCount]);
		
	}
}
