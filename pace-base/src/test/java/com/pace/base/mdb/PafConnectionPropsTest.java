/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.db.RdbPropsTest;
import com.pace.base.migration.PafConnectionPropsMigrationActionV1;
import com.pace.base.migration.ServerFileMigrationActionTestCase;
import com.pace.base.security.AESKeystoreEncryption;

public class PafConnectionPropsTest extends ServerFileMigrationActionTestCase {

	private static final Logger logger = Logger.getLogger(RdbPropsTest.class);
	private static final String PASSWORD_KEY = PafBaseConstants.CONN_STRING_PSWD_KEY;
	
	protected void setUp() throws Exception {
		super.setUp(PafBaseConstants.FN_MdbDataSources, 0, 1);
		this.migrationAction = new PafConnectionPropsMigrationActionV1(this.serverXmlFile);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	@SuppressWarnings("unchecked")
	public void testNotNull() {
		logger.info("Validating xml is not null");
		List<PafConnectionProps> props = (List<PafConnectionProps>) getObjectFromXml();
		for(PafConnectionProps prop : props){
			assertNotNull(prop);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void testPasswordEncryption() {
		
		validateStringIsEncrypted((List<PafConnectionProps>) getObjectFromXml(), false);
		
		this.runMigrationAction();
		
		validateStringIsEncrypted((List<PafConnectionProps>) getObjectFromXml(), true);
	}

	@SuppressWarnings("unchecked")
	public void testPasswordDecryption() {
		
		this.runMigrationAction();
		
		List<PafConnectionProps> pafProps = (List<PafConnectionProps>) getObjectFromXml();
		for(PafConnectionProps pafProp : pafProps){
			Properties props = null;
			try {
				props = pafProp.getProperties();
				
				for(String key : props.stringPropertyNames()) {
					String value = props.getProperty(key);
					if(key.equalsIgnoreCase(PASSWORD_KEY)){
					
						assertFalse(AESKeystoreEncryption.isStringEncrypted(value));
						break;
					}
				}
			} catch (PafKeystoreDecryptionException e) {
				fail(e.getMessage());
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void testPasswordDecryptionThrowsException() {
		
		this.runMigrationAction();
		
		//Get the rdb props and change the encrypted string.
		List<PafConnectionProps> pafProps = (List<PafConnectionProps>) getObjectFromXml();
		for(PafConnectionProps pafProp : pafProps){
			Properties props = null;
			String connString = pafProp.getConnectionString();
			try {
				props = pafProp.parseConnString(connString, false, false);
			} catch (PafKeystoreDecryptionException e) {
				fail(e.getMessage());
			}
			
			for(String key : props.stringPropertyNames()) {
				String value = props.getProperty(key);
				if(key.equalsIgnoreCase(PASSWORD_KEY)){
				
					props.setProperty(key, value.substring(0, value.length() - 2));
					pafProp.setEncryptedConnectionString(connString.replace(value, value.substring(0, value.length() - 2)));
					break;
				}
			}
		}
		
		//Loop thru the props again and check for the password to get reset
		for(PafConnectionProps pafProp : pafProps){
			try {
				pafProp.setProperties(null);
				pafProp.getProperties();
				fail("Should not be able to decrypt modified password (testPasswordDecryptionThrowsException)");
			} catch (Exception e) {
				assertEquals(e.getClass(), PafKeystoreDecryptionException.class);
			}
		}
	}
	
	

	public void validateStringIsEncrypted(List<PafConnectionProps> pafProps, boolean isEncrypted){
		for(PafConnectionProps pafProp : pafProps){
			Properties props;
			try {
				props = pafProp.parseConnString(pafProp.getConnectionString(), false, false);
				for(String key : props.stringPropertyNames()) {
					String value = props.getProperty(key);
					if(key.equalsIgnoreCase(PASSWORD_KEY)){
					
						assertEquals(AESKeystoreEncryption.isStringEncrypted(value), isEncrypted);
						break;
					}
				}
			} catch (PafKeystoreDecryptionException e) {
				fail(e.getMessage());
			}
		}
	}
	
	@Override
	public void testValidateMigrationAction() {
		logger.info("Ignoring migration test.");
		
	}
	
	@Override
	public void testMigrationActionNotNull(){
		logger.info("Ignoring migration test.");
	}
	
	@Override
	public void testMigrationActionOriginalStatus(){
		logger.info("Ignoring migration test.");
	}
	
	@Override
	public void runMigrationAction() {
		migrationAction.run();
	}
	
}
