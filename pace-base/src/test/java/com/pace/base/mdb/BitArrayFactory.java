/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import org.apache.log4j.Logger;

import sun.security.util.BitArray;

public class BitArrayFactory implements ObjectTestFactory {

	private int bitCount = 1;
	private int periodCount = 37;
	private int measureCount = 50;
	private int blockSize = periodCount * measureCount;
	private Logger logger = Logger.getLogger(BitSetArrayFactory.class);
	
	public BitArrayFactory(int bitCount) {
		this.bitCount = bitCount;
	}

	@Override
	public Object makeObject() {

		long begTime = System.currentTimeMillis(),elapsedTime; 
		BitArray[][] objs = new BitArray[measureCount][periodCount];
		for (int i=0; i<measureCount; i++) {
			for (int j=0; j<periodCount; j++) {
				BitArray bitSet = new BitArray(bitCount);
				objs[i][j] = bitSet;
			}
		}
		
		elapsedTime = System.currentTimeMillis() - begTime;
		String msg = "BitSize array created in " + elapsedTime + "ms with a blockSize of " + blockSize
			+ " and a bitcount of: " + bitCount;
		logger.info(msg);
		return objs;

	}

}
