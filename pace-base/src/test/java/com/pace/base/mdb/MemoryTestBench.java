/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.base.mdb;

/**
 * @author Alan
 *
 */
public abstract class MemoryTestBench {
	final static int MEGABYTE = 1048576;
	  public static long calculateMemoryUsage(ObjectTestFactory factory) {
	    Object handle = factory.makeObject();
	    long mem0 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    long mem1 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    handle = null;
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    mem0 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    handle = factory.makeObject();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    mem1 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    return mem1 - mem0;
	  }
	  public static void showMemoryUsage(ObjectTestFactory factory) {
	    long mem = calculateMemoryUsage(factory);
	    System.out.println(
	      factory.getClass().getName() + " produced " +
	      factory.makeObject().getClass().getName() +
	      " which took " + mem + " bytes OR " + String.format("%.2f", (float)mem/(MEGABYTE)) + " MB" );
	  }
	}
