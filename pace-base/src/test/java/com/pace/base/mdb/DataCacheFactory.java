/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.util.Arrays;

import org.apache.log4j.Logger;

import com.pace.base.data.Intersection;


public class DataCacheFactory implements ObjectTestFactory {

	private static final Logger logger = Logger.getLogger(DataCacheFactory.class);

	public static final int NUM_INTERSECTIONS = 1;
	public static final int INTERSECTION_SIZE = 7;
	public static final int MEMBER_LENGTH = 12;
	public static final int MEMBER_POOL_SIZE = 10000;
	public static final String[] DIMENSION_LIST = 
				new String[]{"MEASURES","TIME","PRODUCT","DEPARTMENT","YEAR", "PLANTYPE","SCENARIO","CURRENCY", "VIEW", "MARKET","BUSINESS.UNIT"};
	public int numIS;
	public int intersectSize;
	public int memberLength;
	public int memberPoolSize;
	
	public DataCacheFactory () {
		this(NUM_INTERSECTIONS, INTERSECTION_SIZE, MEMBER_LENGTH, MEMBER_POOL_SIZE);
	}
	

	/**
	 * @param numIS
	 * @param intersectSize
	 * @param memberLength
	 */
	public DataCacheFactory (int numIS, int intersectSize, int memberLength, int memberPoolSize) {
		this.numIS = numIS;
		this.intersectSize = intersectSize;
		this.memberLength = memberLength;
		this.memberPoolSize = memberPoolSize;
	}
	
	@Override
	public Object makeObject() {
		
		String msg = null;
		String[] dimensions = Arrays.copyOf(DIMENSION_LIST, intersectSize);
		BlockDC dataCache = new BlockDC(this.numIS);
		long begTime, elapsedTime;
		
		begTime = System.currentTimeMillis();
		RandomIntersection ri = new RandomIntersection(memberPoolSize, dimensions, memberLength);
		for (int i = 1; i <= this.numIS; i++) {
			Intersection blockIs = ri.getIntersection();
			dataCache.addIntersection(blockIs);
		}
		
		elapsedTime = System.currentTimeMillis() - begTime;
		msg = "Block Data Cache created in " + elapsedTime + "ms with " + numIS + " blocks, "
			+ "an intersection size of: " + intersectSize + ", and a member length of: "
			+ memberLength;
		logger.info(msg);
		
		// TODO Auto-generated method stub
		return dataCache;
	}

}
