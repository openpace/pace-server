/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.security;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;

import com.pace.base.PafException;
import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.utility.StringUtils;

public class AESKeystoreEncryptionTest  extends TestCase {

	private static final Logger logger = Logger.getLogger(AESKeystoreEncryptionTest.class);
	private AESKeystoreEncryption encryptor;
	private static final int MAX_PASSWORD_LENGTH = 256;
	private static final int STEP = 5;
	
	protected void setUp() throws Exception {
		super.setUp();
		encryptor = new AESKeystoreEncryption();
	}
	
	public void testGenerateSecretKey() {
		
		try {
			assertNotNull(AESKeystoreEncryption.generateKey());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	public void testGenerateSecretKeyToHex() {
		
		try {
			assertNotNull(Hex.encodeHexString(AESKeystoreEncryption.generateKey().getEncoded()));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	public void testGenerateIV() {
		
		try {
			assertNotNull(AESKeystoreEncryption.generateIV());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	public void testGenerateIVToHex() {
		
		try {
			assertNotNull(Hex.encodeHexString(AESKeystoreEncryption.generateIV()));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	public void testDecryptForLoop(){
		for(int i = 0; i < 100000; i++){
			testDecrypt();
		}
	}
	
	public void testDecrypt() {
		
		//Test decrypting a null string (hex Method)
		try {
			
			encryptor.decryptFromHexString(null);
			
			fail("Test fail - was able to decrypt (HEX) a null string.");
			
		} catch (Exception e) {

			assertEquals(e.getClass(), IllegalArgumentException.class);
			
		}
		
		//Test decrypting a null string (byte method).
		try {
			
			encryptor.decrypt(null);
			
			fail("Test fail - was able to decrypt a null string.");
			
		} catch (Exception e) {

			assertEquals(e.getClass(), IllegalArgumentException.class);
			
		}
		
		//Test decrypting a btye array that's too small.
		try {
			
			encryptor.decrypt(new byte[31]);
			
			fail("Test fail - was able to decrypt a byte array that's too small.");
			
		} catch (Exception e) {

			assertEquals(e.getClass(), IllegalArgumentException.class);
			
		}
		
		//Test decrypting a non-hex string.
		try {
			
			String hexString = encryptor.encryptToHexString("Just-A-Random-String");
			char c = hexString.charAt(3);
			String badHexString = hexString.replace(c, 'M');
			
			String result = encryptor.decryptFromHexString(badHexString);
			
			assertEquals(result, badHexString);
			
		} catch (Exception e) {

			fail("No exception is thown on an invalid hex string");
			
		}
		
		//Test decrypting a modified hex string.
		try {
			
			String hexString = encryptor.encryptToHexString("Just-A-Random-String");

			char[] badHexChars = hexString.toCharArray();
			
			for(int i = 16; i < badHexChars.length ; i++) { 
				badHexChars[i] = getRandomHexCharacter(); 
			}

			String badHexString = String.valueOf(badHexChars);
			String s = encryptor.decryptFromHexString(badHexString);
			
			fail(String.format("Test fail - was able to decrypt a modified HEX string: %s, got result: %s", badHexString, s));
			
		} catch (Exception e) {

			assertEquals(e.getClass(), PafKeystoreDecryptionException.class);
			
		}
		
	}

	public void testEncrypt() {
		
		
		//Try encrypting a null string (Hex method).
		try {
			
			encryptor.encryptToHexString(null);
			
			fail("Test fail - was able to encrypt a null (HEX) string.");
			
		} catch (Exception e) {

			assertEquals(e.getClass(), IllegalArgumentException.class);
			
		}
		

		//Try encrypting a null string (byte method).
		try {
			
			encryptor.encrypt(null);
			
			fail("Test fail - was able to encrypt a null string.");
			
		} catch (Exception e) {

			assertEquals(e.getClass(), IllegalArgumentException.class);
			
		}
		
		//Try encrypting a empty string (byte method).
		try {
			
			encryptor.encrypt("");
			
			fail("Test fail - was able to encrypt an empty string.");
			
		} catch (Exception e) {

			assertEquals(e.getClass(), IllegalArgumentException.class);
			
		}
		
		//Try encrypting a empty string (Hex method).
		try {
			
			encryptor.encryptToHexString("");
			
			fail("Test fail - was able to encrypt an empty (HEX) string.");
			
		} catch (Exception e) {

			assertEquals(e.getClass(), IllegalArgumentException.class);
			
		}
		
		//Try encrypting a 1 char string
		try {
			
			String smallString = "a";
			
			String encString = encryptor.encryptToHexString(smallString);
			
			assertNotNull(encString);
			
			String decString = encryptor.decryptFromHexString(encString);
			
			assertEquals(smallString, decString);
			
			
		} catch (Exception e) {

			fail("Test fail - was not able to encrypt a single char string.");
			
		}
		
		//Try encrypting a 1 blank string
		try {
			
			String smallString = " ";
			
			String encString = encryptor.encryptToHexString(smallString);
			
			assertNotNull(encString);
			
			String decString = encryptor.decryptFromHexString(encString);
			
			assertEquals(smallString, decString);
			
			
		} catch (Exception e) {

			fail("Test fail - was not able to encrypt a blank string.");
			
		}
		
		//Try encrypting a non ASCII string
		try {
			
			String smallString = "�e�~{Ȳ�0���+�i";
			
			String encString = encryptor.encryptToHexString(smallString);
			
			assertNotNull(encString);
			
			String decString = encryptor.decryptFromHexString(encString);
			
			assertEquals(smallString, decString);
			
			
		} catch (Exception e) {

			fail("Test fail - was not able to encrypt a non ascii (Russian) string.");
			
		}

		//Try encrypting a non ASCII string
		try {
			
			//Russian
			String smallString = "Можете ли вы Шифрование Меня?";
			
			String encString = encryptor.encryptToHexString(smallString);
			
			assertNotNull(encString);
			
			String decString = encryptor.decryptFromHexString(encString);
			
			assertNotNull(decString);
			
			assertEquals(smallString, decString);
			
			
			//Chineese
			smallString = "您可以加密我";
			
			encString = encryptor.encryptToHexString(smallString);
			
			assertNotNull(encString);
			
			decString = encryptor.decryptFromHexString(encString);
			
			assertNotNull(decString);
			
			assertEquals(smallString, decString);
			
			
		} catch (Exception e) {

			fail("Test fail - was not able to encrypt a non ascii (Chineese) string.");
			
		}
		
		//Try encrypting a a blank string
		try {
			
			String smallString = "      ";
			
			String encString = encryptor.encryptToHexString(smallString);
			
			assertNotNull(encString);
			
			String decString = encryptor.decryptFromHexString(encString);
			
			assertEquals(smallString, decString);
			
			
		} catch (Exception e) {

			fail("Test fail - was not able to encrypt a blank (multi-lenght) string.");
			
		}
		
	}

	
	public void testEncryptedStringsAreDifferent(){
		
		int maxRuns = 100000;
		int maxPasswordLen = 50;

		for(int i = 5; i < maxPasswordLen; i++){
			String password = RandomStringUtils.randomAscii(i);
			assertFalse(StringUtils.isHex(password));
			logger.info(String.format("Encrypting password '%s' : %,3d times", password, maxRuns));
			Set<String> randomStrings = new HashSet<String>(maxRuns);
			for(int j = 0; j < maxRuns; j++){
				String encryptedString = null;
				try {
					encryptedString = encryptor.encryptToHexString(password);
				} catch (PafException ex) {
					fail(ex.getMessage());
				}
				
				assertTrue(StringUtils.isHex(encryptedString));
				
				boolean found = randomStrings.contains(encryptedString);
				assertFalse(found);
				
				if(found){
					fail("Duplicate encrypted password string was found.  This should never happen, however if it does the IV was not unique.");
				}else{
					randomStrings.add(encryptedString);
					try {
						assertEquals(password, encryptor.decryptFromHexString(encryptedString));
					} catch (PafException e) {
						fail(e.getMessage());
					}
				}
			}
		}
	}
	
	public void testEncryptedAlphabeticPasswords() {
		
		try {
			int maxPasswordLength = MAX_PASSWORD_LENGTH;
			int step = STEP;
			int bucketSize = (maxPasswordLength/step) + 1;
			
			//Test alphabetic only
			List<String> passwords = new ArrayList<String>(bucketSize);
			for(int i = step; i < maxPasswordLength; i += step){
				passwords.add(RandomStringUtils.randomAlphabetic(i));
			}
			validateStringEncryption(passwords, true);
			
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			fail(e.getMessage());
		}

	}
	
	public void testEncryptedAlphanumericPasswords() {
		
		try {
			int maxPasswordLength = MAX_PASSWORD_LENGTH;
			int step = STEP;
			int bucketSize = (maxPasswordLength/step) + 1;
			
			//Test alphanumeric only
			List<String> passwords = new ArrayList<String>(bucketSize);
			for(int i = step; i < maxPasswordLength; i += step){
				passwords.add(RandomStringUtils.randomAlphanumeric(i));
			}
			validateStringEncryption(passwords, true);
			
		}catch(Exception e){
			e.printStackTrace();
			fail(e.getMessage());
		}

	}
	
	public void testEncryptedAsciiPasswords() {
		
		try {
			int maxPasswordLength = MAX_PASSWORD_LENGTH;
			int step = STEP;
			int bucketSize = (maxPasswordLength/step) + 1;
			
			//Test random ASCII characters
			List<String> passwords = new ArrayList<String>(bucketSize);
			for(int i = step; i < maxPasswordLength; i += step){
				passwords.add(RandomStringUtils.randomAscii(i));
			}
			validateStringEncryption(passwords, true);
			
		}catch(Exception e){
			e.printStackTrace();
			fail(e.getMessage());
		}

	}
	
	public void testEncryptedNumericPasswords() {
		
		try {
			int maxPasswordLength = MAX_PASSWORD_LENGTH;
			int step = STEP;
			int bucketSize = (maxPasswordLength/step) + 1;
			
			//Test numeric only
			List<String> passwords = new ArrayList<String>(bucketSize);
			for(int i = step; i < maxPasswordLength; i += step){
				passwords.add(RandomStringUtils.randomNumeric(i));
			}
			validateStringEncryption(passwords, false);
			
			
		}catch(Exception e){
			e.printStackTrace();
			fail(e.getMessage());
		}

	}
	
	
	public void validateStringEncryption(List<String> passwords, boolean validateHex) {
		
		try {
			
			for(String password : passwords){
				
				logger.info(String.format("Testing string: %s", password));
				assertNotNull(password);

				if(validateHex){
					assertFalse(StringUtils.isHex(password));
				}
				
				String encryptedPassword1 = encryptor.encryptToHexString(password);
				assertTrue(StringUtils.isHex(encryptedPassword1));
				//logger.debug(String.format("String: %s : encrypted to: %s", password, encryptedPassword1 ));
				
				String encryptedPassword2 = encryptor.encryptToHexString(password);
				assertTrue(StringUtils.isHex(encryptedPassword2));
				//logger.debug(String.format("String: %s : encrypted to: %s", password, encryptedPassword2 ));
				
				assertThat(encryptedPassword1, not(encryptedPassword2));
				
				
				String decryptedPassword1 = encryptor.decryptFromHexString(encryptedPassword1);
				//logger.debug(String.format("Encrypted String: %s : decrypted to: %s", encryptedPassword1, decryptedPassword1 ));
				
				String decryptedPassword2 = encryptor.decryptFromHexString(encryptedPassword2);
				//logger.debug(String.format("Encrypted String: %s : decrypted to: %s", encryptedPassword2, decryptedPassword2 ));
				
				
				assertEquals(decryptedPassword1, password);
				
				assertEquals(decryptedPassword2, password);
				
				assertEquals(decryptedPassword1, decryptedPassword2);
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	private char getRandomHexCharacter(){
		Random rand = new Random();
		int myRandomNumber = rand.nextInt(0x10) + 0x10; // Generates a random number between 0x10 and 0x20
		return Integer.toHexString(myRandomNumber).charAt(0);
	}

}
