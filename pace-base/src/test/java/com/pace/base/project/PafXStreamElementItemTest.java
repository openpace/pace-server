/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project;

import java.io.File;
import java.util.Date;

import com.pace.base.PafBaseConstants;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.migration.MigrationActionTestCase;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PafXStreamElementItemTest extends MigrationActionTestCase {

	PafXStreamElementItem<PafApplicationDef[]> pafAppXStreamDataItem;
	
	//String CONF_DIR = "C:\\Program Files (x86)\\Pace\\PafServer\\conf_test\\";
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		
		pafAppXStreamDataItem = new PafXStreamElementItem<PafApplicationDef[]>(tempConfDir.getAbsolutePath() + File.separator + PafBaseConstants.FN_ApplicationMetaData);
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}
	
	/**
	 * Test method for {@link com.pace.base.project.PafXStreamElementItem#PafXStreamDataItem(java.lang.String)}.
	 */
	public void testPafXStreamDataItem() {
		
		pafAppXStreamDataItem = new PafXStreamElementItem<PafApplicationDef[]>(tempConfDir.getAbsolutePath() + File.separator + PafBaseConstants.FN_ApplicationMetaData);
		
		assertNotNull(pafAppXStreamDataItem);
		
	}
	
	/**
	 * Test method for {@link com.pace.base.project.PafXStreamElementItem#read()}.
	 */
	public void testRead() {
		
		PafApplicationDef[] pafApps = null;
		try {
			pafApps = pafAppXStreamDataItem.read();
		} catch (PaceProjectReadException e) {
			fail(e.getMessage());
		}
		
		assertNotNull(pafApps);
		assertEquals(1, pafApps.length);
		
	}

	/**
	 * Test method for {@link com.pace.base.project.PafXStreamElementItem#write(java.lang.Object)}.
	 */
	public void testWrite() {
		
		PafApplicationDef[] pafApps = null;
		try {
			pafApps = pafAppXStreamDataItem.read();
		} catch (PaceProjectReadException e) {
			fail(e.getMessage());
		}
		
		assertNotNull(pafApps);
		assertEquals(1, pafApps.length);
		
		String oldAppId = pafApps[0].getAppId();
		
		String newAppId = (new Date()).toString();
		
		pafApps[0].setAppId(newAppId);
		
		pafAppXStreamDataItem.write(pafApps);
						
		PafApplicationDef[] updatedPafApps = null;
		
		try {
			updatedPafApps = pafAppXStreamDataItem.read();
		} catch (PaceProjectReadException e) {
			fail(e.getMessage());
		}
		
		assertNotNull(updatedPafApps);
		assertEquals(1, updatedPafApps.length);
		assertEquals(newAppId, updatedPafApps[0].getAppId());
		
		pafApps[0].setAppId(oldAppId);
		pafAppXStreamDataItem.write(pafApps);
		
		try {
			updatedPafApps = pafAppXStreamDataItem.read();
		} catch (PaceProjectReadException e) {
			fail(e.getMessage());
		}
		
		assertNotNull(updatedPafApps);
		assertEquals(1, updatedPafApps.length);
		assertEquals(oldAppId, updatedPafApps[0].getAppId());
	}

}
