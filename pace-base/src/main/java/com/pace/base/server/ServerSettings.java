/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.server;

import java.io.File;

import com.pace.base.AuthMode;
import com.pace.base.PafBaseConstants;

/**
 * Server Settings
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class ServerSettings {
	
	/** @deprecated 
	 * 	TTN-2671 - removing any impact from a client mistakenly using the "DebugMode" Server Settings option
	 */
    private boolean debugMode = false;
    
    private boolean evaluationStepLogging = false;
    private boolean changedCellLogging = false;    
    private boolean clearOutlineCache = false;
    private boolean autoLoadAttributes = false;
    private boolean clearTransferDir = false;
    private boolean retainMdbTempFiles = false;
    private String calcScriptTimeout;
    private Boolean enableServerPasswordReset;
    private Boolean enableClientPasswordReset;
    private String smtpMailHost;
    private String smtpUserEmailAccount;
    private Integer minPasswordLength;
    private Integer maxPasswordLength; 
    private boolean autoConvertProject;
    private boolean enableRounding;
    private boolean clearCellNotes;
    private boolean clearAllCellNotes;
    private transient PafLDAPSettings ldapSettings = new PafLDAPSettings();
    private String authMode = AuthMode.nativeMode.toString();
    private String clientUpdateUrl;
    private String clientMinVersion;
	private int version;
    
    private String pafServerHome = ".." + File.separator + ".." + File.separator;
          
    /**
     * @return Returns the clearOutlineCache.
     */
    public boolean isClearOutlineCache() {
        return clearOutlineCache;
    }
    /**
     * @param clearOutlineCache The clearOutlineCache to set.
     */
    public void setClearOutlineCache(boolean clearOutlineCache) {
        this.clearOutlineCache = clearOutlineCache;
    }
    /** @deprecated TTN-2671
     * @return Returns the debugMode.
     */
    public boolean isDebugMode() {
        return debugMode;
    }
    /** @deprecated TTN-2671
     * @param debugMode The debugMode to set.
     */
    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }
    /**
     * @return Returns the pafServerHome.
     */
    public String getPafServerHome() {
        return pafServerHome;
    }

	public boolean isEvaluationStepLogging() {
		return evaluationStepLogging;
	}
	public void setEvaluationStepLogging(boolean evaluationStepLogging) {
		this.evaluationStepLogging = evaluationStepLogging;
	}

    /**
     * @return Returns the calcScriptTimeout.
     */
    public String getCalcScriptTimeout() {
        return calcScriptTimeout;
    }
    /**
     * @param calcScriptTimeout The calcScriptTimeout to set.
     */
    public void setCalcScriptTimeout(String calcScriptTimeout) {
        this.calcScriptTimeout = calcScriptTimeout;
    }
    /**
     * @return Returns the changedCellLogging.
     */
    public boolean isChangedCellLogging() {
        return changedCellLogging;
    }
    /**
     * @param changedCellLogging The changedCellLogging to set.
     */
    public void setChangedCellLogging(boolean changedCellLogging) {
        this.changedCellLogging = changedCellLogging;
    }
	/**
	 * @return the smtpMailHost
	 */
	public String getSmtpMailHost() {
		return smtpMailHost;
	}
	/**
	 * @param smtpMailHost the smtpMailHost to set
	 */
	public void setSmtpMailHost(String smtpMailHost) {
		this.smtpMailHost = smtpMailHost;
	}
	/**
	 * @return the smtpUserEmailAccount
	 */
	public String getSmtpUserEmailAccount() {
		return smtpUserEmailAccount;
	}
	/**
	 * @param smtpUserEmailAccount the smtpUserEmailAccount to set
	 */
	public void setSmtpUserEmailAccount(String smtpUserEmailAccount) {
		this.smtpUserEmailAccount = smtpUserEmailAccount;
	}
	/**
	 * @return Returns the enableClientPasswordReset.
	 */
	public Boolean getEnableClientPasswordReset() {
		return enableClientPasswordReset;
	}
	/**
	 * @param enableClientPasswordReset The enableClientPasswordReset to set.
	 */
	public void setEnableClientPasswordReset(Boolean enableClientPasswordReset) {
		this.enableClientPasswordReset = enableClientPasswordReset;
	}
	/**
	 * @return Returns the enableServerPasswordReset.
	 */
	public Boolean getEnableServerPasswordReset() {
		return enableServerPasswordReset;
	}
	/**
	 * @param enableServerPasswordReset The enableServerPasswordReset to set.
	 */
	public void setEnableServerPasswordReset(Boolean enableServerPasswordReset) {
		this.enableServerPasswordReset = enableServerPasswordReset;
	}
	/**
	 * @return Returns the maxPasswordLength.
	 */
	public Integer getMaxPasswordLength() {
				
		//if max pass is null or greater than the max size of db field, set to default
		if ( maxPasswordLength == null || maxPasswordLength > PafBaseConstants.DATABASE_PASSWORD_MAX_LENGTH) {
		
			return PafBaseConstants.DEFAULT_PASSWORD_MAX_LENGTH;
			
		}		
		
		return maxPasswordLength;
	}
	/**
	 * @param maxPasswordLength The maxPasswordLength to set.
	 */
	public void setMaxPasswordLength(Integer maxPasswordLength) {
		this.maxPasswordLength = maxPasswordLength;
	}
	/**
	 * @return Returns the minPasswordLength.
	 */
	public Integer getMinPasswordLength() {
		
		//if null or less than 0, set default
		if ( minPasswordLength == null || minPasswordLength < 0 ) {
			
			return PafBaseConstants.DEFAULT_PASSWORD_MIN_LENGTH;
			
		}	
		
		return minPasswordLength;
	}
	/**
	 * @param minPasswordLength The minPasswordLength to set.
	 */
	public void setMinPasswordLength(Integer minPasswordLength) {
		this.minPasswordLength = minPasswordLength;
	}
	public String getPathToServerConfDir() {
		return pafServerHome + File.separator + PafBaseConstants.DN_ConfFldr + File.separator;
	}

    /**
     * @param pafServerHome the pafServerHome to set
     */
    public void setPafServerHome(String pafServerHome) {
        this.pafServerHome = pafServerHome;
    }
	/**
	 * @return Returns the autoConvertProject.
	 */
	public boolean isAutoConvertProject() {
		return autoConvertProject;
	}
	/**
	 * @param autoConvertProject The autoConvertProject to set.
	 */
	public void setAutoConvertProject(boolean autoConvertProject) {
		this.autoConvertProject = autoConvertProject;
	}
public void setEnableRounding(boolean enableRounding) {
		this.enableRounding = enableRounding;
	}
	public boolean isEnableRounding() {
		return enableRounding;
	}
	//	/**
//	 * @return Returns the enableRounding.
//	 */
//	public boolean isEnableRounding() {
//		return enableRounding;
//	}
//	/**
//	 * @param enableRounding The enableRounding to set.
//	 */
//	public void setEnableRounding(boolean enableRounding) {
//		this.enableRounding = enableRounding;
//	}
	public boolean isAutoLoadAttributes() {
		return autoLoadAttributes;
	}
	public void setAutoLoadAttributes(boolean autoLoadAttributes) {
		this.autoLoadAttributes = autoLoadAttributes;
	}
	/**
	 * @return the clearAllCellNotes
	 */
	public boolean isClearAllCellNotes() {
		return clearAllCellNotes;
	}
	/**
	 * @return the clearCellNotes
	 */
	public boolean isClearCellNotes() {
		return clearCellNotes;
	}
	/**
	 * @param clearAllCellNotes the clearAllCellNotes to set
	 */
	public void setClearAllCellNotes(boolean clearAllCellNotes) {
		this.clearAllCellNotes = clearAllCellNotes;
	}
	/**
	 * @param clearCellNotes the clearCellNotes to set
	 */
	public void setClearCellNotes(boolean clearCellNotes) {
		this.clearCellNotes = clearCellNotes;
	}
	
	/**
	 * @return the clearTransferDir
	 */
	public boolean isClearTransferDir() {
		return clearTransferDir;
	}
	/**
	 * @param clearTransferDir the clearTransferDir to set
	 */
	public void setClearTransferDir(boolean clearTransferDir) {
		this.clearTransferDir = clearTransferDir;
	}
	/**
	 * @return the retainMdbTempFiles
	 */
	public boolean isRetainMdbTempFiles() {
		return retainMdbTempFiles;
	}
	/**
	 * @param retainMdbTempFiles the retainMdbTempFiles to set
	 */
	public void setRetainMdbTempFiles(boolean retainMdbTempFiles) {
		this.retainMdbTempFiles = retainMdbTempFiles;
	}
	public PafLDAPSettings getLdapSettings() {
		return ldapSettings;
	}
	public void setLdapSettings(PafLDAPSettings ldapSettings) {
		this.ldapSettings = ldapSettings;
	}
	
	public void setAuthMode(String authMode) {
		
		if(authMode.equalsIgnoreCase(AuthMode.mixedMode.toString()) || 
			authMode.equalsIgnoreCase(AuthMode.nativeMode.toString())){
			this.authMode = authMode;
		}
		else{
			this.authMode = AuthMode.nativeMode.toString();
		}
		
	}
	
	public String getAuthMode() {
		return authMode;
	}
	
	
	public AuthMode getAuthModeAsEnum() {
		
		AuthMode authModeEnum = PafBaseConstants.DEFAULT_AUTH_MODE;
		
		if ( authMode != null) {
			
			if(authMode.equalsIgnoreCase(AuthMode.nativeMode.toString())){
				authModeEnum = AuthMode.nativeMode;
			}else if(authMode.equalsIgnoreCase(AuthMode.mixedMode.toString())){
				authModeEnum = AuthMode.mixedMode;
			}	
		}
		
		return authModeEnum;
	}
	
	public String getClientUpdateUrl() {
		return clientUpdateUrl;
	}
	public void setClientUpdateUrl(String clientUpdateUrl) {
		this.clientUpdateUrl = clientUpdateUrl;
	}
	public String getClientMinVersion() {
		return clientMinVersion;
	}
	public void setClientMinVersion(String clientMinVersion) {
		this.clientMinVersion = clientMinVersion;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	} 	
}
