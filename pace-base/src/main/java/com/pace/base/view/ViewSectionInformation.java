/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

import java.util.Arrays;

import com.pace.base.app.AliasMapping;
import com.pace.base.app.SuppressZeroSettings;

/**
 * Object that contains basic information about a view section.
 * @author themoosman
 *
 */
public class ViewSectionInformation {
	
	private GroupingSpec[] groupingSpecs;
	private AliasMapping[] aliasMappings;
	private SuppressZeroSettings suppressZeroSetting;
	private String name;

	/**
	 * Default constructor.
	 */
	public ViewSectionInformation() {
		super();
	}
	
	/**
	 * Constructor with parms.
	 * @param name
	 * @param groupingSpecs
	 * @param aliasMapping
	 * @param suppressZero
	 */
	public ViewSectionInformation(String name, GroupingSpec[] groupingSpecs, AliasMapping[] aliasMapping, SuppressZeroSettings suppressZero) {
		super();
		this.name = name;
		this.groupingSpecs = groupingSpecs;
		this.aliasMappings = aliasMapping;
		this.suppressZeroSetting = suppressZero;
	}

	/**
	 * Gets the grouping specs for the view section
	 * @return
	 */
	public GroupingSpec[] getGroupingSpecs() {
		return groupingSpecs;
	}

	/**
	 * Sets the grouping specs for the view section
	 * @param groupingSpecs
	 */
	public void setGroupingSpecs(GroupingSpec[] groupingSpecs) {
		this.groupingSpecs = groupingSpecs;
	}
	
	/**
	 * Gets the suppress zero settings of the view section.
	 * @return the suppress zero settings.
	 */
	public SuppressZeroSettings getSuppressZeroSetting() {
		return suppressZeroSetting;
	}

	/**
	 * Sets the alias mapping settings of the view section.
	 * @param aliasMappings alias mapping settings
	 */
	public void setAliasMappings(AliasMapping[] aliasMappings) {
		this.aliasMappings = aliasMappings;
	}
	
	/**
	 * Gets the alias mapping settings for the view section.
	 * @return the alias mapping settings of the view section.
	 */
	public AliasMapping[] getAliasMappings() {
		return aliasMappings;
	}
	
	/**
	 * Sets the suppress zero settings of the view.
	 * @param suppressZeroSetting the SuppressZeroSettings
	 */
	public void setSuppressZeroSetting(SuppressZeroSettings suppressZeroSetting) {
		this.suppressZeroSetting = suppressZeroSetting;
	}

	/**
	 * Gets the name of the view section
	 * @return view section name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the view section
	 * @param name name of the view section
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ViewSectionInformation [groupingSpecs="
				+ Arrays.toString(groupingSpecs) + ", aliasMappings="
				+ Arrays.toString(aliasMappings) + ", suppressZeroSetting="
				+ suppressZeroSetting + ", name=" + name + "]";
	}
}
