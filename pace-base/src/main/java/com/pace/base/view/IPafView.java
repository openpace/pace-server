/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

import com.pace.base.ViewPrintState;
import com.pace.base.ui.PrintStyle;

public interface IPafView {

	public String getDesc();

	public void setDesc(String desc);

	public PafViewHeader[] getHeaders();

	public void setHeaders(PafViewHeader headers[]);

	public String getName();

	public void setName(String name);

	public PafViewSection[] getViewSections();

	public void setViewSections(PafViewSection viewSections[]);

	/**
	 * @return Returns the backGroundColor.
	 */
	public String getBackGroundColor();
	
	/**
	 * @param backGroundColor The backGroundColor to set.
	 */
	public void setBackGroundColor(String backGroundColor);
	
	public PrintStyle getPrintStyle();
	
	public void setPrintStyle( PrintStyle printStyle );
	
	public String getGlobalPrintStyleGUID();
	
	public void setGlobalPrintStyleGUID( String guidGlobalPrintStyle );

	public ViewPrintState getViewPrintState();

	public void setViewPrintState(ViewPrintState printState);
}