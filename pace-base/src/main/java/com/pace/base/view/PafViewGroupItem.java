/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

/**
 * Class_description_goes_here
 * 
 * @author jmilliron
 * @version x.xx
 * 
 */
public class PafViewGroupItem {

	private String name;

	private boolean viewGroup;

	public PafViewGroupItem() {

	}

	public PafViewGroupItem(String name, boolean viewGroup) {
		this.name = name;
		this.viewGroup = viewGroup;
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Returns the viewGroup.
	 */
	public boolean isViewGroup() {
		return viewGroup;
	}

	/**
	 * @param viewGroup
	 *            The viewGroup to set.
	 */
	public void setViewGroup(boolean viewGroup) {
		this.viewGroup = viewGroup;
	}

	/**
	 * @return Returns the viewGroup.
	 */
	public boolean isView() {
		return !viewGroup;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (viewGroup ? 1231 : 1237);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PafViewGroupItem other = (PafViewGroupItem) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (viewGroup != other.viewGroup)
			return false;
		return true;
	}
	
	
}
