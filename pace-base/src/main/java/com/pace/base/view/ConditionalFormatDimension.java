/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.pace.base.format.ConditionalFormat;
import com.pace.base.format.MemberFormat;

/**
 * 
 * This class contains the dimension name and both level and generation formatting maps.
 * 
 * @author JavaJ
 * 
 */
public class ConditionalFormatDimension extends Dimension implements Cloneable {

	//dimension name
	private boolean primaryDimension;

	//map to hold level formats
	private Map<String, MemberFormat> memberFormats;

	/**
	 * Creates empty level format and gen format maps
	 * 
	 */
	public ConditionalFormatDimension() {

		super();
		memberFormats = new HashMap<String, MemberFormat>();
		primaryDimension = false;
	}
	
	public ConditionalFormatDimension(String name, boolean primaryDimension){
		this.name = name;
		this.primaryDimension = primaryDimension;
	}
	
	public ConditionalFormatDimension(String name, Map<String, MemberFormat> memberFormats, boolean primaryDimension){
		this.name = name;
		this.primaryDimension = primaryDimension;
		this.memberFormats = memberFormats;
	}
	
	public boolean isPrimaryDimension(){
		return this.primaryDimension;
	}
	
	public void setPrimaryDimension(boolean primaryDimension){
		this.primaryDimension = primaryDimension;
	}

	/**
	 * 
	 * @param levelSearch
	 * @return level format for found level
	 */
	public MemberFormat getMemberFormat(String memberSearch) {

		if (memberFormats != null && memberFormats.size() > 0 && memberSearch != null) {
			return memberFormats.get(memberSearch);
		}

		return null;

	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param levelSearch
	 * @return
	 */
	public String getMemberFormatName(String memberSearch) {

		String formatName = null;

		MemberFormat memberFormat = getMemberFormat(memberSearch);

		if (memberFormat != null) {
			formatName = memberFormat.getFormatName();
		}

		return formatName;

	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param levelFormat
	 */
	public void addMemberFormat(MemberFormat memberFormat) {

		if (memberFormat != null && memberFormat.getMemberName() != null) {

			getMemberFormats().put(memberFormat.getMemberName(), memberFormat);

		}

	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param levelFormat
	 */
	public void removeMemberFormat(MemberFormat memberFormat) {

		if (memberFormat != null && memberFormat.getMemberName() != null) {
			getMemberFormats().remove(memberFormat.getMemberName());
		}

	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @return
	 */
	public Set<String> getMemberFormatKeys() {
		return getMemberFormats().keySet();
	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @param levelFormats
	 */
	public void setMemberFormats(Map<String, MemberFormat> memberFormats) {
		this.memberFormats = memberFormats;
	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @return
	 */
	public int getNumberOfMemberFormats() {

		return getMemberFormats().size();
	}

	/**
	 * 
	 *	Method_description_goes_here
	 *
	 * @return
	 */
	public int getNumberOfHierFormats() {
		return getNumberOfGenFormats() + getNumberOfLevelFormats() + getNumberOfMemberFormats();
	}

	/**
	 * @return the levelFormats
	 */
	public Map<String, MemberFormat> getMemberFormats() {

		if (memberFormats == null) {
			memberFormats = new HashMap<String, MemberFormat>();
		}

		return memberFormats;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		if ( memberFormats != null && memberFormats.size() > 0 ) {
			
			for ( String genKey : memberFormats.keySet() ) {
				
				result = prime * result + genKey.hashCode();
				
				MemberFormat gf = memberFormats.get(genKey);
				
				result = prime * result
				+ ((gf == null) ? 0 : gf.hashCode());
				
			}
			
		}
		
		result = prime * result + ((this.primaryDimension == true) ? 0 : 1);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConditionalFormatDimension other = (ConditionalFormatDimension) obj;
		if (memberFormats == null) {
			if (other.memberFormats != null)
				return false;
		} else if (other.memberFormats == null ) {
			return false;
		} else {
			
			for ( String genKey : memberFormats.keySet() ) {
				
				if ( ! other.memberFormats.containsKey(genKey)) {
					
					return false;
					
				} else {
					
					if ( ! memberFormats.get(genKey).equals(other.memberFormats.get(genKey))) {
						
						return false;
						
					}
					
				}
				
			}
			
		}

		if(this.primaryDimension != other.primaryDimension)
			return false;
		return true;
	}

	
	@Override
	public ConditionalFormatDimension clone(){
		
		ConditionalFormatDimension temp = null;
		
		try {
			
			temp = (ConditionalFormatDimension) super.clone();
			if(this.memberFormats != null){
				for (String member : memberFormats.keySet()) {
					temp.memberFormats.put(member, memberFormats.get(member).clone());
				}
			}
			
		} catch (Exception e) {
			//can't happen if implements cloneable
			//logger.warn(e.getMessage());
		}
		
		return temp;
	}
}
