/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.base.view;

/**
 * @author jmilliron
 * 
 */
public class PafBorder implements Cloneable {

	public static final int BORDER_NONE = 0;

	public static final int BORDER_LEFT = 2;

	public static final int BORDER_RIGHT = 4;

	public static final int BORDER_TOP = 8;

	public static final int BORDER_BOTTOM = 16;

	public static final int BORDER_ALL = 32;

	private int border;

	public PafBorder() {
	}
	
	public PafBorder(int border) {
		setBorder(border);
	}
	
	public void setBorder(int border) {
		this.border = border;
	}
		
		
	public int getBorder() {
		return this.border;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	private boolean isStyle(int style) {
		
		return ((border & style) > 0 )? true : false;
	}
	
	public boolean isLeft() {
		return isStyle(BORDER_LEFT);
	}
	
	public boolean isRight() {
		return isStyle(BORDER_RIGHT);
	}
	
	public boolean isTop() {
		return isStyle(BORDER_TOP);
	}
	
	public boolean isBottom() {
		return isStyle(BORDER_BOTTOM);
	}
	
	public boolean isAll() {
		return isStyle(BORDER_ALL);
	}
		
}
