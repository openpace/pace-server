/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

import java.util.Arrays;

/**
 * Object that contains basic information about a view.
 * @author themoosman
 *
 */
public class ViewInformation {
	
	private String name;
	private PafUserSelection[] userSelections;
	private ViewSectionInformation[] viewSectionInfo;
	
	/**
	 * Default constructor.
	 */
	public ViewInformation() {
		super();
	}
	
	/**
	 * Constructor with params.
	 * @param name
	 * @param userSelections
	 * @param viewSectionInfo
	 */
	public ViewInformation(String name,
			PafUserSelection[] userSelections,
			ViewSectionInformation[] viewSectionInfo) {
		super();
		this.name = name;
		this.userSelections = userSelections;
		this.viewSectionInfo = viewSectionInfo;
	}

	/**
	 * Get the name of the view.
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the view.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the view's user selections.
	 * @return
	 */
	public PafUserSelection[] getUserSelections() {
		return userSelections;
	}

	/**
	 * Sets the views user selections
	 * @param userSelections
	 */
	public void setUserSelections(PafUserSelection[] userSelections) {
		this.userSelections = userSelections;
	}

	/**
	 * Sets the viewSectionInformation object for the view's view sections.
	 * @return
	 */
	public ViewSectionInformation[] getViewSectionInfo() {
		return viewSectionInfo;
	}


	/**
	 * Gets the viewSectionInformation object for the view's view sections.
	 * @param viewSectionInfo
	 */
	public void setViewSectionInfo(ViewSectionInformation[] viewSectionInfo) {
		this.viewSectionInfo = viewSectionInfo;
	}

	@Override
	public String toString() {
		return "ViewInformation [name=" + name + ", userSelections="
				+ Arrays.toString(userSelections) + ", viewSectionInfo="
				+ Arrays.toString(viewSectionInfo) + "]";
	}
}
