/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

import com.pace.base.mdb.PafDimTree.LevelGenType;

/**
 * Specifies how a dimension should be Grouped (Expand/Collpase) on a view section.
 * @author themoosman
 *
 */
public class GroupingSpec implements Cloneable {
	
	private Integer axis;
	private LevelGenType type;
	private Integer level;
	private String dimension;
	
	/**
	 * Default constructor.
	 */
	public GroupingSpec(){
		this(null, null, null, null);
	}
	
	/**
	 * Overloaded Constructor
	 * @param dimensionName
	 * @param level
	 * @param type
	 * @param dimAxis
	 */
	public GroupingSpec(String dimensionName, Integer level, LevelGenType type, Integer dimAxis){
		super();
		this.dimension = dimensionName;
		this.level = level;
		this.type = type;
		this.axis = dimAxis;
	}
	
	/**
	 * 
	 * @return the axis of the grouping spec
	 */
	public Integer getAxis() {
		return axis;
	}
	
	/**
	 * 
	 * @param axis sets the axis
	 */
	public void setAxis(Integer axis) {
		this.axis = axis;
	}
	
	/**
	 * 
	 * @return Returns the type (LEVEL, GEN, or null) of the group
	 */
	public LevelGenType getType() {
		return type;
	}
	
	/**
	 * 
	 * @param type sets the type of the group
	 */
	public void setType(LevelGenType type) {
		this.type = type;
	}
	
	/**
	 * 
	 * @return gets the level of the group
	 */
	public Integer getLevel() {
		return level;
	}
	
	/**
	 * 
	 * @param level sets the level of the group
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}
	
	/**
	 * 
	 * @return gets the name of the dimension
	 */
	public String getDimension() {
		return dimension;
	}
	
	/**
	 * 
	 * @param dimension sets the name of the dimension
	 */
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}
	
	@Override
	public String toString() {
		return "GroupingSpec [axis=" + axis + ", type=" + type + ", level=" + level + ", dimension=" + dimension + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((axis == null) ? 0 : axis.hashCode());
		result = prime * result
				+ ((dimension == null) ? 0 : dimension.hashCode());
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupingSpec other = (GroupingSpec) obj;
		if (axis == null) {
			if (other.axis != null)
				return false;
		} else if (!axis.equals(other.axis))
			return false;
		if (dimension == null) {
			if (other.dimension != null)
				return false;
		} else if (!dimension.equals(other.dimension))
			return false;
		if (level == null) {
			if (other.level != null)
				return false;
		} else if (!level.equals(other.level))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
}
