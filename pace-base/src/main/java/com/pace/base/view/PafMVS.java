/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

import com.pace.base.app.UnitOfWork;
import com.pace.base.mdb.PafDataCache;
import com.pace.base.mdb.PafDataSliceParms;

/**
 * "Materialized View Section" - Container to hold attributes of unique view and view section combinations
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafMVS {

	private PafView view = null;
	private PafViewSection viewSection = null;
	private PafDataCache dataCache = null;
	private PafDataSliceParms dataSliceParms = null;
	
	/**
	 * @param view Paf View object
	 * @param viewSection A Paf View Section belonging to the view
	 */
	public PafMVS(PafView view, PafViewSection viewSection) {
		this.view = view;
		this.viewSection = viewSection;
	}
	
	/**
	 *	Returns key of PafMVS instance
	 *
	 * @return key of PafMVS instance
	 */
	public String getKey() {
		return generateKey(view, viewSection);
	}
	
	/**
	 *	Generate a key for this PafMVS Instance
	 *
	 * @param view Paf View object
	 * @param viewSection A Paf View Section belonging to the view
	 * @return Key for this PafMVS Instance
	 */
	public static String generateKey(PafView view, PafViewSection viewSection) {
		return view.getName() + "." + viewSection.getName();		
	}

	/**
	 * @return Returns the dataSliceParms.
	 */
	public PafDataSliceParms getDataSliceParms() {
		return dataSliceParms;
	}

	/**
	 * @param dataSliceParms The dataSliceParms to set.
	 */
	public void setDataSliceParms(PafDataSliceParms dataSliceParms) {
		this.dataSliceParms = dataSliceParms;
	}

	/**
	 * @return the dataCache
	 */
	public PafDataCache getDataCache() {
		return dataCache;
	}
	/**
	 * @param dataCache the dataCache to set
	 */
	public void setDataCache(PafDataCache dataCache) {
		this.dataCache = dataCache;
	}

	/**
	 * @return Returns the view.
	 */
	public PafView getView() {
		return view;
	}

	/**
	 * @param view The view to set.
	 */
	public void setView(PafView view) {
		this.view = view;
	}

	/**
	 * @return Returns the viewSection.
	 */
	public PafViewSection getViewSection() {
		return viewSection;
	}

	/**
	 * @param viewSection The viewSection to set.
	 */
	public void setViewSection(PafViewSection viewSection) {
		this.viewSection = viewSection;
	}


}

