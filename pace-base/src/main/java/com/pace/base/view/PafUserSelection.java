/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

import com.pace.base.app.PafDimSpec;
import com.pace.base.data.ExpOperation;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jwatkins
 *
 */
public class PafUserSelection implements Comparable, Cloneable {
    private String id;
    private String dimension;
    private PafAxis pafAxis;
    private boolean multiples;
    private String displayString;
    private String promptString;
    private String[] values;
    private PafDimSpec specification;
    private PafDimSpec defaultMembers;
    private boolean parentLast;
    private boolean levelRightClick;
    private boolean generationRightClick;
    
    // synatx for a user selection
    // ID, DImension, Accepts Multiples, Label/Prompt String
    //@USER_SEL(M1, Measures, False, "Measure Selection")
    
    public PafUserSelection(ExpOperation expOp) {
        this.id = expOp.getParms()[0];
        this.dimension = expOp.getParms()[1];
        this.multiples = (expOp.getParms()[2].equalsIgnoreCase("true")? true: false);
        this.promptString = expOp.getParms()[3];
    
    }
    //required by soap layer
    public PafUserSelection() {}
    
    /**
     * @return Returns the values.
     */
    public String[] getValues() {
        return values;
    }
    /**
     * @param values The values to set.
     */
    public void setValues(String[] values) {
        this.values = values;
    }
    /**
     * @return Returns the dimension.
     */
    public String getDimension() {
        return dimension;
    }
    /**
     * @param dimension The dimension to set.
     */
    public void setDimension(String dimension) {
        this.dimension = dimension;
    }
    /**
     * @return Returns the id.
     */
    public String getId() {
        return id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return Returns the multiples.
     */
    public boolean isMultiples() {
        return multiples;
    }
    /**
     * @param multiples The multiples to set.
     */
    public void setMultiples(boolean multiples) {
        this.multiples = multiples;
    }
    public String getDisplayString() {
		return displayString;
	}
	public void setDisplayString(String displayString) {
		this.displayString = displayString;
	}
	/**
     * @return Returns the promptString.
     */
    public String getPromptString() {
        return promptString;
    }
    /**
     * @param promptString The promptString to set.
     */
    public void setPromptString(String promptString) {
        this.promptString = promptString;
    }
    
    /**
     * @return Returns the PafDimSpec 
     */
	public PafDimSpec getSpecification() {
		return specification;
	}
	
	/**
	 * @param specification PafDimSpec to set
	 */
	public void setSpecification(PafDimSpec specification) {
		this.specification = specification;
	}
	
	/**
	 * 
	 * @return Returns the parentLast
	 */
	public boolean isParentLast() {
		return parentLast;
	}
	/**
	 * 
	 * @param parentLast the parentFirst to set.
	 */
	public void setParentLast(boolean parentLast) {
		this.parentLast = parentLast;
	}
	/**
	 * 
	 * @return the levelRightCLick
	 */
	public boolean isLevelRightClick() {
		return levelRightClick;
	}
	/**
	 * 
	 * @param levelRightClick to set
	 */
	public void setLevelRightClick(boolean levelRightClick) {
		this.levelRightClick = levelRightClick;
	}
	/**
	 * 
	 * @return the generationRightClick
	 */
	public boolean isGenerationRightClick() {
		return generationRightClick;
	}
	/**
	 * 
	 * @param generationRightClick to set
	 */
	public void setGenerationRightClick(boolean generationRightClick) {
		this.generationRightClick = generationRightClick;
	}
    
	public String getValuesAsString() {
		if (values==null) return "";
		
		StringBuffer sb = new StringBuffer();
		for (String s : values) {
			sb.append(s);
			sb.append(", ");
		}
		
		sb.trimToSize();
		sb.deleteCharAt(sb.length()-2);
		return sb.toString();
	}
	
	public int compareTo(Object o) {
						
		return this.id.compareTo(((PafUserSelection) o).getId());
		
	}
	
	public String toString() {
		
		return "ID: " + this.getId() + ", Dimension: " + this.getDimension() + ", Multiple: " + this.isMultiples();
		
	}
	/**
	 * @return Returns the pafAxis.
	 */
	public PafAxis getPafAxis() {
		return pafAxis;
	}
	/**
	 * @param pafAxis The pafAxis to set.
	 */
	public void setPafAxis(PafAxis pafAxis) {
		this.pafAxis = pafAxis;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public PafUserSelection clone() throws CloneNotSupportedException {
		
		PafUserSelection clone = (PafUserSelection) super.clone();
		
		if ( getPafAxis() != null ) {
			
			clone.setPafAxis(new PafAxis(this.getPafAxis().getValue()));
		}
		
		if(getSpecification() != null){
			clone.setSpecification(this.getSpecification().clone());
		}
		
		if(getDefaultMembers() != null){
			clone.setDefaultMembers(this.getDefaultMembers().clone());
		}
		
		return clone;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		
		PafUserSelection userSelection = (PafUserSelection) obj;
		
		if ( this.id != null && userSelection.getId() != null ) {
			
			if ( this.id.equals(userSelection.getId())) {
				return true;
			}
		}
		
		return false;
		
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		
		return this.id.hashCode();
	}
	
	/**
	 * 
	 * @return the defaultMembers PafDimSpec
	 */
	public PafDimSpec getDefaultMembers() {
		return defaultMembers;
	}
	
	/**
	 * Sets the defaultMember PafDimSpec
	 * @param defaultMembers 
	 */
	public void setDefaultMembers(PafDimSpec defaultMembers) {
		this.defaultMembers = defaultMembers;
	}
	


}
