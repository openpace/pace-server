/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class LockedCell implements Comparable {

	private int rowIndex = 0;
	private int colIndex = 0;
	
	public LockedCell() {} 

	public LockedCell(int rowIndex, int colIndex) {
		this.rowIndex = rowIndex;
		this.colIndex = colIndex;
	}
	
	public void setColIndex(int colIndex) {
		this.colIndex = colIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}
	
	public int getColIndex() {
		return colIndex;
	}

	public int getRowIndex() {
		return rowIndex;
	}
	
	public String toString() {
		return "Row Index: " + rowIndex + ", Column Index: " + colIndex;
	}
		
	public int compareTo(Object o) {
		
		int rc = 0;
		
		LockedCell lc = (LockedCell) o;
		
		if ( this.rowIndex < lc.rowIndex ) {
			
			rc = -1; 
			
		} else if ( this.rowIndex == lc.rowIndex ) {
			
			if ( this.colIndex < lc.colIndex) {
				rc = -1;
			} else if ( this.colIndex == lc.colIndex) {
				rc = 0;
			} else {
				rc = 1;
			}		
		} else  {
			
			rc = 1;
		}
		
		return rc;
	}

	
}
