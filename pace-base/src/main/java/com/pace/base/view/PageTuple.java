/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.view;


/**
 * Page tuple
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PageTuple implements Cloneable {
	
    private String axis;
    private String member;

    public PageTuple() {}
    
    public PageTuple(String axis, String member) {
        this.axis = axis;
        this.member = member;
    }
    public String getAxis() {
        return axis;
    }
    public void setAxis(String axis) {
        this.axis = axis;
    }
    public String getMember() {
        return member;
    }
    public void setMember(String member) {
        this.member = member;
    }
    
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public PageTuple clone() throws CloneNotSupportedException {
		
		PageTuple pageTuple = (PageTuple) super.clone();
		return pageTuple;
		
	}

    public String toString() {
    	
    	return "PageTuple=> Axis: " + axis + ", Member: " + member;
    	
    }

}
