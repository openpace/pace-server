/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base;

import java.util.ArrayList;
import java.util.List;

import com.pace.base.state.IPafClientState;


/**
 * General exception class for holding PAF specific error information
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PafException extends Exception {


	private static final long serialVersionUID = 1L;
	IPafClientState clientState;
	private ArrayList<String> messageDetail = new ArrayList<String>();



	private PafErrSeverity pes = PafErrSeverity.Info;

	public PafException() {
		super();
	}

	/**
	 * @param message
	 * @param pes 
	 */
	public PafException(String message, PafErrSeverity pes) {
		super(message);
		this.pes = pes;
	}

    public PafException(String message, PafErrSeverity pes, Throwable cause) {
        super(message, cause);
        this.pes = pes;
    }
    
	/**
	 * @param message
	 * @param cause
	 * @param severity 
	 */
	public PafException(String message, Throwable cause, @SuppressWarnings("unused")
	PafErrSeverity severity) {
		super(message, cause);
	}
    
    public PafException(@SuppressWarnings({ "unused", "unused" })
	String message, @SuppressWarnings("unused")
	Throwable cause, IPafClientState clientState) {
        this.clientState = clientState;
    }

	/**
	 * @param cause
	 */
	public PafException(Throwable cause) {
		super(cause);
	}
    
    public PafErrSeverity getSeverity() {
        return pes;
    }
    
    public void setSeverity(PafErrSeverity pes) {
        this.pes = pes;
    }
    
    public PafSoapException getPafSoapException() {
    	return new PafSoapException(this);
    }

    /**
     * @return Returns the clientState.
     */
    public IPafClientState getClientState() {
        return clientState;
    }

    public void setClientState(IPafClientState clientState) {
        this.clientState = clientState;
        
    }

    public void addMessageDetail(String string) {
        messageDetail.add(string);
    }
    
    public List<String> getMessageDetails() {
        return messageDetail;
    }

}
