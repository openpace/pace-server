/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import com.pace.base.server.ServerSettings;

public class ServerSettingsMigrationActionV1 extends AbstractServerConfFileMigrationAction<ServerSettings> {

	public ServerSettingsMigrationActionV1(String pathFileName) {
		super(pathFileName, "Server Settings", 0, 1);

	}

	@Override
	public void upgradeStep() throws Exception {
		//update the version number.
		confFileObject.setVersion(this.newVersion);
	}

	@Override
	public MigrationActionStatus getStatus() {
		if(confFileObject == null){
			return MigrationActionStatus.Completed;
		}
		
		if(confFileObject.getVersion() == this.versionToUpgrade){
			return MigrationActionStatus.NotStarted;
		}
		
		return MigrationActionStatus.Completed;
	}
	
}
