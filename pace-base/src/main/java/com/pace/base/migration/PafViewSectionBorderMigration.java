/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.view.PafBorder;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.ViewTuple;

/**
 * This class is used to convert the ViewTuple border attribute to dataBorder.
 * 
 * @author jmilliron
 * @version 1.00
 * 
 */
public class PafViewSectionBorderMigration extends MigrationAction {

	private static Logger logger = Logger
			.getLogger(PafViewSectionBorderMigration.class);
	private List<PafViewSection> pafViewSectionList;
	/**
	 * Constructor - creates instance of PafViewSectionBorderMigration
	 * 
	 * @param serverHomeDir
	 *            Home directory of the server.
	 */
	public PafViewSectionBorderMigration(XMLPaceProject xmlPaceProject) {
		
		this.xmlPaceProject = xmlPaceProject;

	}

	/*
	 * (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#getActionName()
	 */
	public String getActionName() {

		return "Convert View Sections border to dataBorder";
	}

	/*
	 * (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#getStatus()
	 */	
	public MigrationActionStatus getStatus() {

		// if server home directory location is specified
		if (xmlPaceProject != null) {

			boolean currentUpgradeProject = this.xmlPaceProject.isUpgradeProject();
			
			//turn off current upgrad project flag, but save current value for later reset
			this.xmlPaceProject.setUpgradeProject(false);
			
			new PafViewSectionsSeparationMigrationAction(xmlPaceProject).run();
			
			pafViewSectionList = xmlPaceProject.getViewSections();

			this.xmlPaceProject.setUpgradeProject(currentUpgradeProject);
			
			// if the file does not exist
			if (pafViewSectionList != null) {

				//loop over view sections
				for (PafViewSection pafViewSection : pafViewSectionList) {
					
					List<ViewTuple> viewTupleList = new ArrayList<ViewTuple>();
					
					if ( pafViewSection.getRowTuples() != null ) {
						
						viewTupleList.addAll(Arrays.asList(pafViewSection.getRowTuples()));
						
					}
					
					if ( pafViewSection.getColTuples() != null ) {
						
						viewTupleList.addAll(Arrays.asList(pafViewSection.getColTuples()));
						
					}

					//loop over row tuples.  if older border exists and new one doesn't, return as not started
					for (ViewTuple viewTuple : viewTupleList) {

						PafBorder oldBorder = viewTuple.getBorder();
						PafBorder dataBorder = viewTuple.getDataBorder();

						if (oldBorder != null && dataBorder == null) {
							return MigrationActionStatus.NotStarted;
						}

					}

													
				}
				
				//return completed if didn't already return not started
				return MigrationActionStatus.Completed;				

			}
		} 

		return MigrationActionStatus.NotStarted;
	}

	/*
	 * (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#run()
	 */
	public void run() {

		//if not already completed
		if (getStatus().equals(MigrationActionStatus.NotStarted)) {

			boolean currentUpgradeProject = this.xmlPaceProject.isUpgradeProject();
			
			//turn off current upgrad project flag, but save current value for later reset
			this.xmlPaceProject.setUpgradeProject(false);
			
//			List<PafViewSection> pafViewSectionList = xmlPaceProject.getViewSections();

			this.xmlPaceProject.setUpgradeProject(currentUpgradeProject);
			
			// if the file does not exist
			if (pafViewSectionList != null) {

				//loop view sections
				for (PafViewSection pafViewSection : pafViewSectionList) {

					//get row tuples from view section										
					ViewTuple[] rowTuples = pafViewSection.getRowTuples();

					if (rowTuples != null) {

						//for each row tuple, set data border as old border then set old border to null
						for (ViewTuple viewTuple : rowTuples) {

							PafBorder oldBorder = viewTuple.getBorder();
							
							//if old border is not null, set to data border then null out
							if ( oldBorder != null ) {							
								viewTuple.setDataBorder(oldBorder);
								viewTuple.setBorder(null);
							}						
							
						}

					}

					//get col tuples from view section
					ViewTuple[] colTuples = pafViewSection.getColTuples();

					if (colTuples != null) {

						//for each row tuple, set data border as old border then set old border to null
						for (ViewTuple viewTuple : colTuples) {						
						
							PafBorder oldBorder = viewTuple.getBorder();
							
							//if old border is not null, set to data border then null out							
							if ( oldBorder != null ) {
								viewTuple.setDataBorder(oldBorder);
								viewTuple.setBorder(null);
							}
							
						}

					}

				}

				//turn off current upgrad project flag, but save current value for later reset
				this.xmlPaceProject.setUpgradeProject(true);
				
				//export updated view sections
				xmlPaceProject.setViewSections(pafViewSectionList);

				this.xmlPaceProject.setUpgradeProject(currentUpgradeProject);
				
				try {
					xmlPaceProject.save(ProjectElementId.ViewSections);
				} catch (ProjectSaveException e) {
					
					logger.error(e.getMessage());
					
					throw new RuntimeException(e.getMessage());
				}
				
			}
			
			
		}
	}

}
