/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.app.AppSettings;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;

/* Added new class to migrate new properties added to paf_apps.xml */
public class AppSettingsMigrationAction extends MigrationAction{

	private static Logger logger = Logger.getLogger(AppSettingsMigrationAction.class);
	
	public AppSettingsMigrationAction(XMLPaceProject xmlPaceProject) {
		
		this.xmlPaceProject = xmlPaceProject;
	}

/* (non-Javadoc)
 * @see com.pace.base.migration.IMigrationAction#getActionName()
 */
	public String getActionName() {
		return "Update PafApplication Props with default AppSettings";
		
	}
	
	/* Get Status of the filtered properties */
	public MigrationActionStatus getStatus() {
		MigrationActionStatus status = MigrationActionStatus.Completed;
		
		// if server home directory location is specified
		if (xmlPaceProject != null ) {
			PafApplicationDef pafApp = getPafApp();
			if ( pafApp != null ) {
				if ( pafApp.getAppSettings() != null) {
					AppSettings appSettings = pafApp.getAppSettings();
					//TTN-2524
					//If migration action needs to be performed, then return NotStarted
					//If not continue to the next migration action.
					if(appSettings.getFilteredMemberAliasPrefix() == null && appSettings.getFilteredMemberAliasSuffix() == null){
						return MigrationActionStatus.NotStarted;
					} 
				}
			}
		}		
		return status;
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#run()
	 */
	public void run() {
		
		// if not started
		logger.debug("Starting the filtered properties migration action");
		if ( getStatus().equals(MigrationActionStatus.NotStarted)) {
			PafApplicationDef pafApp = getPafApp();
							
			// if null, remove
			if ( pafApp == null ) {
				return;
			}
			
			// create new app settings if null
			if ( pafApp.getAppSettings() == null ) {
				pafApp.setAppSettings(new AppSettings());						
			}
					
			// TTN-2524 Add Filtered suffix and prefix properties for upgrade if they don't already exist
			if ( pafApp.getAppSettings().getFilteredMemberAliasPrefix() == null){
				logger.info("Adding the Filtered prefix property for upgrade");
				pafApp.getAppSettings().setFilteredMemberAliasPrefix(PafBaseConstants.DEFAULT_SYNTHETIC_MEMBER_ALIAS_PREFIX);
			}
			
			if ( pafApp.getAppSettings().getFilteredMemberAliasSuffix() == null){
				logger.info("Adding the Filtered suffix property for upgrade");
				pafApp.getAppSettings().setFilteredMemberAliasSuffix(PafBaseConstants.DEFAULT_SYNTHETIC_MEMBER_ALIAS_SUFFIX);
			}
			
			
			xmlPaceProject.setApplicationDefinitions(new ArrayList<PafApplicationDef>(Arrays.asList(pafApp)));
			
			try {
				xmlPaceProject.save(new HashSet<ProjectElementId>(Arrays.asList(ProjectElementId.ApplicationDef)));
			} catch (ProjectSaveException e) {
				logger.error(e.getMessage());
				//TODO: Test this
				throw new RuntimeException(e.getMessage());
			}
		}
	}
	/**
	 * 
	 * Reads in the paf application def
	 *
	 * @return the paf application def
	 */
	private PafApplicationDef getPafApp() {
		PafApplicationDef pafApp = null;
		if ( xmlPaceProject != null ) {
			boolean currentUpgradeStatus = xmlPaceProject.isUpgradeProject();
			xmlPaceProject.setUpgradeProject(false);
			List<PafApplicationDef> pafAppList = xmlPaceProject.getApplicationDefinitions();
			if ( pafAppList.size() == 1 ) {
				pafApp = pafAppList.get(0);
			}
			xmlPaceProject.setUpgradeProject(currentUpgradeStatus);
		}
		return pafApp;
	}	
	
	
}