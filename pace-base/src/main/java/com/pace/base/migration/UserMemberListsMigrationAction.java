/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import com.pace.base.PafErrHandler;
import com.pace.base.PafErrSeverity;
import com.pace.base.data.PafMemberList;
import com.pace.base.data.UserMemberLists;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;

public class UserMemberListsMigrationAction extends MigrationAction {
	UserMemberLists uml = null;

	
	public UserMemberListsMigrationAction(XMLPaceProject xmlPaceProject) {
		this.xmlPaceProject = xmlPaceProject;
		if( xmlPaceProject != null) {
			boolean currentUpgradeProject = this.xmlPaceProject.isUpgradeProject();
			this.xmlPaceProject.setUpgradeProject(false);
			try {
				this.uml = xmlPaceProject.getUserMemberLists();				
			} catch(Exception e ){
				// bury for now
				// System.out.println("File Not Found.");
			}
			this.xmlPaceProject.setUpgradeProject(currentUpgradeProject);
		}
	}	
	
	
	@Override
	public String getActionName() {
		return "Create empty user memberlist file if none found";
	}

	@Override
	public MigrationActionStatus getStatus() {
		if(  this.uml == null || this.uml.getMemberLists() == null || this.uml.getMemberLists().size() == 0 ) {
			return MigrationActionStatus.NotStarted;
		}
		return MigrationActionStatus.Completed;
	}

	@Override
	public void run() {
		if( getStatus().equals(MigrationActionStatus.NotStarted) ) {
			uml = new UserMemberLists();
			PafMemberList ml = new PafMemberList();
			ml.setDimName("Measures");
			ml.setMemberNames(new String[] {"@DESC(SLS_DLR)", "SLS_AUR"});
			uml.addMemberList("MyMeasures", ml);
			
			xmlPaceProject.setUserMemberLists(uml);
			try {
				xmlPaceProject.save(ProjectElementId.MemberLists);
			} catch (ProjectSaveException e) {
				PafErrHandler.handleException(e, PafErrSeverity.Error);
			}
		}
	}

}
