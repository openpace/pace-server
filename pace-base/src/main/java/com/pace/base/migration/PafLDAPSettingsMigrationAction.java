/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafErrHandler;
import com.pace.base.PafErrSeverity;
import com.pace.base.server.PafLDAPSettings;
import com.pace.base.utility.PafXStream;

public class PafLDAPSettingsMigrationAction extends MigrationAction {

	private static final Logger logger = Logger.getLogger(PafLDAPSettingsMigrationAction.class);
	private PafLDAPSettings ldapSettings;
	private String pathFileName;

	public PafLDAPSettingsMigrationAction(String pathFileName) {
		this.versionToUpgrade = 0;
		this.newVersion = 1;
		this.pathFileName = pathFileName;
		try {
			this.ldapSettings = (PafLDAPSettings) PafXStream.importObjectFromXml(this.pathFileName);
		} catch (PafConfigFileNotFoundException e) {
			PafErrHandler.handleException(e, PafErrSeverity.Error);
		}
	}
	
	@Override
	public String getActionName() {
		return "Convert PafLDAPSettings from version 0 to version 1";
	}

	@Override
	public MigrationActionStatus getStatus() {
		if(ldapSettings == null){
			return MigrationActionStatus.Completed;
		}
	
		if(ldapSettings.getVersion() == this.versionToUpgrade){
			return MigrationActionStatus.NotStarted;
		}
	
		return MigrationActionStatus.Completed;
	}

	@Override
	public void run() {
		if ( getStatus().equals(MigrationActionStatus.NotStarted)) {
			
			File xmlFile = new File(pathFileName);
			File backupFile = new File(pathFileName + ".bak");
			
			try {
				logger.info(String.format("Backing up file: %s prior to upgrade", pathFileName));
				backupFile.delete();
				FileUtils.copyFile(xmlFile, backupFile);
			} catch (IOException e) {
				logger.info("Cannot backup file, migration will proceed without a backup.");
			}
			
			try {
			
				logger.info("Running migration step on file: " + pathFileName);
				logger.info(String.format("Performing upgrade (version: %s to version: %s) on PafLDAPSettings with key: %s", versionToUpgrade, newVersion, ldapSettings.getLdapProviderName()));
				
				//update the version number.
				ldapSettings.setVersion(this.newVersion);
				
				String password = ldapSettings.getEncryptedSecurityCredentials();
				if(password != null && password != ""){
					logger.info("Encrypting password for LDAP provider: " + ldapSettings.getLdapProviderName());
					ldapSettings.setSecurityCredentials(password);
				} else{
					logger.info("Password for LDAP provider is null or empty, no encryption will be performed.");
				}
				
				PafXStream.exportObjectToXml(ldapSettings, this.pathFileName);
				
				logger.info(String.format("Migration step on file: %s complete", pathFileName));
				
			} catch (Exception ex){
				try {
					logger.error(ex);
					logger.info("Error occured duing migration, backup will be restored.");
					xmlFile.delete();
					FileUtils.copyFile(backupFile, xmlFile);
				} catch (IOException e) {
					logger.info("Cannot restore backup file.");
				}
				
			}finally{
				logger.info(String.format("Removing backup file: %s ", pathFileName + ".bak"));
				backupFile.delete();
			}
		}
	}
}
