/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.util.List;
import java.util.Properties;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafKeystorePasswordReadException;
import com.pace.base.db.RdbProps;
import com.pace.base.security.AESKeystoreEncryption;

public class RdbPropsMigrationActionV1 extends AbstractServerConfFileMigrationAction<List<RdbProps>> {

	private static String PASSWORD_KEY = PafBaseConstants.RDB_PASSWORD_KEY;
	private AESKeystoreEncryption encryptor;

	public RdbPropsMigrationActionV1(String pathFileName) throws PafKeystorePasswordReadException {
		super(pathFileName, "RDP Connection Props", 0, 1);
		this.encryptor = new AESKeystoreEncryption();
	}

	@Override
	public MigrationActionStatus getStatus() {
		if(confFileObject == null || confFileObject.size() == 0){
			return MigrationActionStatus.Completed;
		}
		
		for(RdbProps rdb : confFileObject) {
			if(rdb.getVersion() == this.versionToUpgrade){
				return MigrationActionStatus.NotStarted;
			}
		}
		
		return MigrationActionStatus.Completed;
	}

	@Override
	public void upgradeStep() throws Exception {
		for(RdbProps rdb:  confFileObject) {
			Properties props = rdb.getHibernateProperties();
			
			for(String key : props.stringPropertyNames()){
				
				if(key.equalsIgnoreCase(PASSWORD_KEY)){
					
					String value = props.getProperty(key);
					
					logger.info(String.format("Performing upgrade (version: %s to version: %s) on RdbProps with key: %s", versionToUpgrade, newVersion, rdb.getName()));

					//update the version number.
					rdb.setVersion(this.newVersion);
					//update the password in the collection
					props.setProperty(key, encryptor.encryptToHexString(value));
				
				}
			}
		}
	}
}
