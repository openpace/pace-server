/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafErrHandler;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.utility.PafXStream;
import com.pace.base.view.PafView;

/**
 * This class is used to split the paf_views.xml file into a new directory and
 * individual xml files.
 * 
 * @author jmilliron
 * @version 1.00
 * 
 */
public class PafViewsSeparationMigrationAction extends MigrationAction {

	private static Logger logger = Logger
			.getLogger(PafViewsSeparationMigrationAction.class);

	private String confDirectory;
	
	
	/**
	 * Constructor - creates instance of PafViewsSeperationMigrationAction
	 * 
	 * @param serverHomeDir
	 *            Home directory of the server.
	 */
	public PafViewsSeparationMigrationAction(XMLPaceProject xmlPaceProject) {

		// set server home directory location
		this.xmlPaceProject = xmlPaceProject;
		
		if ( this.xmlPaceProject != null ) {
			
			confDirectory = this.xmlPaceProject.getProjectInput();
			
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pace.base.migration.interfaces.IMigrationAction#getActionName()
	 */
	public String getActionName() {
		return "Separate " + PafBaseConstants.FN_ViewsMetaData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pace.base.migration.interfaces.IMigrationAction#getStatus()
	 */
	public MigrationActionStatus getStatus() {

		// set defaul status to not started
		MigrationActionStatus status = MigrationActionStatus.NotStarted;

		// if server home directory location is specified
		if (confDirectory != null) {

			// get file reference to paf_views.xml file
			File pafViewsXmlFile = new File(confDirectory + File.separator
					+ PafBaseConstants.FN_ViewsMetaData);

			// if the file does not exist
			if ( ! pafViewsXmlFile.exists() ) {

				/*
				 * //create a file reference to the backup file File
				 * pafViewsXmlBackupFile = new File(confDirectory +
				 * File.separator + PafBaseConstants.FN_ViewsMetaData +
				 * PafBaseConstants.BAK_EXT);
				 */

				// create a file to the new views folder
				File newPafViewDir = new File(confDirectory + File.separator
						+ PafBaseConstants.DN_ViewsFldr);

				// if the new paf views directory already exist and is a
				// directory and if the backup file exist, set as completed
				if (newPafViewDir.exists() && newPafViewDir.isDirectory()) {

					status = MigrationActionStatus.Completed;

				}

			}
		}

		return status;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pace.base.migration.interfaces.IMigrationAction#run()
	 */
	public void run() {

		if (getStatus().equals(MigrationActionStatus.NotStarted) && confDirectory != null ) {

			PafView[] pafViews = null;

			// get ref to paf_views.xml file
			String pafViewsXml = confDirectory + File.separator
					+ PafBaseConstants.FN_ViewsMetaData;

			// try to import the paf views.xml file
			try {
				pafViews = (PafView[]) PafXStream
						.importObjectFromXml(pafViewsXml);

			} catch (PafConfigFileNotFoundException ex) {
				PafErrHandler.handleException(ex);
			}

			// if views exist
			if (pafViews != null) {

				List<PafView> pafViewList = Arrays.asList(pafViews);
				
				xmlPaceProject.setViews(pafViewList);
				
				try {
					
					xmlPaceProject.save(ProjectElementId.Views);
					
				} catch (ProjectSaveException e) {
					
					logger.error(e.getMessage());
					
					throw new RuntimeException(e.getMessage());
				}
				

				// get ref to old paf_views.xml
				File pafViewsXmlFile = new File(pafViewsXml);

				// rename file with a .bak on end.
				if (pafViewsXmlFile.exists()) {

					File newPafViewXmlFile = new File(pafViewsXmlFile
							+ PafBaseConstants.BAK_EXT);

					logger.info("Renaming file " + pafViewsXmlFile + " to "
							+ newPafViewXmlFile);

					boolean fileRenamed = pafViewsXmlFile.renameTo(newPafViewXmlFile);
					
					if ( ! fileRenamed ) {
						
						logger.error("File " + pafViewsXmlFile.toString() + " could not be renamed.");
						
					}

				}

			}
		}

	}

}
