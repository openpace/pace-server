/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;

import com.pace.base.PafBaseConstants;
import com.pace.base.project.XMLPaceProject;

/**
 * Migration Action
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public abstract class MigrationAction implements IMigrationAction {
	
	protected XMLPaceProject xmlPaceProject = null;
	protected int versionToUpgrade;
	protected int newVersion;
	
	/**
	 * @return the paceProject
	 */
	public XMLPaceProject getXMLPaceProject() {
		return xmlPaceProject;
	}

	/**
	 * @param paceProject the paceProject to set
	 */
	public void setXMLPaceProject(XMLPaceProject xmlPaceProject) {
		this.xmlPaceProject = xmlPaceProject;
	}

	/**
	 * 
	 * @return
	 */
	public int getVersionToUpgrade() {
		return versionToUpgrade;
	}

	/**
	 * 
	 * @param versionToUprade
	 */
	public void setVersionToUpgrade(int versionToUprade) {
		this.versionToUpgrade = versionToUprade;
	}

	/**
	 * 
	 * @return
	 */
	public int getNewVersion() {
		return newVersion;
	}
	/**
	 * 
	 * 	@param newVersion
 	*/
	public void setNewVersion(int newVersion) {
		this.newVersion = newVersion;
	}

	
	/**
	 * Gets the input file (the xml file) from the conf directory.
	 * Pace Project must not be null and initialized.
	 * @param xmlFileName
	 * @return
	 */
	protected File getInputFile(String xmlFileName) {
		
		if(xmlPaceProject == null || xmlPaceProject.getProjectInput()== null){
			return null;
		}
		
		String confDirectory = xmlPaceProject.getProjectInput();		
		
		//if conf dir is null, return null
		if ( confDirectory == null ) {
			return null;
		}
		
		// get file reference to paf_rules.xml file
		return new File(confDirectory + File.separator + xmlFileName);
	}
	
	/**
	 * Tests a file to see if it is a valid  xml file, and a non .svn file.
	 * @param childFile
	 * @return
	 */
	protected boolean isValidProjectFileOrDir(File childFile) {

		if ( childFile != null) {
			
			if ( childFile.isFile() && childFile.toString().endsWith(PafBaseConstants.XML_EXT) ) {
				return true;
			} else if ( childFile.isDirectory() && ! childFile.toString().endsWith(PafBaseConstants.SVN_HIDDEN_DIR_NAME) ) {
				return true;
			}
		}
		
		return false;
	}

	
	
}
