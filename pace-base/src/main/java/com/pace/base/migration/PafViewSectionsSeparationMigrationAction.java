/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafErrHandler;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.utility.PafXStream;
import com.pace.base.view.PafViewSection;

/**
 * Class_description_goes_here
 * 
 * @author jmilliron
 * @version x.xx
 * 
 */
public class PafViewSectionsSeparationMigrationAction extends MigrationAction {

	private static Logger logger = Logger
			.getLogger(PafViewSectionsSeparationMigrationAction.class);

	private String confDirectory;
	

	/**
	 * Constructor - creates instance of
	 * PafViewSectionsSeperationMigrationAction
	 * 
	 * @param serverHomeDir
	 *            Home directory of the server.
	 */
	public PafViewSectionsSeparationMigrationAction(XMLPaceProject xmlPaceProject) {
		this.xmlPaceProject = xmlPaceProject;
		
		if ( this.xmlPaceProject != null ) {
			
			confDirectory = this.xmlPaceProject.getProjectInput();
			
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pace.base.migration.interfaces.IMigrationAction#getActionName()
	 */
	public String getActionName() {
		return "Separate " + PafBaseConstants.FN_ViewSectionsMetaData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pace.base.migration.interfaces.IMigrationAction#getStatus()
	 */
	public MigrationActionStatus getStatus() {

		// set defaul status to not started
		MigrationActionStatus status = MigrationActionStatus.NotStarted;

		// if server home directory location is specified
		if (confDirectory != null) {

			// get file reference to paf_view_sections.xml file
			File pafViewSectionsXmlFile = new File(confDirectory
					+ File.separator + PafBaseConstants.FN_ViewSectionsMetaData);

			// if the file does not exist
			if (!pafViewSectionsXmlFile.exists()) {

				/*
				 * //create a file reference to the backup file File
				 * pafViewSectionsXmlBackupFile = new File(confDirectory +
				 * File.separator + PafBaseConstants.FN_ViewSectionsMetaData +
				 * PafBaseConstants.BAK_EXT);
				 */

				// create a file to the new views folder
				File newPafViewSectionDir = new File(confDirectory
						+ File.separator + PafBaseConstants.DN_ViewSectionsFldr);

				// if the new paf views directory already exist and is a
				// directory and if the backup file exist, set as completed
				if (newPafViewSectionDir.exists()
						&& newPafViewSectionDir.isDirectory()) {

					status = MigrationActionStatus.Completed;

				}

			}
		}
		return status;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pace.base.migration.interfaces.IMigrationAction#run()
	 */
	public void run() {

		if ( getStatus().equals(MigrationActionStatus.NotStarted) && confDirectory != null ) {

			PafViewSection[] pafViewSections = null;

			// get ref to paf_views.xml file
			String pafViewSectionsXml = confDirectory + File.separator
					+ PafBaseConstants.FN_ViewSectionsMetaData;

			// try to import the paf view_sections.xml file
			try {
				pafViewSections = (PafViewSection[]) PafXStream
						.importObjectFromXml(pafViewSectionsXml);
			} catch (PafConfigFileNotFoundException ex) {
				PafErrHandler.handleException(ex);
			}

			if (pafViewSections != null) {

				List<PafViewSection> viewSectionList = Arrays.asList(pafViewSections);
				
				xmlPaceProject.setViewSections(viewSectionList);
				
				try {
					
					xmlPaceProject.save(ProjectElementId.ViewSections);
					
				} catch (ProjectSaveException e) {
					
					logger.error(e.getMessage());
					
					throw new RuntimeException(e.getMessage());
				}
				

				// get ref to old paf_views.xml
				File pafViewSectionsXmlFile = new File(pafViewSectionsXml);

				// rename file with a .bak on end.
				if (pafViewSectionsXmlFile.exists()) {

					File newPafViewSectionXmlFile = new File(
							pafViewSectionsXmlFile + PafBaseConstants.BAK_EXT);

					logger.info("Renaming file " + pafViewSectionsXmlFile
							+ " to " + newPafViewSectionXmlFile);

					boolean fileRenamed = pafViewSectionsXmlFile.renameTo(newPafViewSectionXmlFile);

					if ( ! fileRenamed ) {
						
						logger.error("File " + pafViewSectionsXmlFile.toString() + " could not be renamed.");
						
					}
				}

			}
		}
	}

}
