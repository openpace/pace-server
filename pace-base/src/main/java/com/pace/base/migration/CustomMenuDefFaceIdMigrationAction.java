/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.comm.CustomMenuDef;
import com.pace.base.project.PaceProjectReadException;
import com.pace.base.project.PafXStreamElementItem;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.utility.FileUtils;

/**
 * Updates the CustomMenuDef FaceId
 * version.
 **/
public class CustomMenuDefFaceIdMigrationAction extends MigrationAction {
	
	private static final Logger logger = Logger.getLogger(CustomMenuDefFaceIdMigrationAction.class);
	private Map<Integer, String> faceIdMap = new HashMap<Integer, String>();
	
	public CustomMenuDefFaceIdMigrationAction(XMLPaceProject xmlPaceProject) {
		this.xmlPaceProject = xmlPaceProject;
		this.faceIdMap.put(5, "MacroDefault");
		this.faceIdMap.put(8, "AdpViewGridPane");
		this.faceIdMap.put(132, "LeftArrow2");
		this.faceIdMap.put(133, "RightArrow2");
		this.faceIdMap.put(134, "UpArrow2");
		this.faceIdMap.put(135, "DownArrow2");
		this.faceIdMap.put(758, "FollowUpComposeMenu");
	}

	@Override
	public String getActionName() {
		return "Convert CustomMenuDef FaceId to msoImageId.";
	}

	@SuppressWarnings("deprecation")
	@Override
	public MigrationActionStatus getStatus()  {
		
		// if server home directory location is specified
		if (xmlPaceProject != null) {		
			
			List<CustomMenuDef> customMenus = null;
			try {
				customMenus = readCustomMenus();
			} catch (PaceProjectReadException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//if map not null
			if ( customMenus != null ) {				
				
				//loop through styles, see if any fill color are not null
				for (CustomMenuDef customMenu : customMenus) {
					if(customMenu.getCustomActionDefs() != null){
						if(customMenu.getFaceID() != null){
							return MigrationActionStatus.NotStarted;
						}
					}
				}
				//get this far, it's completed
				return MigrationActionStatus.Completed;
			}	
		} 

		return MigrationActionStatus.NotStarted;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		
		if (getStatus().equals(MigrationActionStatus.Completed)) {
			return;
		}
		
		
		//get input file
		File inputFile = getInputFile(PafBaseConstants.FN_CustomMenus);
					
		if ( inputFile != null && inputFile.isFile() && inputFile.canRead() ) {
			
			//try to backup orig file
			try {
				FileUtils.copy(inputFile, new File(inputFile.toString() + PafBaseConstants.BAK_EXT));
			} catch (IOException e1) {
				logger.error("Couldn't backup file " + inputFile.toString() + ". Error: " + e1.getMessage());
			} 
					
		}
		
		List<CustomMenuDef> customMenus = null;
		try {
			customMenus = readCustomMenus();
		} catch (PaceProjectReadException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//create a new Dictionary with the new style PafStyle
		List<CustomMenuDef> customMenusToSet = new ArrayList<CustomMenuDef>();
		
		//iterate thru the old style map and convert the PafStyle.
		for (CustomMenuDef customMenu : customMenus) {
			
			if(customMenu.getFaceID() != null){
				Integer id = customMenu.getFaceID();
				String value = this.faceIdMap.get(id);
				if(value != null){
					customMenu.setFaceID(null);
					customMenu.setMsoImageId(value);
				}
			}
			customMenusToSet.add(customMenu);
		}
		
		//export updated global styles
		xmlPaceProject.setCustomMenus(customMenusToSet);
		try {
			xmlPaceProject.save(ProjectElementId.CustomMenus);
		} catch (ProjectSaveException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	private List<CustomMenuDef> readCustomMenus() throws PaceProjectReadException {
		
		PafXStreamElementItem<CustomMenuDef[]> pafXStreamElementItem = new PafXStreamElementItem<CustomMenuDef[]>(xmlPaceProject.getProjectInput() + PafBaseConstants.FN_CustomMenus);
		
		CustomMenuDef[] objectAr = pafXStreamElementItem.read();
			
		return Arrays.asList(objectAr);
		
	}
}
