/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.util.List;

import com.pace.base.PafErrHandler;
import com.pace.base.PafErrSeverity;
import com.pace.base.app.PafDimSpec;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.view.PafUserSelection;

public class UserSelectionMigrationAction extends MigrationAction {

	List<PafUserSelection> usrSels = null;
	
	public UserSelectionMigrationAction(XMLPaceProject xmlPaceProject) {
		this.xmlPaceProject = xmlPaceProject;
		if( xmlPaceProject != null) {
			boolean currentUpgradeProject = this.xmlPaceProject.isUpgradeProject();
			this.xmlPaceProject.setUpgradeProject(false);
			try {
				this.usrSels = xmlPaceProject.getUserSelections();			
			} catch(Exception e ){
				// bury for now
				// System.out.println("File Not Found.");
			}
			this.xmlPaceProject.setUpgradeProject(currentUpgradeProject);
		}
	}	
	
	
	@Override
	public String getActionName() {
		return "Create empty user memberlist file if none found";
	}

	@Override
	public MigrationActionStatus getStatus() {
		MigrationActionStatus status = MigrationActionStatus.Completed;
		if(  this.usrSels != null && this.usrSels.size() > 0 ) {
			for( PafUserSelection us : this.usrSels ) {
				if( us.getSpecification() == null && ! us.getDimension().equals("Version")) {
					status = MigrationActionStatus.NotStarted;
					break;
				}
			}
		}
		return status;
	}

	@Override
	public void run() {
		if( getStatus().equals(MigrationActionStatus.NotStarted) ) {
			if(  this.usrSels != null && this.usrSels.size() > 0 ) {
				for( PafUserSelection us : this.usrSels ) {
					if( us.getSpecification() == null && ! us.getDimension().equals("Version")) {
						PafDimSpec spec = new PafDimSpec();
						spec.setDimension(us.getDimension());
						spec.setExpressionList(new String[]{"@IDESC(@UOW_ROOT)"});
						us.setSpecification(spec);
					}
				}
				
				xmlPaceProject.setUserSelections(this.usrSels);
			
				try {
					xmlPaceProject.save(ProjectElementId.UserSelections);
				} catch (ProjectSaveException e) {
					PafErrHandler.handleException(e, PafErrSeverity.Error);
				}
			}
		}
	}

}
