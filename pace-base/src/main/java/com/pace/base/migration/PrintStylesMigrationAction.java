/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.util.Map;

import com.pace.base.project.ProjectElementId;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.ui.DefaultPrintSettings;
import com.pace.base.ui.PrintStyle;
import com.pace.base.utility.GUIDUtil;

public class PrintStylesMigrationAction extends MigrationAction {
	private Map<String, PrintStyle> printStyleMap= null;
	
	public PrintStylesMigrationAction(XMLPaceProject xmlPaceProject) {
		// TODO Auto-generated constructor stub
		this.xmlPaceProject = xmlPaceProject;
		if( xmlPaceProject != null) {
			boolean currentUpgradeProject = this.xmlPaceProject.isUpgradeProject();
			this.xmlPaceProject.setUpgradeProject(false);
			try {
				this.printStyleMap = this.xmlPaceProject.getPrintStyles();
			} catch(Exception e ){
				System.out.println("File Not Found.");
			}
			this.xmlPaceProject.setUpgradeProject(currentUpgradeProject);
		}
	}

	@Override
	public String getActionName() {
		// TODO Auto-generated method stub
		return "Create a default print style if there is no any global print style.";
	}

	@Override
	public MigrationActionStatus getStatus() {
		// TODO Auto-generated method stub
		if( this.xmlPaceProject != null ) {
			if( printStyleMap == null || printStyleMap.size() == 0) {
				return MigrationActionStatus.NotStarted;
			}
			else if( printStyleMap.size() > 0  ) {
				return MigrationActionStatus.Completed;
			}
		}
		return MigrationActionStatus.NotStarted;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if( getStatus() == MigrationActionStatus.NotStarted ) {
			PrintStyle defaultStyle = DefaultPrintSettings.getInstance().getDefaultPrintSettings().clone();
			defaultStyle.setGUID(GUIDUtil.getGUID());
			defaultStyle.setName("Print Style #1");
			defaultStyle.setDefaultStyle(true);
			printStyleMap.put(defaultStyle.getGUID(), defaultStyle);
			xmlPaceProject.setPrintStyles(printStyleMap);
			try {
				xmlPaceProject.save(ProjectElementId.PrintStyles);
			} catch (ProjectSaveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
