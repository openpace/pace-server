/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.project.XMLPaceProject;
import com.pace.base.utility.FileFilterUtility;
import com.pace.base.utility.FileUtils;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author ihuang
 *
 */
public class PafVersionMigrationAction extends MigrationAction {

	private static Logger logger = Logger
	.getLogger(PafSecurityMigrationAction.class);
	
	private static Set<String> filterSet = new HashSet<String>();
	
	private String confDirectory;
	
	static {
		
		//filters to remove from file
		filterSet.add("<isSelectable>false</isSelectable>");
		filterSet.add("<isSelectable>true</isSelectable>");
	}
	
	/**
	 * Constructor - creates instance of PafSecurityMigrationAction
	 * 
	 * @param serverHomeDir
	 *            Home directory of the server.
	 */
	public PafVersionMigrationAction(XMLPaceProject xmlPaceProject) {

		this.xmlPaceProject = xmlPaceProject;
		
		if ( this.xmlPaceProject != null ) {
			
			confDirectory = this.xmlPaceProject.getProjectInput();
			
		}

	}

	
	/* (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#getActionName()
	 */
	public String getActionName() {
		
		return "Remove isSelectable tag from version file";
	}

	/* (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#getStatus()
	 */
	public MigrationActionStatus getStatus() {

		MigrationActionStatus status = MigrationActionStatus.NotStarted;
		
//		 if server home directory location is specified
		if (confDirectory != null) {
			
			File pafVersionXmlFile = getInputFile(PafBaseConstants.FN_VersionMetaData);
			
			if ( pafVersionXmlFile != null ) {
			
				try {
					if ( ! FileFilterUtility.doFiltersExistsInFile(pafVersionXmlFile, filterSet) ) {
						status = MigrationActionStatus.Completed;
					}
				} catch (FileNotFoundException e) {
					logger.debug("File " + pafVersionXmlFile.toString() + " couldn't be found.");
					//status = MigrationActionStatus.Failed;
				}
				
			}
		}
		
		return status;
		
	}

	/* (non-Javadoc)
	 * @see com.pace.base.migration.IMigrationAction#run()
	 */
	public void run() {
		
		//if not started
		if ( getStatus().equals(MigrationActionStatus.NotStarted)) {
		
			//get input file
			File pafVersionXmlFile = getInputFile(PafBaseConstants.FN_VersionMetaData);
			
			//if is file not null and exists
			if ( pafVersionXmlFile != null  && pafVersionXmlFile.exists() ) {
				
				//try to backup orig file
				try {
					FileUtils.copy(pafVersionXmlFile, new File(pafVersionXmlFile.toString() + PafBaseConstants.BAK_EXT));
				} catch (IOException e1) {
					logger.error("Couldn't backup file " + pafVersionXmlFile.toString() + ". Error: " + e1.getMessage());
				}
				
				try {
					FileFilterUtility.removeFiltersFromFile(pafVersionXmlFile, filterSet);
				} catch (FileNotFoundException e) {
				}
				
			}
			
		}		

	}

}
