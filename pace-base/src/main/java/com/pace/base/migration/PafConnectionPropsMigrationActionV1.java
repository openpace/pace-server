/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.migration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafKeystorePasswordReadException;
import com.pace.base.mdb.IPafConnectionProps;
import com.pace.base.mdb.PafConnectionProps;
import com.pace.base.security.AESKeystoreEncryption;

public class PafConnectionPropsMigrationActionV1 extends AbstractServerConfFileMigrationAction<List<IPafConnectionProps>> {

	private static final String PASSWORD = PafBaseConstants.CONN_STRING_PSWD_KEY;
	private AESKeystoreEncryption encryptor;
	
	public PafConnectionPropsMigrationActionV1(String pathFileName) {
		super(pathFileName, "MDB Connection Properties", 0, 1);
		try {
			this.encryptor = new AESKeystoreEncryption();
		} catch (PafKeystorePasswordReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void upgradeStep() throws Exception {
		
		for(IPafConnectionProps prop : confFileObject){
			if(prop.getVersion() == this.versionToUpgrade){
				
				logger.info(String.format("Performing upgrade (version: %s to version: %s) on PafConnectionProps with key: %s", versionToUpgrade, newVersion, prop.getName()));
				
				List<String> newProps = new ArrayList<String>(); 
				
				prop.setVersion(this.newVersion);

				List<String> keyValues = PafConnectionProps.splitConnStr(prop.getConnectionString());
				for(String keyValue : keyValues) {
					
					String[] kvPair = PafConnectionProps.splitConnStrKeyValue(keyValue,  false);
					
					String key = kvPair[0];
					String value = kvPair[1];
					
					if(key.equalsIgnoreCase(PASSWORD)){
						if(value != null && value != ""){
							value = encryptor.encryptToHexString(value);
							logger.debug("Encrypting password, new encrypted password: " + value);
						} else {
							logger.info("Password for connection string is null or empty, no encryption will be performed.");
						}
					} 
					
					newProps.add(key + "=" + value);
				}
				prop.setEncryptedConnectionString(StringUtils.arrayToDelimitedString(newProps.toArray(new String[newProps.size()]), ";"));
			}
		}
	}

	@Override
	public MigrationActionStatus getStatus() {
		if(confFileObject == null || confFileObject.size() == 0){
			return MigrationActionStatus.Completed;
		}
		
		for(IPafConnectionProps prop : confFileObject){
			if(prop.getVersion() == this.versionToUpgrade){
				return MigrationActionStatus.NotStarted;
			}
		}
		return MigrationActionStatus.Completed;
	}

}
