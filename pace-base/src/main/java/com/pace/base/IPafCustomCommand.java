/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base;

import java.util.Properties;

import com.pace.base.server.ServerSettings;
import com.pace.base.state.IPafClientState;

/**
 * All custom commands implemented to extend the application framework
 * must implement this interface.
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public interface IPafCustomCommand {
    /**
     *	Execute a custom command
     *
 	 * @param clientProps Client state properties
	 * @param clientState Client state object
	 * @param serverSettings Server settings object
	 * @param transferDirPath Transfer directory path
	 * 
	 * @return CustomdCommandResult 
	 * @throws PafException
     */
    public CustomCommandResult execute(Properties clientProps, IPafClientState clientState, ServerSettings serverSettings, String transferDirPath) throws PafException;

}
