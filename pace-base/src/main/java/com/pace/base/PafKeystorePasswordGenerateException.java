/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * Thrown to indicate that an issue was encountered in generating the
 * Keystore password.
 */
package com.pace.base;


/**
 * @author Alan Farkas
 *
 */
public class PafKeystorePasswordGenerateException extends PafException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4723071125028917838L;

	/**
     * 
     *
     */
    public PafKeystorePasswordGenerateException() {
		super();
	}

    /**
     * 
     * @param message
     * @param pes
     */
	public PafKeystorePasswordGenerateException(String message, PafErrSeverity pes) {
		super(message, pes);
	}

	/**
	 * 
	 * @param message
	 * @param pes
	 * @param cause
	 */
	public PafKeystorePasswordGenerateException(String message, PafErrSeverity pes, Throwable cause) {
		super(message, pes, cause);
	}

}
