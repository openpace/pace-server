/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

import java.util.List;

/**
 * This is a version of the PafDimSpec that can be inserted into the 'pojo' object database. The object
 * database does not support the static logger variable that is part of the PafDimSpec.
 *
 * @author KMoos
 *
 */
public class PafDimSpec2 implements Cloneable {

    private String dimension;
    private List<String> expressionList;
    
    public PafDimSpec2() { 
    }
    
    
    public PafDimSpec2(String dimension, List<String> expressionList) {
		super();
		this.dimension = dimension;
		this.expressionList = expressionList;
	}

	
    /**
     * @return Returns the dimension.
     */
    public String getDimension() {
        return dimension;
    }
    /**
     * @param dimension The dimension to set.
     */
    public void setDimension(String dimension) {
        this.dimension = dimension;
    }
    /**
     * @return Returns the expressionList.
     */
    public List<String> getExpressionList() {
        return expressionList;
    }
    /**
     * @param expressionList The expressionList to set.
     */
    public void setExpressionList(List<String> expressionList) {
        this.expressionList = expressionList;
    }

    public PafDimSpec toPafDimSpec(){
    	PafDimSpec temp = new PafDimSpec();
    	temp.setDimension(this.dimension);
    	temp.setExpressionList(this.expressionList.toArray(new String[this.expressionList.size()]));
    	return temp;
    }
    
	/* (non-Javadoc)
	 * @see java.lang.PafDimSpec#clone()
	 */
	@Override
	public PafDimSpec2 clone() {
		
		try {
			
			return (PafDimSpec2) super.clone();
			
		} catch (CloneNotSupportedException e) {
			//can't happen if implements cloneable
		}
		
		return null;
	}

	
}
