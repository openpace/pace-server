/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

import com.pace.base.app.PafUserNamesSecurityGroup;

public class PafSecurityDomainUserNames implements Comparable<PafSecurityDomainUserNames>{
	//domain name for the security group
	private String domainName;
	private PafUserNamesSecurityGroup[] userNamesSecurityGroup;
	
	public int compareTo(PafSecurityDomainUserNames domainUserNames) {
		
		int outcome = 0;
		
		if ( domainUserNames.getDomainName() != null && this.getDomainName() != null) {
			outcome = this.getDomainName().compareTo(domainUserNames.getDomainName());			
		}
		
		return outcome;
	}
	
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	
	public String getDomainName() {
		return domainName;
	}
	
	public void setUserNamesSecurityGroup(PafUserNamesSecurityGroup[] userNamesSecurityGroup) {
		this.userNamesSecurityGroup = userNamesSecurityGroup;
	}
	
	public PafUserNamesSecurityGroup[] getUserNamesSecurityGroup() {
		return userNamesSecurityGroup;
	}
	
}





