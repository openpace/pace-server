/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

import java.util.Arrays;

/**
 * Holds a action definition for a custom menu definition.
 * Consists of the name of the implementing class and the paramters
 * to be passed to it at execution time. Primarily a wrapper to allow
 * the embedding of a multiple action sequence to a custom menu command.
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class CustomActionDef {
	
    String actionClassName;
    String[] actionNamedParameters;
    /**
     * @return Returns the actionClassName.
     */
    public String getActionClassName() {
        return actionClassName;
    }
    /**
     * @param actionClassName The actionClassName to set.
     */
    public void setActionClassName(String actionClassName) {
        this.actionClassName = actionClassName;
    }
	public String[] getActionNamedParameters() {
		return actionNamedParameters;
	}
	public void setActionNamedParameters(String[] actionNamedParameters) {
		this.actionNamedParameters = actionNamedParameters;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((actionClassName == null) ? 0 : actionClassName.hashCode());
		result = prime * result + Arrays.hashCode(actionNamedParameters);
		return result;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomActionDef other = (CustomActionDef) obj;
		if (actionClassName == null) {
			if (other.actionClassName != null)
				return false;
		} else if (!actionClassName.equals(other.actionClassName))
			return false;
		if (!Arrays.equals(actionNamedParameters, other.actionNamedParameters))
			return false;
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomActionDef [actionClassName=" + actionClassName
				+ ", actionNamedParameters="
				+ Arrays.toString(actionNamedParameters) + "]";
	}

	
	
}
