/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

import java.util.Arrays;

/**
 * Dynamic member definition
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class DynamicMemberDef {

	//dimension name
	private String dimension;
	
	//array of member specs {@PLAN_VERSION, @PLAN_VERSION_vs_LY, etc}
	private String[] memberSpecs;
	
	/**
	 * Default Constructor.  Creates a dynamic member definition.
	 *
	 */
	public DynamicMemberDef() {
		
	}

	/**
	 * 	Overloaded Constructor. Creates a dynamic member definition from
	 *  the dimension and memberSpecs;
	 * 
	 * @param dimension
	 * @param memberSpecs
	 */
	public DynamicMemberDef(String dimension, String[] memberSpecs) {
		this.dimension = dimension;
		this.memberSpecs = memberSpecs;
	}

	/**
	 * @return the dimension
	 */
	public String getDimension() {
		return dimension;
	}

	/**
	 * @param dimension the dimension to set
	 */
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	/**
	 * @return the memberSpecs
	 */
	public String[] getMemberSpecs() {
		return memberSpecs;
	}

	/**
	 * @param memberSpecs the memberSpecs to set
	 */
	public void setMemberSpecs(String[] memberSpecs) {
		this.memberSpecs = memberSpecs;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		if ( dimension != null && memberSpecs != null) {
			
			StringBuffer strBuff = new StringBuffer(dimension + ": ");
			
			int memberSpecCnt = 0;
			
			for (String memberSpec : memberSpecs ) {
								
				strBuff.append(memberSpec);
				
				if ( ++memberSpecCnt != memberSpecs.length) {
					
					strBuff.append(", ");
					
				}
				
			}
			
			return strBuff.toString();
			
		}
		
		return super.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dimension == null) ? 0 : dimension.hashCode());
		result = prime * result + Arrays.hashCode(memberSpecs);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DynamicMemberDef other = (DynamicMemberDef) obj;
		if (dimension == null) {
			if (other.dimension != null)
				return false;
		} else if (!dimension.equals(other.dimension))
			return false;
		if (!Arrays.equals(memberSpecs, other.memberSpecs))
			return false;
		return true;
	}
	
	
	
}