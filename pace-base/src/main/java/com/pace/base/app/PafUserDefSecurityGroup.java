/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author Jason
 *
 */
public class PafUserDefSecurityGroup extends PafSecurityGroup {

	//array of user definitions
	private PafUserDef[] pafUserDefs;
	
	public PafUserDefSecurityGroup() {
		
	}

	/**
	 * @return the pafUserDefs
	 */
	public PafUserDef[] getPafUserDefs() {
		return pafUserDefs;
	}

	/**
	 * @param pafUserDefs the pafUserDefs to set
	 */
	public void setPafUserDefs(PafUserDef[] pafUserDefs) {
		this.pafUserDefs = pafUserDefs;
	}
	
	
	
}
