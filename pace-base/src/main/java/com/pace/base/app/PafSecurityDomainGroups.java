/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

public class PafSecurityDomainGroups {
	//domain name for the security group
	private String domain;
	private PafSecurityGroup[] securityGroups;
	

	public void setSecurityGroups(PafSecurityGroup[] securityGroups) {
		this.securityGroups = securityGroups;
	}

	public PafSecurityGroup[] getSecurityGroups() {
		return securityGroups;
	}

	
	public void setDomain(String domain) {
		this.domain = domain;
	}

	
	public String getDomain() {
		return domain;
	}
	
	public String[] getgroups(){
		String[] groups = new String[securityGroups.length];
		int i = 0;
		
		for (PafSecurityGroup securityGroup : securityGroups){
			groups[i++] = securityGroup.getGroupName();
		}
		
		return groups;
	}


}
