/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PafWorkSpec implements Cloneable {
	
	private transient final static Logger logger = Logger.getLogger(PafWorkSpec.class);
	
    private String name;
    
    private PafDimSpec[] dimSpec;

    /**
     * @return Returns the dimSpec.
     */
    public PafDimSpec[] getDimSpec() {
        return dimSpec;
    }

    /**
     * @param dimSpec The dimSpec to set.
     */
    public void setDimSpec(PafDimSpec[] dimSpec) {
        this.dimSpec = dimSpec;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public PafWorkSpec clone() {

		PafWorkSpec clonedPafWorkSpec = null;
		
		try {
			
			clonedPafWorkSpec = (PafWorkSpec) super.clone();
			
			if ( dimSpec != null ) {
				
				List<PafDimSpec> pafDimSpecList = new ArrayList<PafDimSpec>();
				
				for (PafDimSpec pafDimSpec : dimSpec) {
					
					if ( pafDimSpec != null ) {
						
						PafDimSpec clonedPafDimSpec = (PafDimSpec) pafDimSpec.clone();
						
						pafDimSpecList.add(clonedPafDimSpec);
						
					}
					
				}
				
				if ( pafDimSpecList.size() > 0 ) {
					
					clonedPafWorkSpec.setDimSpec(pafDimSpecList.toArray(new PafDimSpec[0]));
					
				} else {
					
					clonedPafWorkSpec.setDimSpec(null);
					
				}
				
			}
			
		} catch (CloneNotSupportedException e) {
			
			logger.warn(e.getMessage());
			
		}
		
		return clonedPafWorkSpec;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(dimSpec);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PafWorkSpec other = (PafWorkSpec) obj;
		if (!Arrays.equals(dimSpec, other.dimSpec))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
