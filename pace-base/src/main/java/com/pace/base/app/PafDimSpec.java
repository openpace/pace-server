/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;

import java.util.Arrays;

import org.apache.log4j.Logger;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PafDimSpec implements Cloneable {

	private transient final static Logger logger = Logger.getLogger(PafDimSpec.class);
	
	//TTN 1738 - removing isSelectable property
//	@Deprecated
//	private transient boolean isSelectable;
	
    private String dimension;
    
    private String[] expressionList;
    
    public PafDimSpec() { 
    }
    
    public PafDimSpec(String dimension, String[] expressionList) {
    	this.dimension = dimension;
    	this.expressionList = expressionList;
    }
    
    /**
     * @return Returns the dimension.
     */
    public String getDimension() {
        return dimension;
    }
    /**
     * @param dimension The dimension to set.
     */
    public void setDimension(String dimension) {
        this.dimension = dimension;
    }
    /**
     * @return Returns the expressionList.
     */
    public String[] getExpressionList() {
        return expressionList;
    }
    /**
     * @param expressionList The expressionList to set.
     */
    public void setExpressionList(String[] expressionList) {
        this.expressionList = expressionList;
    }

	//TTN 1738 - removing isSelectable property
//	/**
//	 * @return Returns the isSelectable.
//	 */
//	public boolean isSelectable() {
//		return isSelectable;
//	}
//	/**
//	 * @param isSelectable The isSelectable to set.
//	 */
//	public void setSelectable(boolean isSelectable) {
//		this.isSelectable = isSelectable;
//	}

	/* (non-Javadoc)
	 * @see java.lang.PafDimSpec#clone()
	 */
	@Override
	public PafDimSpec clone() {
		
		try {
			
			return (PafDimSpec) super.clone();
			
		} catch (CloneNotSupportedException e) {
			//can't happen if implements cloneable
			logger.warn(e.getMessage());
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dimension == null) ? 0 : dimension.hashCode());
		result = prime * result + Arrays.hashCode(expressionList);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PafDimSpec other = (PafDimSpec) obj;
		if (dimension == null) {
			if (other.dimension != null)
				return false;
		} else if (!dimension.equals(other.dimension))
			return false;
		if (!Arrays.equals(expressionList, other.expressionList))
			return false;
		return true;
	}
    
	
	
}
