/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.app;


/**
 * Contains a group name and domain name.
 *
 * @version	x.xx
 * @author Jason
 *
 */
public class PafSecurityGroup  implements Comparable<PafSecurityGroup> {

	//name of security group
	private String groupName;
	
	/**
	 * Creates a Security Group
	 *
	 */
	public PafSecurityGroup() {
		
	}
	
	public int compareTo(PafSecurityGroup group) {
				
		int outcome = 0;
		
		if ( group.getGroupName() != null && this.getGroupName() != null) {
			outcome = this.getGroupName().compareTo(group.getGroupName());			
		}
		
		return outcome;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
		
	
}
