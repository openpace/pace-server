/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/


package com.pace.base;

import java.util.ArrayList;
import java.util.List;

import com.pace.base.state.IPafClientState;

public class PafInvalidIntersectionException extends PafException {

/**
 * This exception indicates an invalid intersection request to the PafDataCache
 *
 */


	
	private static final long serialVersionUID = 1L;
	IPafClientState clientState;
	private ArrayList<String> messageDetail = new ArrayList<String>();



	private PafErrSeverity pes = PafErrSeverity.Info;

	public PafInvalidIntersectionException() {
		super();
	}

	/**
	 * @param message
	 * @param pes 
	 */
	public PafInvalidIntersectionException(String message, PafErrSeverity pes) {
		super(message, pes);
		this.pes = pes;
	}
    
    public PafInvalidIntersectionException(@SuppressWarnings({ "unused", "unused" })
	String message, @SuppressWarnings("unused")
	Throwable cause, IPafClientState clientState) {
        this.clientState = clientState;
    }

	/**
	 * @param cause
	 */
	public PafInvalidIntersectionException(Throwable cause) {
		super(cause);
	}
    
    public PafErrSeverity getSeverity() {
        return pes;
    }
    
    public void setSeverity(PafErrSeverity pes) {
        this.pes = pes;
    }
    
    public PafSoapException getPafSoapException() {
    	return new PafSoapException(this);
    }

    /**
     * @return Returns the clientState.
     */
    public IPafClientState getClientState() {
        return clientState;
    }

    public void setClientState(IPafClientState clientState) {
        this.clientState = clientState;
        
    }

    public void addMessageDetail(String string) {
        messageDetail.add(string);
    }
    
    public List<String> getMessageDetails() {
        return messageDetail;
    }

}
