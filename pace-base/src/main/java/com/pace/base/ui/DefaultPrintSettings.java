/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DefaultPrintSettings {
	private static DefaultPrintSettings _instance;
	@Autowired
	private static PrintStyle defaultPrintStyle;
	
	private DefaultPrintSettings() {}
	
	public static DefaultPrintSettings getInstance() {
		if( _instance == null ) {
			_instance = new DefaultPrintSettings();
			loadDefaultPrintSettings();
		}
		return _instance;
	}
	
	private static void loadDefaultPrintSettings() {
		Thread.currentThread().setContextClassLoader(DefaultPrintSettings.class.getClassLoader());
		ApplicationContext context = 
	    	  new ClassPathXmlApplicationContext(new String[] {"defaultPrintSettings.xml"});
		defaultPrintStyle = (PrintStyle)context.getBean("defaultPrintStyle");
	}

	public PrintStyle getDefaultPrintSettings() {
		return defaultPrintStyle;
	}
}
