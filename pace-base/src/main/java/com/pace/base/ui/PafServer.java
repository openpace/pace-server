/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.ui;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.server.ServerPlatform;

public class PafServer implements Comparable, Cloneable {
	private static final Long DEFAULT_SERVER_URL_TIMEOUT_IN_MILLISECONDS = 200L;
//	private static final Long DEFAULT_WEBSERVICE_CONNECTION_TIMEOUT_IN_MILLISECONDS = 60000L;
//	private static final Long DEFAULT_WEBSERVICE_RECEIEVE_TIMEOUT_IN_MILLISECONDS = 180000L;
	private static final Long SERVER_STARTUP_TIMEOUT_DEFAULT = 300000L;
	private static final String WSDL = "?wsdl";
	private static final String PACE_SERVER_INITSERVLET_CONTEXT_NAME = "PafInitServlet";
	
	private final static int connectionSuccessCode = 200;
	
	private String name;
	private String host;
	private Integer port;
    /**
     * @deprecated
     * This property was used during the old file system deploy and is not longer need as 
     * we have a service call to upload & download the project. 
     */
	private String homeDirectory;
	private boolean defaultServer; 
	private String webappName;
	private String wsdlServiceName;
	private Long urlTimeoutInMilliseconds;
//	private transient Long serviceConnectionTimeoutInMilliseconds = 60000L;
//	private transient Long serviceReceiveTimeoutInMilliseconds = 180000L;
	private String startupFile;
	private String shutdownFile;
	private Long serverStartupTimeoutInMilliseconds;
	private boolean doesNotPromptOnHotDeploy;
	private boolean doesNotPromptOnHotRefresh;
	private boolean https;
	private String osServiceName;
	private Integer jmsMessagingPort;
	private ServerPlatform serverPlatform;
	private String serverGroupName;
	private boolean disabled;
	
	public boolean isTheServerRunning() {
		// set default
		HttpURLConnection httpConnection = null;
		boolean serverRunning = false;
		String url = null;
		try {
			
			//try to get pafserver from url
			url = getCompleteServerInitServletUrl(); 
//			url = getCompleteWSDLService(); 
			//cast to http connection
			httpConnection = (HttpURLConnection) new URL(url).openConnection();
//        	httpConnection.setRequestProperty("Connection", "close");
//			httpConnection.setRequestMethod("GET");
		
			//if paf server exists
			//if pafserver has specified url timeout
			if ( getUrlTimeoutInMilliseconds() != null ) {
					urlTimeoutInMilliseconds = getUrlTimeoutInMilliseconds();
			}            	
			  
			if ( logger.isDebugEnabled()) {
					logger.debug("Setting connection timeout to " + urlTimeoutInMilliseconds + " for url " + url);
			}
		
			//set timeout
			httpConnection.setConnectTimeout(urlTimeoutInMilliseconds.intValue());	
//			httpConnection.connect();
			int retCode = httpConnection.getResponseCode();
			//if successful http status, server/app is running
			if ( retCode == HttpURLConnection.HTTP_OK ) {
				serverRunning = true;
				logger.debug("Successfully connected to url '" + url +"'");
			} else {
				logger.debug("Couldn't connect to url '" + url +"'. HttpURLConnection returned: " + retCode + ".");
			}                                    
                        
		} catch (MalformedURLException e) {
			logger.error("Malformed URL Exception: " + e.getMessage());
		}catch (SocketTimeoutException e) {
			logger.debug("Socket Timeout Exception: " + e.getMessage()); 
		} catch (IOException e) {
			logger.warn("URL IOException: " + e.getMessage());
		} catch (Exception e) {
			logger.warn("URL Exception: " + e.getMessage());
		}
		finally {
			
			if ( httpConnection != null ) {
			  logger.debug("About to disconnect from url: " + url);
          	  httpConnection.disconnect();
          	  logger.debug("Successfully disconnected from url: " + url);
            }
			
		}
		
		// log info
		logger.debug("URL: '" + url + "' is alive: " + serverRunning); //$NON-NLS-1$ //$NON-NLS-2$
		
		return serverRunning;
	}


	private static final Logger logger = Logger
			.getLogger(PafServer.class);
	public PafServer() {		
	}
	
	public PafServer(String name, String host, Integer port) {
		
		this(name, host, port, false);
				
	}
	
	public PafServer(String name, String host, Integer port, boolean defaultServer) {
		
		this.name = name;
		this.host = host;
		this.port = port;
		this.defaultServer = defaultServer;
		this.https = false;
		
	}

    /**
     * @deprecated
     * This property was used during the old file system deploy and is not longer need as 
     * we have a service call to upload & download the project. 
     */
	public String getHomeDirectory() {
		
		//if not null and doesn't end with file sep
		if (homeDirectory != null && ! homeDirectory.endsWith(File.separator) ) {
			
			//append file sep
			homeDirectory += File.separator;
			
		}
		
		return homeDirectory;
	}

    /**
     * @deprecated
     * This property was used during the old file system deploy and is not longer need as 
     * we have a service call to upload & download the project. 
     */
	public void setHomeDirectory(String homeDirectory) {
		this.homeDirectory = homeDirectory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public int compareTo(Object o) {
		
		PafServer otherServer = (PafServer) o;
		
		int outcome = 0;
		
		if ( otherServer.getName() != null && this.name != null) {
			outcome = this.name.compareTo(otherServer.getName());			
		}
		
		return outcome;
	}
	
	/**
	 * @return Returns the host.
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host The host to set.
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return Returns the defaultServer.
	 */
	public boolean isDefaultServer() {
		return defaultServer;
	}

	/**
	 * @param defaultServer The defaultServer to set.
	 */
	public void setDefaultServer(boolean defaultServer) {
		this.defaultServer = defaultServer;
	}

	/**
	 * @return Returns the webappName.
	 */
	public String getWebappName() {
		return webappName;
	}

	/**
	 * @param webappName The webappName to set.
	 */
	public void setWebappName(String webappName) {
		this.webappName = webappName;
	}

	/**
	 * @return Returns the wsdlServiceName.
	 */
	public String getWsdlServiceName() {
		return wsdlServiceName;
	}

	/**
	 * @param wsdlServiceName The wsdlServiceName to set.
	 */
	public void setWsdlServiceName(String wsdlServiceName) {
		this.wsdlServiceName = wsdlServiceName;
	}
	
	public String getCompleteServiceUrl() {
		StringBuilder sb = new StringBuilder(getCompleteServerInitServletUrl());		
		
		sb.append('/');
		sb.append(getWsdlServiceName());
		
		return sb.toString();
		
	}
	
	public String getCompleteAppUrl() {
		
		StringBuilder sb = new StringBuilder();
		
		if(https){
			sb.append("https");
		} else {
			sb.append("http");
		}
		
		sb.append("://" + host + ":" + port + "/" + webappName);
		
		return sb.toString();
	}
	
	public String getCompleteServerSettingsUrl() {
		
		StringBuilder sb = new StringBuilder(getCompleteAppUrl());		
		
		sb.append('/');
		sb.append(PafBaseConstants.PACE_SERVER_SETTINGS_CONTEXT_NAME);
		
		return sb.toString();
	}

	public String getCompleteServerInitServletUrl() {
		
		StringBuilder sb = new StringBuilder(getCompleteAppUrl());		
		
		sb.append('/');
		sb.append(PACE_SERVER_INITSERVLET_CONTEXT_NAME);
		
		return sb.toString();
	}

	public String getCompleteWSDLService() {

		StringBuilder sb = new StringBuilder(getCompleteAppUrl());				
		
		if ( wsdlServiceName != null ) {
			if( ! wsdlServiceName.equals(WSDL)) {
				sb.append('/');
			} 
		
			sb.append(wsdlServiceName);
		
			if (! wsdlServiceName.endsWith(WSDL)) {
				sb.append(WSDL);
			}
		}
		
		return sb.toString();
	}

	/**
	 * @return Returns the shutdownFile.
	 */
	public String getShutdownFile() {
		return shutdownFile;
	}

	/**
	 * @param shutdownFile The shutdownFile to set.
	 */
	public void setShutdownFile(String shutdownFile) {
		this.shutdownFile = shutdownFile;
	}

	/**
	 * @return Returns the startupFile.
	 */
	public String getStartupFile() {
		return startupFile;
	}

	/**
	 * @param startupFile The startupFile to set.
	 */
	public void setStartupFile(String startupFile) {
		this.startupFile = startupFile;
	}

	/**
	 * @return Returns the serverStartupTimeoutInMilliseconds.
	 */
	public Long getServerStartupTimeoutInMilliseconds() {
		return serverStartupTimeoutInMilliseconds!=null?serverStartupTimeoutInMilliseconds:SERVER_STARTUP_TIMEOUT_DEFAULT;
	}

	/**
	 * @param serverStartupTimeoutInMilliseconds The serverStartupTimeoutInMilliseconds to set.
	 */
	public void setServerStartupTimeoutInMilliseconds(
			Long serverStartupTimeoutInMilliseconds) {
		this.serverStartupTimeoutInMilliseconds = serverStartupTimeoutInMilliseconds;
	}

	/**
	 * @return the urlTimeoutInMilliseconds
	 */
	public Long getUrlTimeoutInMilliseconds() {
		return urlTimeoutInMilliseconds!=null?urlTimeoutInMilliseconds:DEFAULT_SERVER_URL_TIMEOUT_IN_MILLISECONDS;
	}

	/**
	 * @param urlTimeoutInMilliseconds the urlTimeoutInMilliseconds to set
	 */
	public void setUrlTimeoutInMilliseconds(Long urlTimeoutInMilliseconds) {
		this.urlTimeoutInMilliseconds = urlTimeoutInMilliseconds;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * @return the doesNotPromptOnDeploy
	 */
	public boolean isDoesNotPromptOnHotDeploy() {
		return doesNotPromptOnHotDeploy;
	}

	/**
	 * @param doesNotPromptOnDeploy the doesNotPromptOnDeploy to set
	 */
	public void setDoesNotPromptOnHotDeploy(boolean doesNotPromptOnHotDeploy) {
		this.doesNotPromptOnHotDeploy = doesNotPromptOnHotDeploy;
	}

	/**
	 * @return the doesNotPromptOnHotRefresh
	 */
	public boolean isDoesNotPromptOnHotRefresh() {
		return doesNotPromptOnHotRefresh;
	}

	/**
	 * @param doesNotPromptOnHotRefresh the doesNotPromptOnHotRefresh to set
	 */
	public void setDoesNotPromptOnHotRefresh(boolean doesNotPromptOnHotRefresh) {
		this.doesNotPromptOnHotRefresh = doesNotPromptOnHotRefresh;
	}

	public void setHttps(boolean https) {
		this.https = https;
	}

	public boolean isHttps() {
		return https;
	}

	public String getOsServiceName() {
		return osServiceName;
	}

	public void setOsServiceName(String osServiceName) {
		this.osServiceName = osServiceName;
	}

	public Integer getJmsMessagingPort() {
		return jmsMessagingPort;
	}

	public void setJmsMessagingPort(Integer jmsMessagingPort) {
		this.jmsMessagingPort = jmsMessagingPort;
	}
			

//	/**
//	 * @return the serviceConnectionTimeoutInMilliseconds
//	 */
//	public Long getServiceConnectionTimeoutInMilliseconds() {
//		return serviceConnectionTimeoutInMilliseconds!=null?serviceConnectionTimeoutInMilliseconds:DEFAULT_WEBSERVICE_CONNECTION_TIMEOUT_IN_MILLISECONDS;
//	}
//
//	/**
//	 * @param serviceConnectionTimeoutInMilliseconds the serviceConnectionTimeoutInMilliseconds to set
//	 */
//	public void setServiceConnectionTimeoutInMilliseconds(
//			Long serviceConnectionTimeoutInMilliseconds) {
//		this.serviceConnectionTimeoutInMilliseconds = serviceConnectionTimeoutInMilliseconds;
//	}
//
//	/**
//	 * @return the serviceReceieveTimeoutInMilliseconds
//	 */
//	public Long getServiceReceiveTimeoutInMilliseconds() {
//		return serviceReceiveTimeoutInMilliseconds!=null?serviceReceiveTimeoutInMilliseconds:DEFAULT_WEBSERVICE_RECEIEVE_TIMEOUT_IN_MILLISECONDS;
//	}
//
//	/**
//	 * @param serviceReceieveTimeoutInMilliseconds the serviceReceieveTimeoutInMilliseconds to set
//	 */
//	public void setServiceReceiveTimeoutInMilliseconds(
//			Long serviceReceiveTimeoutInMilliseconds) {
//		this.serviceReceiveTimeoutInMilliseconds = serviceReceiveTimeoutInMilliseconds;
//	}
	
	public ServerPlatform getServerPlatform() {
		return serverPlatform;
	}

	public void setServerPlatform(ServerPlatform serverPlatform) {
		this.serverPlatform = serverPlatform;
	}




	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PafServer [name=" + name + ", host=" + host + ", port=" + port
				+ ", homeDirectory=" + homeDirectory + ", defaultServer="
				+ defaultServer + ", webappName=" + webappName
				+ ", wsdlServiceName=" + wsdlServiceName
				+ ", urlTimeoutInMilliseconds=" + urlTimeoutInMilliseconds
				+ ", startupFile=" + startupFile + ", shutdownFile="
				+ shutdownFile + ", serverGroupName=" + serverGroupName
				+ ", serverStartupTimeoutInMilliseconds="
				+ serverStartupTimeoutInMilliseconds
				+ ", doesNotPromptOnHotDeploy=" + doesNotPromptOnHotDeploy
				+ ", doesNotPromptOnHotRefresh=" + doesNotPromptOnHotRefresh
				+ ", https=" + https + ", osServiceName=" + osServiceName
				+ ", jmsMessagingPort=" + jmsMessagingPort 
				+ ", disabled=" + disabled 
//				+ ", serviceConnectionTimeoutInMilliseconds=" + serviceConnectionTimeoutInMilliseconds
//				+ ", serviceReceiveTimeoutInMilliseconds=" + serviceReceiveTimeoutInMilliseconds
				+ ", serverPlatform=" + serverPlatform + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PafServer other = (PafServer) obj;
		if (defaultServer != other.defaultServer)
			return false;
		if (disabled != other.disabled)
			return false;
		if (doesNotPromptOnHotDeploy != other.doesNotPromptOnHotDeploy)
			return false;
		if (doesNotPromptOnHotRefresh != other.doesNotPromptOnHotRefresh)
			return false;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (https != other.https)
			return false;
		if (jmsMessagingPort == null) {
			if (other.jmsMessagingPort != null)
				return false;
		} else if (!jmsMessagingPort.equals(other.jmsMessagingPort))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (osServiceName == null) {
			if (other.osServiceName != null)
				return false;
		} else if (!osServiceName.equals(other.osServiceName))
			return false;
		if (port == null) {
			if (other.port != null)
				return false;
		} else if (!port.equals(other.port))
			return false;
		if (serverGroupName == null) {
			if (other.serverGroupName != null)
				return false;
		} else if (!serverGroupName.equals(other.serverGroupName))
			return false;
		if (serverPlatform != other.serverPlatform)
			return false;
		if (serverStartupTimeoutInMilliseconds == null) {
			if (other.serverStartupTimeoutInMilliseconds != null)
				return false;
		} else if (!serverStartupTimeoutInMilliseconds
				.equals(other.serverStartupTimeoutInMilliseconds))
			return false;
		if (shutdownFile == null) {
			if (other.shutdownFile != null)
				return false;
		} else if (!shutdownFile.equals(other.shutdownFile))
			return false;
		if (startupFile == null) {
			if (other.startupFile != null)
				return false;
		} else if (!startupFile.equals(other.startupFile))
			return false;
		if (urlTimeoutInMilliseconds == null) {
			if (other.urlTimeoutInMilliseconds != null)
				return false;
		} else if (!urlTimeoutInMilliseconds
				.equals(other.urlTimeoutInMilliseconds))
			return false;
		if (webappName == null) {
			if (other.webappName != null)
				return false;
		} else if (!webappName.equals(other.webappName))
			return false;
		if (wsdlServiceName == null) {
			if (other.wsdlServiceName != null)
				return false;
		} else if (!wsdlServiceName.equals(other.wsdlServiceName))
			return false;
		return true;
	}

	public String getServerGroupName() {
		return serverGroupName;
	}

	public void setServerGroupName(String serverGroupName) {
		this.serverGroupName = serverGroupName;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
	public boolean testApplicationConnection(String url) {
		
		try {
			URL wsdlUrl = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) wsdlUrl.openConnection();
			connection.setRequestMethod("GET"); 
		    int code = connection.getResponseCode(); 
		    logger.info("Connection response code: " + code);
		    
		    if (code == connectionSuccessCode ) {
		    	logger.info("Successful connection to: " + url);
		    	return true;
		    } else {
		    	logger.info("Connection failed to: " + url);
		    	return false;
		    }
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}
	
}
