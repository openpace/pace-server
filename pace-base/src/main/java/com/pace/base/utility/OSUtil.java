/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;


public class OSUtil {

	/**
	 * Returns the user.home directory for the user who's running the Pace Server.
	 * @return
	 */
	public static String getUserHomeDirectory() {
		
		return System.getProperty("user.home");
	}
	
	/**
	 * Returns the %ProgramData% directory.
	 * @return On non-Windows systems this will return null;
	 */
	public static String getProgramDataDirectory() {
		
		if(OSDetector.isWindows()){
			
			return System.getenv("PROGRAMDATA");
		
		} else {
			return null;		
		}
	}

	/**
	 * Returns the %APPDATA% directory.
	 * @return On non-Windows systems this will return null;
	 */
	public static String getUsersAppDataDirectory() {
		
		if(OSDetector.isWindows()){
			
			return System.getenv("APPDATA");
		
		} else {
			return null;		
		}
	}
	
	/**
	 * Returns the %LOCALAPPDATA% directory.
	 * @return On non-Windows systems this will return null;
	 */
	public static String getUsersLocalAppDataDirectory() {
		
		if(OSDetector.isWindows()){
			
			return System.getenv("LOCALAPPDATA");
		
		} else {
			return null;		
		}
	}
}
