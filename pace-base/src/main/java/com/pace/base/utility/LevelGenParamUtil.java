/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.mdb.PafDimTree.LevelGenType;

/**
 * A utility that parses Level / Generation members.  Typically a format of a member would be
 * L0 or G5.  The L stands for Level and G for Generation.
 * 
 * @author JavaJ
 * @version 1.00
 * 
 */
public class LevelGenParamUtil {
	
	private static Logger logger = Logger.getLogger(LevelGenParamUtil.class);
		
	/**
	 * Parses the member to get the L or G identifier.  If member doens't contain L or G, 
	 * Level is returned by default. 
	 * 
	 * @param member Level / Generation member to parse
	 * @return LevelGenType
	 */
	public static LevelGenType getLevelGenType(String member) {
		
		LevelGenType levelGenType = null;
		
		//if G or L, or set to L
		if (member.startsWith(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT)
				|| member.startsWith(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT)) {

			// holds the L or G
			String levelGenIdent = member.substring(0, 1);
			
			//if L
			if (levelGenIdent
					.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT)) {

				levelGenType = LevelGenType.LEVEL;

			//if G
			} else if (levelGenIdent
					.equalsIgnoreCase(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT)) {

				levelGenType = LevelGenType.GEN;

			}

		} else {

			//default value, not prepended with L or G
			levelGenType = LevelGenType.LEVEL;
			
		}
		
		return levelGenType;
		
	}
	
	/**
	 * Parses the member to get the level or generation number. 
	 * 
	 * @param member Level / Generation member to parse
	 * @return level or generation number
	 */
	public static int getLevelGenNumber(String member) {
		
		int levelGenNumber = 0;
		
		//if identifier is present
		if (member.startsWith(PafBaseConstants.LEVELGEN_TOKEN_LEVEL_IDENT)
				|| member.startsWith(PafBaseConstants.LEVELGEN_TOKEN_GENERATION_IDENT)) {
			
			// holds the level or generation number
			String levelGenNumStr = member.substring(1, member.length());
			
			//if string is all numeric, else set as 0
			if (levelGenNumStr.matches("[0123456789]*")) {

				levelGenNumber = Integer.parseInt(levelGenNumStr);

			} else {

				//default to 0
				levelGenNumber = 0;
			}

		} else {

			//ensure member is only numeric
			if (member.matches("[0123456789]*")) {
			
				levelGenNumber = Integer.parseInt(member);

			}
		}
		
		return levelGenNumber;
		
	}
	
	public static void main(String[] args) {
		
		String member = "0";
		
		logger.info("Member: " + member + "; LevelGenType: " + LevelGenParamUtil.getLevelGenType(member));
		logger.info("Member: " + member + "; LevelGenNum: " + LevelGenParamUtil.getLevelGenNumber(member));
		
		member = "3";
		
		logger.info("Member: " + member + "; LevelGenType: " + LevelGenParamUtil.getLevelGenType(member));
		logger.info("Member: " + member + "; LevelGenNum: " + LevelGenParamUtil.getLevelGenNumber(member));
		
		member = "L0";
		
		logger.info("Member: " + member + "; LevelGenType: " + LevelGenParamUtil.getLevelGenType(member));
		logger.info("Member: " + member + "; LevelGenNum: " + LevelGenParamUtil.getLevelGenNumber(member));
		
		member = "P";
		
		logger.info("Member: " + member + "; LevelGenType: " + LevelGenParamUtil.getLevelGenType(member));
		logger.info("Member: " + member + "; LevelGenNum: " + LevelGenParamUtil.getLevelGenNumber(member));
		
		member = "P3";
		
		logger.info("Member: " + member + "; LevelGenType: " + LevelGenParamUtil.getLevelGenType(member));
		logger.info("Member: " + member + "; LevelGenNum: " + LevelGenParamUtil.getLevelGenNumber(member));
		
		member = "G3";
		
		logger.info("Member: " + member + "; LevelGenType: " + LevelGenParamUtil.getLevelGenType(member));
		logger.info("Member: " + member + "; LevelGenNum: " + LevelGenParamUtil.getLevelGenNumber(member));
		
		
	}
	
}
