/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class JsonUtil {
	
	//instance of ObjectMapper
	private static ObjectMapper mapper = getMapper();
	
	/**
	 * Returns singleton ObjectMapper object.  If ObjectMapper is null, initilizes it and creates it.
	 * 
	 * @return ObjectMapper object for custom serialization scenarios
	 */
	public static ObjectMapper getMapper() {
		
		if (mapper == null) {			
			initMapper();			
		}
		
		return mapper;
	}

	
	/**
	 * Initializes the ObjectMapper class 
	 */
	private static void initMapper() {
		mapper = new ObjectMapper();
	}
	
	/**
	 * Converts a POJO to a Json string.
	 * @param object Object to convert.
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	public static String toJson(Object object) throws JsonGenerationException, JsonMappingException, IOException {
		
	    return getMapper().writeValueAsString(object);
	}
	
	/**
	 * Converts JSON string to a POJO
	 * @param json Json string
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> T toObject(String json, Class c) throws JsonParseException, JsonMappingException, IOException {
		
		return (T) getMapper().readValue(json, c);
	}
}
