/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;

import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Encryp/Decript Util
 * 
 * @version x.xx
 * @author pmack
 * 
 */
public class AESEncryptionUtil {

	private static final Logger logger = Logger.getLogger(AESEncryptionUtil.class);
	
	private static final String ARGUMENT_S_CAN_NOT_BE_NULL = "Argument(s) can not be null";
	
	private static final byte[] initVectorData = { (byte) 50, (byte) 51,
			(byte) 52, (byte) 53, (byte) 54, (byte) 55, (byte) 56, (byte) 57 };

	/**
	 * 
	 *  Encrypts a string using an IV
	 *
	 * @param strToEncrypt	String to encrypt
	 * @param password			IV used to encrypt string
	 * @return				Encrypted string
	 * @throws Exception	
	 */
	public static String encrypt(String strToEncrypt, String IVString) throws Exception {
		
		String password = PafBaseConstants.AES_Password;

		//check input args		
		if ( strToEncrypt == null || IVString == null ) {
			
			throw new IllegalArgumentException(ARGUMENT_S_CAN_NOT_BE_NULL);
			
		}
		
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		
		// setup key
		byte[] keyBytes = new byte[16];
		byte[] b = password.getBytes("UTF-8");
		int len = b.length;
		if (len > keyBytes.length)
			len = keyBytes.length;
		System.arraycopy(b, 0, keyBytes, 0, len);

		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

		// setup key
		byte[] IVBytes = new byte[16];
		byte[] c = IVString.getBytes("UTF-8");
		len = c.length;
		if (len > IVBytes.length)
			len = IVBytes.length;
		System.arraycopy(c, 0, IVBytes, 0, len);
		
		IvParameterSpec ivSpec = new IvParameterSpec(IVBytes);

		cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
		byte[] results = cipher.doFinal(strToEncrypt.getBytes("UTF-8"));
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(results);
	}

	/**
	 * 
	 *  Decrypts string using IV
	 *
	 * @param encryptedStr  Encrypted string to dectrypt
	 * @param password			Used to decrypt encrypted string
	 * @return				Decrypted string
	 * @throws Exception	
	 */
	public static String decrypt(String encryptedStr, String IVString) throws Exception {

		String password = PafBaseConstants.AES_Password;
		
		//check input args
		if ( encryptedStr == null || IVString == null ) {
			
			throw new IllegalArgumentException(ARGUMENT_S_CAN_NOT_BE_NULL);
			
		}
		
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		// setup key
		byte[] keyBytes = new byte[16];
		byte[] b = password.getBytes("UTF-8");
		int len = b.length;
		if (len > keyBytes.length)
			len = keyBytes.length;
		System.arraycopy(b, 0, keyBytes, 0, len);
		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
		
		// setup key
		byte[] IVBytes = new byte[16];
		byte[] c = IVString.getBytes("UTF-8");
		len = c.length;
		if (len > IVBytes.length)
			len = IVBytes.length;
		System.arraycopy(c, 0, IVBytes, 0, len);
		
		IvParameterSpec ivSpec = new IvParameterSpec(IVBytes);
		
		cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

		BASE64Decoder decoder = new BASE64Decoder();
		byte[] results = cipher.doFinal(decoder.decodeBuffer(encryptedStr));
		return new String(results, "UTF-8");

	}
	
	/**
	 * 
	 * Dynamically generates a unique IV.
	 *
	 * @return IV
	 */
	public static String generateIV() {
		
		String generatedIV = new Double(new Long((new Date()).getTime()).doubleValue() * Math.random()).toString();
		
		logger.debug("Generated IV: " + generatedIV);
		
		return generatedIV;
		
	}

}
