/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;

import org.apache.log4j.Logger;

import com.pace.base.mdb.PafDataCache;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jwatkins
 *
 */
public class LogUtil {
	
	private static Logger logger = Logger.getLogger(LogUtil.class);

	
    public static String timedStep(String stepDesc, long startTime) {
        return (stepDesc + " completed in " + StringUtils.decimalFormat((System.currentTimeMillis() - startTime), "#,###") + " ms");
    }

    /**
     * Generate log message that compare pre and post evaluation data cache statistics
     * 
     * @param dataCache Data cache statistics
     * @param initialDcPlanBlocks Pre-evaluation plannable data cache block count
     */
    public static void dcStats(PafDataCache dataCache, long initialDcPlanBlocks) {		
    	logger.info("Data Cache Stats [Pre-Evaluation]:");
    	dataCache.displayCurrentUsageStats(initialDcPlanBlocks);
    	logger.info("Data Cache Stats [Post-Evaluation]:");
    	dataCache.displayCurrentUsageStats();
    }
}
