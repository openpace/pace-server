/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;

import java.lang.reflect.Method;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;

/**
 * 
 * PafXStream is a wrapper class for XStream.
 *
 * @author JWatkins
 * @version	x.xx
 *
 */
public abstract class ConverterUtil implements Converter {
	
	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) 
	{
    	Class<?> c = context.getRequiredType();
    	Object myObj = null;
		try {
			myObj = c.newInstance();
		} catch (Exception e1) {
			e1.printStackTrace();
			return null;
		}
		
    	Method[] methods = c.getDeclaredMethods();

        while (reader.hasMoreChildren()) 
        {
        	reader.moveDown();
        	String tempNodeName = reader.getNodeName();

        	for(Method method: methods){
        		String tempMethodName = method.getName();
        		if(tempMethodName.equalsIgnoreCase("is" + tempNodeName) || tempMethodName.equalsIgnoreCase("get" + tempNodeName)){
        			try {
        				tempMethodName = "set" + tempNodeName;
        				for(Method meth: methods){
        					if(meth.getName().equalsIgnoreCase(tempMethodName)){
        						Class<?>[] types = meth.getParameterTypes();
        						if(types.length == 1){
  									meth.invoke(myObj, context.convertAnother(myObj, types[0]));
        						}
        						break;
        					}
        				}
					} catch (Exception e) {
						e.printStackTrace();
					} 
        			break;
        		}
        	}
            reader.moveUp();           
        }
        
        return myObj;
	}
}
