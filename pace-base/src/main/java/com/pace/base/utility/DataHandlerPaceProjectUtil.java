/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.base.utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import com.pace.base.project.InvalidPaceProjectInputException;
import com.pace.base.project.PaceProject;
import com.pace.base.project.PaceProjectCreationException;
import com.pace.base.project.ProjectSaveException;
import com.pace.base.project.ProjectSerializationType;
import com.pace.base.project.ZipPaceProject;

/**
 * @author JMilliron
 *
 */
public class DataHandlerPaceProjectUtil {
	
	
	private static final String PACE_PAF_FILE_NAME = "pace.paf";

	/**
	 * Converts a file to a DataHandler
	 * @param fileToConvert
	 * @return
	 */
	public static DataHandler convertFileToDataHandler(File fileToConvert){
		DataSource source = new FileDataSource(fileToConvert);
		return new DataHandler(source);
	}
	
	public static File convertDataHandlerToFile(DataHandler dataHandler, String fileExtension) throws IOException {
		
		File tempFile = null;
		if ( dataHandler != null ) {

			InputStream inputStream = null;
			
			try {
				
				inputStream = dataHandler.getInputStream();
				
				ByteArrayOutputStream out = new ByteArrayOutputStream();
	           
				IOStreamUtil.copy(inputStream, out);
	            
	            tempFile = new File(FileUtils.createTempFile(fileExtension));
	            
	            if ( tempFile.exists() ) {
	            	tempFile.delete();
	            }
	            FileOutputStream fos = new FileOutputStream(tempFile);
	            
	            fos.write(out.toByteArray());
	            
	            fos.flush();
	            
	            fos.close();
	            
	            return tempFile;

			} finally {
			
				if ( inputStream != null ) {
					inputStream.close();
				}
				
			}
			
		}
		return tempFile;
	}
	
	/**
	 * Converts a pace project to a DataHandler
	 * 
	 * @param paceProject pace project to convert
	 * @param tempDir scratch dir that the pace.paf file will be written too
	 * @return DataHandler
	 * @throws ProjectSaveException when pace project can't be saved
	 */
	public static DataHandler convertPaceProjectToDataHandler(PaceProject paceProject, String tempDir) throws ProjectSaveException {
		
		DataHandler dataHandler = null;
		
		if ( paceProject != null && tempDir != null ) {
		
			ZipPaceProject zipPaceProject = null;
			
			if ( paceProject instanceof ZipPaceProject ) {
				
				zipPaceProject = (ZipPaceProject) paceProject;
				
			} else {
				
				zipPaceProject = (ZipPaceProject) paceProject.convertTo(ProjectSerializationType.PAF);
				
			}
						
				
			File pafArchive = new File(tempDir + File.separator + PACE_PAF_FILE_NAME);
				
			if ( pafArchive.exists()) {
				pafArchive.delete();
			}
				
			zipPaceProject.saveTo(pafArchive.getAbsolutePath());
				
			dataHandler = convertFileToDataHandler(pafArchive);
			
			
		}		
		
		return dataHandler;
		
	}
	
	/**
	 * Converts a Data Handler to a Pace Project.
	 * 
	 * @param dataHandler data handler to convert
	 * @param tempDir scratch dir that the pace.paf file will be written too
	 * @return pace project
	 * @throws IOException
	 * @throws InvalidPaceProjectInputException
	 * @throws PaceProjectCreationException
	 */
	public static PaceProject convertDataHandlerToPaceProject(DataHandler dataHandler, String tempDir) throws IOException, InvalidPaceProjectInputException, PaceProjectCreationException {
		
		PaceProject paceProject = null;
		
		if ( dataHandler != null && tempDir != null ) {

			InputStream inputStream = null;
			
			try {
				
				inputStream = dataHandler.getInputStream();
				
				ByteArrayOutputStream out = new ByteArrayOutputStream();
	           
				IOStreamUtil.copy(inputStream, out);
	            
	            File pacePafFile = new File(tempDir + File.separator + PACE_PAF_FILE_NAME);
	            
	            if ( pacePafFile.exists() ) {
	            	pacePafFile.delete();
	            }
	            FileOutputStream fos = new FileOutputStream(pacePafFile);
	            
	            fos.write(out.toByteArray());
	            
	            fos.flush();
	            
	            fos.close();
	            
	            ZipPaceProject zpp = new ZipPaceProject(pacePafFile.getAbsolutePath());
	            			
				paceProject = zpp.convertTo(ProjectSerializationType.XML);

			} finally {
			
				if ( inputStream != null ) {
					inputStream.close();
				}
				
			}
			
		}
				
		return paceProject;
		
	}
	

}
