/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;

import com.pace.base.view.PafAxis;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;

/**
 * A utility class to help with view section and information within them.
 *
 * @version	1.00
 * @author jmilliron
 *
 */
public class PafViewSectionUtil {
	
	/**
	 * Tries to get the paf axis for a dimension within a view section.
	 * 
	 * @param pafViewSection
	 *            Complex View Section object
	 * @param pafDimensionName
	 *            Name of dimension to search
	 * 
	 * @return PafAxis if found, a paf axis object holding the state of which axis the dimension is on
	 */
	public static PafAxis getDimensionAxis(PafViewSection pafViewSection, String pafDimensionName) {
		
		PafAxis pafAxis = null;
				
		
		//if view section is null and dimension name is null, don't do
		if ( pafViewSection != null && pafDimensionName != null) {
						
			//hold temp int var for axis int
			Integer pafAxisIntValue = null;
			
			//if page tuples are present
			if ( pafViewSection.getPageTuples() != null ) {
				
				//loop over the page tuples and check the axis attribute for a macth with the pafDimensionName
				for (PageTuple pageTuple : pafViewSection.getPageTuples()) {
					
					//if dimension name is found on page tuple, set int value to page
					if ( pageTuple.getAxis().equalsIgnoreCase(pafDimensionName) ) {
						
						//hold temp int value
						pafAxisIntValue = PafAxis.PAGE;
						break;
					}
				}
			}
			
			//if tmp int value still null
			if ( pafAxisIntValue == null ) {
			
				//loop through column dimension names and search for pafDimensionName
				for ( String columnDimName : pafViewSection.getColAxisDims() ) {
					
					//if match, then set int value to col and break;
					if ( columnDimName.equalsIgnoreCase( pafDimensionName )) {
						
						pafAxisIntValue = PafAxis.COL;
						break;
					}
					
				}			
			
			}

			//if tmp int value still null
			if ( pafAxisIntValue == null ) {
							
				//loop through row dimension names and search for pafDimensionName
				for ( String rowDimName : pafViewSection.getRowAxisDims() ) {
					
					//if match, then set int value to row and break;
					if ( rowDimName.equalsIgnoreCase( pafDimensionName )) {
						
						pafAxisIntValue = PafAxis.ROW;
						break;
						
					}
					
				}
				
			}
		
			//if int value is not null, create new paf axis object
			if ( pafAxisIntValue != null ) {
				
				pafAxis = new PafAxis(pafAxisIntValue);
				
			}
			
		}		
		
		return pafAxis;
		
	}

}
