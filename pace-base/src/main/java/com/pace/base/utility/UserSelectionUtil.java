/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;

import java.util.LinkedHashSet;
import java.util.Set;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafErrHandler;
import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.view.PafViewSection;
import com.pace.base.view.PageTuple;
import com.pace.base.view.ViewTuple;

public class UserSelectionUtil {
	
	private static final String userSelectionTag = PafBaseConstants.USER_SEL_TAG + "(";

	public static Set<String> findUserSelections(PafViewSection viewSection) {
		
		Set<String> userSelectionNames = new LinkedHashSet<String>();
				
		PageTuple[] pageTuples = viewSection.getPageTuples();
		ViewTuple[] rowTuples = viewSection.getRowTuples();
		ViewTuple[] colTuples = viewSection.getColTuples();
				
		for (PageTuple pageTuple : pageTuples) {
		
			if( pageTuple.getMember() != null ) {
				if ( pageTuple.getMember().contains(userSelectionTag)) {
					userSelectionNames.add(getUserSelectionMember(pageTuple.getMember()));
				}
			}
			//TTN-2154. 1994 - handling missing member in page tuple
			else {
				PafErrHandler.handleException(new PafException("View Section: '" + viewSection.getName() + "' is missing member in page tuple: " + pageTuple.getAxis()+ ".", PafErrSeverity.Error));
			}
		}
		
		for (ViewTuple colTuple : colTuples) {
			
			for (String memberDef : colTuple.getMemberDefs()) {
				
				if ( memberDef.contains(userSelectionTag)) {
					userSelectionNames.add(getUserSelectionMember(memberDef));
				}				
			}
			
		}

		for (ViewTuple rowTuple : rowTuples) {
			
			for (String memberDef : rowTuple.getMemberDefs()) {
				
				if ( memberDef.contains(userSelectionTag)) {
					userSelectionNames.add(getUserSelectionMember(memberDef));
				}				
			}
			
		}
		
		return userSelectionNames;
	}

	public static String getUserSelectionMember(String fullMember) {
		
		int beginNdx = fullMember.indexOf(userSelectionTag);
		beginNdx += userSelectionTag.length();
		
		int endNdx = fullMember.indexOf(")");
		
		return fullMember.substring(beginNdx, endNdx).trim();
		
	}
	
	
	public static void main(String[] args) {
		
		System.out.println(UserSelectionUtil.getUserSelectionMember("@IDESC(@USER_SEL(M1))"));
		System.out.println(UserSelectionUtil.getUserSelectionMember("@USER_SEL(M1)"));
		
		LinkedHashSet<String> tmpSet = new LinkedHashSet<String>();
		
		tmpSet.add("AAA");
		tmpSet.add("AAC");
		tmpSet.add("AAB");		
		tmpSet.add("AAA");
		tmpSet.add("AAC");
		tmpSet.add("AAB");
		
		for (String str : tmpSet) {
			System.out.println(str);
		}
		
	}
}
