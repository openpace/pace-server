/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.utility;

import org.apache.log4j.Logger;

import com.pace.base.app.PafPlannerRole;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * 
 * PafXStream is a wrapper class for XStream.
 *
 * @author JWatkins
 * @version	x.xx
 *
 */
public class PlannerRoleConverterUtil extends ConverterUtil {
	
	//instance of Logger
	private static Logger logger = Logger.getLogger(PafXStream.class);

	// protected constructor to control access
	protected PlannerRoleConverterUtil() {
		super();
	}

	@Override
	public boolean canConvert(Class type) {
		return PafPlannerRole.class.equals(type);
	}

	@Override
    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
		PafPlannerRole role = (PafPlannerRole) value;
        if(role.getRoleName() != null){
        	writer.startNode("roleName");
        	writer.setValue(role.getRoleName());
        	writer.endNode();
        }
        if(role.getRoleDesc() != null){
        	writer.startNode("roleDesc");
        	writer.setValue(role.getRoleDesc());
        	writer.endNode();
        }
        if(role.getPlanType() != null){
        	writer.startNode("planType");
        	writer.setValue(role.getPlanType());
        	writer.endNode();
        }
        if(role.getPlanVersion() != null){
        	writer.startNode("planVersion");
        	writer.setValue(role.getPlanVersion());
        	writer.endNode();
        }
        if(role.getSeasonIds() != null){
        	writer.startNode("seasonIds");
        	context.convertAnother(role.getSeasonIds());
        	writer.endNode();
        }
        writer.startNode("readOnly");
        writer.setValue(String.valueOf(role.isReadOnly()));
        writer.endNode();
        if(role.isAssortmentRole()){
            writer.startNode("assortmentRole");
            writer.setValue(String.valueOf(role.isAssortmentRole()));
            writer.endNode();
        }
	}
}
