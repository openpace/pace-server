/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.state;

import java.util.Map;
import java.util.Properties;

import com.pace.base.app.PafApplicationDef;
import com.pace.base.mdb.IPafConnectionProps;
import com.pace.base.view.PafUserSelection;

/**
 * The interface to expose client information to custom functions.
 * Allows a subset of client information to be available to 
 * externally written functions.
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public interface IPafClientState {
	void generateTokenCatalog(Properties props);
    PafApplicationDef getApp();
    String getClientId();
    String getClientIpAddress();
    String getClientVersion();
    String getUserName();
    Map<String, IPafConnectionProps> getDataSources();
    Map<String, PafUserSelection[]> getUserSelections();
	Properties getTokenCatalog();
}
