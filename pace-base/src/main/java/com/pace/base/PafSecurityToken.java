/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base;

public class PafSecurityToken {
	private String userName;
	private boolean admin;
	private String sessionToken;
    private boolean isValid;
    private String Domain;
    
    public PafSecurityToken(String name , String token, boolean valid) {
        super();
        isValid = valid;
        sessionToken = token;
        userName = name;
    }

    public PafSecurityToken()  {}

	public String getUserName() {
		return userName;
	}
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

	public boolean getAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public void setDomain(String domain) {
		Domain = domain;
	}

	public String getDomain() {
		return Domain;
	}
}
