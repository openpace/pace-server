/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.rules;

import java.util.List;
import java.util.Map;

public class ClientRuleSets {

	private RuleSet[] fullRuleSets;
	private Map<String, List<RuleSet>> fullRuleSetMap; 
	private List<RuleSet> clientRuleSetList;
	private Map<String, String[]> ruleSetMemberTagFuncs;
	
	
	
	public RuleSet[] getFullRuleSets() {
		return fullRuleSets;
	}
	public void setFullRuleSets(RuleSet[] fullRuleSets) {
		this.fullRuleSets = fullRuleSets;
	}
	public Map<String, List<RuleSet>> getFullRuleSetMap() {
		return fullRuleSetMap;
	}
	public void setFullRuleSetMap(Map<String, List<RuleSet>> fullRuleSetMap) {
		this.fullRuleSetMap = fullRuleSetMap;
	}
	public List<RuleSet> getClientRuleSetList() {
		return clientRuleSetList;
	}
	public void setClientRuleSetList(List<RuleSet> clientRuleSetList) {
		this.clientRuleSetList = clientRuleSetList;
	}
	public Map<String, String[]> getRuleSetMemberTagFuncs() {
		return ruleSetMemberTagFuncs;
	}
	public void setRuleSetMemberTagFuncs(
			Map<String, String[]> ruleSetMemberTagFuncs) {
		this.ruleSetMemberTagFuncs = ruleSetMemberTagFuncs;
	} 
}
