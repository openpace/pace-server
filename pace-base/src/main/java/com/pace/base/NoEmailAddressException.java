/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base;

import com.pace.base.state.IPafClientState;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class NoEmailAddressException extends PafException {

	public NoEmailAddressException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoEmailAddressException(String message, PafErrSeverity pes, Throwable cause) {
		super(message, pes, cause);
		// TODO Auto-generated constructor stub
	}

	public NoEmailAddressException(String message, PafErrSeverity pes) {
		super(message, pes);
		// TODO Auto-generated constructor stub
	}

	public NoEmailAddressException(String message, Throwable cause, IPafClientState clientState) {
		super(message, cause, clientState);
		// TODO Auto-generated constructor stub
	}

	public NoEmailAddressException(String message, Throwable cause, PafErrSeverity severity) {
		super(message, cause, severity);
		// TODO Auto-generated constructor stub
	}

	public NoEmailAddressException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
