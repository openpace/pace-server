/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import java.util.Set;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;

import com.pace.base.project.ProjectElementId;

@XmlType
public class UploadAppRequest extends PafRequest {
	
	private String appId;
	
	private DataHandler paceProjectDataHandler;
	
	private boolean applyConfigurationUpdate;
	
	private boolean applyCubeUpdate;
	
	private Set<ProjectElementId> projectElementIdFilters;
	
	//when set, will not delete views, view sections and rule sets before writing.  this is
	//important to deploy individual artifacts like a view without wiping the views dir.
	private boolean partialDeployment;
	
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public DataHandler getPaceProjectDataHandler() {
		return paceProjectDataHandler;
	}

	@XmlMimeType("application/octet-stream") 
	public void setPaceProjectDataHandler(DataHandler paceProjectDataHandler) {
		this.paceProjectDataHandler = paceProjectDataHandler;
	}

	public boolean isApplyConfigurationUpdate() {
		return applyConfigurationUpdate;
	}

	public void setApplyConfigurationUpdate(boolean applyConfigurationUpdate) {
		this.applyConfigurationUpdate = applyConfigurationUpdate;
	}

	public boolean isApplyCubeUpdate() {
		return applyCubeUpdate;
	}

	public void setApplyCubeUpdate(boolean applyCubeUpdate) {
		this.applyCubeUpdate = applyCubeUpdate;
	}

	public Set<ProjectElementId> getProjectElementIdFilters() {
		return projectElementIdFilters;
	}

	public void setProjectElementIdFilters(
			Set<ProjectElementId> projectElementIdFilters) {
		this.projectElementIdFilters = projectElementIdFilters;
	}

	public boolean isPartialDeployment() {
		return partialDeployment;
	}

	public void setPartialDeployment(boolean partialDeployment) {
		this.partialDeployment = partialDeployment;
	}

	
	

}
