
/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import java.util.Arrays;

import org.apache.log4j.Logger;

/**
 * Uow User Filter Specification
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class UserFilterSpec implements Cloneable {

	private transient static final Logger logger = Logger.getLogger(UserFilterSpec.class);
	
	private String[] attrDimNames = null;

	/**
	 * @return the attrDimNames
	 */
	public String[] getAttrDimNames() {
		return attrDimNames;
	}

	/**
	 * @param attrDimNames the attrDimNames to set
	 */
	public void setAttrDimNames(String[] attrDimNames) {
		this.attrDimNames = attrDimNames;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(attrDimNames);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserFilterSpec other = (UserFilterSpec) obj;
		if (!Arrays.equals(attrDimNames, other.attrDimNames))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public UserFilterSpec clone() {
		
		UserFilterSpec userFilterSpec = null;
		
		try {
			userFilterSpec = (UserFilterSpec) super.clone();
		} catch (CloneNotSupportedException e) {
			//can't happen if implements cloneable
			logger.warn(e.getMessage());
		}
		
		return userFilterSpec;
	}
	
	
	
}

