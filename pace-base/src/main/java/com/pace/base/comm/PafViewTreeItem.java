/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import java.util.ArrayList;
import java.util.List;

import com.pace.base.app.AliasMapping;
import com.pace.base.app.SuppressZeroSettings;
import com.pace.base.view.PafUserSelection;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PafViewTreeItem implements Cloneable {
	
	private String label = "";

	private String desc = "";

	private boolean group;

	private PafUserSelection[] userSelections;

	private PafViewTreeItem[] items;
	
	private AliasMapping[] aliasMappings;
	
	private SuppressZeroSettings[] suppressZeroSettings;

	/**
	 * @return Returns the desc.
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc The desc to set.
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return Returns the group.
	 */
	public boolean isGroup() {
		return group;
	}

	/**
	 * @param group The group to set.
	 */
	public void setGroup(boolean group) {
		this.group = group;
	}

	/**
	 * @return Returns the items.
	 */
	public PafViewTreeItem[] getItems() {
		return items;
	}

	/**
	 * @param items The items to set.
	 */
	public void setItems(PafViewTreeItem[] items) {
		this.items = items;
	}

	/**
	 * @return Returns the label.
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label The label to set.
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return Returns the userSelections.
	 */
	public PafUserSelection[] getUserSelections() {
		return userSelections;
	}

	/**
	 * @param userSelections The userSelections to set.
	 */
	public void setUserSelections(PafUserSelection[] userSelections) {
		this.userSelections = userSelections;
	}

	/**
	 * @return Returns a string.
	 */
	public String toString() {
		
		StringBuffer strBuff = new StringBuffer();
		
		strBuff.append("View: " + label + "\n");
		strBuff.append("Desc: " + desc + "\n");
		strBuff.append("Group: " + group + "\n");
		
		if ( items != null ) {
			for ( PafViewTreeItem item : items ) {
				strBuff.append("\t" + item);
			}
		}
		
		return strBuff.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		
		PafViewTreeItem clonedObject = null;
		
		clonedObject = (PafViewTreeItem) super.clone(); 
		
		if ( this.items != null && this.items.length > 0 ) {
			
			List<PafViewTreeItem> itemList = new ArrayList<PafViewTreeItem>();
			
			for (PafViewTreeItem item : this.items) {
				
				itemList.add((PafViewTreeItem) item.clone());
				
			}			
			
			clonedObject.setItems(itemList.toArray(new PafViewTreeItem[0]));
			
		}
		
		return clonedObject;
	}

	/**
	 * @return the aliasMappings
	 */
	public AliasMapping[] getAliasMappings() {
		return aliasMappings;
	}

	/**
	 * @param aliasMappings the aliasMappings to set
	 */
	public void setAliasMappings(AliasMapping[] aliasMappings) {
		this.aliasMappings = aliasMappings;
	}

	public void setSuppressZeroSettings(SuppressZeroSettings[] suppressZeroSettings) {
		this.suppressZeroSettings = suppressZeroSettings;
	}

	public SuppressZeroSettings[] getSuppressZeroSettings() {
		return suppressZeroSettings;
	}

	


}
