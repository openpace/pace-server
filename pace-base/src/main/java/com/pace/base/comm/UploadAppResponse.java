/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;


public class UploadAppResponse extends PafSuccessResponse {
	
	private boolean reinitClientState;
	
	private boolean applyConfigurationUpdateSuccess;
	
	private boolean applyCubeUpdateSuccess;
	
	public UploadAppResponse() {} // empty constructor for SOAP layer
	
	public UploadAppResponse(boolean b) {
		this.setSuccess(b);
	}

	public boolean isReinitClientState() {
		return reinitClientState;
	}

	public void setReinitClientState(boolean reinitClientState) {
		this.reinitClientState = reinitClientState;
	}

	public boolean isApplyConfigurationUpdateSuccess() {
		return applyConfigurationUpdateSuccess;
	}

	public void setApplyConfigurationUpdateSuccess(
			boolean applyConfigurationUpdateSuccess) {
		this.applyConfigurationUpdateSuccess = applyConfigurationUpdateSuccess;
	}

	public boolean isApplyCubeUpdateSuccess() {
		return applyCubeUpdateSuccess;
	}

	public void setApplyCubeUpdateSuccess(boolean applyCubeUpdateSuccess) {
		this.applyCubeUpdateSuccess = applyCubeUpdateSuccess;
	}
	
	
		
}
