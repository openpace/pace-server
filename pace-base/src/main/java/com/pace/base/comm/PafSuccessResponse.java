/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import java.util.ArrayList;
import java.util.List;


/**
 * Sends back success/failure response
 *
 * @version	x.xx
 * @author Alan
 *
 */
public class PafSuccessResponse extends PafResponse {

	private boolean isSuccess = false;
	private List<Exception> exceptionList = new ArrayList<Exception>();
	

	public List<Exception> getExceptionList() {
		return exceptionList;
	}

	public void addException(Exception e) {
		this.exceptionList.add(e);
	}
	
	public void clearExceptions() {
		this.exceptionList.clear();
	}

	/**
	 * @param Default constructor
	 */
	public PafSuccessResponse() {}
	
	/**
	 * @param isSuccess
	 */
	public PafSuccessResponse(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	
	/**
	 * @return the isSuccess
	 */
	public boolean isSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
}
