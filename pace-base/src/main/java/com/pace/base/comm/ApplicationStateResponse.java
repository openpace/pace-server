/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class ApplicationStateResponse.
 */
public class ApplicationStateResponse extends PafResponse {
	
	/** The app states. */
	private List<ApplicationState> appStates = new ArrayList<ApplicationState>();

	/**
	 * Gets the app states.
	 *
	 * @return the appStates
	 */
	public List<ApplicationState> getAppStates() {
		return appStates;
	}

	/**
	 * Sets the app states.
	 *
	 * @param appStates the appStates to set
	 */
	public void setAppStates(List<ApplicationState> appStates) {
		this.appStates = appStates;
	}
	

}
