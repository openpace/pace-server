/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import java.util.Arrays;

import org.apache.log4j.Logger;

import com.pace.base.app.PafDimSpec;


/**
 * Uow Data Filter Specification
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class DataFilterSpec implements Cloneable {
	
	private transient static final Logger logger = Logger.getLogger(DataFilterSpec.class);
	
	private PafDimSpec[] dimSpec;
	
	public DataFilterSpec() {
	}
	
	public void setDimSpec(PafDimSpec[] dimSpec) {
		this.dimSpec = dimSpec;
	}

	public PafDimSpec[] getDimSpec() {
		return dimSpec;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(dimSpec);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataFilterSpec other = (DataFilterSpec) obj;
		if (!Arrays.equals(dimSpec, other.dimSpec))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public DataFilterSpec clone() {

		DataFilterSpec dataFilterSpec = null;
		
		try {
			dataFilterSpec = (DataFilterSpec) super.clone();
			
			if ( this.dimSpec != null ) {
				
				PafDimSpec[] clonePafDimSpec = new PafDimSpec[this.dimSpec.length];
				
				for ( int i = 0; i < this.dimSpec.length; i++ ) {
					
					clonePafDimSpec[i] = this.dimSpec[i].clone();
					
				}
				
				dataFilterSpec.setDimSpec(clonePafDimSpec);
				
			}
			
		} catch (CloneNotSupportedException e) {
			//can't happen if implements cloneable
			logger.warn(e.getMessage());
		}
		
		return dataFilterSpec;
		
	}
	
	

}

