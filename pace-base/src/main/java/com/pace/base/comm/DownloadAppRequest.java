/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.pace.base.project.ProjectElementId;

/**
 * Download Application Request.  
 * 
 * @author JMilliron
 *
 */
public class DownloadAppRequest extends PafRequest {
	
	private List<String> appIds = new ArrayList<String>();
	
	private Set<ProjectElementId> projectElementIdFilters;

	public List<String> getAppIds() {
		return appIds;
	}

	public void setAppIds(List<String> appIds) {
		this.appIds = appIds;
	}

	public Set<ProjectElementId> getProjectElementIdFilters() {
		return projectElementIdFilters;
	}

	public void setProjectElementIdFilters(
			Set<ProjectElementId> projectElementIdFilters) {
		this.projectElementIdFilters = projectElementIdFilters;
	}

	
}
