/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class DownloadAppResponse extends PafSuccessResponse {
		
	private DataHandler paceProjectDataHandler;

	private String appId;
	
	private boolean isProjectDataFiltered;
	
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@XmlMimeType("application/octet-stream")
	public DataHandler getPaceProjectDataHandler() {
		return paceProjectDataHandler;
	}

	public void setPaceProjectDataHandler(DataHandler paceProjectDataHandler) {
		this.paceProjectDataHandler = paceProjectDataHandler;
	}

	public boolean isProjectDataFiltered() {
		return isProjectDataFiltered;
	}

	public void setProjectDataFiltered(boolean isProjectDataFiltered) {
		this.isProjectDataFiltered = isProjectDataFiltered;
	}
			

}
