/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import com.pace.base.view.ViewInformation;
import com.pace.base.view.ViewSectionInformation;

/**
 * getViewInformation service call response.
 * @author themoosman
 *
 */
public class ViewInfoResponse extends PafResponse {

	private ViewInformation viewInfo;
	
	/**
	 * Default constructor.
	 */
	public ViewInfoResponse() {
		super();
	}
	
	/**
	 * Constructor with parms.
	 * @param viewInfo
	 * @param viewSectionInfo
	 */
	public ViewInfoResponse(ViewInformation viewInfo,
			ViewSectionInformation[] viewSectionInfo) {
		super();
		this.viewInfo = viewInfo;
	}
	
	/**
	 * Gets the view information object.
	 * @return ViewInformation object
	 */
	public ViewInformation getViewInfo() {
		return viewInfo;
	}
	
	/**
	 * Sets the view information object.
	 * @param viewInfo ViewInformation object
	 */
	public void setViewInfo(ViewInformation viewInfo) {
		this.viewInfo = viewInfo;
	}
}
