/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

import java.util.ArrayList;
import java.util.List;

import com.pace.base.RunningState;

public class ApplicationState {
	private String applicationId;
	private RunningState currentRunState;
	private String appSessionId;
	private List<Exception> exceptions = new ArrayList<Exception>();
	
	public ApplicationState() {} // required by SOAP Layer
	
	public ApplicationState(String appId) {
		applicationId = appId;
		currentRunState= RunningState.STOPPED;
	}
	
	/**
	 * 
	 * @param appId
	 * @param appSessionId
	 */
    //TTN-1950 - Generate the id for the ApplicationState
	public ApplicationState(String appId, String appSessionId) {
		this(appId);
		this.appSessionId = appSessionId;
	}
	
	
	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {
		return applicationId;
	}
	
	public void setApplicationId(String appId) {
		this.applicationId = appId;
	}
		

	/**
	 * @return the currentState
	 */
	public RunningState getCurrentRunState() {
		return currentRunState;
	}

	/**
	 * @param currentState the currentState to set
	 */
	public void setCurrentRunState(RunningState currentState) {
		this.currentRunState = currentState;
	}
	
	public void clearExceptions() {
		exceptions.clear();
	}
	
	public void addException(Exception ex) {
		exceptions.add(ex);
	}
	
	public List<Exception> getExceptions() {
		return exceptions;
	}
	
	public String getAppSessionId() {
		return appSessionId;
	}

	public void setAppSessionId(String appSessionId) {
		this.appSessionId = appSessionId;
	}


	@Override
	public String toString() {
		return "ApplicationState [applicationId=" + applicationId
				+ ", currentRunState=" + currentRunState + ", appSessionId="
				+ appSessionId + ", exceptions=" + exceptions + "]";
	}
}
