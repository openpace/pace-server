/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.comm;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class ClientInitRequest implements Cloneable {
    String ipAddress;
    String clientType;
    String clientVersion;
    String clientLanguage;
    
    public String getClientType() {
        return clientType;
    }
    public void setClientType(String clientType) {
        this.clientType = clientType;
    }
    public String getClientVersion() {
        return clientVersion;
    }
    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }
    public String getIpAddress() {
        return ipAddress;
    }
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
    
    public String toString() {
        return "Client Address: " + ipAddress + "\nClient Type: " + clientType + "\nClient Version: " + clientVersion;
    }
    /**
     * @return Returns the clientLanguage.
     */
    public String getClientLanguage() {
        return clientLanguage;
    }
    /**
     * @param clientLanguage The clientLanguage to set.
     */
    public void setClientLanguage(String clientLanguage) {
        this.clientLanguage = clientLanguage;
    }

    public ClientInitRequest clone() throws CloneNotSupportedException {	
    	ClientInitRequest cirClone = (ClientInitRequest) super.clone();
    	return cirClone;
    }
}
