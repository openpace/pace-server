/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base;

/**
 * 
 * Thrown when a user is not authorized.
 * 
 * @author javaj
 * @version 1.0
 * 
 */
public class PafNotAuthenticatedSoapException extends Exception { //PafSoapException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6608741350330176780L;

	/**
	 * Constructor. 
	 */
	public PafNotAuthenticatedSoapException() {
	}
	
	/**
	 * Constructor.  A PafException is used to create this exception
	 * 
	 * @param exception
	 */
	public PafNotAuthenticatedSoapException(PafException exception) {
		super(exception);
	}

}
