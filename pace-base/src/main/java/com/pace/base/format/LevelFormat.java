/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.format;

/**
 * 
 * Used for Generation Formatting.
 * 
 * @author JavaJ
 *
 */
public class LevelFormat extends PafFormat {
	
	private Integer level;
	
	/**
	 * Empty Constructor
	 *
	 */
    public LevelFormat() {}
    
    /**
     * Creates a Level Format.  A level format includes a level number and formatName.
     * 
     * @param level
     * @param formatName
     */
	public LevelFormat(Integer level, String formatName)  {
		
		super(formatName);
		this.level = level;		
		
	}

	/**
	 * 
	 * @return level
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * Set's level. 
	 * 
	 * @param level
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LevelFormat other = (LevelFormat) obj;
		if (level == null) {
			if (other.level != null)
				return false;
		} else if (!level.equals(other.level))
			return false;
		return true;
	}
	
	

}
