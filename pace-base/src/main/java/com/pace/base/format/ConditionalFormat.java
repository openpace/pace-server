/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.format;

import java.util.ArrayList;
import java.util.List;

import com.pace.base.app.PafDimSpec;
import com.pace.base.view.ConditionalFormatDimension;

/**
 * 
 * Used for Generation Formatting.
 * 
 * @author JavaJ
 *
 */
public class ConditionalFormat implements Cloneable {
	
	private String name;
	
	private ConditionalFormatDimension primaryDimension;
	private List<ConditionalFormatDimension> secondaryDimensions;
	    
	public ConditionalFormat(){
		
		this.secondaryDimensions = new ArrayList<ConditionalFormatDimension>();
		
	}
	
    /**
     * Creates a Level Format.  A level format includes a level number and formatName.
     * 
     * @param level
     * @param formatName
     */
	public ConditionalFormat(String name, ConditionalFormatDimension primaryDimension, List<ConditionalFormatDimension> secondaryDimensions)  {
		
		this.name = name;
		this.primaryDimension = primaryDimension;
		this.secondaryDimensions = secondaryDimensions;		
	}
	
	public ConditionalFormatDimension getPrimaryDimension(){
		return this.primaryDimension;
	}
	
	public void setPrimaryDimension(ConditionalFormatDimension primaryDimension){
		this.primaryDimension = primaryDimension;
	}
	
	public List<ConditionalFormatDimension> getSecondaryDimensions(){
		return this.secondaryDimensions;
	}
	
	public void setSecondaryDimensions(List<ConditionalFormatDimension> secondaryDimensions){
		this.secondaryDimensions = secondaryDimensions;
	}
	
	public void addSecondaryDimension(ConditionalFormatDimension secondary){
		
		if(secondaryDimensions == null){
			secondaryDimensions = new ArrayList<ConditionalFormatDimension>();
		}
		
		for(ConditionalFormatDimension dim : secondaryDimensions){
			if(dim.getName().equalsIgnoreCase(secondary.getName())){
				this.secondaryDimensions.remove(dim);
			}
		}
		this.secondaryDimensions.add(secondary);
		
	}

	/**
	 * 
	 * @return level
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set's level. 
	 * 
	 * @param level
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	@Override
	public ConditionalFormat clone(){
		ConditionalFormat temp = null;
		
		try {
			
			temp = (ConditionalFormat) super.clone();
			
			
			if ( secondaryDimensions != null ) {
				
				List<ConditionalFormatDimension> pafDimSpecList = new ArrayList<ConditionalFormatDimension>();
				
				for (ConditionalFormatDimension pafDimSpec : secondaryDimensions) {
					
					if ( pafDimSpec != null ) {
						
						ConditionalFormatDimension clonedPafDimSpec = (ConditionalFormatDimension) pafDimSpec.clone();
						
						pafDimSpecList.add(clonedPafDimSpec);
						
					}
					
				}
				
				if ( pafDimSpecList.size() > 0 ) {
					
					temp.setSecondaryDimensions(pafDimSpecList);
					
				} else {
					
					temp.setSecondaryDimensions(null);
					
				}
				
			}
			
		} catch (Exception e) {
			//can't happen if implements cloneable
			//logger.warn(e.getMessage());
		}
		
		return temp;
	}

}
