/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.base.format;

public class IconStyle extends AbstractPaceConditionalStyle
{ 
	public IconStyle() {
		super();
	}
	
	public IconStyle(String guid) {
		super(guid);
		// TODO Auto-generated constructor stub
	}

	private IconCriteria criteriaOne;
	private IconCriteria criteriaTwo;
	private IconCriteria criteriaThree;
	private IconCriteria criteriaFour;
	private IconCriteria criteriaFive;
	private IconStyleType iconStyle;
	
	public IconStyleType getIconStyle(){
		return this.iconStyle;
	}
	
	public void setIconStyle(IconStyleType iconStyle){
		this.iconStyle = iconStyle;
	}
	
	public IconCriteria getCriteriaOne(){
		return this.criteriaOne;
	}
	
	public void setCriteriaOne(IconCriteria criteriaOne){
		this.criteriaOne = criteriaOne;
	}
	
	public IconCriteria getCriteriaTwo(){
		return this.criteriaTwo;
	}
	
	public void setCriteriaTwo(IconCriteria criteriaTwo){
		this.criteriaTwo = criteriaTwo;
	}
	
	public IconCriteria getCriteriaThree(){
		return this.criteriaThree;
	}
	
	public void setCriteriaThree(IconCriteria criteriaThree){
		this.criteriaThree = criteriaThree;
	}
	
	public IconCriteria getCriteriaFour(){
		return this.criteriaFour;
	}
	
	public void setCriteriaFour(IconCriteria criteriaFour){
		this.criteriaFour = criteriaFour;
	}
	
	public IconCriteria getCriteriaFive() {
		return criteriaFive;
	}

	public void setCriteriaFive(IconCriteria criteriaFive) {
		this.criteriaFive = criteriaFive;
	}
	
	@Override
	public String toString(){
		return "";
	}
	
	@Override
	public IconStyle clone(){
		IconStyle cloneIcon = null;
		
		try {
			cloneIcon = (IconStyle)super.clone();
			
			if(this.criteriaOne != null){
				cloneIcon.criteriaOne = this.criteriaOne.clone();
			}
			if(this.criteriaTwo != null){
				cloneIcon.criteriaTwo = this.criteriaTwo.clone();
			}
			if(this.criteriaThree != null){
				cloneIcon.criteriaThree = this.criteriaThree.clone();
			}
			if(this.criteriaFour != null){
				cloneIcon.criteriaFour = this.criteriaFour.clone();
			}
			if(this.criteriaFive != null){
				cloneIcon.criteriaFive = this.criteriaFive.clone();
			}
		} catch (Exception e) {

		}		
		
		return cloneIcon;
	}
}