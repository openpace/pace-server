/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.base.format;

public class ColorScale extends AbstractPaceConditionalStyle
{ 
	public ColorScale() {
		super();
	}
	
	public ColorScale(String guid) {
		super(guid);
		// TODO Auto-generated constructor stub
	}

	private ColorScaleColorNumType scaleType;
	private ColorScaleCriteria minimum;
	private ColorScaleCriteria midpoint;
	private ColorScaleCriteria maximum;
	
	public ColorScaleCriteria getMinimum(){
		return this.minimum;
	}
	
	public void setMinimum(ColorScaleCriteria minimum){
		this.minimum = minimum;
	}

	public ColorScaleCriteria getMaximum(){
		return this.maximum;
	}
	
	public void setMaximum(ColorScaleCriteria maximum){
		this.maximum = maximum;
	}
	
	public ColorScaleCriteria getMidpoint(){
		return this.midpoint;
	}
	
	public void setMidpoint(ColorScaleCriteria midpoint){
		this.midpoint = midpoint;
	}
	
	public ColorScaleColorNumType getScaleType(){
		return this.scaleType;
	}
	
	public void setScaleType(ColorScaleColorNumType scaleType){
		this.scaleType = scaleType;
	}
	
	@Override
	public String toString(){
		return "";
	}
	
	@Override
	public ColorScale clone(){
		ColorScale temp = null;
		
		try {
			
			temp = (ColorScale) super.clone();
			
			if(minimum != null){
				temp.minimum = this.minimum.clone();
			}
			if(maximum != null){
				temp.maximum = this.maximum.clone();
			}
			if(midpoint != null){
				temp.midpoint = this.midpoint.clone();
			}
			
		} catch (Exception e) {
			//can't happen if implements cloneable
			//logger.warn(e.getMessage());
		}
		
		return temp;
	}
}