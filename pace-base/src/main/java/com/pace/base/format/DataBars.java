/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.base.format;

public class DataBars extends AbstractPaceConditionalStyle 
{ 
	public DataBars() {
		super();
	}

	public DataBars(String guid) {
		super(guid);
		// TODO Auto-generated constructor stub
	}

	private DataBarCriteria minimum;
	private DataBarCriteria maximum;
	private DataBarAppearance appearance;
	
	public DataBarCriteria getMinimum(){
		return this.minimum;
	}
	
	public void setMinimum(DataBarCriteria minimum){
		this.minimum = minimum;
	}
	
	public DataBarCriteria getMaximum(){
		return this.maximum;
	}
	
	public void setMaximum(DataBarCriteria maximum){
		this.maximum = maximum;
	}
	
	public DataBarAppearance getAppearance(){
		return this.appearance;
	}
	
	public void setAppearance(DataBarAppearance appearance){
		this.appearance = appearance;
	}
	
	@Override
	public String toString(){
		return "";
	}
	
	@Override
	public DataBars clone(){
		DataBars cloneBar = null;
		
		try {
			cloneBar = (DataBars)super.clone();
			
			if(minimum != null){
				cloneBar.minimum = this.minimum.clone();
			}
			if(maximum != null){
				cloneBar.maximum = this.maximum.clone();
			}
			if(appearance != null){
				cloneBar.appearance = this.appearance.clone();
			}
		} catch (Exception e) {

		}
		
		return cloneBar;
	}
}