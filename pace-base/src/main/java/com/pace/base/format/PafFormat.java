/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.format;

/**
 * Paf format.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class PafFormat {

	//format name
	private String formatName;

	/**
	 * Creates instance of PafFormat
	 *
	 */
	public PafFormat() {		
	}
	
	/**
	 * Creates a PafFormat
	 * 
	 * @param formatName
	 */
	public PafFormat(String formatName) {
		this.formatName = formatName;
	}
	
	/**
	 * 
	 * @return format name
	 */
	public String getFormatName() {
		return formatName;
	}

	/**
	 * Set's a format name.
	 * 
	 * @param formatName set the format name
	 */
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((formatName == null) ? 0 : formatName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PafFormat other = (PafFormat) obj;
		if (formatName == null) {
			if (other.formatName != null)
				return false;
		} else if (!formatName.equals(other.formatName))
			return false;
		return true;
	}
}
