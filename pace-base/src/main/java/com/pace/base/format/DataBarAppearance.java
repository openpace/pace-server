/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.base.format;

public class DataBarAppearance 
{ 
	private String fillHexColor;
	private String borderHexColor;
	private DataBarAppearanceBarDirectionType barDirection;
	private DataBarAppearanceBorderType borderType;
	private DataBarAppearanceFillType fillType;
	
	public String getFillHexColor(){
		return this.fillHexColor;
	}
	
	public void setFillHexColor(String fillHexColor){
		this.fillHexColor = fillHexColor;
	}
	
	public String getBorderHexColor(){
		return this.borderHexColor;
	}
	
	public void setBorderHexColor(String borderHexColor){
		this.borderHexColor = borderHexColor;
	}
	
	public DataBarAppearanceBarDirectionType getBarDirection(){
		return this.barDirection;
	}
	
	public void setBarDirection(DataBarAppearanceBarDirectionType barDirection){
		this.barDirection = barDirection;
	}
	
	public DataBarAppearanceBorderType getBorderType(){
		return this.borderType;
	}
	
	public void setBorderType(DataBarAppearanceBorderType borderType){
		this.borderType = borderType;
	}
	
	public DataBarAppearanceFillType getFillType(){
		return this.fillType;
	}
	
	public void setFillType(DataBarAppearanceFillType fillType){
		this.fillType = fillType;
	}
	
	@Override
	public String toString(){
		return "";
	}
	
	@Override
	public DataBarAppearance clone(){
		DataBarAppearance cloneCrit = null;
		
		try {
			cloneCrit = (DataBarAppearance)super.clone();
		} catch (CloneNotSupportedException e) {

		}
		
		return cloneCrit;
	}
}