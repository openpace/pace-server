/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.format;

/**
 * 
 * Used for Generation Formatting.
 * 
 * @author JavaJ
 * 
 */
public class GenFormat extends PafFormat {

	private Integer generation;

	/**
	 * Empty Constructor
	 * 
	 */
	public GenFormat() {
	}

	/**
	 * Creates a Level Format. A level format includes a level number and
	 * formatName.
	 * 
	 * @param level
	 * @param formatName
	 */
	public GenFormat(Integer generation, String formatName) {

		super(formatName);
		this.generation = generation;		

	}

	/**
	 * @return the generation
	 */
	public Integer getGeneration() {
		return generation;
	}

	/**
	 * @param generation
	 *            the generation to set
	 */
	public void setGeneration(Integer generation) {
		this.generation = generation;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((generation == null) ? 0 : generation.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenFormat other = (GenFormat) obj;
		if (generation == null) {
			if (other.generation != null)
				return false;
		} else if (!generation.equals(other.generation))
			return false;
		return true;
	}
	
}
