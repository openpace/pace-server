/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.base.format;

public class DataBarCriteria implements IConditionalStyleCriteria, Cloneable
{ 
	private double criteriaValue;
	private CriteriaType criteriaType;
	private CriteriaTypeValues criteriaTypeValues;

	@Override
	public double getCriteriaValue() {
		return criteriaValue;
	}

	@Override
	public void setCriteriaValue(double criteriaValue) {
		this.criteriaValue = criteriaValue;
	}

	@Override
	public CriteriaType getCriteriaType() {
		return criteriaType;
	}

	@Override
	public void setCriteriaType(CriteriaType criteriaType) {
		this.criteriaType = criteriaType;
	}
	
	@Override
	public String toString(){
		return "";
	}

	@Override
	public CriteriaTypeValues getCriteriaTypeValue() {
		return this.criteriaTypeValues;
	}

	@Override
	public void setCriteriaTypeValue(CriteriaTypeValues typeval) {
		// TODO Auto-generated method stub
		this.criteriaTypeValues = typeval;
	}
	
	@Override
	public DataBarCriteria clone(){
		DataBarCriteria cloneCrit = null;
		
		try {
			cloneCrit = (DataBarCriteria)super.clone();
		} catch (CloneNotSupportedException e) {

		}
		
		return cloneCrit;
	}
}