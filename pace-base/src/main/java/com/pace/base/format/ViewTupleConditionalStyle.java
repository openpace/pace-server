/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.format;

import com.pace.base.mdb.PafDimTree.LevelGenType;

public class ViewTupleConditionalStyle {
	
	private String styleName;
	private Integer styleIndex;
	private LevelGenType indexType;
	private boolean isPrimary;
	
	public ViewTupleConditionalStyle() {
		super();
		this.styleIndex = null;
		this.indexType = null;
	}
	
	public ViewTupleConditionalStyle(String styleName, Integer styleIndex, LevelGenType type, boolean isPrimary) {
		super();
		this.styleName = styleName;
		this.styleIndex = styleIndex;
		this.indexType = type;
		this.isPrimary = isPrimary;
	}
	
	/**
	  * Copy constructor.
	  */
	public ViewTupleConditionalStyle(ViewTupleConditionalStyle style) {
		this(style.getStyleName(), style.getStyleIndex(), style.getIndexType(), style.isPrimary());
	}

	
	public String getStyleName() {
		return styleName;
	}
	public Integer getStyleIndex() {
		return styleIndex;
	}
	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}
	public void setStyleIndex(Integer styleIndex) {
		this.styleIndex = styleIndex;
	}
	public LevelGenType getIndexType() {
		return indexType;
	}
	public void setIndexType(LevelGenType indexType) {
		this.indexType = indexType;
	}
	public boolean isPrimary() {
		return isPrimary;
	}
	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}
}
