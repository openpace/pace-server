/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.funcs;

import java.util.Set;

import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.state.IPafEvalState;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jim
 *
 */
public class ParsedFunction extends AbstractFunction {

	public ParsedFunction(String functionString) {
		this.parseParameters(functionString);
	}
	
	/* (non-Javadoc)
	 * @see com.pace.base.funcs.AbstractFunction#calculate(com.pace.base.data.Intersection, com.pace.base.data.IPafRoDataCache, com.pace.base.state.IPafEvalState, java.lang.String[])
	 */
	@Override
	public double calculate(Intersection sourceIs, IPafDataCache dataCache,
			IPafEvalState evalState) throws PafException {
		throw new PafException("Can't calculate a Parsed Function. This function has not been converted into a concrete funtion.", PafErrSeverity.Warning);
	}

	@Override
	public Set<Intersection> getTriggerIntersections(IPafEvalState evalState) throws PafException {
		// TODO Auto-generated method stub
		return null;
	}


}
