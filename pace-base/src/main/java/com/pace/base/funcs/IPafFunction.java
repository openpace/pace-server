/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.funcs;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.rules.Formula;
import com.pace.base.state.EvalState;
import com.pace.base.state.IPafEvalState;

public interface IPafFunction  {
    public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException, PafInvalidIntersectionException;
    public Set<Intersection> getTriggerIntersections(IPafEvalState evalState) throws PafException;

	public String getOpCode();
	public String getMeasureName();
	public String[] getParms();
	public Map<String, Set<String>> getMemberDependencyMap(IPafEvalState evalState) throws PafException;
    public void parseParameters(String functionString);
    public boolean changeTriggersFormula(Intersection is, IPafEvalState evalState);
	public boolean isRecalcComponent(Intersection is, IPafEvalState evalState);
//    public IPafFunction createFunction(String functionString);
	public List<IPafFunction> getNestedFunctionTerms(EvalState evalState) throws PafException;
    
}
