/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.funcs;

import java.util.Set;

import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.state.IPafEvalState;

/**
 * Abstract class for any member tag function that performs a comparison >, <, !=, etc
 * @author kmoos
 *
 */
public abstract class AbstractMemberTagComparisonFunction extends AbstractMemberTagFunction {

	//protected enum compOpEnum { lessEqual, greatEqual, lessThen, greaterThen, notEqual, equal }
	protected enum compOpEnum { notEqual, equal }
	protected String compareTo;
	protected compOpEnum operator;
	
	@Override
	public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException {
		
		//Validate the set of parms
		validateParms(evalState);
		
		Intersection mbrTagIs = getMemberTagIntersection(sourceIs, evalState, 2);
		
		//Return the data (if available) for the current member tag intersection.
		String s = getMemberTagStringData(mbrTagIs);
		
		if(s != null && logger.isDebugEnabled()){
			logger.debug(String.format("Intersection: %s, found member tag value: %s", mbrTagIs, s));
		}
		
		if(s == null){
			return 0;
		}
		
		boolean equals = compareTo.equals(s);
		
		switch(operator){
			case notEqual:
				return equals ? 0 : 1;
			case equal:
				return equals ? 1 : 0;
		}	
		
		return 0;
	}
	
	@Override
	public Set<Intersection> getTriggerIntersections(IPafEvalState evalState)
			throws PafException {
		
		return evalState.getCurrentChangedCells();
	}
	
	@Override
	public void validateParms(IPafEvalState evalState) throws PafException {
		super.validateParms(evalState);
		
		String errMsg = "Error in [" + this.getClass().getName() + "] - ";
		
		if(!this.isValidated){
			return;
		}
		
		this.isValidated = false;
		
		if ( parms.length == 1 ){
			throw new PafException(errMsg, PafErrSeverity.Error);
        } else {
        	compareTo = parms[1];
        }
		
    	this.isValidated = true;
	}	
}