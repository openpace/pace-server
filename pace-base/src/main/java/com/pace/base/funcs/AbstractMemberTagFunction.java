/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.funcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.data.Intersection;
import com.pace.base.data.IntersectionUtil;
import com.pace.base.db.membertags.MemberTagData;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.db.membertags.PafMemberTagManager;
import com.pace.base.db.membertags.SimpleMemberTagId;
import com.pace.base.mdb.PafDimMember;
import com.pace.base.mdb.PafDimTree;
import com.pace.base.state.IPafEvalState;
import com.pace.base.state.PafClientState;
import com.pace.base.utility.StringUtils;

/**
 * Base member tag function.  All member tag functions should extend this base class.
 * @author kmoos
 *
 */
public abstract class AbstractMemberTagFunction extends AbstractFunction {

	protected static Logger logger = Logger.getLogger(AbstractMemberTagFunction.class);
	protected String memberTagName;
	protected MemberTagDef memberTagDef;
	protected PafMemberTagManager memberTagManager;
	protected String applicationName;
	protected Intersection memberTagIs;
	private Map<Intersection, MemberTagData> memberTagDataMap;
	private Map<SimpleMemberTagId,Map<Intersection, MemberTagData>> memberTagAppDataMap;
	private List<String> functionDims = null;
	
	protected AbstractMemberTagFunction() {
		try {
			memberTagManager = PafMemberTagManager.getInstance();
		} catch (PafException e) {
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * Takes the source intersection, applies any overrides then converts the intersection to a member
	 * tag intersection.
	 * @param sourceIs 
	 * @param evalState
	 * @return
	 * @throws PafException
	 */
	protected Intersection getMemberTagIntersection(Intersection sourceIs, IPafEvalState evalState, int overrideStartPos) throws PafException{
		
		//Check for Intersection override and set if necessary
		String[] parms2 = null;
		if(parms.length > 1){
			parms2 = Arrays.copyOfRange(parms, overrideStartPos, parms.length);
		}
		
		//Build the intersection, using overrides if necessary
		Intersection overrideIs = parseOverrideDimParms(sourceIs, evalState, parms2);

		// Clone current intersection
		Intersection cloneSourceIs = sourceIs.clone();
		
		// Update coordinates with the dimension member override values
		for (int i = 0; i < overrideIs.getDimensions().length; i++) {
			cloneSourceIs.setCoordinate(overrideIs.getDimensions()[i], overrideIs.getCoordinates()[i]);
		}
		
		
		//Build a partial intersection for the current member tag intersection
		Intersection mbrTagIs = new Intersection(memberTagDef.getDims());
		for(String d : memberTagDef.getDims()){
			mbrTagIs.setCoordinate(d, cloneSourceIs.getCoordinate(d));
		}
		
		return mbrTagIs;
	}
	
	/**
	 * Gets double value of the member tag.  If the member tag value is not a double, then null is returned.
	 * @param memberTagIs
	 * @return
	 * @throws PafException
	 */
	protected Double getMemberTagData(Intersection memberTagIs) throws PafException {
		Session session = null;
		
		try {
			// Open a new hibernate session
			session = PafMemberTagManager.getSession();
			
			loadMemberTagDataMap(session);
			
			List<MemberTagData> memberTagData = getMemberTagIntersectionData(memberTagIs, session);
			
			
			Double data = null;
			if(memberTagData != null && memberTagData.size() > 0){
				String mtData = memberTagData.get(0).getSimpleMemberTagData().getData();
				try {
					data = Double.parseDouble(mtData);
				} catch(Exception e){
					//Not valid, just return default (0.00)
				}
				
			}

			return data;
			
		}  finally {
			// Terminate hibernate session
			PafMemberTagManager.terminateSession(session);
		} 
	}
	
	/**
	 * Gets the string value of the member tag.
	 * @param memberTagIs
	 * @return
	 * @throws PafException
	 */
	protected String getMemberTagStringData(Intersection memberTagIs) throws PafException {
		Session session = null;
		
		try {
			// Open a new hibernate session
			session = PafMemberTagManager.getSession();
			
			loadMemberTagDataMap(session);
			
			List<MemberTagData> memberTagData = getMemberTagIntersectionData(memberTagIs, session);
			
			if(memberTagData != null && memberTagData.size() > 0){
				return memberTagData.get(0).getSimpleMemberTagData().getData();
			}

			return null;
			
		}  finally {
			// Terminate hibernate session
			PafMemberTagManager.terminateSession(session);
		} 
	}
	
	/**
	 * Loads the initial member tag map / cache.
	 * @param session
	 * @throws PafException
	 */
	private void loadMemberTagDataMap(Session session) throws PafException {
		
		if(memberTagDataMap == null) {
			memberTagDataMap = memberTagManager.getMemberTagMap(applicationName, memberTagName, session);
			memberTagAppDataMap = new HashMap<SimpleMemberTagId,Map<Intersection, MemberTagData>>(2); 
			if (memberTagDataMap != null){

				SimpleMemberTagId simpleMTId = new SimpleMemberTagId(applicationName, memberTagName);

				if ( ! memberTagAppDataMap.containsKey(simpleMTId)){
					memberTagAppDataMap.put(simpleMTId, memberTagDataMap);
				}
			}
		}
	}
	
	/**
	 * Gets the MemberTagData for the specified Intersection.
	 * @param memberTagIs
	 * @param session
	 * @return
	 * @throws PafException
	 */
	private List<MemberTagData> getMemberTagIntersectionData(Intersection memberTagIs, Session session) throws PafException {
		
		List<Intersection> intersections = new ArrayList<Intersection>(1);
		intersections.add(memberTagIs);

		MemberTagData[] tempData = memberTagManager.getMemberTagData(
				applicationName, 
				memberTagName, 
				IntersectionUtil.convertIntersectionsToSimpleCoordList(intersections), 
				session,
				true,
				memberTagAppDataMap);
		
		return new ArrayList<MemberTagData>(Arrays.asList(tempData));

	}
	
	
	public void validateParms(IPafEvalState evalState) throws PafException {
		
		String errMsg = "Error in [" + this.getClass().getName() + "] - ";
		
		applicationName = evalState.getClientState().getApp().getAppId();
		
		if ( parms.length == 0 ){
			throw new PafException(errMsg, PafErrSeverity.Error);
        } else {
        	memberTagName = parms[0];
        	memberTagDef = evalState.getClientState().getApp().getMemberTagDefs().get(memberTagName);
        	if(memberTagDef == null){
        		throw new PafException(String.format("Member Tag: %s cannot be found.", memberTagName), PafErrSeverity.Error);
        	}
        }
		
    	this.isValidated = true;
	}
	
	/**
	 * 
	 * @param sourceIs
	 * @param evalState
	 * @param parms
	 * @return
	 */
	protected Intersection parseOverrideDimParms(Intersection sourceIs, IPafEvalState evalState, String[] parms){

		PafClientState clientState = evalState.getClientState();
		List<String> allDims = clientState.getUowTrees().getAllDimensions();
		Properties tokenCatalog = clientState.getTokenCatalog();
		
		try {
			
			if(parms == null || parms.length < 2){
				return new Intersection(new String[0], new String[0]);
			}
			
			int dimCount = parms.length / 2;
			functionDims = new ArrayList<String>();
			List<String> dims = new ArrayList<String>(), members = new ArrayList<String>();
			for (int parmInx = 0; parmInx < dimCount; parmInx++) {
				
				// Parse dimension and member specification string
				String dim = parms[parmInx*2];
				if (!allDims.contains(dim)) {
					String errMsg = String.format("@CrossDim signature error - an invalid dimension name of [%s] was specified", dim); 
					logger.error(errMsg);
					throw new IllegalArgumentException(errMsg);					
				}
				
				String memberSpec = parms[parmInx*2+1];
				
				// Skip this set of parameters if no source intersection was supplied and a 
				// member token is used
				if (sourceIs == null && memberSpec.startsWith(PafBaseConstants.FUNC_TOKEN_START_CHAR)) {
					continue;
				}
				
				// Add override parms
				dims.add(dim);
				String currMember = null;
				if (sourceIs != null) {
					currMember = sourceIs.getCoordinate(dim);
				}
				members.add(resolveMemberSpec(dim, memberSpec, clientState, currMember, tokenCatalog));
			}
			return new Intersection(dims.toArray(new String[0]), members.toArray(new String[0]));
			
		} catch (RuntimeException e) {
			// Check for an invalid number of parameters
			String errMsg = null;
			if (parms.length == 0 || parms.length % 2 != 0) {
				errMsg = "AbstractMemberTagFunction signature error - an invalid number of parameters were specified";
				logger.error(errMsg);
				throw new IllegalArgumentException(errMsg);
			} else {
				throw e;
			}

		}
	}
	
	/**
	 *  Resolve member specification
	 *
	 * @param dimension Dimension name
	 * @param memberSpec Member specification
	 * @param clientState ClientState
	 * @param currMbrName Current intersection member
	 * @param tokenCatalog Token catalog
	 * 
	 * @return Member name
	 */
	private String resolveMemberSpec(String dimension, String memberSpec, PafClientState clientState, String currMbrName, Properties tokenCatalog) {
		
		// If not a token, then just return member name (original membSpec value)
		if (!memberSpec.startsWith(PafBaseConstants.FUNC_TOKEN_START_CHAR)) {
			return memberSpec;
		}
		
		String resolvedMemberSpec = null, key = null, validDim = null;
		String timeDim = clientState.getMdbDef().getTimeDim();
		String yearDim = clientState.getMdbDef().getYearDim();
		

		// Get dimension tree and current member
		PafDimTree dimTree = clientState.getUowTrees().getTree(dimension);
		PafDimMember currMember = dimTree.getMember(currMbrName);
		
		// Check for PARENT token
		if (memberSpec.equalsIgnoreCase(PafBaseConstants.FUNC_TOKEN_PARENT)) {
			if (currMember == dimTree.getRootNode()) {
				// Return current member name if current member is root of tree
				resolvedMemberSpec = currMember.getKey();
			} else {
				// Else return name of parent
				resolvedMemberSpec = currMember.getParent().getKey();				
			}
			functionDims.add(dimension);
			return resolvedMemberSpec;
		}
			
		// Check for UOWROOT token
		if (memberSpec.equalsIgnoreCase(PafBaseConstants.FUNC_TOKEN_UOWROOT)) {
			// Return name of root node
			resolvedMemberSpec = dimTree.getRootNode().getKey();
			functionDims.add(dimension);
			return resolvedMemberSpec;
		}
		
		// Check for FIRST_PLAN_YEAR token (TTN-1597)
		key = PafBaseConstants.FUNC_TOKEN_FIRST_PLAN_YEAR; 
		validDim = yearDim;
		if (memberSpec.equalsIgnoreCase(key)) {
			if (dimension.equals(validDim)) {
				// Return token value
				resolvedMemberSpec = StringUtils.removeDoubleQuotes(tokenCatalog.getProperty(key));
				functionDims.add(dimension);
				return resolvedMemberSpec;
			} else {
				// Return error - token only valid on year dimension
				String errMsg = "@CrossDim signature error - the token: " + key
						+ " is only valid on the [" + validDim + "] dimension";
				logger.error(errMsg);
				throw new IllegalArgumentException(errMsg);
			}
		}
		
		// Check for FIRST_NONPLAN_YEAR token (TTN-1597)
		key = PafBaseConstants.FUNC_TOKEN_FIRST_NONPLAN_YEAR; 
		validDim = yearDim;
		if (memberSpec.equalsIgnoreCase(key)) {
			if (dimension.equals(validDim)) {
				// Return token value
				resolvedMemberSpec = StringUtils.removeDoubleQuotes(tokenCatalog.getProperty(key));
				functionDims.add(dimension);
				return resolvedMemberSpec;
			} else {
				// Return error - token only valid on year dimension
				String errMsg = "@CrossDim signature error - the token: " + key
						+ " is only valid on the [" + validDim + "] dimension";
				logger.error(errMsg);
				throw new IllegalArgumentException(errMsg);
			}
		}
		
		// Check for FIRST_PLAN_PERIOD token (TTN-1597)
		key = PafBaseConstants.FUNC_TOKEN_FIRST_PLAN_PERIOD; 
		validDim = timeDim;
		if (memberSpec.equalsIgnoreCase(key)) {
			if (dimension.equals(validDim)) {
				// Return token value
				resolvedMemberSpec = StringUtils.removeDoubleQuotes(tokenCatalog.getProperty(key));
				functionDims.add(dimension);
				return resolvedMemberSpec;
			} else {
				// Return error - token only valid on year dimension
				String errMsg = "@CrossDim signature error - the token: " + key
						+ " is only valid on the [" + validDim + "] dimension";
				logger.error(errMsg);
				throw new IllegalArgumentException(errMsg);
			}
		}
		
		// Invalid token	
		String errMsg = "@CrossDim signature error - invalid token specified";
		logger.error(errMsg);
		throw new IllegalArgumentException(errMsg);
		
	}
}