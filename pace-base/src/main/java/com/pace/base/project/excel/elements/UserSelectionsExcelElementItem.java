/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project.excel.elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.pace.base.PafException;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PafDimSpec;
import com.pace.base.project.ExcelPaceProjectConstants;
import com.pace.base.project.ExcelProjectDataErrorException;
import com.pace.base.project.PaceProjectReadException;
import com.pace.base.project.PaceProjectWriteException;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.excel.IExcelDynamicReferenceElementItem;
import com.pace.base.project.excel.PafExcelInput;
import com.pace.base.project.excel.PafExcelRow;
import com.pace.base.project.excel.PafExcelValueObject;
import com.pace.base.project.utils.PafExcelUtil;
import com.pace.base.view.PafUserSelection;

/**
 * Reads/writes user selections from/to an Excel 2007 workbook.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class UserSelectionsExcelElementItem<T extends List<PafUserSelection>> extends PafExcelElementItem<T> {

	/**
	 * Creates an excel element item.
	 * 
	 * @param workbook workbook used for read/write
	 */
	public UserSelectionsExcelElementItem(Workbook workbook) {
		super(workbook);
	}
	
	/**
	 * Creates an excel element item. Mainly used for writing user selections.
	 * 
	 * @param workbook workbook used for read/write
	 * @param isCellReferencingEnabled true if cell referencing is enabled
	 */
	public UserSelectionsExcelElementItem(Workbook workbook,
			boolean isCellReferencingEnabled) {
		super(workbook, isCellReferencingEnabled);
	
	}	

	@Override
	protected void createHeaderListMapEntries() {

		getHeaderListMap().put(getSheetName(), Arrays.asList("name", "dimension", "allow multiple selection", "display string", "prompt string", "parent first", "level rt click", "gen rt click", "selection specificatons"));
		
	}

	@Override
	public ProjectElementId getProjectElementId() {
		return ProjectElementId.UserSelections;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T readExcelSheet() throws PaceProjectReadException, PafException {
		
		PafExcelInput input = new PafExcelInput.Builder(getWorkbook(), getSheetName(), getHeaderListMap().get(getSheetName()).size())
			.headerListMap(getHeaderListMap())
			.excludeHeaderRows(true)
			.excludeEmptyRows(true)
			.sheetRequired(true)
			.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
			.multiDataRow(true)
			.build();

		List<PafExcelRow> excelRowList = PafExcelUtil.readExcelSheet(input);
		
		List<PafUserSelection> userSelectionList = new ArrayList<PafUserSelection>();
				
		PafUserSelection userSel = null;
		
		for ( PafExcelRow row : excelRowList ) {
		
			userSel = new PafUserSelection();
			
			for ( Integer rowIndex : row.getRowItemOrderedIndexes()) {
			
				List<PafExcelValueObject> rowItemList = row.getRowItem(rowIndex);
				
				PafExcelValueObject firstValueObject = rowItemList.get(0);
			
				try {
				
				switch (rowIndex) {
				
					//name
					case 0:											
					
						userSel.setId(PafExcelUtil.getString(getProjectElementId(), firstValueObject, true));
						break;
					
					//dimension
					case 1:						
					
						userSel.setDimension(PafExcelUtil.getString(getProjectElementId(), firstValueObject, true));
						break;
					
					//allow multiple selection
					case 2:	
					
						userSel.setMultiples(PafExcelUtil.getBoolean(getProjectElementId(), firstValueObject, true));
						break;

					//display string
					case 3:		
					
						userSel.setDisplayString(PafExcelUtil.getString(getProjectElementId(), firstValueObject));
						break;
					
					//prompt string
					case 4:		
					
						userSel.setPromptString(PafExcelUtil.getString(getProjectElementId(), firstValueObject));
						break;
					//Parent First
					case 5:		
					
						userSel.setParentLast(!PafExcelUtil.getBoolean(getProjectElementId(), firstValueObject));
						break;
					//Level Right Click
					case 6:		
					
						userSel.setLevelRightClick(PafExcelUtil.getBoolean(getProjectElementId(), firstValueObject));
						break;
					//Generation Right Click
					case 7:		
					
						userSel.setGenerationRightClick(PafExcelUtil.getBoolean(getProjectElementId(), firstValueObject));
						break;
					//selection specificatons
					case 8:		
					
						List<String> specList = new ArrayList<String>();
						boolean specsReadError = false;
						for ( PafExcelValueObject spec : rowItemList ) {
							try {
								if( ! spec.isBlank() ) {
									specList.add(PafExcelUtil.getString(getProjectElementId(), spec));
								}
								else {
									if( ! userSel.getDimension().equals("Version") ) {
										specList.add("@IDESC(@UOW_ROOT)");
									}
								}
							} catch (ExcelProjectDataErrorException epdee) {
								addProjectDataErrorToList(epdee.getProjectDataError());
								specsReadError = true;
							}
						}
						if ( ! specsReadError && specList.size() > 0 ) {
							PafDimSpec spec = new PafDimSpec();
							spec.setDimension(userSel.getDimension());
							spec.setExpressionList(specList.toArray(new String[0]));
							userSel.setSpecification(spec);
						}
						break;
					}
				
				} catch (ExcelProjectDataErrorException epdee) {
					
					addProjectDataErrorToList(epdee.getProjectDataError());
					
				}
			
			
			}
		
		userSelectionList.add(userSel);
		
		}
		
		return (T) userSelectionList;
		
	}

	@Override
	protected void writeExcelSheet(T t) throws PaceProjectWriteException, PafException {

		Map<String, String> dimensionRefMap = null;
		
		//if cell referencing is enabled, read in global styles
		if ( isCellReferencingEnabled() ) {			
			
			//dimension name refs
			IExcelDynamicReferenceElementItem appDefElementItem = new ApplicationDefExcelElementItem<List<PafApplicationDef>>(getWorkbook());
			dimensionRefMap = appDefElementItem.getDynamicReferenceMap();
			
		}
		
		PafExcelInput input = new PafExcelInput.Builder(getWorkbook(), getSheetName(), getHeaderListMap().get(getSheetName()).size())
			.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
			.build();	
	
		List<PafUserSelection> userSelectionList = t;
		
		List<PafExcelRow> excelRowList = new ArrayList<PafExcelRow>();
	
		//create and add header to list
		excelRowList.add(PafExcelUtil.createHeaderRow(getHeaderListMap().get(getSheetName())));
		
		if ( userSelectionList != null ) {
			
			for ( PafUserSelection userSelection : userSelectionList ) {
				
				PafExcelRow excelRow = new PafExcelRow();
				
				//name
				excelRow.addRowItem(0, PafExcelValueObject.createFromString(userSelection.getId()));
				
				//dimension
				excelRow.addRowItem(1, PafExcelValueObject.createFromFormulaReferenceMap(userSelection.getDimension(), dimensionRefMap));
								
				//allow multiple selection
				excelRow.addRowItem(2, PafExcelValueObject.createFromBoolean(userSelection.isMultiples()));				
				
				//display string
				excelRow.addRowItem(3, PafExcelValueObject.createFromString(userSelection.getDisplayString()));
				
				//prompt string
				excelRow.addRowItem(4, PafExcelValueObject.createFromString(userSelection.getPromptString()));
				
				//parent first
				excelRow.addRowItem(5, PafExcelValueObject.createFromBoolean(!userSelection.isParentLast()));				
				
				//level right click
				excelRow.addRowItem(6, PafExcelValueObject.createFromBoolean(userSelection.isLevelRightClick()));
				
				//generation right click
				excelRow.addRowItem(7, PafExcelValueObject.createFromBoolean(userSelection.isGenerationRightClick()));
				
				//selection specificatons
				PafDimSpec dimSpecs = userSelection.getSpecification();
				if( dimSpecs != null ) {
					String[] specs = dimSpecs.getExpressionList();
					if ( specs != null && specs.length > 0 ) {
						for (String spec : specs ) {
							excelRow.addRowItem(8, PafExcelValueObject.createFromString(spec));
						}
					}
				}
				else {
					if( ! userSelection.getDimension().equals("Version")) {
						excelRow.addRowItem(8, PafExcelValueObject.createFromString("@IDESC(@UOW_ROOT)"));
					}
				}
				
				excelRowList.add(excelRow);			
			}
			
		}	
			
		PafExcelUtil.writeExcelSheet(input, excelRowList);
		
	}	

}
