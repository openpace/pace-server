/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PaceProjectCreationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8890566629348003719L;
	
	
	private Map<ProjectElementId, List<ProjectDataError>> projectCreationErrorMap = null;
	
	public PaceProjectCreationException(
			Map<ProjectElementId, List<ProjectDataError>> projectCreationErrorMap) {
		
		super();
		
		if ( projectCreationErrorMap != null ) {
			
			this.projectCreationErrorMap = new HashMap<ProjectElementId, List<ProjectDataError>>();
			this.projectCreationErrorMap.putAll(projectCreationErrorMap);
			
		}
		
	}
	
	public PaceProjectCreationException(String message) {
		
		super(message);
		
	}

	/**
	 * @return the projectCreationErrorMap
	 */
	public Map<ProjectElementId, List<ProjectDataError>> getProjectCreationErrorMap() {
		return projectCreationErrorMap;
	}

}
