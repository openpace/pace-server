/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project.excel.elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.pace.base.PafException;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PafDimSpec;
import com.pace.base.project.ExcelPaceProjectConstants;
import com.pace.base.project.ExcelProjectDataErrorException;
import com.pace.base.project.PaceProjectReadException;
import com.pace.base.project.PaceProjectWriteException;
import com.pace.base.project.ProjectDataError;
import com.pace.base.project.ProjectElementId;
import com.pace.base.project.excel.IExcelDynamicReferenceElementItem;
import com.pace.base.project.excel.PafExcelInput;
import com.pace.base.project.excel.PafExcelRow;
import com.pace.base.project.excel.PafExcelValueObject;
import com.pace.base.project.utils.PafExcelUtil;
import com.pace.base.security.AdminPersistLockDef;
import com.pace.base.view.PafStyle;

/**
 * Reads/writes measures from/to an Excel 2007 workbook.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class AdminLocksExcelElementItem<T extends Map<String, AdminPersistLockDef>> extends
		PafExcelElementItem<T> implements IExcelDynamicReferenceElementItem {

	/**
	 * Creates an excel element item. Mainly used for reading measures.
	 * 
	 * @param workbook workbook used for read/write
	 */
	public AdminLocksExcelElementItem(Workbook workbook) {
		super(workbook);
	}
	
	/**
	 * Creates an excel element item. Mainly used for writing measures.
	 * 
	 * @param workbook workbook used for read/write
	 * @param isCellReferencingEnabled true if cell referencing is enabled
	 */
	public AdminLocksExcelElementItem(Workbook workbook,
			boolean isCellReferencingEnabled) {
		super(workbook, isCellReferencingEnabled);
	}



	@Override
	protected void createHeaderListMapEntries() {

		getHeaderListMap().put(getSheetName(), Arrays.asList("name", "dimension", "expression"));

	}

	@Override
	public ProjectElementId getProjectElementId() {
		return ProjectElementId.AdminLocks;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T readExcelSheet() throws PaceProjectReadException, PafException {
		
		PafExcelInput input = new PafExcelInput.Builder(getWorkbook(), getSheetName(), getHeaderListMap().get(getSheetName()).size())
			.headerListMap(getHeaderListMap())
			.excludeHeaderRows(true)
			.excludeEmptyRows(true)
			.sheetRequired(true)
			.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
			.multiDataRow(true)
			.build();

		List<PafExcelRow> excelRowList = PafExcelUtil.readExcelSheet(input);
		
		Map<String, AdminPersistLockDef> adminLockMap = new HashMap<String, AdminPersistLockDef>();
				
		AdminPersistLockDef adminlock = null;
		
		for ( PafExcelRow row : excelRowList ) {
		
			adminlock = new AdminPersistLockDef();
			List<PafDimSpec> specs = adminlock.getDimSpecList();
			List<PafExcelRow> tempExcelRowList = null;
			
			for ( Integer rowIndex : row.getRowItemOrderedIndexes()) {
			
				List<PafExcelValueObject> rowItemList = row.getRowItem(rowIndex);
				
				PafExcelValueObject firstValueObject = rowItemList.get(0);
				
				switch (rowIndex) {
				
					//id
					case 0:											
						if (firstValueObject.isString()) {
							adminlock.setName(firstValueObject.getString());
						} else if (firstValueObject.isBlank()) {

							addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
									getProjectElementId(), firstValueObject));

						} else {

							addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
									getProjectElementId(), firstValueObject));
						}
						
						tempExcelRowList = new ArrayList<PafExcelRow>(rowItemList.size());

						// create a new row for the number of rows merged into
						// the row
						for (int i = 0; i < rowItemList.size(); i++) {

							tempExcelRowList.add(new PafExcelRow());

						}
						
						break;
					default:

						int tmpRowNdx = 0;

						// populate the temp row with relevant index item from
						// rowItemList
						for (PafExcelRow tempRow : tempExcelRowList) {

							tempRow.addRowItem(rowIndex, rowItemList.get(tmpRowNdx++));

						}

						if(rowIndex < 2){
							break;
						}
						
						// populate over the temp rows to parse out dimension,
						// type, number and global style
						for (PafExcelRow tempRow : tempExcelRowList) {
							
							PafDimSpec spec = new PafDimSpec();
							List<String> expList = new ArrayList<String>();
							
							for (Integer tempRowNdx : tempRow.getRowItemOrderedIndexes()) {

								firstValueObject = tempRow.getRowItem(tempRowNdx).get(0);

								switch (tempRowNdx) {

								// is primary dimension or secondary
								case 1:
									if(firstValueObject.isString()){
										spec.setDimension(firstValueObject.getString());
									}
									break;
								case 2:
									if(firstValueObject.isString()){
										//expList.add(firstValueObject.getString());
										try {
											spec.setExpressionList(PafExcelUtil.getStringArFromDelimValueObject(getProjectElementId(), firstValueObject));
										} catch (ExcelProjectDataErrorException e) {
											addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
													getProjectElementId(), firstValueObject));
										}
									}
								}
							}
							specs.add(spec);
						}
						break;
				}
			}
		
		adminLockMap.put(adminlock.getName(), adminlock);
		
		}
		
		return (T) adminLockMap;
	}

	@Override
	public void writeExcelSheet(T t) throws PaceProjectWriteException, PafException {
		
		Map<String, String> globalStyleDynamicRefMap = null;
		Map<String, String> dimensionRefMap = null;
		
		//if cell referencing is enabled, read in global styles
		if ( isCellReferencingEnabled() ) {			

			//global style refs
			IExcelDynamicReferenceElementItem globalStyleElementItem = new GlobalStylesExcelElementItem<Map<String,PafStyle>>(getWorkbook());
			globalStyleDynamicRefMap = globalStyleElementItem.getDynamicReferenceMap();
			
			//dimension name refs
			IExcelDynamicReferenceElementItem appDefElementItem = new ApplicationDefExcelElementItem<List<PafApplicationDef>>(getWorkbook());
			dimensionRefMap = appDefElementItem.getDynamicReferenceMap();
			
		}
		
		PafExcelInput input = new PafExcelInput.Builder(getWorkbook(), getSheetName(), getHeaderListMap().get(getSheetName()).size())
			.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
			.build();
		
		
		Map<String, AdminPersistLockDef> adminLockMap = t;
		
		List<PafExcelRow> excelRowList = new ArrayList<PafExcelRow>();
	
		//create and add header to list
		excelRowList.add(PafExcelUtil.createHeaderRow(getHeaderListMap().get(getSheetName())));
		
		if ( adminLockMap != null ) {
			
			for ( String adminLockName : adminLockMap.keySet() ) {
				
				AdminPersistLockDef adminLock = adminLockMap.get(adminLockName);
				
				PafExcelRow excelRow = new PafExcelRow();
				excelRow.addRowItem(0, PafExcelValueObject.createFromString(adminLockName));
				
				List<PafDimSpec> specs = adminLock.getDimSpecList();
				if(specs != null){
					
					for(PafDimSpec spec : specs){
						excelRow.addRowItem(1, PafExcelValueObject.createFromString(spec.getDimension()));
						excelRow.addRowItem(2, PafExcelUtil.getDelimValueObjectFromStringAr(spec.getExpressionList()));
					}
				}
				
				excelRowList.add(excelRow);
			}
		}
		
		
					
		PafExcelUtil.writeExcelSheet(input, excelRowList);
		
	}
	
	/**
	 * Creates a dynamic reference map used to reference measure cells.  Key is measure name, value
	 * is the Excel sheet/cell reference.  Example =Measures!$A$1.
	 */
	public Map<String, String> getDynamicReferenceMap() {

		Map<String, String> dynamicRefMap = null;
		
		PafExcelInput input = new PafExcelInput.Builder(this.getWorkbook(), this.getSheetName(), 1)
			.headerListMap(this.getHeaderListMap())
			.excludeHeaderRows(true)
			.excludeEmptyRows(true)
			.multiDataRow(true)
			.startDataReadColumnIndex(0)
			.sheetRequired(false)
			.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
			.build();
	
		try {
			
			dynamicRefMap = PafExcelUtil.createCellReferenceMap(input);
			
		} catch (PafException e) {
									
			logger.warn(ExcelPaceProjectConstants.COULD_NOT_CREATE_THE_REFERENCE_MAP + e.getMessage());
			
		}
		
		return dynamicRefMap;		

	}

}
