/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project;

import java.util.List;

import com.pace.base.project.excel.PafExcelValueObject;

/**
 * @author JavaJ
 *
 */
public class ProjectDataError {

	private ProjectElementId projectElementId;
	
	private String errorSource;
	
	private String errorMessage;
	
	private boolean isFatal = true; // all project errors are fatal by default
	
	
	
	public ProjectDataError(ProjectElementId projectElementId, String errorMessage) {
	
		this.projectElementId = projectElementId;
		this.errorMessage = errorMessage;
		
	}
	
	public ProjectDataError(ProjectElementId projectElementId, String errorMessage, boolean isFatal) {
		
		this.projectElementId = projectElementId;
		this.errorMessage = errorMessage;
		this.isFatal = isFatal;
		
	}
	
	public ProjectDataError(ProjectElementId projectElementId, PafExcelValueObject errorSourceValueObject, String errorMessage) {
		
		this(projectElementId, errorMessage);		
		
		if ( errorSourceValueObject != null ) {
			
			this.errorSource = errorSourceValueObject.getCellAddress();
			
		}
				
		
	}
	
	public ProjectDataError(ProjectElementId projectElementId, String errorSource, String errorMessage) {
		
		this(projectElementId, errorMessage);
		this.errorSource = errorSource;	
				
	}
	
	/**
	 * @return the projectElementId
	 */
	public ProjectElementId getProjectElementId() {
		return projectElementId;
	}

	/**
	 * @param projectElementId the projectElementId to set
	 */
	public void setProjectElementId(ProjectElementId projectElementId) {
		this.projectElementId = projectElementId;
	}

	/**
	 * @return the errorSource
	 */
	public String getErrorSource() {
		return errorSource;
	}

	/**
	 * @param errorSource the errorSource to set
	 */
	public void setErrorSource(String errorSource) {
		this.errorSource = errorSource;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public boolean getIsFatal(){
		return isFatal;
	}
	


public static ProjectDataError createRequiredProjectDataError(ProjectElementId projectElementId, PafExcelValueObject valueObject) {
		
		return new ProjectDataError(projectElementId, valueObject, ExcelPaceProjectConstants.REQUIRED);
		
	}
	
	public static ProjectDataError createInvalidNumberProjectDataError(ProjectElementId projectElementId, PafExcelValueObject valueObject) {
		
		return new ProjectDataError(projectElementId, valueObject, ExcelPaceProjectConstants.INVALID_NUMBER + valueObject.getValueAsString());
		
	}
	
	public static ProjectDataError createInvalidValueProjectDataError(ProjectElementId projectElementId, PafExcelValueObject valueObject) {
		
		return createInvalidValueProjectDataError(projectElementId, valueObject, null);
				
	}
	
	public static ProjectDataError createRequiredValueProjectDataError(ProjectElementId projectElementId, PafExcelValueObject valueObject, String errorMessage) {
		
		return new ProjectDataError(projectElementId, valueObject, errorMessage);
				
	}
	
	public static ProjectDataError createInvalidValueProjectDataError(ProjectElementId projectElementId, PafExcelValueObject valueObject, List<String> validValueList) {
		
		ProjectDataError invalidProjectDataError = null;
		
		if ( validValueList != null && validValueList.size() > 0 ) {
			
			invalidProjectDataError = new ProjectDataError(projectElementId, valueObject, ExcelPaceProjectConstants.INVALID_VALUE + valueObject.getValueAsString() + ExcelPaceProjectConstants.VAILD_OPTIONS_ARE + validValueList);
			
		} else {
		
			invalidProjectDataError = new ProjectDataError(projectElementId, valueObject, ExcelPaceProjectConstants.INVALID_VALUE + valueObject.getValueAsString());
			
		}
		
		return invalidProjectDataError;
		
	}

	public static ProjectDataError createMissingKeyProjectDataError(ProjectElementId projectElementId, PafExcelValueObject valueObject) {
		
		return new ProjectDataError(projectElementId, valueObject, ExcelPaceProjectConstants.MISSING_KEY);
		
	}
	
	public static ProjectDataError createMissingValueProjectDataError(ProjectElementId projectElementId, PafExcelValueObject valueObject) {
		
		return new ProjectDataError(projectElementId, valueObject, ExcelPaceProjectConstants.MISSING_VALUE);
		
	}

	public static ProjectDataError createBooleanProjectDataError(ProjectElementId projectElementId, PafExcelValueObject valueObject) {
		
		return createInvalidValueProjectDataError(projectElementId, valueObject, ExcelPaceProjectConstants.VALID_BOOLEAN_LIST);
		
	}	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder(super.toString() + ":");
		sb.append("\nProject element id: " + projectElementId);
		sb.append("\nProject error source: " + errorSource);
		sb.append("\nProject error message: " + errorMessage + "\n");
				
		return sb.toString();
	}
		
	
}
