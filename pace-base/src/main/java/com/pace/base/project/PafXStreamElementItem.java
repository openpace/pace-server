/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project;

import org.apache.log4j.Logger;

import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafErrHandler;
import com.pace.base.utility.PafXStream;


public class PafXStreamElementItem<T> {

	private static final Logger logger = Logger.getLogger(PafXStreamElementItem.class);
	
	private String fullFileName;
	
	private T data;
	
	public PafXStreamElementItem(String fullFileName) {
		
		this.fullFileName = fullFileName;
		
	}
	
	@SuppressWarnings("unchecked")
	public T read() throws PaceProjectReadException {
		
		//try to import the styles
		try {
									
			logger.debug("About to import: " + fullFileName );
			
			data = (T) PafXStream.importObjectFromXml(fullFileName, true);
									
		} catch (PafConfigFileNotFoundException ex) {
			
			PafErrHandler.handleException(ex);
//			throw new PaceProjectReadException(fullFileName, ex.getMessage());			
			
		} catch (RuntimeException re) {
			
			throw new PaceProjectReadException(fullFileName, re.getMessage());			
			
		} catch (Exception e) {
			
			throw new PaceProjectReadException(fullFileName, e.getMessage());
			
		}
		
		return data;
		
	}
	
	public void write(T t) {
	
		logger.debug("About to export: " + fullFileName );
		
		PafXStream.exportObjectToXml(t, fullFileName);
		
	}
	
}
