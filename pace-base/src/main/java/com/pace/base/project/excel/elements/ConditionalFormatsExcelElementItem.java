/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project.excel.elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.pace.base.PafException;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.format.ConditionalFormat;
import com.pace.base.format.GenFormat;
import com.pace.base.format.LevelFormat;
import com.pace.base.format.MemberFormat;
import com.pace.base.mdb.PafDimTree.LevelGenType;
import com.pace.base.project.*;
import com.pace.base.project.excel.IExcelDynamicReferenceElementItem;
import com.pace.base.project.excel.PafExcelInput;
import com.pace.base.project.excel.PafExcelRow;
import com.pace.base.project.excel.PafExcelValueObject;
import com.pace.base.project.utils.PafExcelUtil;
import com.pace.base.utility.CollectionsUtil;
import com.pace.base.view.ConditionalFormatDimension;
import com.pace.base.view.PafStyle;

/**
 * Reads/writes hierarchy formats from/to an Excel 2007 workbook.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class ConditionalFormatsExcelElementItem<T extends Map<String, ConditionalFormat>> extends PafExcelElementItem<T> {
	
	/**
	 * Creates an excel element item. Mainly used for reading hierarchy formats.
	 * 
	 * @param workbook workbook used for read/write
	 */
	public ConditionalFormatsExcelElementItem(Workbook workbook) {
		super(workbook);
	}
	
	/**
	 * Creates an excel element item. Mainly used for writing hierarchy formats.
	 * 
	 * @param workbook workbook used for read/write
	 * @param isCellReferencingEnabled true if cell referencing is enabled
	 */
	public ConditionalFormatsExcelElementItem(Workbook workbook,
			boolean isCellReferencingEnabled) {
		super(workbook, isCellReferencingEnabled);
	
	}
	
	@Override
	protected void createHeaderListMapEntries() {

		getHeaderListMap().put(getSheetName(), Arrays.asList("name", "primary dimension", "dimension", "type (Level, Generation, Member)", "value", "global style name"));		
		
	}

	@Override
	public ProjectElementId getProjectElementId() {
		
		return ProjectElementId.ConditionalFormats;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T readExcelSheet() throws PaceProjectReadException, PafException {
		
		PafExcelInput input = new PafExcelInput.Builder(getWorkbook(), getSheetName(), getHeaderListMap().get(getSheetName()).size())
			.headerListMap(getHeaderListMap())
			.excludeHeaderRows(true)
			.excludeEmptyRows(true)
			.sheetRequired(true)
			.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
			.build();

		List<PafExcelRow> excelRowList = PafExcelUtil.readExcelSheet(input);
		
		Map<String, ConditionalFormat> conditionalFormatMap = new HashMap<String, ConditionalFormat>();

		ConditionalFormat mapEntry = null;

		for (PafExcelRow row : excelRowList) {

			// temp excel row list, used to unmerge the consolidated row
			List<PafExcelRow> tempExcelRowList = null;

			mapEntry = new ConditionalFormat();
			
			for (Integer rowIndex : row.getRowItemOrderedIndexes()) {

				List<PafExcelValueObject> rowItemList = row
						.getRowItem(rowIndex);

				PafExcelValueObject firstValueObject = rowItemList.get(0);

				switch (rowIndex) {

				// name
				case 0:

					tempExcelRowList = new ArrayList<PafExcelRow>(
							rowItemList.size());

					// create a new row for the number of rows merged into
					// the row
					for (int i = 0; i < rowItemList.size(); i++) {

						tempExcelRowList.add(new PafExcelRow());

					}

					if (firstValueObject.isString()) {

						String hierarchyFormatName = firstValueObject
								.getString();

						String caseInsenstiveKey = CollectionsUtil
								.getCaseSenstiveKeyFromMap(conditionalFormatMap,
										hierarchyFormatName);

						if (caseInsenstiveKey != null) {

							mapEntry = conditionalFormatMap.get(caseInsenstiveKey);

						} else {

							mapEntry.setName(firstValueObject.getString());

						}

					} else if (firstValueObject.isBlank()) {

						addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
								getProjectElementId(), firstValueObject));

					} else {

						addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
								getProjectElementId(), firstValueObject));

					}

					break;

				default:

					int tmpRowNdx = 0;

					// populate the temp row with relevant index item from
					// rowItemList
					for (PafExcelRow tempRow : tempExcelRowList) {

						tempRow.addRowItem(rowIndex, rowItemList.get(tmpRowNdx++));

					}

					if(rowIndex < 5){
						break;
					}

					ConditionalFormatDimension dimension = null;
					LevelGenType lastLevelGen = null;
					LevelFormat levelFormat = null;
					GenFormat genFormat = null;
					MemberFormat memberFormat = null;
					boolean isPrimary = true;

					// populate over the temp rows to parse out dimension,
					// type, number and global style
					for (PafExcelRow tempRow : tempExcelRowList) {

						for (Integer tempRowNdx : tempRow.getRowItemOrderedIndexes()) {

							firstValueObject = tempRow.getRowItem(tempRowNdx).get(0);

							switch (tempRowNdx) {

							// is primary dimension or secondary
							case 1:
								if(firstValueObject.isBoolean()){
									isPrimary = firstValueObject.getBoolean();
								}
								break;
							// dimension name
							case 2:
								if (firstValueObject.isString() || firstValueObject.isFormula()) {
									// clear last dim
									dimension = null;

									String dimName = firstValueObject.getValueAsString();
									ConditionalFormatDimension prim = mapEntry.getPrimaryDimension();
									if(prim != null){
										if(dimName.equalsIgnoreCase(prim.getName())){
											dimension = prim;
										}
									}
									
									if (dimension == null && mapEntry.getSecondaryDimensions() != null) {

										List<ConditionalFormatDimension> secondaries = mapEntry.getSecondaryDimensions();
										for(ConditionalFormatDimension dim : secondaries)
										{
											if(dimName.equalsIgnoreCase(dim.getName())){
												
												dimension = dim;
												break;
											}
										}
									}

									if (dimension == null) {

										dimension = new ConditionalFormatDimension();
										dimension.setName(dimName);
										dimension.setPrimaryDimension(isPrimary);
									}

								} else if (dimension == null
										&& firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));

								} else if (!firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));

								}

								break;
							// type { Level, Generation }
							case 3:

								if (firstValueObject.isString()) {

									String levelGen = firstValueObject
											.getString().toLowerCase();

									if (levelGen.equalsIgnoreCase(ExcelPaceProjectConstants.LEVEL)) {

										lastLevelGen = LevelGenType.LEVEL;

									} else if (levelGen.equalsIgnoreCase(ExcelPaceProjectConstants.GENERATION)) {

										lastLevelGen = LevelGenType.GEN;

									} else if (levelGen.equalsIgnoreCase(ExcelPaceProjectConstants.MEMBER)) {

										lastLevelGen = LevelGenType.MEMBER;

									} else {

										addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
												getProjectElementId(),
												firstValueObject,
												ExcelPaceProjectConstants.VALID_LEVEL_GENERATION_LIST));

									}

								} else if (lastLevelGen == null
										&& firstValueObject.isBlank()) {
										
									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));

								} else if (!firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));

								}
								break;

							// level/gen number
							case 4:

								if (firstValueObject.isNumeric()) {

									Integer intValue = firstValueObject
											.getInteger();

									if (dimension != null
											&& lastLevelGen
													.equals(LevelGenType.LEVEL)
											&& intValue >= 0) {

										levelFormat = new LevelFormat();
										levelFormat.setLevel(intValue);

									} else if (dimension != null
											&& lastLevelGen
													.equals(LevelGenType.GEN)
											&& intValue >= 1) {

										genFormat = new GenFormat();
										genFormat.setGeneration(intValue);

									} else {

										addProjectDataErrorToList(new ProjectDataError(
												getProjectElementId(),
												firstValueObject,
												ExcelPaceProjectConstants.INVALID_VALUE
														+ firstValueObject
																.getValueAsString()
														+ ExcelPaceProjectConstants.VAILD_OPTIONS_ARE
														+ ExcelPaceProjectConstants.LEVEL
														+ " >= 0, "
														+ ExcelPaceProjectConstants.GENERATION
														+ " >= 1"
														));

									}

								} else if(firstValueObject.isString()) {
									
									String value = firstValueObject.getString();
									if(dimension != null && lastLevelGen.equals(LevelGenType.MEMBER)){
										memberFormat = new MemberFormat();
										memberFormat.setMemberName(value);
									}
									
								} else if (levelFormat == null
										&& genFormat == null
										&& memberFormat == null
										&& firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));

								} else {

									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));

								}
								break;

							// global style name
							case 5:
								if (firstValueObject.isBlank() && dimension.isPrimaryDimension()) {

									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));

								} else {

									if (dimension != null
										&& lastLevelGen.equals(LevelGenType.LEVEL)
										&& levelFormat != null) {

										levelFormat.setFormatName(firstValueObject.getString());
										dimension.addLevelFormat(levelFormat);

									} else if (dimension != null
											&& lastLevelGen.equals(LevelGenType.GEN)
											&& genFormat != null) {

										genFormat.setFormatName(firstValueObject.getString());
										dimension.addGenFormat(genFormat);
										
									} else if(dimension != null
											&& lastLevelGen.equals(LevelGenType.MEMBER)
											&& memberFormat != null){
										
										memberFormat.setFormatName(firstValueObject.getString());
										dimension.addMemberFormat(memberFormat);
										
									}
									
								 }
							}

						}

						// adds or replaces existing dimension
						if(dimension.isPrimaryDimension()){
							mapEntry.setPrimaryDimension(dimension);
						} else{
							mapEntry.addSecondaryDimension(dimension);
						}
							
					}

					break;

				}

			}							
		

			// if required vars are there, add to map
			if (mapEntry.getName() != null) {

				conditionalFormatMap.put(mapEntry.getName(), mapEntry);

			}

		}
		return (T) conditionalFormatMap;
		
	}

	@Override
	protected void writeExcelSheet(T t) throws PaceProjectWriteException, PafException {

		Map<String, String> globalStyleDynamicRefMap = null;
		Map<String, String> dimensionRefMap = null;
		
		//if cell referencing is enabled, read in global styles
		if ( isCellReferencingEnabled() ) {			

			//global style refs
			IExcelDynamicReferenceElementItem globalStyleElementItem = new GlobalStylesExcelElementItem<Map<String,PafStyle>>(getWorkbook());
			globalStyleDynamicRefMap = globalStyleElementItem.getDynamicReferenceMap();
			
			//dimension name refs
			IExcelDynamicReferenceElementItem appDefElementItem = new ApplicationDefExcelElementItem<List<PafApplicationDef>>(getWorkbook());
			dimensionRefMap = appDefElementItem.getDynamicReferenceMap();
			
		}
		
		PafExcelInput input = new PafExcelInput.Builder(getWorkbook(), getSheetName(), getHeaderListMap().get(getSheetName()).size())
			.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
			.build();
		
		
		Map<String, ConditionalFormat> conditionalFormatMap = t;
		
		List<PafExcelRow> excelRowList = new ArrayList<PafExcelRow>();
	
		//create and add header to list
		excelRowList.add(PafExcelUtil.createHeaderRow(getHeaderListMap().get(getSheetName())));
		
		if ( conditionalFormatMap != null ) {
			
			for ( String conditionalFormatName : conditionalFormatMap.keySet() ) {
				
				ConditionalFormat conditionalFormat = conditionalFormatMap.get(conditionalFormatName);
				
				ConditionalFormatDimension dimension = conditionalFormat.getPrimaryDimension();						
								
				excelRowList.add(writeConditionalFormatDimension(dimension, globalStyleDynamicRefMap,
						dimensionRefMap, conditionalFormatName));
				
				List<ConditionalFormatDimension> secondaries = conditionalFormat.getSecondaryDimensions();
				if(secondaries != null){
					
					for(ConditionalFormatDimension dim : secondaries){
						excelRowList.add(writeConditionalFormatDimension(dim, globalStyleDynamicRefMap,
								dimensionRefMap, conditionalFormatName));						
					}
					
				}
			}
			
		}
					
		PafExcelUtil.writeExcelSheet(input, excelRowList);
		
	}

	protected PafExcelRow writeConditionalFormatDimension(ConditionalFormatDimension dimension,
			Map<String, String> globalStyleDynamicRefMap, Map<String, String> dimensionRefMap, String conditionalFormatName)
	{
		PafExcelRow excelRow = new PafExcelRow();
		if ( dimension != null ) {
			
			//name
			excelRow.addRowItem(0, PafExcelValueObject.createFromString(conditionalFormatName));
			
			excelRow.addRowItem(1, PafExcelValueObject.createFromBoolean(dimension.isPrimaryDimension()));
			
			Map<String, MemberFormat> memberFormatMap = dimension.getMemberFormats();
			
			if(memberFormatMap != null){
				int levelFormatNdx = 0;
				for(String memberFormatName : memberFormatMap.keySet()){
					MemberFormat mf = memberFormatMap.get(memberFormatName);
					
					if(mf!= null){
						if ( levelFormatNdx++ == 0 ) {
							
							//dimension name
							excelRow.addRowItem(2, PafExcelValueObject.createFromFormulaReferenceMap(dimension.getName(), dimensionRefMap));
							
							//type
							excelRow.addRowItem(3, PafExcelValueObject.createFromString(ExcelPaceProjectConstants.MEMBER));
																	
						} else {
							
							//blank dimension name
							excelRow.addRowItem(2, PafExcelValueObject.createBlank());
							
							//blank type
							excelRow.addRowItem(3, PafExcelValueObject.createBlank());
							
						}
						
						//member name
						excelRow.addRowItem(4, PafExcelValueObject.createFromString(mf.getMemberName()));
						
						//global style
						excelRow.addRowItem(5, PafExcelValueObject.createFromFormulaReferenceMap(mf.getFormatName(), globalStyleDynamicRefMap));
					}
				}
			}
			
			Map<Integer, LevelFormat> levelFormatMap = dimension.getLevelFormats();
			
			if ( levelFormatMap != null ) {
				
				int levelFormatNdx = 0;
				
				for ( Integer levelFormatNumber : levelFormatMap.keySet()) {
					
					LevelFormat lf = levelFormatMap.get(levelFormatNumber);
					
					if ( lf != null) {
						
						if ( levelFormatNdx++ == 0 ) {
							
							//dimension name
							excelRow.addRowItem(2, PafExcelValueObject.createFromFormulaReferenceMap(dimension.getName(), dimensionRefMap));
							
							//type
							excelRow.addRowItem(3, PafExcelValueObject.createFromString(ExcelPaceProjectConstants.LEVEL));
																	
						} else {
							
							//blank dimension name
							excelRow.addRowItem(2, PafExcelValueObject.createBlank());
							
							//blank type
							excelRow.addRowItem(3, PafExcelValueObject.createBlank());
							
						}
						
						//level number
						excelRow.addRowItem(4, PafExcelValueObject.createFromInteger(lf.getLevel()));
						
						//global style
						excelRow.addRowItem(5, PafExcelValueObject.createFromFormulaReferenceMap(lf.getFormatName(), globalStyleDynamicRefMap));
						
					}
					
				}
				
			}
			
			Map<Integer, GenFormat> genFormatMap = dimension.getGenFormats();
			
			if ( genFormatMap != null ) {
				
				int genFormatNdx = 0;
				
				for ( Integer genFormatNumber : genFormatMap.keySet()) {
					
					GenFormat gf = genFormatMap.get(genFormatNumber);
					
					if ( gf != null) {
						
						if ( genFormatNdx++ == 0 ) {
							
							//dimension name
							excelRow.addRowItem(2, PafExcelValueObject.createFromFormulaReferenceMap(dimension.getName(), dimensionRefMap));
							
							//type
							excelRow.addRowItem(3, PafExcelValueObject.createFromString(ExcelPaceProjectConstants.GENERATION));
																	
						} else {
							
							//blank dimension name
							excelRow.addRowItem(2, PafExcelValueObject.createBlank());
							
							//blank type
							excelRow.addRowItem(3, PafExcelValueObject.createBlank());
							
						}
						
						//generation number
						excelRow.addRowItem(4, PafExcelValueObject.createFromInteger(gf.getGeneration()));
						
						//global style
						excelRow.addRowItem(5, PafExcelValueObject.createFromFormulaReferenceMap(gf.getFormatName(), globalStyleDynamicRefMap));
						
					}
					
				}
				
			}
		}
		
		return excelRow;
	}
	
}
