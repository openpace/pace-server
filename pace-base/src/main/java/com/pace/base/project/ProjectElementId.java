/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project;

/**
 * Project Data sections.  It will be used by a Pace Project and import/export data classes.
 *
 * @author jmilliron
 * @version	1.0
 *
 */
public enum ProjectElementId {

	ApplicationDef ("Application Definition", "Application information such as data sources, global settings, etc."),  
	Views ("Views", "All Views."), 
	ViewSections ("View Sections", "All View Sections."), 
	ViewGroups ("View Groups", "All View Groups."),
	RuleSets ("Rule Sets", "All Rule Sets."),
	UserSecurity ("User Security", "Users defined for this application."),
	PlanCycles ("Plan Cycles", "A list of Plan Cycles used in Roles and Processes."), 
	Seasons ("Seasons", "A list of seasons used in Roles and Processes."), 
	Roles ("Roles", "A list of roles used in Roles and Processes."), 
	RoleConfigs ("Role Configurations", "A list of role configurations used in Roles in Processes."),
	Versions ("Versions", "Pace Version definitions."),
	Measures ("Measures", "Pace Measure definitions"), 
	NumericFormats ("Numeric Formats", "Numerical formats that can be applied throughout the application."), 
	HierarchyFormats ("Hierarchy Formats", "Formats that can be applied at a view level."),
	ConditionalFormats ("Conditional Formats", "Formats that can be applied at a view level."),
	ConditionalStyles("Conditional Styles", "Styles that can be applied to a specific view"),
	AdminLocks("Admin Controlled Persistent Locks", "Locks that are persisten.t"),
	GlobalStyles ("Global Styles", "Styles that can be used thoughout the application."), 
	UserSelections ("User Selections", "User selections used within views."), 
	DynamicMembers ("Dynamic Members", "Dynamic Members used within views."), 
	CustomMenus ("Custom Menus", "Custom Menus provide the ability to setup menus in Pace."), 
	CustomFunctions ("Custom Functions", "Custom Functions provide the ability to write and use custom functions in Pace."),
 	MemberTags ("Member Tags", "Member Tag Definitions."), 
	RoundingRules ("Rounding Rules", "A list of Rounding Rules."),
	PrintStyles ("Print Styles", "A list of print styles"),
	MemberLists("Member Lists", "A list of user defined member lists"),
	
	//only used for excel and as sub data regions
	RuleSet_RuleSet,
	RuleSet_RuleGroup,
	RuleSet_Rule;
	
	//user friendly name
	private String name;
	
	//description
	private String description;
	
	ProjectElementId() {
		
	}
	
	ProjectElementId(String name, String description) {
	
		this.name = name;
		this.description = description;
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	
	
}
