/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class_description_goes_here
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class ProjectSaveException extends Exception {
	
	//TTN 1832- refine AC framework when exporting error occurs
	//Added to display non-blocking errors when exporting a pace project  
	private Map<ProjectElementId, List<ProjectDataError>> projectSaveErrorMap = null;
	//TTN 1832- refine AC framework when exporting error occurs
	//Added this field used for flagging blocking or non-blocking errors when exporting a pace project  
	private boolean fatalError = false;
	
	public boolean isFatalError() {
		return fatalError;
	}

	public void setFatalError(boolean fatalError) {
		this.fatalError = fatalError;
	}

	public ProjectSaveException(
			Map<ProjectElementId, List<ProjectDataError>> projectExportErrorMap) {
		
		super();
		
		if ( projectExportErrorMap != null ) {
			
			this.projectSaveErrorMap = new HashMap<ProjectElementId, List<ProjectDataError>>();
			this.projectSaveErrorMap.putAll(projectExportErrorMap);
			
		}
		
	}
	
	public ProjectSaveException(
			Map<ProjectElementId, List<ProjectDataError>> projectExportErrorMap, boolean fatalError) {
		
		this(projectExportErrorMap);
		
		setFatalError(fatalError);
	}
	
	/**
	 * @param message
	 */
	public ProjectSaveException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the projectCreationErrorMap
	 */
	public Map<ProjectElementId, List<ProjectDataError>> getProjectSaveErrorMap() {
		return projectSaveErrorMap;
	}
}
