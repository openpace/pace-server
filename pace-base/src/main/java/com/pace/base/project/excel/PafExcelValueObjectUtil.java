/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project.excel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.pace.base.project.ProjectElementId;

/**
 * Utility class for Excel Value Objects
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PafExcelValueObjectUtil {

	/**
	 * 
	 * Creates a list of excel value objects from a string array.
	 *
	 * @param stringAr
	 * @return
	 */
	public static List<PafExcelValueObject> createListOfPafExcelValueObjects(String[] stringAr) {
		
		List<PafExcelValueObject> valueObjectList = new ArrayList<PafExcelValueObject>();
		
		if ( stringAr != null ) {
			
			for ( String string : stringAr ) {
				
				valueObjectList.add(PafExcelValueObject.createFromString(string));
				
			}
			
		}
		
		return valueObjectList;
		
	}
	
	/**
	 * 
	 * Creates a list of excel value objects from a string array.
	 *
	 * @param stringAr
	 * @return
	 */
	public static List<PafExcelValueObject> createListOfDynamicReferencePafExcelValueObjects(String[] stringAr, Map<String, String> dynamicRefMap) {
		
		List<PafExcelValueObject> valueObjectList = new ArrayList<PafExcelValueObject>();
		
		if ( stringAr != null ) {
			
			for ( String string : stringAr ) {
				
				PafExcelValueObject excelValueObject = PafExcelValueObject.createFromFormulaReferenceMap(string, dynamicRefMap);
				
				if ( excelValueObject != null ) {
					
					valueObjectList.add(excelValueObject);
					
				}
				
			}
			
		}
		
		return valueObjectList;
		
	}
	
	
	/**
	 * 
	 * Resolves a string to a dynamic reference map.  If a dynamic reference is found, will create a PafExcelValueObject as a 
	 * formula, otherwise would be string.  
	 *
	 * @param tryToResolveDynamicReference if true, will try to resolve the dynamic reference using map
	 * @param elementToResolve string to resolve using the dynamic reference
	 * @param dynamicRefMap map of dynamic references
	 * @return a new PafExcelValueObject
	 */
	public static PafExcelValueObject resolveDynamicReference(boolean tryToResolveDynamicReference, String elementToResolve, Map<String, String> dynamicRefMap) {
		
		PafExcelValueObject pafExcelValueObject = null;
		
		if ( tryToResolveDynamicReference && elementToResolve != null && dynamicRefMap != null &&
				dynamicRefMap.containsKey(elementToResolve)) {
			
			pafExcelValueObject = PafExcelValueObject.createFromFormula(dynamicRefMap.get(elementToResolve));
			
		} else {
		
			pafExcelValueObject = PafExcelValueObject.createFromString(elementToResolve);
		
		}
		
		return pafExcelValueObject;
		
	}
	
	
	
}
