/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.project.excel.elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.pace.base.PafException;
import com.pace.base.format.AbstractPaceConditionalStyle;
import com.pace.base.format.ColorScale;
import com.pace.base.format.ColorScaleColorNumType;
import com.pace.base.format.ColorScaleCriteria;
import com.pace.base.format.CriteriaType;
import com.pace.base.format.CriteriaTypeValues;
import com.pace.base.format.DataBarAppearance;
import com.pace.base.format.DataBarAppearanceBarDirectionType;
import com.pace.base.format.DataBarAppearanceBorderType;
import com.pace.base.format.DataBarAppearanceFillType;
import com.pace.base.format.DataBarCriteria;
import com.pace.base.format.DataBars;
import com.pace.base.format.IconCriteria;
import com.pace.base.format.IconStyle;
import com.pace.base.format.IconStyleType;
import com.pace.base.mdb.PafDimTree.ConditionalStyleType;
import com.pace.base.project.*;
import com.pace.base.project.excel.IExcelDynamicReferenceElementItem;
import com.pace.base.project.excel.PafExcelInput;
import com.pace.base.project.excel.PafExcelRow;
import com.pace.base.project.excel.PafExcelValueObject;
import com.pace.base.project.utils.PafExcelUtil;
import com.pace.base.utility.CollectionsUtil;

/**
 * Reads/writes global styles from/to an Excel 2007 workbook.
 *
 * @author jmilliron
 * @version	1.00
 *
 */
public class ConditionalStylesExcelElementItem<T extends Map<String, AbstractPaceConditionalStyle>> extends PafExcelElementItem<T> implements IExcelDynamicReferenceElementItem {

	/**
	 * Creates an excel element item.
	 * 
	 * @param workbook workbook used for read/write
	 */
	public ConditionalStylesExcelElementItem(Workbook workbook) {
		super(workbook);
	}
	
	public ConditionalStylesExcelElementItem(Workbook workbook,
			boolean isCellReferencingEnabled) {
		super(workbook, isCellReferencingEnabled);
	
	}

	@Override
	public void createHeaderListMapEntries() {
		getHeaderListMap().put(getSheetName(), Arrays.asList("Type (IconStyle, DataBar, ColorScale)", "Name", "GUID", "Style Type", "Crit Type Value (Min, Max, etc)", "Crit Value", "Crit Type", "Operator (Icon)", "Fill Hex Color (Bar, Color Scale)", "Fill Type (Bar)", "Bar Direction (Bar)", "Border Hex Color (Bar)", "Border Type (Bar)"));
		
	}

	@Override
	public ProjectElementId getProjectElementId() {
		
		return ProjectElementId.ConditionalStyles;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T readExcelSheet() throws PaceProjectReadException, PafException {
	
		PafExcelInput input = new PafExcelInput.Builder(getWorkbook(), getSheetName(), getHeaderListMap().get(getSheetName()).size())
				.headerListMap(getHeaderListMap())
				.excludeHeaderRows(true)
				.excludeEmptyRows(true)
				.sheetRequired(true)
				.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
				.build();

		List<PafExcelRow> excelRowList = null;
		Map<String, AbstractPaceConditionalStyle> condStyleMap = new HashMap<String, AbstractPaceConditionalStyle>();
		excelRowList = PafExcelUtil.readExcelSheet(input);
		ConditionalStyleType lastStyle = null;
		String guid = null;
		String name = null;

		for (PafExcelRow row : excelRowList) {
			
			List<PafExcelRow> tempExcelRowList = null;
			AbstractPaceConditionalStyle mapEntry = null;

			for (Integer rowIndex : row.getRowItemOrderedIndexes()) {

				List<PafExcelValueObject> rowItemList = row.getRowItem(rowIndex);

				PafExcelValueObject firstValueObject = rowItemList.get(0);

				switch (rowIndex) {

				// name
				case 0:

					tempExcelRowList = new ArrayList<PafExcelRow>(rowItemList.size());

					// create a new row for the number of rows merged into
					// the row
					for (int i = 0; i < rowItemList.size(); i++) {

						tempExcelRowList.add(new PafExcelRow());

					}

					if (firstValueObject.isString()) {
						String styleType = firstValueObject.getString();
						
						if (styleType.equalsIgnoreCase(ExcelPaceProjectConstants.COLOR_SCALE)) {
							lastStyle = ConditionalStyleType.COLOR_SCALE;
						} else if (styleType.equalsIgnoreCase(ExcelPaceProjectConstants.DATA_BAR)) {

							lastStyle = ConditionalStyleType.DATA_BAR;
						} else if (styleType.equalsIgnoreCase(ExcelPaceProjectConstants.ICON_STYLE)) {

							lastStyle = ConditionalStyleType.ICON;
						} else {

							addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
									getProjectElementId(),
									firstValueObject,
									ExcelPaceProjectConstants.VALID_CONDITIONAL_STYLES_LIST));

						}

					} else if (firstValueObject.isBlank() && mapEntry == null && lastStyle == null) {
						
							addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
								getProjectElementId(), firstValueObject));
					} else if (mapEntry == null && lastStyle == null) {

						addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
								getProjectElementId(), firstValueObject));

					}

					break;
					
				default:
					int tmpRowNdx = 0;

					// populate the temp row with relevant index item from
					// rowItemList
					for (PafExcelRow tempRow : tempExcelRowList) {

						tempRow.addRowItem(rowIndex, rowItemList.get(tmpRowNdx++));

					}
					
					if(rowIndex != 8 && lastStyle == ConditionalStyleType.COLOR_SCALE
							|| rowIndex != 12 && lastStyle == ConditionalStyleType.DATA_BAR
							|| rowIndex != 7 && lastStyle == ConditionalStyleType.ICON){
						break;
					}
					
					ColorScale colorScale = null;
					ColorScaleCriteria crit = new ColorScaleCriteria();
					
					DataBars dataBar = null;
					DataBarCriteria barCrit = new DataBarCriteria();
					DataBarAppearance dba = new DataBarAppearance();
					
					IconStyle icon = null;
					IconCriteria iconCrit = new IconCriteria();
					
					for (PafExcelRow tempRow : tempExcelRowList) {

						for (Integer tempRowNdx : tempRow.getRowItemOrderedIndexes()) {

							firstValueObject = tempRow.getRowItem(tempRowNdx).get(0);

							switch (tempRowNdx) {
							
							case 1:
			
								if (firstValueObject.isString()) {
									name = firstValueObject.getString();
									
								} else if (mapEntry == null	&& firstValueObject.isBlank() && name == null) {
									
									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
										getProjectElementId(),
										firstValueObject));
								} else if (!firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));

								}
								break;
			
							// font name
							case 2:
			
								if (firstValueObject.isString()) {
									
									guid = firstValueObject.getString();

									String caseInsenstiveKey = CollectionsUtil.getCaseSenstiveKeyFromMap(condStyleMap, guid);

									if (caseInsenstiveKey != null) {
										mapEntry = condStyleMap.get(caseInsenstiveKey);
									} 
									
									if(lastStyle == ConditionalStyleType.COLOR_SCALE){
										if(mapEntry == null){
											mapEntry = new ColorScale();
										}
										colorScale = (ColorScale) mapEntry;
									} else if(lastStyle == ConditionalStyleType.DATA_BAR){
										if(mapEntry == null){
											mapEntry = new DataBars();
										}
										dataBar = (DataBars) mapEntry;
									} else if(lastStyle == ConditionalStyleType.ICON){
										if(mapEntry == null){
											mapEntry = new IconStyle();
										}
										icon = (IconStyle) mapEntry;
									}

									
									if(mapEntry != null){
										mapEntry.setName(name);
										mapEntry.setGuid(guid);
									}
								} else if (mapEntry == null	&& firstValueObject.isBlank()) {

									String caseInsenstiveKey = CollectionsUtil.getCaseSenstiveKeyFromMap(condStyleMap, guid);

									if (caseInsenstiveKey != null) {
										mapEntry = condStyleMap.get(caseInsenstiveKey);
									} 
									
									if(lastStyle == ConditionalStyleType.COLOR_SCALE){
										if(mapEntry == null){
											mapEntry = new ColorScale();
										}
										colorScale = (ColorScale) mapEntry;
									} else if(lastStyle == ConditionalStyleType.DATA_BAR){
										if(mapEntry == null){
											mapEntry = new DataBars();
										}
										dataBar = (DataBars) mapEntry;
									} else if(lastStyle == ConditionalStyleType.ICON){
										if(mapEntry == null){
											mapEntry = new IconStyle();
										}
										icon = (IconStyle) mapEntry;
									}
									
									if(mapEntry == null){
										addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));
									}
								} else if (!firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));

								}
								
								break;
							case 3:
								if (firstValueObject.isString()) {
									try{
										if (lastStyle == ConditionalStyleType.COLOR_SCALE){
											colorScale.setScaleType(ColorScaleColorNumType.valueOf(firstValueObject.getString()));
										} else if (lastStyle == ConditionalStyleType.ICON){
											icon.setIconStyle(IconStyleType.valueOf(firstValueObject.getString()));
										}
									}catch(Exception e){
										addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
												getProjectElementId(),
												firstValueObject));
									}
								} else if (mapEntry == null	&& firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));

								} else if (!firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
								} 
								break;
							case 4:
								if (firstValueObject.isString()) {
									try{
										if(lastStyle == ConditionalStyleType.COLOR_SCALE){
											crit.setCriteriaTypeValue(CriteriaTypeValues.valueOf(firstValueObject.getString()));
										} else if(lastStyle == ConditionalStyleType.DATA_BAR){
											barCrit.setCriteriaTypeValue(CriteriaTypeValues.valueOf(firstValueObject.getString()));
										}
									} catch(Exception e){
										addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
												getProjectElementId(),
												firstValueObject));
									}
								} else if (mapEntry == null	&& firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));

								} else if (!firstValueObject.isBlank()) {

									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
								} 
								break;
							case 5:
								if (firstValueObject.isNumeric()) {
									if(lastStyle == ConditionalStyleType.COLOR_SCALE){
										crit.setCriteriaValue(firstValueObject.getDouble());
									} else if(lastStyle == ConditionalStyleType.DATA_BAR){
										barCrit.setCriteriaValue(firstValueObject.getDouble());
									} else if(lastStyle == ConditionalStyleType.ICON){
										iconCrit.setCriteriaValue(firstValueObject.getDouble());
									}
									
								} else if (mapEntry == null
										&& firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								} else if (!firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								} 
								break;
							case 6:
								if (firstValueObject.isString()) {
									try{
										if(lastStyle == ConditionalStyleType.COLOR_SCALE){
												crit.setCriteriaType(CriteriaType.valueOf(firstValueObject.getString()));
										} else if (lastStyle == ConditionalStyleType.DATA_BAR){
												barCrit.setCriteriaType(CriteriaType.valueOf(firstValueObject.getString()));
											if(barCrit.getCriteriaTypeValue() == CriteriaTypeValues.Minimum){
												dataBar.setMinimum(barCrit);
											} else if(barCrit.getCriteriaTypeValue() == CriteriaTypeValues.Maximum){
												dataBar.setMaximum(barCrit);
											}
										} else if (lastStyle == ConditionalStyleType.ICON){
											iconCrit.setCriteriaType(CriteriaType.valueOf(firstValueObject.getString()));
										}
									}catch(Exception e){
										addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
												getProjectElementId(),
												firstValueObject));
									}
								} else if (mapEntry == null
										&& firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								} else if (!firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								}
								break;
							case 7:
								if (firstValueObject.isString()) {
									if (lastStyle == ConditionalStyleType.ICON){
										iconCrit.setCriteriaOperator(firstValueObject.getString());
										if(icon.getCriteriaOne() == null){
											icon.setCriteriaOne(iconCrit);
										} else if(icon.getCriteriaTwo() == null){
											icon.setCriteriaTwo(iconCrit);
										} else if(icon.getCriteriaThree() == null){
											icon.setCriteriaThree(iconCrit);
										} else if(icon.getCriteriaFour() == null){
											icon.setCriteriaFour(iconCrit);
										} else if(icon.getCriteriaFive() == null){
											icon.setCriteriaFive(iconCrit);
										}  
									}
								}
								break;
							case 8:
								if (firstValueObject.isString()) {
									if(lastStyle == ConditionalStyleType.COLOR_SCALE){
										crit.setCriteriaHexColor(firstValueObject.getString());
										if(crit.getCriteriaTypeValue() == CriteriaTypeValues.Minimum){
											colorScale.setMinimum(crit);
										} else if(crit.getCriteriaTypeValue() == CriteriaTypeValues.Maximum){
											colorScale.setMaximum(crit);
										} else if(crit.getCriteriaTypeValue() == CriteriaTypeValues.MidpointMin
												|| crit.getCriteriaTypeValue() == CriteriaTypeValues.MidpointMax
												|| crit.getCriteriaTypeValue() == CriteriaTypeValues.Midpoint){
											colorScale.setMidpoint(crit);
										}
									} else if(lastStyle == ConditionalStyleType.DATA_BAR){
										dba.setFillHexColor(firstValueObject.getString());
									}
								} else if (mapEntry == null	&& firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								} else if (!firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								}
								break;
							case 9:
								if (firstValueObject.isString()) {
									try{
										if(lastStyle == ConditionalStyleType.DATA_BAR){
											dba.setFillType(DataBarAppearanceFillType.valueOf(firstValueObject.getString()));
										}
									}catch(Exception e){
										addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
									}
								} else if (mapEntry == null	&& firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								} else if (!firstValueObject.isBlank()) {
			
									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
			
								}
								break;
							case 10:
								if(firstValueObject.isString()){
									try{
										if(lastStyle == ConditionalStyleType.DATA_BAR){
											dba.setBarDirection(DataBarAppearanceBarDirectionType.valueOf(firstValueObject.getString()));
										}
									}catch(Exception e){
										addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
									}
								} else if (mapEntry == null	&& firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								} else if (!firstValueObject.isBlank()) {
			
									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
			
								}
								break;
							case 11:
								if (firstValueObject.isString()) {
									if(lastStyle == ConditionalStyleType.DATA_BAR){
										dba.setBorderHexColor(firstValueObject.getString());
									}
								} else if (mapEntry == null	&& firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								} else if (!firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								}
								break;
							case 12:
								if (firstValueObject.isString()) {
									try{
										if(lastStyle == ConditionalStyleType.DATA_BAR){
											dba.setBorderType(DataBarAppearanceBorderType.valueOf(firstValueObject.getString()));
											dataBar.setAppearance(dba);
										}
									}catch(Exception e){
										addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
									}
								} else if (mapEntry == null	&& firstValueObject.isBlank()) {
	
									addProjectDataErrorToList(ProjectDataError.createRequiredProjectDataError(
											getProjectElementId(),
											firstValueObject));
	
								} else if (!firstValueObject.isBlank()) {
			
									addProjectDataErrorToList(ProjectDataError.createInvalidValueProjectDataError(
											getProjectElementId(),
											firstValueObject));
			
								}
								break;
							}
						}
					}
				}

			}

			// if required vars are there, add to map
			if (mapEntry != null && mapEntry.getGuid() != null) {

				condStyleMap.put(mapEntry.getGuid(), mapEntry);

			}
		}
	
		
		return (T) condStyleMap;
	}

	@Override
	protected void writeExcelSheet(T t) throws PaceProjectWriteException, PafException {

		PafExcelInput input = new PafExcelInput.Builder(getWorkbook(), getSheetName(), getHeaderListMap().get(getSheetName()).size())
			.excludeDefaultValuesOnWrite(false)
			.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
			.build();
	
	
		Map<String, AbstractPaceConditionalStyle> globalStyleMap = t;
		
		List<PafExcelRow> excelRowList = new ArrayList<PafExcelRow>();
	
		//create and add header to list
		excelRowList.add(PafExcelUtil.createHeaderRow(getHeaderListMap().get(getSheetName())));
		
		if ( globalStyleMap != null ) {
			
			for ( String condStyleName : globalStyleMap.keySet() ) {
				
				AbstractPaceConditionalStyle condStyle = globalStyleMap.get(condStyleName);
				
				if(condStyle == null)
					continue;
				
				PafExcelRow excelRow = new PafExcelRow();
				
				if(condStyle.getClass() == ColorScale.class){ 
					
					ColorScale colorScale = (ColorScale) condStyle;
					excelRow.addRowItem(0, PafExcelValueObject.createFromString(ExcelPaceProjectConstants.COLOR_SCALE));
					excelRow.addRowItem(1, PafExcelValueObject.createFromString(condStyle.getName()));
					excelRow.addRowItem(2, PafExcelValueObject.createFromString(condStyle.getGuid()));
					excelRow.addRowItem(3, PafExcelValueObject.createFromString(colorScale.getScaleType().toString()));					
					
					writeColorScaleCriteria(colorScale.getMinimum(), excelRow, 4);
					writeColorScaleCriteria(colorScale.getMaximum(), excelRow, 4);
					
					if(colorScale.getScaleType() == ColorScaleColorNumType.Three_Color){
						
						writeColorScaleCriteria(colorScale.getMidpoint(), excelRow, 4);
					}					
					
				} else if (condStyle.getClass() == DataBars.class){
					
					excelRow.addRowItem(0, PafExcelValueObject.createFromString(ExcelPaceProjectConstants.DATA_BAR));
					excelRow.addRowItem(1, PafExcelValueObject.createFromString(condStyle.getName()));
					excelRow.addRowItem(2, PafExcelValueObject.createFromString(condStyle.getGuid()));
					
					DataBars dataBar = (DataBars)condStyle;
					DataBarCriteria critOne = dataBar.getMinimum();
					writeGenericCriteria(critOne.getCriteriaType(), critOne.getCriteriaValue(), critOne.getCriteriaTypeValue(), excelRow, 4);
					DataBarAppearance dba = dataBar.getAppearance();
					excelRow.addRowItem(8, PafExcelValueObject.createFromString(dba.getFillHexColor()));
					excelRow.addRowItem(9, PafExcelValueObject.createFromString(dba.getFillType().toString()));
					excelRow.addRowItem(10, PafExcelValueObject.createFromString(dba.getBarDirection().toString()));
					excelRow.addRowItem(11, PafExcelValueObject.createFromString(dba.getBorderHexColor()));
					excelRow.addRowItem(12, PafExcelValueObject.createFromString(dba.getBorderType().toString()));
					
					DataBarCriteria critTwo = dataBar.getMaximum();
					writeGenericCriteria(critTwo.getCriteriaType(), critTwo.getCriteriaValue(), critTwo.getCriteriaTypeValue(), excelRow, 4);
					
				} else if (condStyle.getClass() == IconStyle.class){
					
					excelRow.addRowItem(0, PafExcelValueObject.createFromString(ExcelPaceProjectConstants.ICON_STYLE));	
					excelRow.addRowItem(1, PafExcelValueObject.createFromString(condStyle.getName()));
					excelRow.addRowItem(2, PafExcelValueObject.createFromString(condStyle.getGuid()));
					
					IconStyle iconStyle = (IconStyle) condStyle;					
					excelRow.addRowItem(3, PafExcelValueObject.createFromString(iconStyle.getIconStyle().toString()));
					
					writeIconCriteria(iconStyle.getCriteriaOne(), excelRow, 4);
					writeIconCriteria(iconStyle.getCriteriaTwo(), excelRow, 4);
					
					if(iconStyle.getCriteriaThree() != null){
						writeIconCriteria(iconStyle.getCriteriaThree(), excelRow, 4);
					}
					
					if(iconStyle.getCriteriaFour() != null){
						writeIconCriteria(iconStyle.getCriteriaFour(), excelRow, 4);
					}
					if(iconStyle.getCriteriaFive() != null){
						writeIconCriteria(iconStyle.getCriteriaFive(), excelRow, 4);
					}
					
				}				
				
				excelRowList.add(excelRow);			
				
			}
			
		}
					
		PafExcelUtil.writeExcelSheet(input, excelRowList);
							
	}
	
	private int writeIconCriteria(IconCriteria crit, PafExcelRow excelRow, int ndx){
		
		excelRow.addRowItem(ndx+1, PafExcelValueObject.createFromDouble(crit.getCriteriaValue()));
		excelRow.addRowItem(ndx+2, PafExcelValueObject.createFromString(crit.getCriteriaType().toString()));
		excelRow.addRowItem(ndx+3, PafExcelValueObject.createFromString(crit.getCriteriaOperator()));
		return ndx;
	}
	
	private int writeColorScaleCriteria(ColorScaleCriteria crit, PafExcelRow excelRow, int ndx){
		
		ndx = writeGenericCriteria(crit.getCriteriaType(), crit.getCriteriaValue(), crit.getCriteriaTypeValue(), excelRow, ndx);
		excelRow.addRowItem(ndx+1, PafExcelValueObject.createFromString(crit.getCriteriaHexColor()));
		return ndx;
	}
	
	private int writeGenericCriteria(CriteriaType type, Double val, CriteriaTypeValues criteriaTypeValues, PafExcelRow excelRow, int ndx){
		excelRow.addRowItem(ndx++, PafExcelValueObject.createFromString(criteriaTypeValues.toString()));
		excelRow.addRowItem(ndx++, PafExcelValueObject.createFromDouble(val));
		excelRow.addRowItem(ndx++, PafExcelValueObject.createFromString(type.toString()));
		return ndx;
	}

	/**
	 * Creates a dynamic reference map used to reference global style cells.  Key is global style name, value
	 * is the Excel sheet/cell reference.  Example =GlobalStyles!$A$2.
	 */
	public Map<String, String> getDynamicReferenceMap() {

		Map<String, String> dynamicRefMap = null;
		
		PafExcelInput versionInput = new PafExcelInput.Builder(this.getWorkbook(), this.getSheetName(), 1)
			.headerListMap(this.getHeaderListMap())
			.excludeHeaderRows(true)
			.excludeEmptyRows(true)
			.multiDataRow(true)
			.startDataReadColumnIndex(0)
			.sheetRequired(false)
			.endOfSheetIdnt(ExcelPaceProjectConstants.END_OF_SHEET_IDENT)
			.build();
	
		try {
			
			dynamicRefMap = PafExcelUtil.createCellReferenceMap(versionInput);
			
		} catch (PafException e) {
									
			logger.warn(ExcelPaceProjectConstants.COULD_NOT_CREATE_THE_REFERENCE_MAP + e.getMessage());
			
		}
		
		return dynamicRefMap;		

	}

}
