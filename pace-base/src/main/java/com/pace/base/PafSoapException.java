/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base;

public class PafSoapException extends Exception {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 5922560095240193189L;
	
	private String[] messageDetail;
		
	public PafSoapException() {}
	   
	public PafSoapException(PafException exception) {
		super( exception );

		// if cause is null then just get message details
		if (exception.getCause() == null) {
			if (exception.getMessageDetails() != null) 
					messageDetail = exception.getMessageDetails().toArray(new String[0]);
				else
					messageDetail = new String[] {"No message detail available"};
		}
		else {
			if (exception.getCause().getMessage() != null) {	
				
				messageDetail =  new String[] { exception.getCause().getMessage() };
			}
			else
				messageDetail = new String[] {"No message detail available"};			
		}	
	}

	public PafSoapException(Exception exception) {
		super( exception );

		// if cause is null then just get message details
		if (exception.getCause() == null) {
			if (exception.getMessage() != null) 
					messageDetail =  new String[] { exception.getMessage() };
				else
					messageDetail = new String[] {"No message detail available"};
		}
		else {
			if (exception.getCause().getMessage() != null) {	
				
				messageDetail =  new String[] { exception.getCause().getMessage() };
			}
			else
				messageDetail = new String[] {"No message detail available"};			
		}	
	}
	
	public String getMessage() {
		return super.getMessage();
	}
	
	public void setMessage(String message) {
	}

    /**
     * @return Returns the messageDetail.
     */
    public String[] getMessageDetail() {
        return messageDetail;
    }

    /**
     * @param messageDetail The messageDetail to set.
     */
    public void setMessageDetail(String[] messageDetail) {
        this.messageDetail = messageDetail;
    }

}
