/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.jira;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Class that represents the response from creating a JIRA issue.
 * @author themoosman
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class JiraResponse {

	@JsonProperty("id")
	private int id;
	@JsonProperty("key")
	private String key;
	@JsonProperty("self")
	private String self;

	public JiraResponse(){}
	
	public JiraResponse(int id, String key, String self) {
		super();
		this.id = id;
		this.key = key;
		this.self = self;
	}

	@JsonProperty("id")
	public int getId() {
		return id;
	}

	@JsonProperty("key")
	public String getKey() {
		return key;
	}

	@JsonProperty("self")
	public String getSelf() {
		return self;
	}
	
	@JsonProperty("id")
	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty("key")
	public void setKey(String key) {
		this.key = key;
	}

	@JsonProperty("self")
	public void setSelf(String self) {
		this.self = self;
	}

	@Override
	public String toString() {
		return "JiraResponse [id=" + id + ", key=" + key + ", self=" + self	+ "]";
	}
}