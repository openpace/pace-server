/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.jira;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Class that represents a JIRA issue type.
 * @author themoosman
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class JiraIssueType {

	/**
	 * Enum to match JIRA issue types.
	 * Currently we only support Bug.
	 * @author themoosman
	 *
	 */
	public enum IssueType{
		Bug
	}
	
	@JsonProperty("name")
	private IssueType IssueType;

	public JiraIssueType(com.pace.base.jira.JiraIssueType.IssueType issueType) {
		super();
		IssueType = issueType;
	}

	/**
	 * Gets the JIRA issue type.
	 * @return
	 */
	@JsonProperty("name")
	public IssueType getIssueType() {
		return IssueType;
	}

	/**
	 * Sets the JIRA issue type.
	 * @param issueType
	 */
	@JsonProperty("name")
	public void setIssueType(IssueType issueType) {
		IssueType = issueType;
	}

	@Override
	public String toString() {
		return "JiraIssueType [IssueType=" + IssueType + "]";
	}
}