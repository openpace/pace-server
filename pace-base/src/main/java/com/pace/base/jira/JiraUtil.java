/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.jira;

import java.io.File;
import java.util.Collection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.type.TypeReference;

import com.pace.base.PafBaseConstants;
import com.pace.base.utility.JsonUtil;

public class JiraUtil {
	
	private static Logger logger = Logger.getLogger(JiraUtil.class);
	private static HttpClient httpClient = getHttpClient();
	private static final String jiraServerBase = PafBaseConstants.JIRA_SERVER_BASE_URL;
	private static final String jiraUser = PafBaseConstants.JIRA_SERVER_USERNAME;
	private static final String jiraUserPwd = PafBaseConstants.JIRA_SERVER_PASSWORD;
	private static final String VERSION_REST = "/versions";
	private static final String PROJECT_REST = "project/";
	private static final String ISSUE_REST = "issue/";
	private static final String ATTACHMENT_REST = "/attachments";
	private static final String APP_JSON = "application/json";
	
	/**
	 * Test connectivity to the JIRA server.
	 * @return
	 */
	public static boolean isJiraServerAlive(){
		
		boolean result = true;
		try {
			getJiraVersions();
		} catch (Exception e) {
			logger.error(e);
			result = false;
		}
		return result;
	}

	/**
	 * Gets the collection of available product versions.
	 * @return
	 * @throws Exception
	 */
	public static Collection<JiraProjectVersion> getJiraVersions() throws Exception{

		return getJiraVersions(jiraServerBase);
	}
	
	/**
	 * Gets the collection of available product versions.
	 * @return
	 * @throws Exception
	 */
	public static Collection<JiraProjectVersion> getJiraVersions(String jiraServerBase) throws Exception{

		String json = executeGetQuery(jiraServerBase, PROJECT_REST + PafBaseConstants.JIRA_DEFAULT_PROJECT + VERSION_REST);

		Collection<JiraProjectVersion> res;
		
		try {
			res = JsonUtil.getMapper().readValue(json, new TypeReference<Collection<JiraProjectVersion>>() { });
		}catch(Exception ex){
			throw new Exception (ex.getMessage());
		}
		return res;
	}
	
	
	/**
	 * Creates a JIRA issue
	 * @param jiraIssue JIRA issue
	 * @return
	 * @throws Exception
	 */
	public static String executeGetQuery(String url) throws Exception{

		return executeGetQuery(jiraServerBase, url);
	}
	
	/**
	 * Creates a JIRA issue
	 * @param jiraIssue JIRA issue
	 * @return
	 * @throws Exception
	 */
	public static String executeGetQuery(String jiraServerBase, String url) throws Exception{

		HttpGet httpGet = getHttpGet(jiraServerBase + url);
		
		//Execute the HTTP Post and get the response
		String responseData = getHttpGetResponse(httpGet);

		return responseData;
	}
	
	
	/**
	 * Creates a JIRA issue
	 * @param jiraIssue JIRA issue
	 * @return
	 * @throws Exception
	 */
	public static JiraResponse createJiraIssue(String jiraServerBase, JiraIssue jiraIssue) throws Exception{
		
		JiraFields fields = new JiraFields(jiraIssue);
		String json = JsonUtil.toJson(fields);
		
		HttpPost httpPost = getHttpPost(jiraServerBase + ISSUE_REST);

		//Create a string entry which will be used to send the Json
		StringEntity entity = new StringEntity(json);
		entity.setContentType(APP_JSON);
		//Set the entity in the http post object.
		httpPost.setEntity(entity);
		
		//Execute the HTTP Post and get the response
		String responseData = getHttpPostResponse(httpPost);
		//responseData = "{\"issue\": " + responseData + " }";
		//Convert the response to a POJO and return it
		return JsonUtil.toObject(responseData, JiraResponse.class);
	}
	
	
	/**
	 * Creates a JIRA issue
	 * @param jiraIssue JIRA issue
	 * @return
	 * @throws Exception
	 */
	public static JiraResponse createJiraIssue(JiraIssue jiraIssue) throws Exception{

		return createJiraIssue(jiraServerBase, jiraIssue);
	}
	
	/**
	 * Deletes a JIRA issue
	 * @param jiraIssue JIRA issue
	 * @return
	 * @throws Exception
	 */
	public static boolean deleteJiraIssue(String jiraServerBase, int issueKey) throws Exception{
		
		HttpDelete httpDelete = getHttpDelete(jiraServerBase + ISSUE_REST + issueKey );

		return getHttpDeleteResponse(httpDelete);
	}
	
	/**
	 * Deletes a JIRA issue
	 * @param jiraIssue JIRA issue
	 * @return
	 * @throws Exception
	 */
	public static boolean deleteJiraIssue(int issueKey) throws Exception{
		
		return deleteJiraIssue(jiraServerBase, issueKey);
	}
	
	
	/**
	 * Adds an attachment to an existing JIRA issue
	 * @param issueKey JIRA issue key
	 * @param path full path (string) to the file to attach
	 * @throws Exception
	 */
	public static void addAttachmentToIssue(String issueKey, String path) throws Exception{
		addAttachmentToIssue(issueKey, new File(path));
	}
	
	/**
	 * Adds an attachment to an existing JIRA issue
	 * @param issueKey JIRA issue key
	 * @param path File object for the attachment
	 * @throws Exception
	 */
	public static void addAttachmentToIssue(String issueKey, File path) throws Exception{

		HttpPost httpPost = getHttpPost(jiraServerBase + ISSUE_REST + issueKey + ATTACHMENT_REST);
		httpPost.setHeader("X-Atlassian-Token", "nocheck");

		
		MultipartEntityBuilder entity = MultipartEntityBuilder.create();        
		entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

		//Create an object to hold the file to upload
		FileBody fileBody = new FileBody(path);
		//Add the file to the entity. Note that JIRA Rest requires the name to be "file"
		entity.addPart("file", fileBody); 
		
		
		//Add the entity to the httpPost
		httpPost.setEntity(entity.build());
		//Execute 
		getHttpPostResponse(httpPost);
	}
	
	private static HttpClient getHttpClient(){
		
		if(httpClient == null){
			httpClient = HttpClientBuilder.create().build();
		}
		return httpClient;
	}
	
	private static HttpPost getHttpPost(String url){

		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("Authorization", "Basic "+ new String(org.apache.commons.codec.binary.Base64.encodeBase64((jiraUser+":"+jiraUserPwd).getBytes())));

		return httpPost;
	}
	
	private static HttpGet getHttpGet(String url){
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("Authorization", "Basic "+ new String(org.apache.commons.codec.binary.Base64.encodeBase64((jiraUser+":"+jiraUserPwd).getBytes())));
		httpGet.addHeader("accept", APP_JSON);
		
		return httpGet;
	}
	
	private static HttpDelete getHttpDelete(String url){
		HttpDelete httpDelete = new HttpDelete(url);
		httpDelete.setHeader("Authorization", "Basic "+ new String(org.apache.commons.codec.binary.Base64.encodeBase64((jiraUser+":"+jiraUserPwd).getBytes())));
		
		return httpDelete;
	}
	
	/**
	 * Executes an HTTP post against an HTTP Client
	 * @param httpPost
	 * @return
	 * @throws Exception
	 */
	private static HttpResponse executeHttpPost(HttpPost httpPost) throws Exception{

		return getHttpClient().execute(httpPost);

	}
	
	/**
	 * Executes an HTTP get against an HTTP Client
	 * @param httpPost
	 * @return
	 * @throws Exception
	 */
	private static HttpResponse executeHttpGet(HttpGet httpGet) throws Exception{

		return getHttpClient().execute(httpGet);

	}
	
	/**
	 * Executes an HTTP delete against an HTTP Client
	 * @param httpDelete
	 * @return
	 * @throws Exception
	 */
	private static HttpResponse executeHttpDelete(HttpDelete httpDelete) throws Exception{

		return getHttpClient().execute(httpDelete);

	}
	
	/**
	 * Executes the HTTP post and returns the result string.
	 * @param httpPost
	 * @return
	 * @throws Exception
	 */
	private static String getHttpPostResponse(HttpPost httpPost) throws Exception{
		
		return parseHttpResponse(executeHttpPost(httpPost));
	}
	
	/**
	 * Executes the Get post and returns the result string.
	 * @param httpPost
	 * @return
	 * @throws Exception
	 */
	private static String getHttpGetResponse(HttpGet httpGet) throws Exception{
		
		return parseHttpResponse(executeHttpGet(httpGet));
	}
	
	/**
	 * Executes the Delete post and returns the result boolean.
	 * @param httpDelete
	 * @return
	 * @throws Exception
	 */
	private static boolean getHttpDeleteResponse(HttpDelete httpDelete) throws Exception{
		
		//Get the HTTP Response payload
		HttpResponse response = executeHttpDelete(httpDelete);
		//Get the HTTP response code
		int responseCode = response.getStatusLine().getStatusCode();
		
		if(responseCode != 204)	{
			String responseData =  new String(EntityUtils.toByteArray(response.getEntity()));
			logger.error(responseData);
			throw new HttpException(responseData);
		}
		return true;
	}
	
	/**
	 * Executes the HTTP post and returns the result string.
	 * @param httpPost
	 * @return
	 * @throws Exception
	 */
	private static String parseHttpResponse(HttpResponse response) throws Exception{
		//Get the HTTP Response payload
		HttpEntity result = response.getEntity();
		//Get the HTTP response code
		int responseCode = response.getStatusLine().getStatusCode();
		//Log the response code
		logger.info("Response from server: " + responseCode);
		//Get the response data string
		String responseData =  new String(EntityUtils.toByteArray(result));
		//Check for an error
		if(responseCode != 200 && responseCode != 201)	{
			logger.error(responseData);
			throw new HttpException(responseData);
		}
		
		logger.info(responseData);
		
		return responseData;
	}
	
}
