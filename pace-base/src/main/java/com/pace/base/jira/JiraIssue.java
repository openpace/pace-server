/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.jira;

import java.util.Arrays;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Class that represents a JIRA issue.
 * @author themoosman
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class JiraIssue {

	@JsonProperty("project")
	private JiraProject project;
	
	@JsonProperty("summary")
	private String summary;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("issuetype")
	private JiraIssueType issueType;
	
	@JsonProperty("priority")
	private JiraIssuePriority issuePriority;
	
	@JsonProperty("versions")
	private JiraVersion[] jiraVersion;
	
	@JsonProperty("customfield_10780")
	private String contactInfo;
	
	@JsonProperty("environment")
	private String environment;
	
	@JsonProperty("customfield_10000")
	private JiraClient client;
	
	
	public JiraIssue() {
		super();
	}
	
	public JiraIssue(JiraProject project, String summary, String description, JiraIssueType issueType) {
		this(project, summary, description, issueType, null, null, null, null, null);

	}
	
	public JiraIssue(JiraProject project, String summary, String description, 
			JiraIssueType issueType, JiraIssuePriority issuePriority, JiraVersion[] jiraVersion, String environment, String contactInfo, JiraClient client) {
		super();
		this.project = project;
		this.summary = summary;
		this.description = description;
		this.issueType = issueType;
		this.issuePriority = issuePriority;
		this.jiraVersion = jiraVersion;
		this.environment = environment;
		this.contactInfo = contactInfo;
		this.client = client;
	}

	/**
	 * Gets the JIRA project.
	 * @return
	 */
	@JsonProperty("project")
	public JiraProject getProject() {
		return project;
	}

	/**
	 * Sets the JIRA project.
	 * @param project
	 */
	@JsonProperty("project")
	public void setProject(JiraProject project) {
		this.project = project;
	}

	/**
	 * Gets the summary of the JIRA issue.
	 * @return
	 */
	@JsonProperty("summary")
	public String getSummary() {
		return summary;
	}

	/**
	 * Sets the summary of the JIRA issue.
	 * @param summary
	 */
	@JsonProperty("summary")
	public void setSummary(String summary) {
		this.summary = summary;
	}

	/**
	 * Gets the description of the JIRA issue.
	 * @return
	 */
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description of the JIRA issue.
	 * @param description
	 */
	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the JIRA issue type.
	 * @return
	 */
	@JsonProperty("issuetype")
	public JiraIssueType getIssueType() {
		return issueType;
	}

	/**
	 * Sets the JIRA issue type.
	 * @param issueType
	 */
	@JsonProperty("issuetype")
	public void setIssueType(JiraIssueType issueType) {
		this.issueType = issueType;
	}
	
	/**
	 * Gets the JIRA issue priority.
	 * @return
	 */
	@JsonProperty("priority")
	public JiraIssuePriority getIssuePriority() {
		return issuePriority;
	}

	/**
	 * Sets the JIRA issue priority.
	 * @param issuePriority
	 */
	@JsonProperty("priority")
	public void setIssuePriority(JiraIssuePriority issuePriority) {
		this.issuePriority = issuePriority;
	}

	/**
	 * Gets an array of available JIRA versions.
	 * @return
	 */
	@JsonProperty("versions")
	public JiraVersion[] getJiraVersion() {
		return jiraVersion;
	}

	/**
	 * Sets an array of available JIRA versions.
	 * @param jiraVersion
	 */
	@JsonProperty("versions")
	public void setJiraVersion(JiraVersion[] jiraVersion) {
		this.jiraVersion = jiraVersion;
	}

	/**
	 * Gets the JIRA contact information.
	 * @return
	 */
	@JsonProperty("customfield_10780")
	public String getContactInfo() {
		return contactInfo;
	}

	/**
	 * Sets the JIRA contact information.
	 * @param contactInfo
	 */
	@JsonProperty("customfield_10780")
	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
	 * Gets the JIRA environment string.
	 * @return
	 */
	@JsonProperty("environment")
	public String getEnvironment() {
		return environment;
	}

	/**
	 * Sets the JIRA environment string.
	 * @param environment
	 */
	@JsonProperty("environment")
	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	/**
	 * Gets the JIRA client.
	 * @return
	 */
	@JsonProperty("customfield_10000")
	public JiraClient getClient() {
		return client;
	}

	/**
	 * Sets the JIRA client.
	 * @param client
	 */
	@JsonProperty("customfield_10000")
	public void setClient(JiraClient client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "JiraIssue [project=" + project + ", summary=" + summary
				+ ", description=" + description + ", issueType=" + issueType
				+ ", issuePriority=" + issuePriority + ", jiraVersion="
				+ Arrays.toString(jiraVersion) + ", contactInfo=" + contactInfo
				+ ", environment=" + environment + ", client=" + client + "]";
	}
}
