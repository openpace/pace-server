/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

/**
 * Simple version of PafDimMember
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafSimpleDimMember {

	private String key;
	private String parentKey = null;
	private String[] childKeys = null;
	private PafSimpleDimMemberProps pafSimpleDimMemberProps = null;


	/**
	 *	Return the PafSimpleDimMember key (member name)
	 *
	 * @return the PafSimpleDimMember key (member name)
	 */
	public String getKey() {
		return key;
	}
	/**
	 *	Set the PafSimpleDimMember key (member name)
	 *
	 * @param key The PafSimpleDimMember key (member name)
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 *	Return the PafSimpleDimMember parent key (member name)
	 *
	 * @return the PafSimpleDimMember parent key (member name)
	 */
	public String getParentKey() {
		return parentKey;
	}
	/**
	 *	Set the PafSimpleDimMember parent key (member name)
	 *
	 * @param parentKey The PafSimpleDimMember parent key (member name)
	 */
	public void setParentKey(String parentKey) {
		this.parentKey = parentKey;
	}

	/**
	 *	Return the array of PafSimpleDimMember child keys 
	 *
	 * @return Returns the of PafSimpleDimMember child keys
	 */
	public String[] getChildKeys() {

		String[] keys = null;

		// If no childKeys are found, return empty array of members
		if (childKeys == null) {
			keys = new String[0];
		} else {
			// Else, return pointer to childKeys
			keys = childKeys;
		}
		return keys;
	}
	/**
	 *	Set the array of PafSimpleDimMember child keys
	 *
	 * @param childKeys The array of PafSimpleDimMember child keys to set
	 */
	public void setChildKeys(String[] childKeys) {
		this.childKeys = childKeys;
	}
	/**
	 * @return the pafSimpleDimMemberProps
	 */
	public PafSimpleDimMemberProps getPafSimpleDimMemberProps() {
		return pafSimpleDimMemberProps;
	}
	/**
	 * @param pafSimpleDimMemberProps the pafSimpleDimMemberProps to set
	 */
	public void setPafSimpleDimMemberProps(
			PafSimpleDimMemberProps pafSimpleDimMemberProps) {
		this.pafSimpleDimMemberProps = pafSimpleDimMemberProps;
	}

    /*
     *	Return the member key
     *
     * @see java.lang.Object#toString()
     */
    public String toString() {
    	return key;
     }
    
}
