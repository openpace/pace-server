/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * Cell property super class
 */
package com.pace.base.mdb;

/**
 * @author Alan Farkas
 *
 */
public abstract class CellProperty {

	protected CellPropertyType type = null;
	protected boolean[] propertyBits = null;
	
	protected boolean[] getPropertyBits() {
		return propertyBits;
	}
	
	protected void setPropertyBits(boolean[] propertyBits) {
		this.propertyBits = propertyBits;
	}

	public abstract Object getValue();
	public abstract void setValue(Object value); 
	public abstract boolean isValidValue(Object value);
	
	public static CellProperty getCellPropertyFactory(CellPropertyType propertyType) {

		CellProperty cellProperty = null;

		switch(propertyType) {
		case Dirty:
			cellProperty = new CellPropertyBoolean(propertyType);
			break;
		case Empty:
			cellProperty = new CellPropertyReverseBoolean(propertyType);
			break;
		default: 
			String logMsg = "Illegal property type: " + propertyType.toString()
					+ "  passed to cell property function";
			throw new IllegalArgumentException(logMsg);
		}

		return cellProperty;
	}

	/**
	 * @return the type
	 */
	public CellPropertyType getType() {
		return type;
	}

}
