/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.util.ArrayList;
import java.util.List;

public class SortingTuples implements Cloneable {
	List<SortingTuple> sortingTupleList;
	
	public SortingTuples() {
		// TODO Auto-generated constructor stub
	}

	public SortingTuples(List<SortingTuple> sortingTupleList) {
		this.sortingTupleList = sortingTupleList;
	}

	public SortingTuple getPrimarySortingTuple() {
		if( sortingTupleList.size() >= 1 ) {
			return sortingTupleList.get(0) != null ? sortingTupleList.get(0) : null;
		}
		else
			return null;
	}

	public SortingTuple getSecondarySortingTuple() {
		if( sortingTupleList.size() >= 2 ) {
			return sortingTupleList.get(1) != null ? sortingTupleList.get(1) : null;
		}
		else
			return null;
	}

	public SortingTuple getTertiarySortingTuple() {
		if( sortingTupleList.size() == 3 ) {
			return sortingTupleList.get(2) != null ? sortingTupleList.get(2) : null;
		}
		else
			return null;
	}

	/**
	 * @return the sortingTupleList
	 */
	public List<SortingTuple> getSortingTupleList() {
		return sortingTupleList;
	}

	/**
	 * @param sortingTupleList the sortingTupleList to set
	 */
	public void setSortingTupleList(List<SortingTuple> sortingTupleList) {
		this.sortingTupleList = sortingTupleList;
	}

	@Override
	public SortingTuples clone() {
		SortingTuples clonedSortingTuples = new SortingTuples();
		
		if ( this.sortingTupleList != null ) {
			
			clonedSortingTuples.sortingTupleList = new ArrayList<SortingTuple>();
			
			for ( SortingTuple sortingTuple : this.sortingTupleList ) {
				
				clonedSortingTuples.sortingTupleList.add(sortingTuple.clone());
				
			}
			
		}
		
		return clonedSortingTuples;
	}
}
