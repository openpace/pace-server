/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import com.pace.base.Axis;
import com.pace.base.comm.SimpleCoordList;
import com.pace.base.data.Intersection;

/* Class to hold the freeze pane tuple in the view section*/
public class FreezePaneTuple implements Cloneable {
//	private Intersection intersection;
	
	private SimpleCoordList coordList;
	
	public SimpleCoordList getCoordList() {
		return coordList;
	}

	public void setCoordList(SimpleCoordList coordList) {
		this.coordList = coordList;
	}

	private Axis axis;
	
	public FreezePaneTuple() {
		
	}
	
	public FreezePaneTuple(SimpleCoordList list, Axis axis) {
		this.coordList = list;
		this.axis = axis;
	}
	


	public Axis getAxis() {
		return axis;
	}

	public void setAxis(Axis axis) {
		this.axis = axis;
	}
	
	@Override
	public FreezePaneTuple clone() {
		FreezePaneTuple clonedTuple = null;
		try {
			clonedTuple = (FreezePaneTuple) super.clone();
		//	clonedTuple.coordList = this.coordList.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return clonedTuple;
	}
}
