/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;


/**
 * Basic Multidimensional Database Properties
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafMdbProps {
	
	// basic properties
	private String[] aliasTables = null;
	private String[] attributeDims = null;
	private String[] baseDims = null;
	private String[] cachedAttributeDims = null;
	
	// base dimension lookup - Because of wsdl restrictions,  hash map is broken out into separate arrays 
	private String[] baseDimLookupKeys;		
	private String[] baseDimLookupValues;
	
	// gen/level info
	private DimLevelGenInfo[] levelInfoArray = null;
	private DimLevelGenInfo[] genInfoArray = null;
	
	// set dimension types. added for new project wizard for EssbaseOutline

	private String accountsDim = null;
	private String timeDim = null;
	
	// logger
	//private static Logger logger = Logger.getLogger(PafMdbProps.class);

	public String getAccountsDim() {
		return accountsDim;
	}
	public void setAccountsDim(String accountsDim) {
		this.accountsDim = accountsDim;
	}
	public String getTimeDim() {
		return timeDim;
	}
	public void setTimeDim(String timeDim) {
		this.timeDim = timeDim;
	}
	/**
	 * @return Returns the aliasTables.
	 */
	public String[] getAliasTables() {
		return aliasTables;
	}
	/**
	 * @param aliasTables The aliasTables to set.
	 */
	public void setAliasTables(String[] aliasTables) {
		this.aliasTables = aliasTables;
	}
	/**
	 * @return Returns the attributeDims.
	 */
	public String[] getAttributeDims() {
		return attributeDims;
	}
	/**
	 * @param attributeDims The attributeDims to set.
	 */
	public void setAttributeDims(String[] attributeDims) {
		this.attributeDims = attributeDims;
	}
	/**
	 * @return Returns the baseDims.
	 */
	public String[] getBaseDims() {
		return baseDims;
	}
	/**
	 * @param baseDims The baseDims to set.
	 */
	public void setBaseDims(String[] baseDims) {
		this.baseDims = baseDims;
	}
	/**
	 * @return the baseDimLookupKeys
	 */
	public String[] getBaseDimLookupKeys() {
		return baseDimLookupKeys;
	}
	/**
	 * @param baseDimLookupKeys the baseDimLookupKeys to set
	 */
	public void setBaseDimLookupKeys(String[] baseDimLookupKeys) {
		this.baseDimLookupKeys = baseDimLookupKeys;
	}
	/**
	 * @return the baseDimLookupValues
	 */
	public String[] getBaseDimLookupValues() {
		return baseDimLookupValues;
	}
	/**
	 * @param baseDimLookupValues the baseDimLookupValues to set
	 */
	public void setBaseDimLookupValues(String[] baseDimLookupValues) {
		this.baseDimLookupValues = baseDimLookupValues;
	}
	
	/**
	 * Gets the cached dimensions.
	 * @return An array of cached dimensions.
	 */
	public String[] getCachedAttributeDims() {
		return cachedAttributeDims;
	}
	
	/**
	 * Sets the attributes that are currently cached.
	 * @param cachedAttributeDims Array of cached dims.
	 */
	public void setCachedAttributeDims(String[] cachedAttributeDims) {
		this.cachedAttributeDims = cachedAttributeDims;
	}

	/**
	 * @return the levelInfoArray
	 */
	public DimLevelGenInfo[] getLevelInfoArray() {
		return levelInfoArray;
	}
	/**
	 * @param levelInfoArray the levelInfoArray to set
	 */
	public void setLevelInfoArray(DimLevelGenInfo[] levelInfoArray) {
		this.levelInfoArray = levelInfoArray;
	}
	/**
	 * @return the genInfoArray
	 */
	public DimLevelGenInfo[] getGenInfoArray() {
		return genInfoArray;
	}
	/**
	 * @param genInfoArray the genInfoArray to set
	 */
	public void setGenInfoArray(DimLevelGenInfo[] genInfoArray) {
		this.genInfoArray = genInfoArray;
	}
	
	/**
	 * Return a map that provides a lookup of attribute dimension to base dimension
	 * 
	 * @param attributeDim Attribute dimension name
	 * 
	 * @return Base dimension lookup
	 * 
	 */
	public String getAssociatedBaseDim(String attributeDim) {

		// Look for specified attribute dimension
		if (getBaseDimLookupKeys() != null) {
			for (int i = 0; i < getBaseDimLookupKeys().length; i++) {
				if (attributeDim.equalsIgnoreCase(getBaseDimLookupKeys()[i])) {
					// Return associated base dimension
					return getBaseDimLookupValues()[i];
				}
			}
		}
		
		// Dimension not found, throw exception		
		String errMsg = "Unable to get associated Base Dimension for Attribute Dimension: [" + attributeDim + "] - Dimension was not found";
//		logger.error(errMsg);
		IllegalArgumentException iae = new IllegalArgumentException(errMsg);    
		throw iae; 

	}

}
