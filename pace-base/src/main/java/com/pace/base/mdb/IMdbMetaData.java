/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import com.pace.base.PafException;
import com.pace.base.SystemStatus;

/**
 * Provides access to the mid-tier meta-data layer
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public interface IMdbMetaData {

    /**
     *	Return a tree structure containing an Essbase dimension member, it's descendants,
     *  and any member properties. This method will in-turn call getDimension(0, branch, 0).
     *
     * @param branch 
     * @return PafBaseTree
     * @throws PafException 
     */
    public PafBaseTree getBaseDimension(String branch) throws PafException;

    /**
     *	Return a tree structure containing an Essbase dimension member, it's descendants,
     *  and any member properties. This method will in-turn call getDimension(essNetTimeOut, branch, 0).
     *
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
     * @param branch 
     * @return PafBaseTree
     * @throws PafException 
     */
    public PafBaseTree getBaseDimension(int essNetTimeOut, String branch) throws PafException;

    /**
     *	Return a tree structure containing an Essbase dimension member, it's descendants,
     *  and any member properties - down to the specified Essbase level.
     *
     * @param branch
     * @param toLevel
     * @return PafBaseTree 
     * @throws PafException 
     */
    public PafBaseTree getBaseDimension(String branch, int toLevel) throws PafException;

    /**
     *	Return a tree structure containing an Essbase dimension member, it's descendants,
     *  and any member properties - down to the specified Essbase level.
     *
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
     * @param branch
     * @param toLevel
     * @return PafBaseTree 
     * @throws PafException 
     * @throws ClassNotFoundException 
     */
    public PafBaseTree getBaseDimension(int essNetTimeOut, String branch, int toLevel) throws PafException;

	/**
	 *	Extract the specificed Attribute Dimension from Essbase
	 *
	 * @param dimension An array of attribute dimension names
	 * @param mdbProps Properties of multi-dimensional database
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * 
	 * @return Paf attribute tree
	 * @throws PafException 
	 */

	public PafAttributeTree getAttributeDimension(int essNetTimeOut, String dimension, PafMdbProps mdbProps) throws PafException;
      
    /**
     * Disconnect from Essbase
     * @throws PafException 
     * 
     */
    public void disconnect() throws PafException;

	/**
	 *	Return basic multidimensional database properties
	 *
	 * @param essNetTimeOut Essbase network timeout (in milliseconds)
	 * @return MdbProps Basic multidimensional database properties
	 * 
	 * @throws PafException 
	 */
	public abstract PafMdbProps getMdbProps(int essNetTimeOut) throws PafException;
	
	public boolean varyingAttributesExist(String[] dims, PafMdbProps mdbProps, int essNetTimeOut)throws PafException;
	
	/**
	 * Retrieves the value of the substitution variable with the specified name, if it exists
	 * 
	 * @param varName
	 * @return
	 */
    public String findSubVarValue(String varName);
    
    /**
     * Gets statistics about the underlying MDB provider.
     * @param runVerboseTest Get verbose level results
     * @return IMdbSystemStatus
     */
	public IMdbSystemStatus getMdbStats(boolean runVerboseTest); 

	/**
	 * @return the connectionAlias
	 */    
	public String getConnectionAlias();

}
