/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.pace.base.data.Intersection;

//import org.apache.log4j.Logger;

/**
 * List of PafDataCacheCells
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafDataCacheCells {

	private List<PafDataCacheCell> cells = new ArrayList<PafDataCacheCell>();
//	private static Logger logger = Logger.getLogger(PafDataCacheCells.class);
	
	
	PafDataCacheCells () {}

	/**
	 *	Add cell to list of cells
	 *
	 * @param cellIntersection
	 * @param cellValue
	 */
	public void add(Intersection cellIntersection, double cellValue) {
		PafDataCacheCell cell = new PafDataCacheCell(cellIntersection, cellValue);
		cells.add(cell);
	}
		
	/**
	 * @return Returns the cells.
	 */
	public List<PafDataCacheCell> getCells() {
		return cells;
	}
	
	
	/**
	 * 
	 * Return the set of changed cell intersections
	 * 
	 * @return List<Intersection>
	 */
	public Set<Intersection> getCellIntersections() {
		
		Set<Intersection> intersections = new HashSet<Intersection>();
		for (PafDataCacheCell cell : cells) {
			intersections.add(cell.getCellIntersection());
		}
		return intersections;		
	}
}

