/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

/**
 * Data cache cell properties
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafDataCacheCellProps {
	
	Integer roundedDecimalPlaces = null;

	/**
	 * @return the roundedDecimalPlaces
	 */
	public Integer getRoundedDecimalPlaces() {
		return roundedDecimalPlaces;
	}

	/**
	 * @param roundedDecimalPlaces the roundedDecimalPlaces to set
	 */
	public void setRoundedDecimalPlaces(Integer roundedDecimalPlaces) {
		this.roundedDecimalPlaces = roundedDecimalPlaces;
	}
	
	/**
	 *	Inidicates if the cell is to be rounded
	 *
	 * @return True if cell if to be rounded
	 */
	public boolean isRounded() {
		
		if (getRoundedDecimalPlaces() != null) {
			return true;
		}
		
		return false;

	}

}
