/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

/**
 * A simple version of the PafDimMemberProps object
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public class PafSimpleDimMemberProps {

	private long id;
	private String[] aliasKeys = null;
	private String[] aliasValues = null;
	
	private int consolidationType = 0;
	public static final int CONSOLIDATION_TYPE_ADDITION = 0; 
	public static final int CONSOLIDATION_TYPE_SUBTRACTION = 1; 
	public static final int CONSOLIDATION_TYPE_MULTIPLICATION = 2; 
	public static final int CONSOLIDATION_TYPE_DIVISION = 3; 
	public static final int CONSOLIDATION_TYPE_PERCENT = 4; 
	public static final int CONSOLIDATION_TYPE_IGNORE = 5; 

	protected int timeBalanceOption;
	public static final int TIME_BALANCE_OPTION_NONE = 0; 
	public static final int TIME_BALANCE_OPTION_FIRST = 1; 
	public static final int TIME_BALANCE_OPTION_LAST = 2; 
	public static final int TIME_BALANCE_OPTION_AVERAGE = 3; 

	private boolean isExpense = false;
	protected boolean isTwoPassCalc = false;
	
	private int generationNumber = 0;
	private int levelNumber = 0;
	private boolean isReadOnly = false;
	private boolean isSynthetic = false;

	/**
	 *	Return the SimplePafMemberAttr id
	 *
	 * @return Returns the SimplePafMemberAttr id
	 */
	public long getId() {
		return id;
	}
	/**
	 *	Set the SimplePafMemberAttr id
	 *
	 * @param id The SimplePafMemberAttr id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 *	Return the Essbase alias keys (alias table names)
	 *
	 * @return Returns the Essbase alias keys (alias table names)
	 */	
	public String[] getAliasKeys() {
		return aliasKeys;
	}
	/**
	 *	Set the Essbase alias keys (alias table names)
	 *
	 * @param aliasKeys The Essbase alias keys (alias table names)
	 */	
	public void setAliasKeys(String[] aliasKeys) {
		this.aliasKeys = aliasKeys;
	}
		
	/**
	 *	Return the Essbase alias values
	 *
	 * @return Returns the Essbase alias values
	 */	
	public String[] getAliasValues() {
		return aliasValues;
	}
	/**
	 *	Set the Essbase alias values
	 *
	 * @param aliasValues The Essbase alias values 
	 */	
	public void setAliasValues(String[] aliasValues) {
		this.aliasValues = aliasValues;
	}
		
	/**
	 * Return the member consolidation type
	 *
	 * @return Returns the member consolidation type
	 */
	public int getConsolidationType() {
		return consolidationType;
	}
	/**
	 * Set the member consolidation type
	 *
	 * @param consolidationType The member consolidation type to set
	 */
	public void setConsolidationType(int consolidationType) {
		this.consolidationType = consolidationType;
	}	
		
	/**
	 *	Return the generation number
	 *
	 * @return Returns the generation number
	 */
	public int getGenerationNumber() {
		return generationNumber;
	}
	/**
	 *	Set the generation number
	 *
	 * @param generationNumber The generation number to set
	 */
	public void setGenerationNumber(int generationNumber) {
		this.generationNumber = generationNumber;
	}
	
	/**
	 * Return the member level number
	 *
	 * @return Returns the member level number
	 */
	public int getLevelNumber() {
		return levelNumber;
	}
	/**
	 * Set the member level number
	 *
	 * @param levelNumber The member level number to set
	 */
	public void setLevelNumber(int levelNumber) {
		this.levelNumber = levelNumber;
	}
	/**
	 * @return the isReadOnly
	 */
	public boolean isReadOnly() {
		return isReadOnly;
	}
	/**
	 * @param isReadOnly the isReadOnly to set
	 */
	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}
	/**
	 * @return the isSynthetic
	 */
	public boolean isSynthetic() {
		return isSynthetic;
	}
	/**
	 * @param isSynthetic the isSynthetic to set
	 */
	public void setSynthetic(boolean isSynthetic) {
		this.isSynthetic = isSynthetic;
	}
	
	
	/**
	 * @return the timeBalanceOption
	 */
	public int getTimeBalanceOption() {
		return timeBalanceOption;
	}
	/**
	 * @param timeBalanceOption the timeBalanceOption to set
	 */
	public void setTimeBalanceOption(int timeBalanceOption) {
		this.timeBalanceOption = timeBalanceOption;
	}
	
	/**
	 * @return the isTwoPassCalc
	 */
	public boolean isTwoPassCalc() {
		return isTwoPassCalc;
	}
	/**
	 * @param isTwoPassCalc the isTwoPassCalc to set
	 */
	public void setTwoPassCalc(boolean isTwoPassCalc) {
		this.isTwoPassCalc = isTwoPassCalc;
	}
	
	
	/**
	 * @return the isExpense
	 */
	public boolean isExpense() {
		return isExpense;
	}
	/**
	 * @param isExpense the isExpense to set
	 */
	public void setExpense(boolean isExpense) {
		this.isExpense = isExpense;
	}
}
