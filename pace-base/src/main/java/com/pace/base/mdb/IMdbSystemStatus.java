/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.io.File;

import com.pace.base.ISystemStatus;

/**
 * Interface to hold status information about the underlying MDB structure.
 * @author themoosman
 *
 */
public interface IMdbSystemStatus extends ISystemStatus {

	/**
	 * Get the IMdbCubeInfo object
	 * @return
	 */
	public IMdbCubeInfo getMdbCubeInfo() ;
	
	/**
	 * Sets the IMdbCubeInfo object 
	 * @param mdbServerInfo
	 */
	public void setMdbCubeInfo(IMdbCubeInfo mdbServerInfo);
	
	/**
	 * Gets the Outline associated to the Cube.
	 * @return A temporary file
	 */
	public File getOutline();
	
	/**
	 * Sets the outline associated to the Cube.
	 * @param outline
	 */
	public void setOutline(File outline);
	
	/**
	 * Gets an array of calculation script files.
	 * @return
	 */
	public File[] getCalcScripts();
	
	/**
	 * Sets an array of calculation script files.
	 * @param calcScripts
	 */
	public void setCalcScripts(File[] calcScripts);
	
	/**
	 * Gets an array of report script files.
	 * @return
	 */
	public File[] getReportScripts();
	
	/**
	 * Sets an array of report script files.
	 * @param reportScripts
	 */
	public void setReportScripts(File[] reportScripts);
}
