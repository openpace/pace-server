/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

/**
 * Interface to hold information about an MDB Cube or applicable structure.
 * @author themoosman
 *
 */
public interface IMdbCubeInfo {

	/**
	 * Gets the cube name.
	 * @return
	 */
	public String getName();
	
	/**
	 * Gets the retrieval buffer size (PROP_RETRIEVAL_BUFFER)
	 * @return
	 */
	public int getRetrievalBufferSize();
	
	/**
	 * Gets the retrieval sort buffer size (PROP_RETRIEVAL_SORT_BUFFER)
	 * @return
	 */
	public int getRetrievalSortBuffer();
	
	/**
	 * Signals that data resides in the cube (PROP_TOTAL_BLOCKS).
	 * @return
	 */
	public double getExistingBlocks();
	
	/**
	 * Gets the index cache setting (PROP_INDEX_CACHE_SETTING).
	 * @return
	 */
	public int getIdxCacheSetting();
	
	/**
	 * Gets the data cache setting (PROP_DATA_CACHE_SETTING).
	 * @return
	 */
	public int getDataCacheSetting();
	
	/**
	 * Gets the names of the dimensions in the cube.
	 * @return
	 */
	public String[] getDimensionNames();
	
	/**
	 * Gets an array of the calculation scripts available to the cube.
	 * @return
	 */
	public String[] getCalcScriptNames();
	
	/**
	 * Gets an array of the report scripts available to the cube.
	 * @return
	 */
	public String[] getReportScriptNames();
	
	/**
	 * Sets the cube name.
	 * @param name
	 */
	public void setName(String name);
	
	/**
	 * Sets the retrieval buffer size (PROP_RETRIEVAL_BUFFER)
	 * @param retrievalBufferSize
	 */
	public void setRetrievalBufferSize(int retrievalBufferSize);
	
	/**
	 * Sets the retrieval sort buffer size (PROP_RETRIEVAL_SORT_BUFFER)
	 * @param retrievalSortBuffer
	 */
	public void setRetrievalSortBuffer(int retrievalSortBuffer);
	
	/**
	 * Sets signals that data resides in the cube (PROP_TOTAL_BLOCKS).
	 * @param existingBlocks
	 */
	public void setExistingBlocks(double existingBlocks);
	
	/**
	 * Sets the index cache setting (PROP_INDEX_CACHE_SETTING).
	 * @param idxCacheSetting
	 */
	public void setIdxCacheSetting(int idxCacheSetting);
	
	/**
	 * Sets the data cache setting (PROP_DATA_CACHE_SETTING).
	 * @param dataCacheSetting
	 */
	public void setDataCacheSetting(int dataCacheSetting);
	
	/**
	 * Sets the names of the dimensions in the cube.
	 * @param dimensionNames
	 */
	public void setDimensionNames(String[] dimensionNames) ;
	
	/**
	 * Sets an array of the calculation scripts available to the cube.
	 * @param calcScripts
	 */
	public void setCalcScriptNames(String[] calcScripts);
	
	/**
	 * Sets an array of the report scripts available to the cube.
	 * @param reportScripts
	 */
	public void setReportScriptNames(String[] reportScripts);
	
}
