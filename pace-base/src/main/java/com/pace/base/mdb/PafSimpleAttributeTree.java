/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

/**
 * This is a simplified version of the Paf Attribute Member Tree
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafSimpleAttributeTree extends PafSimpleDimTree {
 
    public PafSimpleAttributeTree() {}

    /**
     * @param rootKey Root Node to use in initializing the PafSimpleBaseTree
	 * @param aliasTableNames List of Essbase table names
     */
    @Deprecated
    public PafSimpleAttributeTree(String rootKey, String[] aliasTableNames) {
        
    	// Set instance variables
        setId(rootKey);
    	setRootKey(rootKey);
      	setAliasTableNames(aliasTableNames);
   }
    
    /**
     * @param id Attribute name 
     * @param rootKey Root Node to use in initializing the PafSimpleBaseTree
	 * @param aliasTableNames List of Essbase table names
     */
    public PafSimpleAttributeTree(String id, String rootKey, String[] aliasTableNames) {
        
    	// Set instance variables
        setId(id);
    	setRootKey(rootKey);
      	setAliasTableNames(aliasTableNames);
   }

}
