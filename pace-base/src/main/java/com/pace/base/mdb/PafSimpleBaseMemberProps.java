/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

/**
 * A simple version of the PafBaseMemberProps object
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public class PafSimpleBaseMemberProps extends PafSimpleDimMemberProps {
	
	private String[] associatedAttrKeys = null;	
	private String[] associatedAttrValues = null;	
	private String memberDescription = null;
	private String memberFormula = null;
	private String lastMemberFormula = null;
//	private boolean isExpense = false;
//	private boolean isTwoPassCalc = false;
	
	private int shareOption;
	public static final int SHARE_OPTION_STORE_DATA = 0; 
	public static final int SHARE_OPTION_NEVER_SHARE = 1; 
	public static final int SHARE_OPTION_LABEL_ONLY = 2; 
	public static final int SHARE_OPTION_SHARED_MEMBER = 3; 
	public static final int SHARE_OPTION_DYNAMIC_CALC_AND_STORE = 4; 
	public static final int SHARE_OPTION_DYNAMIC_CALC = 5; 
	
/*	private int timeBalanceOption;
	public static final int TIME_BALANCE_OPTION_NONE = 0; 
	public static final int TIME_BALANCE_OPTION_FIRST = 1; 
	public static final int TIME_BALANCE_OPTION_LAST = 2; 
	public static final int TIME_BALANCE_OPTION_AVERAGE = 3; 

	*/
	private String[] UDAs = null;
	
	
	/**
	 * @return the associatedAttrKeys
	 */
	public String[] getAssociatedAttrKeys() {
		return associatedAttrKeys;
	}
	/**
	 * @param associatedAttrKeys the associatedAttrKeys to set
	 */
	public void setAssociatedAttrKeys(String[] associatedAttrKeys) {
		this.associatedAttrKeys = associatedAttrKeys;
	}

	/**
	 * @return the associatedAttrValues
	 */
	public String[] getAssociatedAttrValues() {
		return associatedAttrValues;
	}
	/**
	 * @param associatedAttrValues the associatedAttrValues to set
	 */
	public void setAssociatedAttrValues(String[] associatedAttrValues) {
		this.associatedAttrValues = associatedAttrValues;
	}
	
	/**
	 * Return the member description
	 *
	 * @return Returns the member description
	 */
	public String getDescription() {

		String description = null;
		
		if (memberDescription != null) {
			description = memberDescription;
		} else {
			description =  "";
		}
		return description;
	}
	/**
	 * Set the member description
	 *
	 * @param description The member description to set
	 */
	public void setDescription(String description) {
		this.memberDescription = description;
	}
	
	
	/**
	 * Return the member formula
	 *
	 * @return Returns the member formula.
	 */
	public String getFormula() {

		String formula = null;
		
		if (memberFormula != null) {
			formula = memberFormula;
		} else {
			formula = "";
		}
		return formula;
	}
	/**
	 * Set the member formula
	 *
	 * @param formula The member formula to set
	 */
	public void setFormula(String formula) {
		this.memberFormula = formula;
	}
	
	/**
	 * Return the last member formula
	 *
	 * @return Returns the last member formula
	 */
	public String getLastFormula() {
		
		String formula = null;
		
		if (lastMemberFormula != null) {
			formula = lastMemberFormula;
		} else {
			formula = "";
		}
		return formula;
	}
	/**
	 *	Set the last member formula
	 *
	 * @param lastFormula The last member formula used to evaluate this member
	 */
	public void setLastFormula(String lastFormula) {
		this.lastMemberFormula = lastFormula;
	}
	
	/**
	 * Return the member share option
	 *
	 * @return Returns the member share option
	 */
	public int getShareOption() {
		return shareOption;
	}
	/**
	 * Set the member share option
	 *
	 * @param shareOption The member share option to set
	 */	
	public void setShareOption(int shareOption) {
		this.shareOption = shareOption;
	}
	
	
	
	/**
	 * Return the member UDAs
	 *
	 * @return Returns the member UDAs.
	 */
	public String[] getUDAs() {
		return UDAs;
	}
	/**
	 * Set the member UDAs
	 *
	 * @param uDAs The member UDAs to set.
	 */
	public void setUDAs(String[] uDAs) {
		this.UDAs = uDAs;
	}
	
}
