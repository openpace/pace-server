/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.PafKeystoreEncryptionException;
import com.pace.base.PafKeystorePasswordReadException;
import com.pace.base.security.AESKeystoreEncryption;

/**
 * 
 * @author JMilliron
 *
 */
public class PafConnectionProps implements IPafConnectionProps, Cloneable {
	
	private static Logger logger = Logger.getLogger(PafConnectionProps.class);
	private static final String PASSWORD_KEY = PafBaseConstants.CONN_STRING_PSWD_KEY;
	private String name;
    private String connectionString;
    private transient Properties properties;
    private String metaDataServiceProvider;
    private String dataServiceProvider;
    private String mdbClassLoader;
    private String connectionToolTip;
    private int version;
    private transient AESKeystoreEncryption encrptor;


	public PafConnectionProps() {
    }
        
    /**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @throws PafKeystoreEncryptionException 
	 * 
	 */
	@Override
	public void setConnectionString(String connectionString) throws PafKeystoreEncryptionException {
        this.connectionString = encryptConnectionString(connectionString);
        this.properties = null;
    }

	@Override
	public void setEncryptedConnectionString(String connectionString) {
		this.connectionString = connectionString;
		
	}
	
    /**
	 * @return the unmodified connectionString 
	 */
	@Override
	public String getConnectionString() {
		return connectionString;
	}
	
	/**
	 * @return the decrypted connectionString
	 * @throws PafKeystoreDecryptionException 
	 */
	//@Override
	public String decryptConnectionString() throws PafKeystoreDecryptionException {
		return decryptConnectionString(connectionString);
	}

	@Override
	public Properties getProperties() throws PafKeystoreDecryptionException {
		if(connectionString != null && properties == null){
			this.properties = parseConnString(connectionString, true, true); 
		}
        return properties;
    }

    /**
     * Use this method with caution.  
     * It will NOT modify the connection string parameter.
     * 
     * @param properties
     */
    public void setProperties(Properties properties) {
        this.properties = properties; 
    }

    /**
     * @return Returns the dataServiceProvider.
     */
	@Override
    public String getDataServiceProvider() {
        return dataServiceProvider;
    }

    /**
     * @param dataServiceProvider The dataServiceProvider to set.
     */
	@Override
    public void setDataServiceProvider(String dataServiceProvider) {
        this.dataServiceProvider = dataServiceProvider;
    }

    /**
     * @return Returns the metaDataServiceProvider.
     */
	@Override
    public String getMetaDataServiceProvider() {
        return metaDataServiceProvider;
    }

    /**
     * @param metaDataServiceProvider The metaDataServiceProvider to set.
     */
	@Override
    public void setMetaDataServiceProvider(String metaDataServiceProvider) {
        this.metaDataServiceProvider = metaDataServiceProvider;
    }

    /**
	 * @return the mdbClassLoader
	 */
	@Override
	public String getMdbClassLoader() {
		return mdbClassLoader;
	}

	/**
	 * @param mdbClassLoader the mdbClassLoader to set
	 */
	@Override
	public void setMdbClassLoader(String mdbClassLoader) {
		this.mdbClassLoader = mdbClassLoader;
	}
	
    @Override
	public String getConnectionToolTip() {
		return connectionToolTip;
	}

    @Override
	public void setConnectionToolTip(String connectionToolTip) {
		this.connectionToolTip = connectionToolTip;
	}

	/**
	 * 
	 * @return
	 */
	@Override
    public int getVersion() {
		return version;
	}

    /**
     * 
     * @param version
     */
	@Override
	public void setVersion(int version) {
		this.version = version;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PafConnectionProps [name=" + name + ", connectionString="
				+ connectionString + ", metaDataServiceProvider="
				+ metaDataServiceProvider + ", dataServiceProvider="
				+ dataServiceProvider + ", mdbClassLoader=" + mdbClassLoader
				+ ", properties=" + properties + "]";
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public PafConnectionProps clone() throws CloneNotSupportedException {
		PafConnectionProps tmpProps = (PafConnectionProps) super.clone();
		tmpProps.setEncryptedConnectionString(this.getConnectionString());
		return tmpProps;

	}

    /**
     * Central method to handle the "First" split of a connection string.
     * @param connString
     * @return
     */
    public static List<String> splitConnStr(String connString){
    	List<String> ret = new ArrayList<String>(20);
    	Collections.addAll(ret, connString.split(";"));
    	return ret;
    }
    
    /**
     * Central method to handle the split of the connection string key value pair (Key=Value)
     * @param keyValue
     * @param upperCaseKey If true the key of the Key/Value pair will be upper cased.
     * @return
     */
    public static String[] splitConnStrKeyValue(String keyValue, boolean upperCaseKey){
    	String[] kvPair = keyValue.split("=");
        if (kvPair.length != 2) { 
        	throw new IllegalArgumentException ("Invalid Connection String Term: " + keyValue);            
        }
        if(upperCaseKey){
        	kvPair[0] = kvPair[0].toUpperCase();
        }
        
        return kvPair;
    }
    
    /**
     * Performs a decryption of the connection string.
     * @param connString
     * @return
     * @throws PafKeystoreEncryptionException
     */
    private String encryptConnectionString(String connString) throws PafKeystoreEncryptionException {
    	try {
			return cryptConnectionString(connString, true);
		} catch (PafKeystoreDecryptionException e) {
			//This should never happen since we are performing a encryption
			e.printStackTrace();
			return null;
		}
    }
    
    /**
     * Performs a decryption of the connection string.
     * @param connString
     * @return
     * @throws PafKeystoreDecryptionException
     */
    private String decryptConnectionString(String connString) throws PafKeystoreDecryptionException {
    	try {
			return cryptConnectionString(connString, false);
		} catch (PafKeystoreEncryptionException e) {
			//This should never happen since we are performing a decryption
			e.printStackTrace();
			
			return null;
		}
    }
    
    
    /**
     * Method to encrypt or decrypt the connection string.  
     * If called from the Getter, then a decryption will be called
     * if called from setter then an encryption will be called.
     * @param connString
     * @param performEncryption if true the Password parm will be encrypted, if false a decryption will occur.
     * @return the encrypted or decrypted connection string
     * @throws PafException 
     */
    private String cryptConnectionString(String connString, boolean performEncryption) throws PafKeystoreDecryptionException, PafKeystoreEncryptionException {
    	if ( connString != null ) {
    		List<String> terms = splitConnStr(connString);
	        String[] keyValue;
	        for (String term : terms) {
	        	keyValue = splitConnStrKeyValue(term, false);
	            String key = keyValue[0];
	            if(key.equalsIgnoreCase(PASSWORD_KEY)){
	            	String newValue = "";
	            	
	            	if(performEncryption){
	            		try {
							newValue = getEncryptor().encryptToHexString(keyValue[1]);
						} catch (PafKeystoreEncryptionException e) {
							//logger.error(e.getMessage());
							logger.error(String.format("Cannot encrypt password for connection string: %s, the password must be reentered using the settings application.", name), e);
							throw e;
						} catch (PafKeystorePasswordReadException e) {
							//logger.error(e.getMessage());
							String errMsg = String.format("Cannot encrypt password for connection string: %s, the password must be reentered using the settings application.", name);
							logger.error(errMsg, e);
							throw new PafKeystoreEncryptionException(errMsg, e.getSeverity(), e.getPafSoapException());
						}
	            	} else{
	            		try {
							newValue = getEncryptor().decryptFromHexString(keyValue[1]);
							
						} catch (PafKeystoreDecryptionException e) {
							//logger.error(e.getMessage());
							logger.error(String.format("Cannot decrypt password for connection string: %s, the password must be reentered using the settings application.", name), e);
							throw e;
						} catch (PafKeystorePasswordReadException e) {
							//logger.error(e.getMessage());
							String errMsg = String.format("Cannot decrypt password for connection string: %s, the password must be reentered using the settings application.", name);
							logger.error(errMsg, e);
							throw new PafKeystoreDecryptionException(errMsg, e.getSeverity(), e.getPafSoapException());
						}
	            	}
	            	return connString.replace(key + "=" + keyValue[1], key + "=" +  newValue);
	            }
	        }
    	}
    	return connString;
    }
    
    /**
     * Converts the connection string into a Properties object
     * @param connString 
     * @param upperCaseKey True to upper case the keys.
     * @param decrypt True to decrypt the "Password" value
     * @return
     * @throws Exception 
     * @throws PafException 
     */
    public  Properties parseConnString(String connString, boolean upperCaseKey, boolean decrypt) throws PafKeystoreDecryptionException {
        Properties p = new Properties();
        
        if ( connString != null ) {
	        List<String> terms = splitConnStr(connString);
	        String[] kvPair;
	        for (String term : terms) {
	            kvPair = splitConnStrKeyValue(term, upperCaseKey);
	            String key = kvPair[0];
	            String value = kvPair[1];
	            if(decrypt && key.equalsIgnoreCase(PASSWORD_KEY)){
	            	try {
						value = getEncryptor().decryptFromHexString(value);
					} catch (PafKeystorePasswordReadException e) {
						String errMsg = String.format("Cannot decrypt password for connection string: %s, the password must be reentered using the settings application.", name);
						logger.error(errMsg, e);
						throw new PafKeystoreDecryptionException(errMsg, e.getSeverity(), e.getPafSoapException());
					}
	            	
	            }
	         /*   if(value==null)
	            	p.put(key, "");
	            else */
	            	p.put(key, value);
	        }
        }
        return p;
    }
	
    /**
     * Lazy load the Encryption object.
     * @return
     * @throws PafKeystorePasswordReadException 
     */
	private  AESKeystoreEncryption getEncryptor() throws PafKeystorePasswordReadException{
		if(encrptor == null){
			encrptor = new AESKeystoreEncryption();
		}
		return encrptor;
	}
}
