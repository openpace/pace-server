/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * Boolean value cell property
 */
package com.pace.base.mdb;

/**
 * @author Alan Farkas
 *
 */
public class CellPropertyBoolean extends CellProperty{

	public CellPropertyBoolean(CellPropertyType propertyType) {
		this.type = propertyType;
		propertyBits = new boolean[propertyType.getBitCount()];
	}

	public Object getValue() {
		return (Boolean) propertyBits[0];
	}
		
	public void setValue(Object value) {
		propertyBits[0] = (Boolean) value;
	}

	public boolean isValidValue(Object value) {
		return (value instanceof Boolean);
	}

}
