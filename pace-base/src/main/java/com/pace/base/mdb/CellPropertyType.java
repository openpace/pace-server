/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * Cell property types
 */
package com.pace.base.mdb;

/**
 * @author Alan Farkas
 *
 */
public enum CellPropertyType {
	
	Dirty(1,false),
	Empty(1,true);
	
	private int bitCount = 0;		// Number of bits in property type
	private Object defaultValue;	// Default property value (if cell does not exist)
	
	CellPropertyType(int bitCount, Object defaultValue) {
		this.bitCount = bitCount;
		this.defaultValue = defaultValue;
	}
	
	
	/**
	 * @return the bitCount
	 */
	public int getBitCount() {
		return bitCount;
	}

	/**
	 * @return the defaultValue
	 */
	public Object getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @return the property type's offset index
	 */
	public int getOffSet() {
		int offSet = 0;
		for (CellPropertyType propertyType : CellPropertyType.values()) {
			if (propertyType.equals(this)) break;
			offSet += propertyType.getBitCount();
		}
		return offSet;
	}
	
	/**
	 * @return the total number of bits across all property types
	 */
	public static int getTotBitCount() {
		int totBitCount = 0;
		for (CellPropertyType propertyType : CellPropertyType.values()) {
			totBitCount += propertyType.getBitCount();
		}
		return totBitCount;
	}

}

