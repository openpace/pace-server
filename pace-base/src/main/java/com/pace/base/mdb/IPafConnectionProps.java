/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.util.Properties;

import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.PafKeystoreEncryptionException;

/**
 * Provides an interface to various Paf connection objects
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public interface IPafConnectionProps {

	public String getConnectionString();
	public String decryptConnectionString() throws PafKeystoreDecryptionException;
	public void setConnectionString(String connString) throws PafKeystoreEncryptionException;
	public void setEncryptedConnectionString(String connString);
	
    public Properties getProperties() throws PafKeystoreDecryptionException;
    
    public void setMetaDataServiceProvider(String mdspClassName);
    public String getMetaDataServiceProvider();
    
    public void setDataServiceProvider(String mdspClassName);
    public String getDataServiceProvider();
    
    public void setMdbClassLoader(String mdspClassName);
	public String getMdbClassLoader();
    
	public void setConnectionToolTip(String connectionToolTip);
	public String getConnectionToolTip();
	
	public int getVersion();
    public void setVersion(int version);
    
    public String getName();
    public void setName(String name);
    
}