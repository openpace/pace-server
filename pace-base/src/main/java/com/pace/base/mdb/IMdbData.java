/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.util.List;
import java.util.Map;

import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.app.PafDimSpec;
import com.pace.base.app.UnitOfWork;
import com.pace.base.state.PafClientState;

/**
 * Provides access to the mid-tier data layer
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */

public interface IMdbData {


	/** 
	 *	Refresh the data cache with mdb data for the specified versions. Each 
	 *	specified version will be completely cleared and reloaded for across
	 *	all unit of work intersections.
	 *
	 *  No data will be refreshed if the version filter is empty.
	 *  
	 * @param dataCache Data cache
	 * @param mdbDataSpec Specifies the intersections to retrieve for each version
	 * @param versionFilter List of versions to refresh
	 * 	  
	 * @return Map describing which intersections were actually retrieved.
	 * @throws PafException 
	 * @throws PafInvalidIntersectionException 
	 */ 
	public Map<String, Map<Integer, List<String>>> refreshDataCache(PafDataCache dataCache, Map<String, Map<Integer, List<String>>> mdbDataSpec, List<String> versionFilter) throws PafException, PafInvalidIntersectionException;


	/** 
	 *	Update the data cache with mdb data for the specified versions. For 
	 * 	performance reasons, existing data blocks will be not be updated.
	 * 
	 *  Any versions that need to be completely refreshed should be cleared before 
	 *  calling this method.
	 *
	 *
	 *  No data will be refreshed if the version filter is empty.
	 *  
	 * @param dataCache Data cache
	 * @param expandedUow Expanded unit of work specification
	 * @param versionFilter List of versions to refresh
	 * 	  
	 * @return Map describing which intersections were actually retrieved.
	 * @throws PafException 
	 * @throws PafInvalidIntersectionException 
	 */ 
	public Map<String, Map<Integer, List<String>>> updateDataCache(PafDataCache dataCache, UnitOfWork expandedUow, List<String> versionFilter) throws PafException, PafInvalidIntersectionException;
	

	/** 
	 *	Update the data cache with mdb data for the intersections specified, by version.
	 * 	For performance reasons, existing data blocks will not be updated.
	 * 
	 *  Any versions that need to be completely refreshed should be cleared before 
	 *  calling this method.
	 *
	 *  No data will be loaded if the data specification is empty.
	 *  
	 * @param dataCache Data cache
	 * @param mdbDataSpec Specifies the intersections to retrieve for each version
	 * 
	 * @return Map describing which intersections were actually retrieved.
	 * @throws PafException 
	 * @throws PafInvalidIntersectionException 
	 */
    public Map<String, Map<Integer, List<String>>> updateDataCache(PafDataCache dataCache, Map<String, Map<Integer, List<String>>> mdbDataSpec) throws PafException, PafInvalidIntersectionException;

	
	/**
     *	Send data back to Essbase
     *
	 * @param dataCache Data cache - Updated data and associated meta-data
     * @param clientState Client State Object
     * 
	 * @throws PafException
	 * @throws PafInvalidIntersectionException 
     */
    public void sendData(PafDataCache dataCache, PafClientState clientState) throws PafException, PafInvalidIntersectionException;
    
    
    /**
     *	Get Filtered meta-data from Essbase
     *
	 * @param expandedUow Fully expanded unit of work
	 * @param appDef Paf Application Definition
	 * @throws PafException
     */
    public PafDimSpec[] getFilteredMetadata(Map<Integer, List<String>> expandedUOW, PafApplicationDef appDef) throws PafException;
}
