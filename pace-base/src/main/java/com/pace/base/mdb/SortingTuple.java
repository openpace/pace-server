/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import com.pace.base.SortOrder;
import com.pace.base.data.Intersection;

public class SortingTuple  implements Cloneable {
	private Intersection intersection;
	private SortOrder sortOrder;
	
	public SortingTuple() {
		
	}
	
	public SortingTuple(Intersection intersection, SortOrder sortOrder) {
		this.intersection = intersection;
		this.sortOrder = sortOrder;
	}
	/**
	 * @return the sortOrder
	 */
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	/**
	 * @return the intersection
	 */
	public Intersection getIntersection() {
		return intersection;
	}
	/**
	 * @param intersection the intersection to set
	 */
	public void setIntersection(Intersection intersection) {
		this.intersection = intersection;
	}
	
	@Override
	public SortingTuple clone() {
		SortingTuple clonedTuple = null;
		try {
			clonedTuple = (SortingTuple) super.clone();
			clonedTuple.intersection = this.intersection.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return clonedTuple;
	}
}
