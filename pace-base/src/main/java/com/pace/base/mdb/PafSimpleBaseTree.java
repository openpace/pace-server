/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;


/**
 * This is a simplified version of the Paf Base Member Tree
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
public class PafSimpleBaseTree extends PafSimpleDimTree {

 //   private PafSimpleBaseMember[] memberObjects;
 
    public PafSimpleBaseTree() {}

    /**
     * @param rootKey Root Node to use in initializing the PafSimpleBaseTree
	 * @param aliasTableNames List of Essbase table names
     */
    @Deprecated
    public PafSimpleBaseTree(String rootKey, String[] aliasTableNames) {
        
    	// Set instance variables
    	setId(rootKey);
       	setRootKey(rootKey);
        setAliasTableNames(aliasTableNames);
   }
    
    /**
     * @param id Dimension name
     * @param rootKey Root Node to use in initializing the PafSimpleBaseTree
	 * @param aliasTableNames List of Essbase table names
     */
    public PafSimpleBaseTree(String id, String rootKey, String[] aliasTableNames) {
        
    	// Set instance variables
    	setId(id);
       	setRootKey(rootKey);
        setAliasTableNames(aliasTableNames);
   }

 
//	/**
//	 *	Return the array of member objects
//	 *
//	 * @return Returns the array of member objects
//	 */
//    public PafSimpleBaseMember[] getMemberObjects() {
//        return memberObjects;
//    }
//	/**
//	 *	Set the array of member objects
//	 *
//	 * @param memberObjects The array of member objects to set
//	 */
//    public void setMemberObjects(PafSimpleBaseMember[] memberObjects) {
//        this.memberObjects = memberObjects;
//    }

}
