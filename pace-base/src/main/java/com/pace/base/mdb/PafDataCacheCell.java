/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import com.pace.base.data.Intersection;

/**
 * Representation of a data cache cell used to track changes
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafDataCacheCell {
	
	Intersection cellIntersection = null;
	double cellValue = 0;

	/**
	 * @param cellIntersection Data cache cell intersection
	 * @param cellValue Data cache cell value
	 */
	public PafDataCacheCell(Intersection cellIntersection, double cellValue) {
		this.cellIntersection = cellIntersection;
		this.cellValue = cellValue;	
	}

	/**
	 * @return Returns the cellIntersection.
	 */
	public Intersection getCellIntersection() {
		return cellIntersection;
	}

	/**
	 * @param cellIntersection The cellIntersection to set.
	 */
	public void setCellIntersection(Intersection cellIntersection) {
		this.cellIntersection = cellIntersection;
	}

	/**
	 * @return Returns the cellValue.
	 */
	public double getCellValue() {
		return cellValue;
	}

	/**
	 * @param cellValue The cellValue to set.
	 */
	public void setCellValue(double cellValue) {
		this.cellValue = cellValue;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		if (cellIntersection != null) {
			return cellIntersection.toString() + " - " + cellValue;
		} else {
			return "[not initialized]";
		}
	}

}
