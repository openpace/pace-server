/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * Data Block Entry
 */
package com.pace.base.mdb;

/**
 * @author Alan Farkas
 *
 */
public class DataBlockEntry {
	
	private Integer surrogateKey = null;
	private DataBlock dataBlock = null;

	/**
	 * @return the surrogateKey
	 */
	public Integer getSurrogateKey() {
		return surrogateKey;
	}
	/**
	 * @param surrogateKey the surrogateKey to set
	 */
	public void setSurrogateKey(Integer surrogateKey) {
		this.surrogateKey = surrogateKey;
	}
	
	/**
	 * @return the dataBlock
	 */
	public DataBlock getDataBlock() {
		return dataBlock;
	}
	/**
	 * @param dataBlock the dataBlock to set
	 */
	public void setDataBlock(DataBlock dataBlock) {
		this.dataBlock = dataBlock;
	}

}
