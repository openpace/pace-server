/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import com.pace.base.app.UnitOfWork;
import com.pace.base.view.PafMVS;

/**
 * Data slice cache parms
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafDataSliceCacheParms {

	private UnitOfWork sliceCacheSpec = null;
	private PafMVS pafMVS = null;
	private PafDataCache uowCache = null;
	
	/**
	 * @return the pafMVS
	 */
	public PafMVS getPafMVS() {
		return pafMVS;
	}
	/**
	 * @param pafMVS the pafMVS to set
	 */
	public void setPafMVS(PafMVS pafMVS) {
		this.pafMVS = pafMVS;
	}
	/**
	 * @return the sliceCacheSpec
	 */
	public UnitOfWork geDatatSliceCacheSpec() {
		return sliceCacheSpec;
	}
	/**
	 * @param sliceCacheSpec the sliceCacheSpec to set
	 */
	public void setDataSliceCacheSpec(UnitOfWork sliceCacheSpec) {
		this.sliceCacheSpec = sliceCacheSpec;
	}
	/**
	 * @return the uowCache
	 */
	public PafDataCache getUowCache() {
		return uowCache;
	}
	/**
	 * @param uowCache the uowCache to set
	 */
	public void setUowCache(PafDataCache uowCache) {
		this.uowCache = uowCache;
	}

}
