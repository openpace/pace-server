/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.base.mdb;

/**
 * Essbase Dimension Level / Generation Information
 * 
 * @author Alan Farkas
 *
 */
public class DimLevelGenInfo {

	private String dimension = null;	// Dimension name
	private String[] nameArray = null;	// Gen/level names
	private int[] numberArray = null;	// Gen/Level numbers (each element matches the corresponding gen/level name)

	
	/**
	 * Dummy constructor
	 */
	public DimLevelGenInfo() {}
		
	
	/**
	 * @param dimension Dimension Name
	 */
	public DimLevelGenInfo(String dimension) {
		this.dimension = dimension;
	}
	
	/**
	 * @return the dimension
	 */
	public String getDimension() {
		return dimension;
	}
	/**
	 * @param dimension the dimension to set
	 */
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	/**
	 * @return the nameArray
	 */
	public String[] getNameArray() {
		return nameArray;
	}
	/**
	 * @param nameArray the nameArray to set
	 */
	public void setNameArray(String[] nameArray) {
		this.nameArray = nameArray;
	}
	/**
	 * @return the numberArray
	 */
	public int[] getNumberArray() {
		return numberArray;
	}
	/**
	 * @param numberArray the numberArray to set
	 */
	public void setNumberArray(int[] numberArray) {
		this.numberArray = numberArray;
	}

	
}
