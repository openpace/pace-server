/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * Data Cache Cell Address
 */
package com.pace.base.mdb;

import com.pace.base.data.Intersection;

/**
 * @author Alan Farkas
 *
 */
public class DataCacheCellAddress {

	Intersection dataBlockKey = null;
	int coordX = 0;	// Measures
	int coordY = 0;	// Time
	
	
	/**
	 * @return the dataBlockIndex
	 */
	public Intersection getDataBlockKey() {
		return dataBlockKey;
	}
	/**
	 * @param dataBlockKey the dataBlockKey to set
	 */
	public void setDataBlockKey(Intersection dataBlockKey) {
		this.dataBlockKey = dataBlockKey;
	}
	/**
	 * @return the coordX
	 */
	public int getCoordX() {
		return coordX;
	}
	/**
	 * @param coordX the coordX to set
	 */
	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}
	/**
	 * @return the coordY
	 */
	public int getCoordY() {
		return coordY;
	}
	/**
	 * @param coordY the coordY to set
	 */
	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}
	
}
