/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.io.IOException;

import com.pace.base.PafException;

/**
 * Loads necessary mdb provider classes
 *
 * @version	x.xx
 * @author Alan Farkas
 *
 */
/**
 * @author ADG
 *
 */
public interface IMdbClassLoader {


	/**
	 *  Load mdb provider classes
	 *   
	 * @throws IOException 
	 *  
	 */
	public void load() throws IOException;

	/**
	 * @return the mdbApiVersion
	 */
	public String getMdbApiVersion();

	/**
	 * @return the metaDataProvider
	 * @throws PafException 
	 */
	public IMdbMetaData getMetaDataProvider() throws PafException;

	/**
	 * @return the mdbDataProvider
	 * @throws PafException 
	 */
	public IMdbData getMdbDataProvider() throws PafException;

}
