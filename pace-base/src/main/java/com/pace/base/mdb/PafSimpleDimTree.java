/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.pace.base.IPafCompressedObj;

/**
 * This is a simplified version of the Paf Dim Member Tree
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafSimpleDimTree implements IPafCompressedObj {

	private String id = null;
    private String[] traversedMembers;
    private String rootKey;
    private String[] aliasTableNames  = null;
    protected PafSimpleDimMember[] memberObjects;
    private PafSimpleBaseMember[] baseMemberObjects;
    private boolean isDiscontig = false;
    
    private String compAliasTableNames;
    private String compMemberIndex;
    private String compParentChild;
//    private String compTraversedNames;
    private boolean isCompressed;
    
    private String elementDelim = "^";
    private String groupDelim = "|^";
    

	public void compressData() {
		
		String entry;
		StringBuffer parentChild = new StringBuffer( 30 * memberObjects.length );

		Map<String, String> memberIndexLookup = new HashMap<String, String>(memberObjects.length*2);
		Map<String, String> parentKeyLookup = new HashMap<String, String>(memberObjects.length*2);
		int index = 0;
		String sIndex;
		String parentIndex;
		String fullChildKey;
		String shortMemberKey;
		String fullMemberKey;
		String fullParentKey;
		
		
		// Compress alias tables. The order of the alias table names in the aliasTableNames
		// property, dictates how the alias keys will be sorted on every tree member properties
		// object. The order of the alias values for each member corresponds to the alias
		// key order (TTN-1350).
		if (aliasTableNames != null && aliasTableNames.length > 0) {
				compAliasTableNames = delimitArray(aliasTableNames, elementDelim);				
		}
					

		// Build parentChild and memgerIndex strings. Traverse the member objects which
		// are already ordered based on an in-order traversal. 
		// 
		// To support shared members, full keys (member prefixed by parent) are
		// used in internal collections to keep track of each specific member's 
		// index (surrogate key).
		
		for ( PafSimpleDimMember m : memberObjects ) {

			// capture current index to string and increment
			sIndex = Integer.toString(index++);
			
			// get short and full member key
			shortMemberKey = m.getKey();
			fullMemberKey = m.getParentKey() + elementDelim + shortMemberKey;

			// map each full child key to full parent key index
			for (String shortChildKey: m.getChildKeys()) {
				fullChildKey = shortMemberKey + elementDelim + shortChildKey;
				parentKeyLookup.put(fullChildKey, fullMemberKey);
			}
			// map member and string index
			memberIndexLookup.put(fullMemberKey, sIndex);
			
			// get parent index
			fullParentKey = parentKeyLookup.get(fullMemberKey);
			if (fullParentKey != null) {
				parentIndex = memberIndexLookup.get(fullParentKey);
			} else {
				parentIndex = null;
			}
			
			// add record to parent/child collection (parent index, member index,
			// generation, level, default alias, alias 1, alias 2, ......)
			
			String isCalcTwoPass = "0";
			if(m.getPafSimpleDimMemberProps().isTwoPassCalc())
				isCalcTwoPass = "1";
			else
				isCalcTwoPass = "0";
			entry = delimitArray(
					new String[] { parentIndex, sIndex, 
					Integer.toString(m.getPafSimpleDimMemberProps().getGenerationNumber()), 
					Integer.toString(m.getPafSimpleDimMemberProps().getLevelNumber()),Integer.toString(m.getPafSimpleDimMemberProps().getTimeBalanceOption()),isCalcTwoPass,}
				, elementDelim);
			
			entry += elementDelim + delimitArray( m.getPafSimpleDimMemberProps().getAliasValues(), elementDelim);
			
			parentChild.append(entry);
			parentChild.append(groupDelim);
		}
		
		compParentChild = parentChild.toString();

		// process member index lookup into string
		StringBuffer memberIndex = new StringBuffer( 8 * memberObjects.length );
		for (String fullKey : memberIndexLookup.keySet()) {
			String key = fullKey.substring(fullKey.indexOf(elementDelim) + 1);
			memberIndex.append(delimitArray(new String[] { key, memberIndexLookup.get(fullKey) }, elementDelim));
			memberIndex.append(groupDelim);
		}
		
		compMemberIndex = memberIndex.toString();
		
		memberObjects = null;		
		traversedMembers = null;
		isCompressed = true;
		
	}

	
	
	
	private String delimitArray(String [] items, String delimeter) {
		StringBuffer sb = new StringBuffer(items.length * 8);
		
		for (String s : items) {
			sb.append(s);
			sb.append(delimeter);
		}
		
		if (sb.length() > 0)
			sb.delete(  sb.length()-delimeter.length(), sb.length() );
		
		return sb.toString();
	}
	
	public void uncompressData() {
		// not needed on the server side.
	};
	
	
	
    public boolean isCompressed() {
    	return isCompressed;
    }
    
    public void setCompressed(boolean isCompressed){
    	this.isCompressed = isCompressed;
    }    
    
    
    /**
	 *	Return the PafSimpleBaseTree id
	 *
	 * @return Returns the PafSimpleBaseTree id
	 */
	public String getId() {
        return id;
    }
	/**
	 *	Set the PafSimpleBaseTree id
	 *
	 * @param id The PafSimpleBaseTree id
	 */
    public void setId(String id) {
        this.id = id;
    }


	/**
	 *	Return the array of member keys in traversal order
	 *
	 * @return Returns the array of member keys in traversal order
	 */
    public String[] getTraversedMembers() {
        return this.traversedMembers;
    }
	/**
	 *	Set the array of members in traversal order
	 *
	 * @param traversedMembers The array of members in traversal order to set
	 */
    public void setTraversedMembers(String[] traversedMembers) {
        this.traversedMembers = traversedMembers;
    }

    /**
	 *	Return the pafSimpleBaseTree root key
	 *
	 * @return Returns the pafSimpleBaseTree root key
	 */
    public String getRootKey() {
        return rootKey;
    }
	/**
	 *	Set the pafSimpleBaseTree root
	 *
	 * @param rootKey The pafSimpleBaseTree root to set
	 */
   public void setRootKey(String rootKey) {
        this.rootKey = rootKey;
    }

	/**
	 *	Return the alias table names
	 *
	 * @return Returns the alias table names
	 */
	public String[] getAliasTableNames() {
		return aliasTableNames;
	}
	/**
	 *	Set the alias table names
	 *
	 * @param aliasTableNames The alias table names to set
	 */
	public void setAliasTableNames(String[] aliasTableNames) {
		this.aliasTableNames = aliasTableNames;
	}
	/**
	 * @return the memberObjects
	 */
	public  PafSimpleDimMember[] getMemberObjects(){
		return memberObjects;
	}
	/**
	 * @param memberObjects the memberObjects to set
	 */
	public void setMemberObjects(PafSimpleDimMember[] memberObjects) {
		this.memberObjects = memberObjects;
	}
	
	/**
	 * @return the isDiscontig
	 */
	public boolean isDiscontig() {
		return isDiscontig;
	}
	/**
	 * @param isDiscontig the isDiscontig to set
	 */
	public void setDiscontig(boolean isDiscontig) {
		this.isDiscontig = isDiscontig;
	}

	public String getCompAliasTableNames() {
		return compAliasTableNames;
	}
	public void setCompAliasTableNames(String compAliasTableNames) {
		this.compAliasTableNames = compAliasTableNames;
	}
	public String getCompMemberIndex() {
		return compMemberIndex;
	}
	public void setCompMemberIndex(String compMemberIndex) {
		this.compMemberIndex = compMemberIndex;
	}
	public String getCompParentChild() {
		return compParentChild;
	}
	public void setCompParentChild(String compParentChild) {
		this.compParentChild = compParentChild;
	}
//	public String getCompTraversedNames() {
//		return compTraversedNames;
//	}
//	public void setCompTraversedNames(String compTraversedNames) {
//		this.compTraversedNames = compTraversedNames;
//	}


	public String getElementDelim() {
		return elementDelim;
	}


	public void setElementDelim(String elementDelim) {
		this.elementDelim = elementDelim;
	}


	public String getGroupDelim() {
		return groupDelim;
	}


	public void setGroupDelim(String groupDelim) {
		this.groupDelim = groupDelim;
	}


	public PafSimpleBaseMember[] getBaseMemberObjects() {
		
	/*	PafSimpleBaseMember[] baseMemberObjects = new PafSimpleBaseMember[memberObjects.length];
		
		for(int i=0;i<memberObjects.length;i++)
		{
			baseMemberObjects[i] = (PafSimpleBaseMember)memberObjects[i];
		}
		
		return baseMemberObjects;  */
		
		System.out.println("Lenght of memberObjects is:"+memberObjects.length);
		
		return Arrays.copyOf(memberObjects, memberObjects.length,PafSimpleBaseMember[].class);
	}
	
	
	

}
