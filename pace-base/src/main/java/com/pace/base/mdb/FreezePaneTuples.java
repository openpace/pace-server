/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import java.util.ArrayList;
import java.util.List;
// Class to hold the freeze pane tuples in a view section.
public class FreezePaneTuples implements Cloneable {
	
	private List<FreezePaneTuple> freezePaneTupleList;
	

	public FreezePaneTuples() {
		// TODO Auto-generated constructor stub
	}

	public FreezePaneTuples(List<FreezePaneTuple> freezePaneTupleList) {
		this.freezePaneTupleList = freezePaneTupleList;
	}
	
	@Override
	public FreezePaneTuples clone() {
		FreezePaneTuples clonedFreezePaneTuples = new FreezePaneTuples();
		
		if ( this.freezePaneTupleList != null ) {
			
			clonedFreezePaneTuples.freezePaneTupleList = new ArrayList<FreezePaneTuple>();
			
			for ( FreezePaneTuple freezePaneTuple : this.freezePaneTupleList ) {
				
				clonedFreezePaneTuples.freezePaneTupleList.add(freezePaneTuple.clone());
				
			}
			
		}
		
		return clonedFreezePaneTuples;
	}


	public List<FreezePaneTuple> getFreezePaneTupleList() {
		return freezePaneTupleList;
	}

	public void setFreezePaneTupleList(List<FreezePaneTuple> freezePaneTupleList) {
		this.freezePaneTupleList = freezePaneTupleList;
	}
}
