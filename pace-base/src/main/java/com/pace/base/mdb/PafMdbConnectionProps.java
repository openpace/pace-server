/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.mdb;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

 // new class added to hold the properties used especially for MDBDatasources. 
// These properties are displayed on the Setting Application UI for MDBDatasources.

public class PafMdbConnectionProps extends PafConnectionProps{
	
	 @XStreamOmitField
	    private String edsurl;
	    @XStreamOmitField
	    private String edsdomain;
	    @XStreamOmitField
	    private String server;
	    @XStreamOmitField
	    private String user;
	    @XStreamOmitField
	    private String password;
	    @XStreamOmitField
	    private String application;
	    @XStreamOmitField
	    private String database;
	    
	    public PafMdbConnectionProps() {
	    	super();
	    }
	    public String getEdsurl() {
			return edsurl;
		}

		
		public void setEdsurl(String edsUrl) {
			this.edsurl = edsUrl;
		}
		
			public String getEdsdomain() {
			return edsdomain;
		}

		
		public void setEdsdomain(String edsDomain) {
			this.edsdomain = edsDomain;
		}
		
			public String getServer() {
			return server;
		}

		
		public void setServer(String server) {
			this.server = server;
		}
		
			public String getUser() {
			return user;
		}

		
		public void setUser(String user) {
			this.user = user;
		}
		
			public String getPassword() {
			return password;
		}

		
		public void setPassword(String password) {
			this.password = password;
		}
		
		public String getApplication() {
			return application;
		}

		
		public void setApplication(String app) {
			this.application = app;
		}
		
		public String getDatabase() {
			return database;
		}

		
		public void setDatabase(String database) {
			this.database = database;
		}
		

}
