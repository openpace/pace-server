/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import com.pace.base.PafErrSeverity;
import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.PafKeystoreEncryptionException;
import com.pace.base.PafKeystorePasswordGenerateException;
import com.pace.base.PafKeystorePasswordReadException;
import com.pace.base.utility.Base64;
import com.pace.base.utility.OSDetector;
import com.pace.base.utility.OSUtil;
import com.pace.base.utility.StringUtils;

/**
 * Singleton class for AES encryption using a keystore.
 * @author themoosman
 *
 */
public class AESKeystoreEncryption {

	private static Logger logger = Logger.getLogger(AESKeystoreEncryption.class);
	private static final String KEY_GEN_ALGORITHM = "AES";
	private static final String KEYSTORE_TYPE = "JCEKS";
	private static final String RANDOM_NUM_GEN_ALGORITHM = "SHA1PRNG";
	private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
	private static final String KEY_ALIAS = "paceaeskey";
	private static final String KEY_STORE_SUB_DIR_WIN = File.separator + "Open Pace" + File.separator + "Pace Server" + File.separator + "keys";
	private static final String KEY_STORE_SUB_DIR_OTHER = File.separator + ".open-pace" + File.separator + "pace-server" + File.separator + "keys";
	private static final String KEY_STORE_NAME = "keystore.jceks";
	private static final String KEY_STORE_PASSWORD_FILE_NAME = "keystore.key";
	
	private Cipher cipher;
	private char[] keyStorePassword;
	private String keyAlias;
	private String keyStoreDirectory;
	private String keyStoreName;
	private SecretKeySpec secretKeySpec;
	

	
	/**
	 * The default constructor will use 
	 * keyAlias = paceaeskey
	 * keyStoreDirectory = user.home/.am-pace
	 * keyStoreName = keystore.jceks
	 * keyStorePassword = A default password will be used.
	 * @throws PafKeystorePasswordReadException 
	 */	
	public AESKeystoreEncryption() throws PafKeystorePasswordReadException{
		this(KEY_ALIAS, null, KEY_STORE_NAME, null);
	}

	/**
	 * 
	 * @param keyAlias Alias key name
	 * @param keyStoreDirectory Keystore directory
	 * @throws PafKeystorePasswordReadException 
	 */
	public AESKeystoreEncryption(String keyStoreDirectory) throws PafKeystorePasswordReadException{
		this(KEY_ALIAS, keyStoreDirectory, KEY_STORE_NAME, null);
	}
	
	/**
	 * 
	 * @param keyAlias Alias key name
	 * @param keyStoreDirectory Keystore directory
	 * @param keyStoreName Keystore file name
	 * @param keyStorePassword = Keystore password
	 * @throws PafKeystorePasswordReadException 
	 */
	public AESKeystoreEncryption(String keyAlias, String keyStoreDirectory, String keyStoreName, String keyStorePassword) throws PafKeystorePasswordReadException{
		
		// Get KeyStore
		this.keyAlias = keyAlias;
		if(keyStoreDirectory == null){
			this.keyStoreDirectory = getKeystoreDirectory();
		} else {
			this.keyStoreDirectory = keyStoreDirectory;
		}
		this.keyStoreName = keyStoreName;
		
		// Get KeyStore password (TTN-2712)
		if (keyStorePassword == null) {
			this.keyStorePassword = getKeyStorePassword(this.keyStoreDirectory, KEY_STORE_PASSWORD_FILE_NAME).toCharArray();
		} else {
			this.keyStorePassword = keyStorePassword.toCharArray();
		}
		
		// Get requested key from KeyStore
		try {
			this.cipher = Cipher.getInstance(CIPHER_ALGORITHM);
		} catch (Exception e) {
			logger.error(e);
		} 
  		this.secretKeySpec = getKeyFromStore(this.keyStoreDirectory, this.keyStoreName, this.keyStorePassword, this.keyAlias);
	}
	
	
	/**
	 * Encrypts a string to a byte[] then converts the byte[] to a Hex string.
	 *
	 * @param textString String to encrypt
	 * @return	An Hex representation of the encrypted string
	 * @throws PafKeystoreEncryptionException 
	 */
  	public String encryptToHexString(String textString) throws PafKeystoreEncryptionException {
  		return encryptToHexString(textString, false);
  	}

	
	/**
	 * Encrypts a string to a byte[] then converts the byte[] to a Hex string.
	 *
	 * @param textString String to encrypt
	 * @param chkTextStr If True the string will be validated and if found to be already encrypted no encryption will occur.
	 * @return	An Hex representation of the encrypted string
	 * @throws PafKeystoreEncryptionException 
	 */
  	public String encryptToHexString(String textString, boolean chkTextStr) throws PafKeystoreEncryptionException {
  		
  		if(chkTextStr && isStringEncrypted(textString)){
  			logger.warn(String.format("String: %s is already encrypted, no encryption will occur.", textString));
  			return textString;
  		}
  		
  		return Hex.encodeHexString(encrypt(textString));
  	}
	
	/**
	 * Encrypts a string to a byte[]
	 *
	 * @param textString String to encrypt
	 * @return	Array of bytes containing encrypted string
	 * @throws PafKeystoreEncryptionException 
	 */
  	public byte[] encrypt(String textString) throws PafKeystoreEncryptionException  {
  		if ( textString == null || textString == "") throw new IllegalArgumentException("String to encrypt cannot be null.");
  		byte[] encrypted = null;
  		try {
  			byte[] iv = generateIV();
  			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, new IvParameterSpec(iv));
  			encrypted = cipher.doFinal(textString.getBytes());
  			
  			byte[] result = new byte[iv.length + encrypted.length];
	  		System.arraycopy(iv, 0, result, 0, iv.length);
	  		System.arraycopy(encrypted, 0, result, iv.length, encrypted.length);
	  		
	  		return result;
  			
  		} catch (Exception e) {
			logger.error(e.getMessage());
  			throw new PafKeystoreEncryptionException(String.format("Cannot encrypt string: %s", encrypted), PafErrSeverity.Error, e.getCause());
  		}
  	}
  	
  	
  	/**
	 *  Decrypts an Hex representation of an encrypted string
	 *
	 * @param encryptedString		Encrypted Hex string 
	 * @return	Array of bytes containing encrypted string
  	 * @throws PafKeystoreDecryptionException 
	 */
  	public String decryptFromHexString(String encryptedString ) throws PafKeystoreDecryptionException {
  		if ( encryptedString == null) throw new IllegalArgumentException("String to decrypt cannot be null.");
  		String text = null;
  		try{
  			if(!isStringEncrypted(encryptedString)){
  				logger.debug("String to decrypt was not in a hex format, no decryption will be performed.");
  				text = encryptedString;
  			} else {
  				text = decrypt(Hex.decodeHex(encryptedString.toCharArray()));
  			}
  		}catch (Exception e) {
			logger.error(e.getMessage());
  			throw new PafKeystoreDecryptionException(String.format("Cannot decrypt string: %s", encryptedString), PafErrSeverity.Error, e.getCause());
  		}
  		return text;
  	}
  	
	/**
	 * Decrypts a byte[] into a string
	 *
	 * @param encryptedBytes Encrypted bytes 
	 * @return	Array of bytes containing encrypted string
	 * @throws PafKeystoreDecryptionException 
	 */
  	public String decrypt(byte[] encryptedBytes ) throws PafKeystoreDecryptionException {
  		if (encryptedBytes == null) throw new IllegalArgumentException("String to decrypt cannot be null.");
  		if (encryptedBytes.length < 32) throw new IllegalArgumentException("String to decrypt must be at least 32 bytes");
  		String originalString = null;
  		try {
  			
  			byte[] iv = Arrays.copyOfRange(encryptedBytes, 0, 16);
  			byte[] value = Arrays.copyOfRange(encryptedBytes, 16, encryptedBytes.length);
  			
  			cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, new IvParameterSpec(iv));
  			byte[] original = cipher.doFinal(value);
  			originalString = new String(original);
  		} catch (Exception e) {
			logger.error(e.getMessage());
  			throw new PafKeystoreDecryptionException(String.format("Cannot decrypt string: %s", encryptedBytes), PafErrSeverity.Error, e.getCause());
  		}
  		return originalString;
  	}
  	
  	/**
  	 * Generates a SecretKey using the AES algorithm
  	 * @return SecretKey
  	 * @throws NoSuchAlgorithmException
  	 * @throws NoSuchProviderException
  	 */
  	protected static SecretKey generateKey() throws NoSuchAlgorithmException, NoSuchProviderException{
		KeyGenerator generator = KeyGenerator.getInstance(KEY_GEN_ALGORITHM);
		generator.init(128); //128 bit
		return generator.generateKey();
	}
	
  	/**
  	 * Generates a unique IV, using the SHA1PRNG algorithm
  	 * @return a byte[] 
  	 * @throws NoSuchAlgorithmException
  	 * @throws NoSuchProviderException
  	 */
  	protected static byte[] generateIV() throws NoSuchAlgorithmException, NoSuchProviderException{
		SecureRandom sr = SecureRandom.getInstance(RANDOM_NUM_GEN_ALGORITHM);
		byte[] bytes = new byte[16]; //16 bytes = 128 bits
        sr.nextBytes(bytes); 
        return bytes;
	}
  	
  	/**
  	 * Tests if a string is encrypted.
  	 * @param text String value to test
  	 * @return True if the string is encrypted, false if not.
  	 */
  	public static boolean isStringEncrypted(String text){
  		return StringUtils.isHex(text);
  	}
  	
  	
  	/**
  	 * Removes a keystore file.
  	 * @return true if successful, false if not.
  	 */
  	public boolean removeKeystore(){
  		
  		File file = new File(keyStoreDirectory + File.separator + keyStoreName);
  		
  		logger.warn(String.format("Removing keystore: %s", file.getAbsolutePath()));
  		
		return file.delete();
  	}
  	
  	/**
  	 * Gets the default keystore directory.
  	 * @return
  	 */
  	public static String getKeystoreDirectory(){
  		if(OSDetector.isWindows()){
  			return OSUtil.getProgramDataDirectory() + KEY_STORE_SUB_DIR_WIN;
  		} else {
  			return OSUtil.getUserHomeDirectory() + KEY_STORE_SUB_DIR_OTHER;
  		}
  			
  	}
  	
	/**
	 * Writes a keystore to a filesystem
	 *
	 */
	private void writeKeyStoreToFile(
	      final KeyStore  keyStore,
	      final File      file,
	      final char[]    masterPassword )
	  	  throws KeyStoreException, IOException, 
	      NoSuchAlgorithmException, CertificateException  {
		
		final FileOutputStream  out = new FileOutputStream(file);
		try {
			keyStore.store( out, masterPassword );
		} finally {
			out.close();
		}
	}

	/**
	 * Reads key from keystore on a filesystem
	 *
	 */
	private SecretKeySpec getKeyFromStore( 
			String keyStoreDir,
			String keyStoreFile, 
			char[] storPass, 
			String alias ) {
		
		SecretKeySpec secretKeyFromStore = null;
		
		File directory = new File(keyStoreDir);
		if(!directory.exists()){
			logger.warn(String.format("Directory: %s does not exist.", directory.getAbsolutePath()));
			logger.info(String.format("Creating directory: %s", directory.getAbsolutePath()));
			directory.mkdirs();
		}
		
		File file = new File(directory + File.separator + keyStoreFile); 
		
		try {
		  
			final KeyStore keyStore = KeyStore.getInstance(KEYSTORE_TYPE);
	  		
			if ( file.exists() ) {   // don't buffer keystore; it's tiny anyway
				final FileInputStream input   = new FileInputStream( file );
				keyStore.load( input, storPass );
				input.close();
			} else {
		  	    logger.warn(String.format("Keystore: %s not found, the system will generate a new keystore.", file));
				return genKeystore( file, storPass, alias );
			}
      	
  			// Get key from KeyStore on disk 
  			Key keyFromStore = keyStore.getKey( alias, storPass);
  			secretKeyFromStore = new SecretKeySpec(keyFromStore.getEncoded(), KEY_GEN_ALGORITHM);
  			
		} catch (Exception e) {
			logger.error(e);
		}
		
		return secretKeyFromStore;
	}

  	/**
  	 * Generates a new keystore
  	 * 
  	 * @throws Exception 
  	 */
  	private SecretKeySpec genKeystore( 
  			File file, 
  			char[] storPass,
  			String keyAlias) throws Exception {

  		SecretKeySpec skeySpec = null;

  		// Generate the secret key
		SecretKey skey = generateKey();
		
		// Generate the secret key spec
  		byte[] raw = skey.getEncoded();
  		skeySpec = new SecretKeySpec(raw, KEY_GEN_ALGORITHM);
  		
  	    // Get the KeyStore
  	    KeyStore ks = KeyStore.getInstance(KEYSTORE_TYPE);
  		
  		// Create the KeyStore
  		ks.load( (InputStream) null, storPass);
  		
  	    // Put key into store
  	    ks.setKeyEntry(keyAlias, skey, storPass, null);
  		
  	    logger.info(String.format("Creating %s keystore: %s", KEYSTORE_TYPE, file));
  	    
  	    // Write KeyStore to disk
  	    writeKeyStoreToFile( ks, file, storPass );

  		return skeySpec;
  	}
  	
  	
  	
	/**
	 * Reads the KeyStore password from the file system
	 * 
	 * @param keyStorePasswordDir 
	 * @param keyStorePasswordFileName 
	 * 
	 * @return KeyStore password
	 * @throws PafKeystorePasswordReadException 
	 */
  	private String getKeyStorePassword(String keyStorePasswordDir, String keyStorePasswordFileName) throws PafKeystorePasswordReadException {

  		// Create password file directory (if it doesn't already exist)
  		File directory = new File(keyStorePasswordDir);
  		if(!directory.exists()){
  			logger.warn(String.format("Password file directory: %s does not exist.", directory.getAbsolutePath()));
  			logger.info(String.format("Creating directory: %s", directory.getAbsolutePath()));
  			directory.mkdirs();
  		}

  		// Open password file - generate a new one if it doesn't already exist
  		File file = new File(directory + File.separator + keyStorePasswordFileName); 
  		FileInputStream inputStream = null;
  		try {	
  			if (file.exists()) {
  				inputStream = new FileInputStream(file);
  				StringBuilder sb = new StringBuilder();
  				int content;
  				while ((content = inputStream.read()) != -1) {
  					sb.append((char) content); 
  				}			
  				return sb.toString();
  			} else {
  				logger.warn(String.format("Keystore password file: %s not found, the system will generate a new one.", file));
  				try {
  					// Generate Keystore password file
                    String password = genKeyStorePasswordFile(file);
                    
                    // In case of upgrade from a 2.8.8.x Pace version, delete the existing KeyStore as the keys
                    // will be unretrievable.
                    boolean status = removeKeystore();
                    
                    // Return newly generate password
                    return password;
				} catch (PafKeystorePasswordGenerateException e) {
					throw new PafKeystorePasswordReadException(e.getMessage(), e.getSeverity());
				}
  			}

  		} catch (IOException e) {
  			String errMsg = "Unable to read the Keystore password file. The java error encountered was: " + e.getMessage();
  			logger.error(errMsg, e);
			throw new PafKeystorePasswordReadException(errMsg, PafErrSeverity.Error);
		} finally {
  			try {
  				if (inputStream != null) {
  					inputStream.close();
  				}
  			} catch (IOException e) {
  				logger.error(e);
  				e.printStackTrace();
  			}
  		}

  	}

  	
	/**
	 * Generates a random password of the given length
	 * 
	 * @param length Number of characters in generated password
	 * @return Password string
	 * 
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 */
	public static String generatePassword(int length) throws NoSuchAlgorithmException, NoSuchProviderException {
		
		// Check length
		if (length < 1) {
			String errMsg = String.format("Error encountered in generating password. An invalid length of [%d] was supplied", length);
			logger.error(errMsg);
			throw new IllegalArgumentException(errMsg);
		}
		
		// Generate password
		String password = "";
		do {
			password += Base64.encodeBytes(generateIV());	
		} while (password.length() < length);
		
		password = password.substring(0,  length);  	// Truncate password to requested length
		return password;
	}

	
	/**
	 * Generate a new Keystore password file
	 * 
	 * @param file Keystore file
	 * @return Generated Keystore password
	 * 
	 * @throws PafKeystorePasswordGenerateException 
	 */
	private String genKeyStorePasswordFile(File file) throws PafKeystorePasswordGenerateException {
		
		String ksPassword = "";
		OutputStream output = null;
		
		// Generate new password
		try {
			ksPassword = generatePassword(128);
		} catch (NoSuchAlgorithmException e) {
            String errMsg = String.format("Unable to generate the Keystore File password. The java error is: %s", e.getMessage());
            logger.error(errMsg, e);
            throw new PafKeystorePasswordGenerateException(errMsg, PafErrSeverity.Error);
		} catch (NoSuchProviderException e) {
            String errMsg = String.format("Unable to generate the Keystore File password. The java error is: %s", e.getMessage());
            logger.error(errMsg, e);
            throw new PafKeystorePasswordGenerateException(errMsg, PafErrSeverity.Error);
		}
		
		// Create new password file containing the generated password
		try {
			output = new FileOutputStream(file);
			output.write(ksPassword.getBytes());
		} catch (FileNotFoundException e) {
            String errMsg = String.format("Unable to generate the Keystore password file. The java error is: %s", e.getMessage());
            logger.error(errMsg, e);
            throw new PafKeystorePasswordGenerateException(errMsg, PafErrSeverity.Error);
		} catch (IOException e) {
            String errMsg = String.format("Unable to generate the Keystore password file. The java error is: %s", e.getMessage());
            logger.error(errMsg, e);
            throw new PafKeystorePasswordGenerateException(errMsg, PafErrSeverity.Error);
		}
		
		
		return ksPassword;
	}

	
}
