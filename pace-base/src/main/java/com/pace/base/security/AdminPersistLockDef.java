/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.pace.base.app.PafDimSpec;
import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * Administrator Controlled Persistent Lock Definition
 * 
 * @author AFarkas
 *
 */
@XStreamAlias("AdminPersistLockDef")
public class AdminPersistLockDef implements Cloneable {
	
	private transient final static Logger logger = Logger.getLogger(AdminPersistLockDef.class);
	private String name = null;
	private List<PafDimSpec> dimSpecList = new ArrayList<PafDimSpec>();
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the dimSpecList
	 */
	public List<PafDimSpec> getDimSpecList() {
		return dimSpecList;
	}
	/**
	 * @param dimSpecList the dimSpecList to set
	 */
	public void setDimSpecList(List<PafDimSpec> dimSpecList) {
		this.dimSpecList = dimSpecList;
	}
	
	public AdminPersistLockDef clone() {
		
		AdminPersistLockDef clonedAdminLock = null;
		
		try {
			
			clonedAdminLock = (AdminPersistLockDef) super.clone();
			
			if ( dimSpecList != null ) {
				
				List<PafDimSpec> pafDimSpecList = new ArrayList<PafDimSpec>();
				
				for (PafDimSpec pafDimSpec : dimSpecList) {
					
					if ( pafDimSpec != null ) {
						
						PafDimSpec clonedPafDimSpec = (PafDimSpec) pafDimSpec.clone();
						
						pafDimSpecList.add(clonedPafDimSpec);
						
					}
					
				}
				
				if ( pafDimSpecList.size() > 0 ) {
					
					clonedAdminLock.setDimSpecList(pafDimSpecList);
					
				} else {
					
					clonedAdminLock.setDimSpecList(null);
					
				}
				
			}
			
		} catch (CloneNotSupportedException e) {
			
			logger.warn(e.getMessage());
			
		}
		
		return clonedAdminLock;
	}
}
