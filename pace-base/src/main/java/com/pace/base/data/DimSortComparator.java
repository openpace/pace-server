/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.data;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import com.pace.base.SortOrder;

/**
 * This sorts a collection of Intersection objects
 * based upon the order of the members passed in
 * and the axis the members belong to.
 *
 * @version	x.xx
 * @author jim
 *
 */
public class DimSortComparator implements Comparator<Intersection> {
    
    private Map<String, Map<String, Integer>> memberSeqs;
    private String[] axisSequence;
    private SortOrder sortOrder;
    
    public DimSortComparator(Map <String, Map<String, Integer>>memberSequences, String[] axisSequence, SortOrder sortOrder) {
        memberSeqs = memberSequences;
        this.axisSequence = axisSequence;
        this.sortOrder = sortOrder;
    }
    
    
    public int compare(Intersection i1, Intersection i2) {
           
        // sorts like an alphabetical order. If 1st axis determines priority consider sorted,
        // else continue until tie is resolved.

    	
    	// first check for an "enhanced sorting intersection" and use if possible
  	
//    	if (i1.isSortable() && i2.isSortable()) {
//    		return i1.compareTo(i2);
//    	}
//    	else {
//        	i1.makeSortable(memberSeqs);
//        	i2.makeSortable(memberSeqs);
//    		return i1.compareTo(i2);        	
//    	}
  	

        Map <String, Integer> axisSeq;
        String o1Coord, o2Coord;
        int axisVal1, axisVal2;
        for (String axis : axisSequence) {
            axisSeq = memberSeqs.get(axis);
            o1Coord = i1.getCoordinate(axis);
            o2Coord = i2.getCoordinate(axis);


            
            // Allow for this comparator to use the same axis sequence for both attribute
            // and non-attribute intersections during attribute evaluation. (TTN-1506) 
            if (o1Coord == null || o2Coord == null) {
            	// Just skip to the next axis, if one or both intersections don't contain
            	// the current axis. Hopefully it's both, since the intent is to compare
            	// like intersections to each other.
            	continue;
            }
 
            axisVal1 = axisSeq.get(o1Coord);
            axisVal2 = axisSeq.get(o2Coord);
            
            if (sortOrder == SortOrder.Ascending) {          	
                if (axisVal1 < axisVal2) return 1;
                else if (axisVal1 > axisVal2) return -1;         
            }
            else {
                if (axisVal1 < axisVal2) return -1;
                else if (axisVal1 > axisVal2) return 1;      
            }
        }
        return 0;

    }
    
}
