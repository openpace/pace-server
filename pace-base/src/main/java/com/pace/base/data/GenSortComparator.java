/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.data;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import com.pace.base.SortOrder;

/**
 * This sorts a collection of Intersection objects
 * based upon the order of the members passed in
 * and the axis the members belong to.
 *
 * @version	x.xx
 * @author jim
 *
 */
public class GenSortComparator implements Comparator {
    
    private Map<String, HashMap<String, Integer>> memberSeqs;
    private String[] axisSequence;
    private SortOrder sortOrder;
    
    public GenSortComparator(Map <String, HashMap<String, Integer>>memberSequences, String[] axisSequence, SortOrder sortOrder) {
        memberSeqs = memberSequences;
        this.axisSequence = axisSequence;
        this.sortOrder = sortOrder;
    }
    
    public int compare(Object o1, Object o2) {
//        Intersection i1 = (Intersection) o1;
//        Intersection i2 = (Intersection) o2;
        
        
        // sorts like an alphabetical order. If 1st axis determines priority consider sorted,
        // else continue until tie is resolved.
        
        Map <String, Integer> axisSeq;
        String axis;
        for (int i = 0; i < axisSequence.length; i++) {
            axis = axisSequence[i];
            axisSeq = memberSeqs.get(axis);        
            if (sortOrder == SortOrder.Ascending) {
                if (axisSeq.get(((Intersection) o1).getCoordinate(axis)) < axisSeq.get(((Intersection) o2).getCoordinate(axis)) ) return 1;
                else if (axisSeq.get(((Intersection) o1).getCoordinate(axis)) > axisSeq.get(((Intersection) o2).getCoordinate(axis)) ) return -1;         
            }
            else {
                if (axisSeq.get(((Intersection) o1).getCoordinate(axis)) < axisSeq.get(((Intersection) o2).getCoordinate(axis)) ) return -1;
                else if (axisSeq.get(((Intersection) o1).getCoordinate(axis)) > axisSeq.get(((Intersection) o2).getCoordinate(axis)) ) return 1;         
            }
        }
        
        // assumes all tests resulted in tie so must be equal.
//        if (!o1.equals(o2))
//            throw new ArithmeticException("Failure in intersection sorting algorthithm.\n" + "\nI1: " + i1.toString() + " -- I2: " + i2.toString() );
        
        return 0;
        
    }
}
