/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.data;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jwatkins
 *
 */
public enum ExpOpCode {
   CHILDREN, CHILDREN_MULTI, ICHILDREN, ICHILDREN_MULTI, DESC, DESC_MULTI, IDESC, IDESC_MULTI, LEVEL, MEMBERS, USER_SEL, UOW_ROOT, GEN, PLAN_VERSION, PLAN_YEARS, NONPLAN_YEARS, FIRST_PLAN_YEAR, FIRST_NONPLAN_YEAR, FIRST_PLAN_PERIOD, OFFSET_MEMBERS;
}
