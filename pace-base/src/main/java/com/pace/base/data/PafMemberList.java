/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jim
 *
 */
@XStreamAlias("memberList")
public class PafMemberList {
	String dimName;
	String memberNames[];
	public String getDimName() {
		return dimName;
	}
	public void setDimName(String dimName) {
		this.dimName = dimName;
	}
    public String[] getMemberNames() {
        return memberNames;
    }
    public void setMemberNames(String[] memberNames) {
        this.memberNames = memberNames;
    }

}
