/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base;

/**
 * Implementation of ISystemStatus.  
 * Result of a sub-system test (LDAP, RDP, MDB) 
 * @author kmoos
 *
 */
public class SystemStatus implements ISystemStatus{

	protected String testResults;
	protected boolean canConnect;
	
	public SystemStatus() {
		this(null, false);
	}
	
	public SystemStatus(String testResults, boolean canConnect) {
		super();
		this.testResults = testResults;
		this.canConnect = canConnect;
	}
	
	/**
	 * Gets the test results in a string format
	 * @return
	 */
	public String getTestResults() {
		return testResults;
	}
	/**
	 * Sets the test results
	 * @param testResults
	 */
	public void setTestResults(String testResults) {
		this.testResults = testResults;
	}
	/**
	 * Retuns if the connection was a success
	 * @return
	 */
	public boolean isCanConnect() {
		return canConnect;
	}
	/**
	 * Sets if the connection was a success
	 * @param canConnect
	 */
	public void setCanConnect(boolean canConnect) {
		this.canConnect = canConnect;
	}

	@Override
	public String toString() {
		return "SystemStatus [testResults=" + testResults + ", canConnect=" + canConnect + "]";
	}
}