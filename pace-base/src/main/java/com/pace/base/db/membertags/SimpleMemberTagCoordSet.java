/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

import com.pace.base.comm.SimpleCoordList;

/**
 * Defines a set of member tag coordinates suitable for the soap layer
 *
 * @version	1.00
 * @author AFarkas
 *
 */
public class SimpleMemberTagCoordSet {

	private MemberTagId memberTagId;
	private SimpleCoordList simpleCoordList;

/**
 * Default constructor
 */
public SimpleMemberTagCoordSet() {}

/**
 * @param memberTagId Member tag id
 * @param simpleCoordList Member tag intersection coordinates
 */
public SimpleMemberTagCoordSet(MemberTagId memberTagId, SimpleCoordList simpleCoordList) {
	
	this.memberTagId = memberTagId;
	this.simpleCoordList = simpleCoordList;
}


/**
 * @return the memberTagId
 */
public MemberTagId getMemberTagId() {
	return memberTagId;
}

/**
 * @param memberTagId the memberTagId to set
 */
public void setMemberTagId(MemberTagId memberTagId) {
	this.memberTagId = memberTagId;
}

/**
 * @return the simpleCoordList
 */
public SimpleCoordList getSimpleCoordList() {
	return simpleCoordList;
}

/**
 * @param simpleCoordList the simpleCoordList to set
 */
public void setSimpleCoordList(SimpleCoordList simpleCoordList) {
	this.simpleCoordList = simpleCoordList;
}
	

}
