/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

/**
 * Defines the position of a member tag comment within a view tuple.
 * This information is needed by the client.
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class MemberTagCommentPosition {
	
	private String name;
	private String axis;
	
	
	/**
	 * No-arg constructor required for soap layer
	 */
	public MemberTagCommentPosition() {
		
	}
	
	/**
	 * @param name Member tag comment name
	 * @param axis View tuple dimension that comment is attached to
	 */
	public MemberTagCommentPosition(String name, String axis) {
		this.name = name;
		this.axis = axis;
	}
	
	/**
	 * @return the axis
	 */
	public String getAxis() {
		return axis;
	}
	/**
	 * @param axis the axis to set
	 */
	public void setAxis(String axis) {
		this.axis = axis;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
