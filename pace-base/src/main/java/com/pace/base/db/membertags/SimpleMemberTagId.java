/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

import com.pace.base.data.Intersection;

public class SimpleMemberTagId {
	private String appName;
	private String memberTagName;
	
	public SimpleMemberTagId() {}
	
	public SimpleMemberTagId(String appName, String memberTagName) {

		this.appName = appName;
		this.memberTagName = memberTagName;
	}
	
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getAppName() {
		return appName;
	}
	public void setMemberTagName(String memberTagName) {
		this.memberTagName = memberTagName;
	}
	public String getMemberTagName() {
		return memberTagName;
	}
	
	@Override
	public String toString() {
		
		StringBuffer strBuff = new StringBuffer("Simple Member Tag ID");
		
		// Display simple member tag data
		strBuff.append("\n\tApplication: " + this.appName);
		strBuff.append("\n\tName: " + this.memberTagName);
		strBuff.append('\n');
		
		return strBuff.toString();
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof SimpleMemberTagId)){
			return false;
		}

		SimpleMemberTagId simpleMTId = (SimpleMemberTagId) o;
		if (this.toString().equals(simpleMTId.toString())){
			return true;
		}else{
			return false;
		}	
	}
	
	public int hashCode() {
		int result = this.toString().hashCode();

		return result;
	}

}
