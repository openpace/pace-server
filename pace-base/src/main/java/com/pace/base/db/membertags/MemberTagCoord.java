/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

import com.pace.base.app.PafSecurityGroup;
import com.pace.base.data.Intersection;
import com.pace.base.db.Dimension;
import com.pace.base.db.membertags.MemberTagId;

/**
 * Member tag coorindate
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class MemberTagCoord implements Comparable<MemberTagCoord> {

	private Integer id;
	private MemberTagId memberTagId;
	private Dimension dimension;
	private String memberName;
	private MemberTagData memberTagData;
	
	public MemberTagCoord() {}

	/**
	 * @param memberTagId Member tag id
	 * @param dimension Coordinate dimension
	 * @param memberName Coordinate Member name
	 * @param memberTagData Member tag data
	 */
	public MemberTagCoord(MemberTagId memberTagId, Dimension dimension, String memberName, MemberTagData memberTagData) {
		this.memberTagId = memberTagId;
		this.dimension = dimension;
		this.memberName = memberName;
		this.memberTagData = memberTagData;
	}

	/**
	 * @return the dimension
	 */
	public Dimension getDimension() {
		return dimension;
	}

	/**
	 * @param dimension the dimension to set
	 */
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	/**
	 * @return the memberTagId
	 */
	public MemberTagId getMemberTagId() {
		return memberTagId;
	}

	/**
	 * @param memberTagId the memberTagId to set
	 */
	public void setMemberTagId(MemberTagId memberTagId) {
		this.memberTagId = memberTagId;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the memberName
	 */
	public String getMemberName() {
		return memberName;
	}

	/**
	 * @param memberName the memberName to set
	 */
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	/**
	 * @return the memberTagData
	 */
	public MemberTagData getMemberTagData() {
		return memberTagData;
	}

	/**
	 * @param memberTagData the memberTagData to set
	 */
	public void setMemberTagData(MemberTagData memberTagData) {
		this.memberTagData = memberTagData;
	}
	
	public int compareTo(MemberTagCoord coord) {
		
		int outcome = 0;
		
		if ( coord.getDimension() != null && this.getDimension() != null) {
			outcome = this.getDimension().getName().compareTo(coord.getDimension().getName());			
		}
		
		return outcome;
	}
}
