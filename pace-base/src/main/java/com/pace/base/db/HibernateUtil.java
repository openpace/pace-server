/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db;

import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafErrHandler;
import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.comm.SessionFactoryType;
import com.pace.base.server.PafMetaData;
import com.pace.base.server.PafMetaData;

public class HibernateUtil {

	private static Map<SessionFactoryType, SessionFactory> sessionFactoryMap = new HashMap<SessionFactoryType, SessionFactory>();
	
	public static Map<SessionFactoryType, ThreadLocal> sessionMap = new HashMap<SessionFactoryType, ThreadLocal>();

	static {
		
		String databaseName = null;
		
		try {

			// Create the SessionFacotry from <filename>.cfg.xml
			SessionFactory sessionFactory = null;
            final String conPrefix = "jdbc:derby:";            
            final String propKey = "hibernate.connection.url";
            String conSuffix = "";
            RdbProps rdbProps = null;
            
			for (SessionFactoryType sessionFactoryType : SessionFactoryType
					.values()) {

				//String configurationFileName = null;

				List<String> resourceList = new ArrayList<String>();

				if (sessionFactoryType.equals(SessionFactoryType.PafDB)) {

					//configurationFileName = PafBaseConstants.HIBERNATE_PAF_DB_CONFIG_FL;
					databaseName = PafBaseConstants.PAF_CACHE_DB;
                    conSuffix = "";
                    rdbProps = (RdbProps) PafMetaData.getRdbProp(PafBaseConstants.DB_NAME_PAF_DB);
                    
                    resourceList.add("com/pace/server/maps/PafBaseMember.hbm.xml");
                    resourceList.add("com/pace/server/maps/PafBaseTree.hbm.xml");
                    resourceList.add("com/pace/server/maps/PafAttributeTree.hbm.xml");
                    resourceList.add("com/pace/server/maps/PafAttributeMember.hbm.xml");		
                    
				} else if (sessionFactoryType.equals(SessionFactoryType.PafSecurityDB)) {

					//configurationFileName = PafBaseConstants.HIBERNATE_PAF_SECURITY_DB_CONFIG_FL;
					databaseName = PafBaseConstants.PAF_SECURITY_DB;
                    conSuffix = ";bootPassword=xIdk82723Sdfke83";
                    rdbProps = (RdbProps) PafMetaData.getRdbProp(PafBaseConstants.DB_NAME_PAF_SECURITY);
                    
                    resourceList.add("com/pace/server/maps/PafUserDef.hbm.xml");
                    
				} else if (sessionFactoryType.equals(SessionFactoryType.PafExtAttrDB)) {

					//configurationFileName = PafBaseConstants.HIBERNATE_PAF_EXT_ATTR_DB_CONFIG_FL;
					databaseName = PafBaseConstants.PAF_EXT_ATTR_DB;
                    conSuffix = "";
                    rdbProps = (RdbProps) PafMetaData.getRdbProp(PafBaseConstants.DB_NAME_PAF_EXT_ATTR);
                    
                	resourceList.add("com/pace/server/maps/PafAttributeTree.hbm.xml");
    				resourceList.add("com/pace/server/maps/PafAttributeMember.hbm.xml");	
                    
				} else if ( sessionFactoryType.equals(SessionFactoryType.PafClientCacheDB)) {

					//configurationFileName = PafBaseConstants.HIBERNATE_PAF_CLIENT_CACHE_DB_CONFIG_FL;
					databaseName = PafBaseConstants.PAF_CLIENT_CACHE_DB;
                    conSuffix = "";
                    rdbProps = (RdbProps) PafMetaData.getRdbProp(PafBaseConstants.DB_NAME_PAF_CLIENT_CACHE);
                    
                    resourceList.add("com/pace/server/maps/Application.hbm.xml");
    				resourceList.add("com/pace/server/maps/CellNote.hbm.xml");
    				resourceList.add("com/pace/server/maps/DataSource.hbm.xml");
    				resourceList.add("com/pace/server/maps/Dimension.hbm.xml");	
    				resourceList.add("com/pace/server/maps/CellNoteMapping.hbm.xml");	
    				resourceList.add("com/pace/server/maps/MemberTagCoord.hbm.xml");	
    				resourceList.add("com/pace/server/maps/MemberTagData.hbm.xml");	
    				resourceList.add("com/pace/server/maps/MemberTagId.hbm.xml");	
    				resourceList.add("com/pace/server/maps/SecurityGroups.hbm.xml");			
					
				}
				
				//if (configurationFileName != null && databaseName != null) {
				if (databaseName != null) {

					Configuration conf = new Configuration();
					conf.setProperties(rdbProps.decryptHibernateProperties());
					for (String resource: resourceList ) {
						conf.addResource(resource);
					}
						                    
                    if (conf.getProperty(propKey) == null || conf.getProperty(propKey).trim().equals("")) {
                        String propVal = conPrefix + PafMetaData.getServerSettings().getPafServerHome() 
                            + databaseName + conSuffix;
                        conf.setProperty(propKey, propVal);
                    }

                        
					sessionFactory = conf.buildSessionFactory();

					sessionFactoryMap.put(sessionFactoryType, sessionFactory);

				}

			}

		} catch (Throwable ex) {
			PafErrHandler.handleException(new PafException(
					"Error initializing hibernate for " + databaseName + ". "
							+ ex.getMessage(), PafErrSeverity.Fatal));
		}
	}

	public static SessionFactory getSessionFactory(SessionFactoryType sessionFactoryType) {
		
		return sessionFactoryMap.get(sessionFactoryType);
	}

	public static void shutdown(SessionFactoryType sessionFactoryType) {

		// Close caches and connection pools
		getSessionFactory(sessionFactoryType).close();
		
	}
	
	@SuppressWarnings({"unchecked","unchecked"})
	public static Session currentSession(SessionFactoryType sessionFactoryType) throws HibernateException {
		
		ThreadLocal localThread = sessionMap.get(sessionFactoryType); 
		
		Session s = null;
		
		if ( localThread != null ) {
			
			s = (Session) localThread.get();
			
		}
		
		// open a new session, if this thread has none yet
		if (localThread == null || s == null) {
			
			final ThreadLocal threadLocal = new ThreadLocal();
			
			s = sessionFactoryMap.get(sessionFactoryType).openSession();
			
			threadLocal.set(s);
			
			sessionMap.put(sessionFactoryType, threadLocal);
		}
		
		return s;
	}
	
	@SuppressWarnings("unchecked")
	public static void closeSession(SessionFactoryType sessionFactoryType) throws HibernateException {
		
		//if thread local exist in map
		if ( sessionMap.containsKey(sessionFactoryType)) {
		
			//get thread local from map
			ThreadLocal threadLocal = sessionMap.get(sessionFactoryType);
			
			//if not null
			if ( threadLocal != null ) {
			
				//get session from thread local
				Session s = (Session) threadLocal.get();
				
				//if session is not null, close it
				if (s != null) {
					s.close();
				}
				
				//set thread local's obj to null
				threadLocal.set(null);
				
				//push back into map
				sessionMap.put(sessionFactoryType, threadLocal);
			}
		}
		
	}
	
	
}