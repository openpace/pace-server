/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

/**
 * Member tag type
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public enum MemberTagType {
	
	TEXT {public String getCode() {return "TX";}}, 
	FORMULA {public String getCode() {return "FO";}},
	HYPERLINK {public String getCode() {return "HY";}};

	/**
	 *	Return corresponding text mnemonic 
	 */
	public abstract String getCode();
	
	
	/**
	 *  Decode member tag type code
	 *
	 * @param code Member tag type code 
	 * @return MemberTagType
	 */
	static public MemberTagType deCode(String code) {
		for (MemberTagType tagType:MemberTagType.values()) {
			if (tagType.getCode().equalsIgnoreCase(code)) {
				return tagType;
			}
		}
		
		// Invalid code
		String errMsg = "Unable to decode invalid MemberTagType code of: " + code;
		throw new IllegalArgumentException(errMsg);
	}

}
