/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.pace.base.db.Application;

/**
 * Identifies a member tag assigned to an application
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
@Entity
@Table(name = "MEMBER_TAG_ID", schema = "PAFSYS")
public class MemberTagId {
	
	private Integer id;
	private Application application;
	private String memberTagName;
	private Set<MemberTagCoord> memberTagCoordSet;
	
	public MemberTagId() {}
	
	/**
	 * Create a member tag id
	 * 
	 * @param application Application object
	 * @param memberTagName Member tag name
	 */
	public MemberTagId(Application application, String memberTagName) {

		this.application = application;
		this.memberTagName = memberTagName;
	}

	/**
	 * @return the application
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "APP_ID_INT", nullable = false)
	public Application getApplication() {
		return application;
	}
	/**
	 * @param application the application to set
	 */
	public void setApplication(Application application) {
		this.application = application;
	}
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name = "ID_INT", nullable = false)
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the memberTagName
	 */
	@Column(name = "MEMBER_TAG_NAME_TXT", nullable = false, length = 80)
	public String getMemberTagName() {
		return memberTagName;
	}
	/**
	 * @param memberTagName the memberTagName to set
	 */
	public void setMemberTagName(String memberTagName) {
		this.memberTagName = memberTagName;
	}

	/**
	 * @return the memberTagCoordSet
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "memberTagId")
	public Set<MemberTagCoord> getMemberTagCoordSet() {
		return memberTagCoordSet;
	}

	/**
	 * @param memberTagCoordSet the memberTagCoordSet to set
	 */
	public void setMemberTagCoordSet(Set<MemberTagCoord> memberTagCoordSet) {
		this.memberTagCoordSet = memberTagCoordSet;
	}

	/**
	 *  Generate a key for the member tag id
	 *
	 * @return String
	 */
	public String generateKey() {
		
		// Key is appName + "." + memberTagName
		if (this.application != null) {
			return application.getName() + "." + this.memberTagName;
		}
		return null;
	}


}
