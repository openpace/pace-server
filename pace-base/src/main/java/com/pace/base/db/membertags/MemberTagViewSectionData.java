/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

import java.io.IOException;

import com.pace.base.IPafCompressedObj;
import com.pace.base.IPafCompressedSingleObj;
import com.pace.base.PafBaseConstants;
import com.pace.base.utility.CompressionUtil;

/**
 * Holds member tag data on a view section
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class MemberTagViewSectionData implements IPafCompressedSingleObj {
	
	private String memberTagName;
	private String[] memberTagValues;
    private String compressedData;
    private boolean isCompressed;
    
	
	/**
	 * @return the memberTagValues
	 */
	public String[] getMemberTagValues() {
		return memberTagValues;
	}
	/**
	 * @param memberTagValues the memberTagValues to set
	 */
	public void setMemberTagValues(String[] memberTagData) {
		this.memberTagValues = memberTagData;
	}
	
	/**
	 * @return the memberTagName
	 */
	public String getMemberTagName() {
		return memberTagName;
	}
	/**
	 * @param memberTagName the memberTagName to set
	 */
	public void setMemberTagName(String memberTagName) {
		this.memberTagName = memberTagName;
	}
	
	/* (non-Javadoc)
	 * @see com.pace.base.IPafCompressedObj#compressData()
	 */
	public void compressData() throws IOException {
		compressData(PafBaseConstants.DELIMETER_ELEMENT);
	}

	/* (non-Javadoc)
	 * @see com.pace.base.IPafCompressedObj#compressData()
	 * @param String dynamic delimiter
	 */
	public void compressData(String dynamicDelimiter) throws IOException {

		// Don't compress if data values are null or empty
		if (memberTagName == null || memberTagValues == null || memberTagValues.length == 0) {
			return;
		}

		// Compress data
		StringBuffer strBuff = new StringBuffer(memberTagValues.length * 20);
		strBuff.append(memberTagName);
		for (int i = 0; i < memberTagValues.length; i++) {
			strBuff.append(dynamicDelimiter);
			strBuff.append(memberTagValues[i]);
		}
		compressedData = strBuff.toString();
		
		// Clean up
		memberTagName = null;
		memberTagValues = null;
		isCompressed = true;
	
	}

	/* (non-Javadoc)
	 * @see com.pace.base.IPafCompressedObj#uncompressData()
	 */
	public void uncompressData() {
		// Not needed on server	
	}


	public boolean isCompressed() {
		return isCompressed;
	}
	
	
	public void setCompressed(boolean isCompressed) {
		this.isCompressed = isCompressed;	
	}
	

	/**
	 * @return the compressedData
	 */
	public String getCompressedData() {
		return compressedData;
	}
	/**
	 * @param compressedData the compressedData to set
	 */
	public void setCompressedData(String compressedData) {
		this.compressedData = compressedData;
	}
		
	public Integer getDynamicEscapeCount(String delimiter, String escapeChar) {
		return CompressionUtil.generateEscapeSequenceForArray(memberTagValues, delimiter, escapeChar);
		
	}



	
}
