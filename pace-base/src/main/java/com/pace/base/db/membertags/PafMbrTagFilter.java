/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

/**
 * Member tag filter to be used in server provider requests
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafMbrTagFilter {

	private String appName;
	private String[] memberTagNames = new String[0];
	private String[] newMemberTagNames = new String[0];
	
	/**
	 *  default constructor
	 */
	public PafMbrTagFilter() {}
	
	/**
	 * @param appName Application name
	 */
	public PafMbrTagFilter(String appName) {
		this.appName = appName;
	}
	
	/**
	 * @param appName Application name
	 * @param memberTagNames Member tag names
	 */
	public PafMbrTagFilter(String appName, String[] memberTagNames) {
		this.appName = appName;
		this.memberTagNames = memberTagNames;
	}

	/**
	 * @return the appName
	 */
	public String getAppName() {
		return appName;
	}
	/**
	 * @param appName the appName to set
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
	/**
	 * @return the memberTagNames
	 */
	public String[] getMemberTagNames() {
		return memberTagNames;
	}
	/**
	 * @param memberTagNames the memberTagNames to set
	 */
	public void setMemberTagNames(String[] memberTagNames) {
		this.memberTagNames = memberTagNames;
	}
	/**
	 * @return the newMemberTagNames
	 */
	public String[] getNewMemberTagNames() {
		return newMemberTagNames;
	}
	/**
	 * @param newMemberTagNames the newMemberTagNames to set
	 */
	public void setNewMemberTagNames(String[] newMemberTagNames) {
		this.newMemberTagNames = newMemberTagNames;
	}		
}
