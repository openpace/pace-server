/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.cellnotes;

import com.pace.base.db.Dimension;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class CellNoteMapping {

	private int id = -1;
	
	private Dimension dimension;
	
	private CellNote cellNote;
	
	private String memberName;

	/**
	 * Empty constructor
	 *
	 */
	public CellNoteMapping() {
	}

	/**
	 * 
	 * Creates a cell note mapping from a dimension, member name and cell note
	 * 
	 * @param dimension		Dimension
	 * @param memberName	Member Name
	 * @param cellNote		Cell note mapping is tied to.
	 */
	public CellNoteMapping(Dimension dimension, String memberName, CellNote cellNote) {
		this.dimension = dimension;
		this.memberName = memberName;
		this.cellNote = cellNote;
	}
	
	/**
	 * @return the dimension
	 */
	public Dimension getDimension() {
		return dimension;
	}



	/**
	 * @param dimension the dimension to set
	 */
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}


	/**
	 * @return the memberName
	 */
	public String getMemberName() {
		return memberName;
	}

	/**
	 * @param memberName the memberName to set
	 */
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	@SuppressWarnings("unused")
	private void setId(int id) {
		this.id = id;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		
		if ( obj instanceof CellNoteMapping ) {
			
			CellNoteMapping secondPafDimMember = (CellNoteMapping) obj;
			
			if ( this.dimension != null && secondPafDimMember.dimension != null ){
			
				if ( this.dimension.getName().equals(secondPafDimMember.dimension.getName())
						&& this.cellNote.getId() == secondPafDimMember.cellNote.getId()) {
					return true;
				}
				
			}
			
			
			
		}
		
		return super.equals(obj);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		
		if (this.dimension != null && this.dimension.getName() != null ) {
			
			return this.dimension.getName().hashCode();
			
		}
		
		return super.hashCode();
	}

	/**
	 * @return the cellNote
	 */
	public CellNote getCellNote() {
		return cellNote;
	}

	/**
	 * @param cellNote the cellNote to set
	 */
	public void setCellNote(CellNote cellNote) {
		this.cellNote = cellNote;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuffer strBuff = new StringBuffer("Cell Note Mapping: " + super.toString());
		
		strBuff.append("\n\tCell Note Mapping Id: " + id);
		strBuff.append("\n\tDimension: " + dimension.getName() + ", Dimension Id: " + dimension.getId());
		strBuff.append("\n\tMember Name: " + memberName);
		
		if ( cellNote == null) {
			strBuff.append("\n\tNot tied to a cell note.");
		} else {
			strBuff.append("\n\tCell Note Id: " + cellNote.getId() + "\n");
		}
		
		
		return strBuff.toString();
	}


	

	
}
