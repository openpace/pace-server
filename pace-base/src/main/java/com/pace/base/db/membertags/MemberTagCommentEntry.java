/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

/**
 * Member Tag Comment Entry
 *
 * @version	1.00
 * @author AFarkas
 *
 */
public class MemberTagCommentEntry {
	
	String name;
	boolean isVisible;
	
	/**
	 * No-arg constructor required for soap layer
	 */
	public MemberTagCommentEntry() {}
	
	/**
	 * @param name Member tag comment name
	 */
	public MemberTagCommentEntry(String name) {
		
		this.name = name;
	}

	/**
	 * @param name Member tag comment name
	 * @param isVisible Member tag visibility behavior on view
	 */
	public MemberTagCommentEntry(String name, boolean isVisible) {
		
		this.name = name;
		this.isVisible = isVisible;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the isVisible
	 */
	public boolean isVisible() {
		return isVisible;
	}

	/**
	 * @param isVisible the isVisible to set
	 */
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

}
