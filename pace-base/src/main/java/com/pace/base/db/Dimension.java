/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db;

/**
 * Dimension which contains name, id and if dimension is enabled.
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class Dimension {

	private int id = -1;
	
	private String name;
	
	private boolean enabled = false;
	
	/**
	 * Default constructor
	 *
	 */
	public Dimension() {
		
	}
	
	/**
	 * 
	 * Overloaded constructor
	 * 
	 * @param name Name of dimension
	 */
	public Dimension(String name) {
		this.name = name;
	}

	/**
	 * 
	 * Overloaded constructor
	 * 
	 * @param name Name of dimension
	 * @param isEnabled If dimension is enabled or not for queries
	 */
	public Dimension(String name, boolean isEnabled) {
		
		this.name = name;
		this.enabled = isEnabled;
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
}
