/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.base.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.PafKeystoreEncryptionException;
import com.pace.base.PafKeystorePasswordReadException;
import com.pace.base.misc.KeyValue;
import com.pace.base.security.AESKeystoreEncryption;

/**
 * Relational DB properties
 * 
 * @author JWatkins
 *
 */
public class RdbProps implements Serializable {
		
	private static final long serialVersionUID = -1796795377109159322L;
	
	private transient static final String PASSWORD_KEY = PafBaseConstants.RDB_PASSWORD_KEY;
	
	private transient static final Logger logger = Logger.getLogger(RdbProps.class);
	
	private String name;
	
	private Properties hibernateProperties;
	
	private transient List<KeyValue> hibernatePropertyList;
	
	private int version;
	
    private transient AESKeystoreEncryption encrptor;
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public RdbProps() {
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the unmodified property collection.
	 * @return the unmodified hibernateProperties
	 */
	public Properties getHibernateProperties()  {
		return this.hibernateProperties;
	}
	
	/**
	 * Get the decryption property collection.
	 * @return the decrypted hibernateProperties
	 * @throws PafKeystoreDecryptionException 
	 */
	public Properties decryptHibernateProperties() throws PafKeystoreDecryptionException {
		try{
			
			return decryptProperties(hibernateProperties);
			
		} catch (PafKeystoreDecryptionException e){
			
			String errorMessage = String.format("Cannot reset password since the connection is not using Derby.  " +
					"Plese contact your system administrator to get the password for this connection: %s", getName());
			
			try {
				Properties p = resetHibernateDerbyPasswords(hibernateProperties);
				if(p == null){
					logger.info(errorMessage);
					throw e;
				}
				return p;
			} catch (Exception e1) {
				logger.info(errorMessage);
				throw e;
			}
		}
	}

	/**
	 * This method will encrypt the necessary properties before setting the collection.
	 * If the string is already encrypted, no encryption will occur.
	 * @param hibernateProperties the hibernateProperties to set
	 * @throws PafKeystoreEncryptionException 
	 */
	public void setHibernateProperties(Properties hibernateProperties) throws PafKeystoreEncryptionException {
		this.hibernateProperties = encryptProperties(hibernateProperties);
	}

	/**
	 * @return the hibernatePropertyList
	 */
	public List<KeyValue> getHibernatePropertyList() {
		
		if ( hibernatePropertyList == null ) {
			
			hibernatePropertyList = new ArrayList<KeyValue>();
			
		}
		
		return hibernatePropertyList;
	}

	/**
	 * @param hibernatePropertyList the hibernatePropertyList to set
	 */
	public void setHibernatePropertyList(List<KeyValue> hibernatePropertyList) {
		this.hibernatePropertyList = hibernatePropertyList;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RdbProps [name=" + name + ", hibernateProperties="
				+ hibernateProperties + ", hibernatePropertyList="
				+ hibernatePropertyList + "]";
	}

	/**
	 * If the RDB object is using Derby, then reset to application defaults.
	 * @param properties
	 * @return null if the RDB object is not using derby.
	 * @throws PafKeystoreEncryptionException
	 */
	private Properties resetHibernateDerbyPasswords(Properties properties) {
		
		boolean match = false;
		
		if ( properties != null ) {
    	
			List<String> pafSysProps = Arrays.asList(PafBaseConstants.PAF_SYS_PROPS);
			List<String> pafSys123Props = Arrays.asList(PafBaseConstants.PAF_SYS_123_PROPS);
			
    		for(String key : properties.stringPropertyNames()){
    			
    			String value = properties.getProperty(key);
    			
    			if(key.equalsIgnoreCase(PafBaseConstants.RDB_DIALECT_KEY) &&  value.equalsIgnoreCase(PafBaseConstants.RDB_DIALECT_VALUE)){
    				
    				String message = String.format("Error decrypting password for RdbProperty: %s, since the Derby provider is in use the default system password will be used.", getName());

    				if(pafSysProps.contains(getName())){
    					
    					match = true;
    					logger.info(message);
    					properties.setProperty(PafBaseConstants.RDB_PASSWORD_KEY, PafBaseConstants.PAFSYS);
    					break;
    					
    				} else if(pafSys123Props.contains(getName())){
    					
    					match = true;
    					logger.info(message);
    					properties.setProperty(PafBaseConstants.RDB_PASSWORD_KEY, PafBaseConstants.PAFSYS123);
    					break;
    					
    				}

    			}
    		}
		}
		
		return match ? properties : null;
	}
	
	
	/**
     * Performs a encryption of the properties.
     * @param connString
     * @return
     * @throws PafKeystoreEncryptionException
     */
    private Properties encryptProperties(Properties properties) throws PafKeystoreEncryptionException {
    	try {
			return cryptProperties(properties, true);
		} catch (PafKeystoreDecryptionException e) {
			//This should never happen since we are performing a encryption
			e.printStackTrace();
			return null;
		}
    }
    
    /**
     * Performs a decryption of the properties.
     * @param connString
     * @return
     * @throws PafKeystoreDecryptionException
     */
    private Properties decryptProperties(Properties properties) throws PafKeystoreDecryptionException {
    	try {
			return cryptProperties(properties, false);
		} catch (PafKeystoreEncryptionException e) {
			//This should never happen since we are performing a decryption
			e.printStackTrace();
			return null;
		}
    }
	
    /**
     * Method to encrypt or decrypt the property collection.  
     * If called from the Getter, then a decryption will be called
     * if called from setter then an encryption will be called.
     * @param properties
     * @param performEncryption if true the Password parm will be encrypted, if false a decryption will occur.
     * @return the encrypted or decrypted properties
     * @throws PafException 
     */
    private Properties cryptProperties(Properties properties, boolean performEncryption) throws PafKeystoreDecryptionException, PafKeystoreEncryptionException {
    	if ( properties != null ) {
    		
    		Properties props = new Properties();
    		
    		for(String key : properties.stringPropertyNames()){
				
    			String value = properties.getProperty(key);
    			
    			if(key.equalsIgnoreCase(PASSWORD_KEY)){

					//update the password in the collection
					if(performEncryption){
	            		try {
	            			props.setProperty(key, getEncryptor().encryptToHexString(value, true));
						} catch (PafKeystoreEncryptionException e) {
							//logger.error(e.getMessage());
							logger.error(String.format("Cannot encrypt password for Rdb: %s, property: %s.", name, key), e);
							throw e;
						} catch (PafKeystorePasswordReadException e) {
							//logger.error(e.getMessage());
							logger.error(String.format("Cannot encrypt password for Rdb: %s, property: %s.", name, key), e);
							throw new PafKeystoreEncryptionException(e.getMessage(), e.getSeverity());
						}
	            	} else{
	            		try {
	            			props.setProperty(key, getEncryptor().decryptFromHexString(value));
						} catch (PafKeystoreDecryptionException e) {
							//logger.error(e.getMessage());
							logger.error(String.format("Cannot decrypt password for Rdb: %s, property: %s.", name, key), e);
							throw e;
						} catch (PafKeystorePasswordReadException e) {
							//logger.error(e.getMessage());
							logger.error(String.format("Cannot decrypt password for Rdb: %s, property: %s.", name, key), e);
							throw new PafKeystoreDecryptionException(e.getMessage(), e.getSeverity());
						}
	            	}
				} else {
					props.setProperty(key, value);
				}
			}
    		return props;
    	}
    	return properties;
    }
    
    /**
     * Lazy load the Encryption object.
     * @return
     * @throws PafKeystorePasswordReadException 
     */
	private  AESKeystoreEncryption getEncryptor() throws PafKeystorePasswordReadException{
		if(encrptor == null){
			encrptor = new AESKeystoreEncryption();
		}
		return encrptor;
	}

}
