/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.base.db.membertags;

import java.util.Arrays;


public class MemberTagQueryResults {
	
	private int totalMemberTagCount;
	private MemberTagInformation[] memberTagInformation;
	
	public MemberTagQueryResults() {}
	
	/**
	 * @param totalMemberTagCount Total member tag intersection count
	 * @param memberTagInformation Member tag information by application
	 */
	public MemberTagQueryResults(int totalMemberTagCount, MemberTagInformation[] memberTagInformation) {
		
		this.totalMemberTagCount = totalMemberTagCount;
		this.memberTagInformation = memberTagInformation;
	}

	/**
	 * @return the memberTagInformation
	 */
	public MemberTagInformation[] getMemberTagInfo() {
		return memberTagInformation;
	}
	/**
	 * @param memberTagInformation the memberTagInformation to set
	 */
	public void setMemberTagInfo(MemberTagInformation[] memberTagInfo) {
		this.memberTagInformation = memberTagInfo;
	}
	/**
	 * @return the totalCount
	 */
	public int getTotalMemberTagCount() {
		return totalMemberTagCount;
	}
	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalMemberTagCount(int totalCount) {
		this.totalMemberTagCount = totalCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(memberTagInformation);
		result = prime * result + totalMemberTagCount;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberTagQueryResults other = (MemberTagQueryResults) obj;
		if (!Arrays.equals(memberTagInformation, other.memberTagInformation))
			return false;
		if (totalMemberTagCount != other.totalMemberTagCount)
			return false;
		return true;
	}
	

}
