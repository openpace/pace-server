/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.funcs;

import gnu.trove.set.hash.THashSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import com.pace.base.PafBaseConstants;
import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.app.MeasureDef;
import com.pace.base.app.MeasureType;
import com.pace.base.data.EvalUtil;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.data.MemberTreeSet;
import com.pace.base.funcs.AbstractFunction;
import com.pace.base.mdb.PafDimMember;
import com.pace.base.mdb.PafDimTree;
import com.pace.base.state.IPafEvalState;
import com.pace.base.state.PafClientState;
import com.pace.base.utility.StringOdometer;
import com.pace.base.utility.StringUtils;

/**
 * "CrossDim" Custom Function - Returns the value of the intersection represented by 
 * overriding one or more dimension members of the current data cache intersection with
 * the specfied dimension member override values.
 * 
 * The calling signature of this function is '@CrossDim(String Dim1, String Member1, String Dim2, 
 * 		String Member2, ......, String DimN, String MemberN)'.
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class F_CrossDim extends AbstractFunction {

	private static Logger logger = Logger.getLogger(F_CrossDim.class);
	
	// These variables will hold pre-parsed information, for performance reasons
	private Intersection overrideParms = null;
	private List<String> functionDims = null;
	
	
	public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException, PafInvalidIntersectionException {

		// Get required arguments if not already parsed
//		if (overrideParms == null) {
		// Generate token catalog
			Properties tokenCatalog = evalState.getClientState().getTokenCatalog();
			overrideParms = parseCrossDimParms(sourceIs, evalState.getClientState(), tokenCatalog);
//		}

		// Clone current intersection
		Intersection crossDimIs = sourceIs.clone();
		
		// Update coordinates with the dimension member override values
		for (int i = 0; i < overrideParms.getDimensions().length; i++) {
			crossDimIs.setCoordinate(overrideParms.getDimensions()[i], overrideParms.getCoordinates()[i]);
		}
		
//		// Lock cross dim intersection so that any direct changes are preserved
//		evalState.getOrigLockedCells().add(crossDimIs);
//
//		// Interesting things to try...
//		evalState.getCurrentLockedCells().add(crossDimIs);
//	    Collection<Intersection> cellsToLock = new HashSet<Intersection>(1);
//	    cellsToLock.add(crossDimIs);
//	    evalState.addAllAllocations(cellsToLock);
		
		
		lockRecalcComps(sourceIs, crossDimIs, evalState);

		// Return value of cross dimensional intersection
		String yearCoord = crossDimIs.getCoordinate(dataCache.getYearAxis());
		PafDimTree yearTree = evalState.getEvaluationTree(dataCache.getYearDim());
		double result;
		if (yearTree.getSyntheticMemberNames().contains(yearCoord)
				&& !dataCache.hasValidTimeHorizonCoord(crossDimIs)) {
			// The synthetic year root is only valid in combination with the UOWROOT
			// of the time dimension.  Therefore, any attempts to reference an
			// invalid time intersection will just result in a zero being returned.
			// (TTN-2147)
			result = 0;
		} else {
			result = dataCache.getCellValue(crossDimIs);
		}
		return result;
	}


	/**
	 *	Parse function parameters
	 *
	 *	If sourceIs parameter is null then any set of parameters that use a member token
	 *	will be skipped. This is necessary for trigger intersection processing.
	 *
	 * @param sourceIs Source intersection
	 * @param clientState ClientState
	 * @param tokenCatalog Token catalog
	 *
	 * @return Intersection
	 */
	private Intersection parseCrossDimParms(Intersection sourceIs, PafClientState clientState, Properties tokenCatalog) {
		
		List<String> allDims = clientState.getUowTrees().getAllDimensions();
		try {
			int dimCount = parms.length / 2;
			functionDims = new ArrayList<String>();
			List<String> dims = new ArrayList<String>(), members = new ArrayList<String>();
			for (int parmInx = 0; parmInx < dimCount; parmInx++) {
				
				// Parse dimension and member specification string
				String dim = parms[parmInx*2];
				if (!allDims.contains(dim)) {
					String errMsg = String.format("@CrossDim signature error - an invalid dimension name of [%s] was specified", dim); 
					logger.error(errMsg);
					throw new IllegalArgumentException(errMsg);					
				}
				
				String memberSpec = parms[parmInx*2+1];
				
				// Skip this set of parameters if no source intersection was supplied and a 
				// member token is used
				if (sourceIs == null && memberSpec.startsWith(PafBaseConstants.FUNC_TOKEN_START_CHAR)) {
					continue;
				}
				
				// Add override parms
				dims.add(dim);
				String currMember = null;
				if (sourceIs != null) {
					currMember = sourceIs.getCoordinate(dim);
				}
				members.add(resolveMemberSpec(dim, memberSpec, clientState, currMember, tokenCatalog));
			}
			overrideParms = new Intersection(dims.toArray(new String[0]), members.toArray(new String[0]));
			return overrideParms;
			
		} catch (RuntimeException e) {
			// Check for an invalid number of parameters
			String errMsg = null;
			if (parms.length == 0 || parms.length % 2 != 0) {
				errMsg = "@CrossDim signature error - an invalid number of parameters were specified";
				logger.error(errMsg);
				throw new IllegalArgumentException(errMsg);
			} else {
				throw e;
			}

		}
	}


	/**
	 *  Resolve member specification
	 *
	 * @param dimension Dimension name
	 * @param memberSpec Member specification
	 * @param clientState ClientState
	 * @param currMbrName Current intersection member
	 * @param tokenCatalog Token catalog
	 * 
	 * @return Member name
	 */
	private String resolveMemberSpec(String dimension, String memberSpec, PafClientState clientState, String currMbrName, Properties tokenCatalog) {
		
		// If not a token, then just return member name (original membSpec value)
		if (!memberSpec.startsWith(PafBaseConstants.FUNC_TOKEN_START_CHAR)) {
			return memberSpec;
		}
		
		String resolvedMemberSpec = null, key = null, validDim = null;
		String timeDim = clientState.getMdbDef().getTimeDim();
		String yearDim = clientState.getMdbDef().getYearDim();
		

		// Get dimension tree and current member
		PafDimTree dimTree = clientState.getUowTrees().getTree(dimension);
		PafDimMember currMember = dimTree.getMember(currMbrName);
		
		// Check for PARENT token
		if (memberSpec.equalsIgnoreCase(PafBaseConstants.FUNC_TOKEN_PARENT)) {
			if (currMember == dimTree.getRootNode()) {
				// Return current member name if current member is root of tree
				resolvedMemberSpec = currMember.getKey();
			} else {
				// Else return name of parent
				resolvedMemberSpec = currMember.getParent().getKey();				
			}
			functionDims.add(dimension);
			return resolvedMemberSpec;
		}
			
		// Check for UOWROOT token
		if (memberSpec.equalsIgnoreCase(PafBaseConstants.FUNC_TOKEN_UOWROOT)) {
			// Return name of root node
			resolvedMemberSpec = dimTree.getRootNode().getKey();
			functionDims.add(dimension);
			return resolvedMemberSpec;
		}
		
		// Check for FIRST_PLAN_YEAR token (TTN-1597)
		key = PafBaseConstants.FUNC_TOKEN_FIRST_PLAN_YEAR; 
		validDim = yearDim;
		if (memberSpec.equalsIgnoreCase(key)) {
			if (dimension.equals(validDim)) {
				// Return token value
				resolvedMemberSpec = StringUtils.removeDoubleQuotes(tokenCatalog.getProperty(key));
				functionDims.add(dimension);
				return resolvedMemberSpec;
			} else {
				// Return error - token only valid on year dimension
				String errMsg = "@CrossDim signature error - the token: " + key
						+ " is only valid on the [" + validDim + "] dimension";
				logger.error(errMsg);
				throw new IllegalArgumentException(errMsg);
			}
		}
		
		// Check for FIRST_NONPLAN_YEAR token (TTN-1597)
		key = PafBaseConstants.FUNC_TOKEN_FIRST_NONPLAN_YEAR; 
		validDim = yearDim;
		if (memberSpec.equalsIgnoreCase(key)) {
			if (dimension.equals(validDim)) {
				// Return token value
				resolvedMemberSpec = StringUtils.removeDoubleQuotes(tokenCatalog.getProperty(key));
				functionDims.add(dimension);
				return resolvedMemberSpec;
			} else {
				// Return error - token only valid on year dimension
				String errMsg = "@CrossDim signature error - the token: " + key
						+ " is only valid on the [" + validDim + "] dimension";
				logger.error(errMsg);
				throw new IllegalArgumentException(errMsg);
			}
		}
		
		// Check for FIRST_PLAN_PERIOD token (TTN-1597)
		key = PafBaseConstants.FUNC_TOKEN_FIRST_PLAN_PERIOD; 
		validDim = timeDim;
		if (memberSpec.equalsIgnoreCase(key)) {
			if (dimension.equals(validDim)) {
				// Return token value
				resolvedMemberSpec = StringUtils.removeDoubleQuotes(tokenCatalog.getProperty(key));
				functionDims.add(dimension);
				return resolvedMemberSpec;
			} else {
				// Return error - token only valid on year dimension
				String errMsg = "@CrossDim signature error - the token: " + key
						+ " is only valid on the [" + validDim + "] dimension";
				logger.error(errMsg);
				throw new IllegalArgumentException(errMsg);
			}
		}
		
		// Invalid token	
		String errMsg = "@CrossDim signature error - invalid token specified";
		logger.error(errMsg);
		throw new IllegalArgumentException(errMsg);
		
	}


	/* 
	 * Return the set of intersections that might cause this function to need to re-evaulate
	 * TODO Re-arrange parsing logic and trigger logic so that this function can be called without a current member
	 */
	public Set<Intersection> getTriggerIntersections(IPafEvalState evalState) throws PafException {

		PafClientState clientState = evalState.getClientState();

		// Get required arguments if not already parsed
//		if (overrideParms == null) {
			overrideParms = parseCrossDimParms(null, clientState, null);
//		}
		
		String measureDim = evalState.getAppDef().getMdbDef().getMeasureDim();
		String versionDim = evalState.getAppDef().getMdbDef().getVersionDim();
		String yearDim = evalState.getAppDef().getMdbDef().getYearDim();
		List<String> explodedDims = new ArrayList<String>();
		Set<String> lockedYearSet = clientState.getLockedYears();
		Map<String, Set<String>> filterMap = new HashMap<String, Set<String>>(); 
		
		// Assume that trigger measure is current measure (TTN-2056)
		String currentMeasure = evalState.getMeasureName(), triggerMeasure = currentMeasure;		

		// Create a filter that encompasses that matches the overridden dimensions
		// passed to this function.
		for (int i = 0; i < overrideParms.getDimensions().length; i++) {

			// Create a member filter on each cross dimension			
			String dim = overrideParms.getDimensions()[i];
			String member = overrideParms.getCoordinates()[i];
			Set<String>members = new HashSet<String>();
			members.add(member);

			// Determine the trigger members (TTN-2056)
			if (dim.equals(measureDim)) {
				// Measures dimension - update trigger measure to specified coordinate
				// and don't include in filter since we will only be pulling these 
				// intersections
				triggerMeasure = member;
			} else if (dim.equals(versionDim)) {
				// Version dimension - if not the plan version then there won't be
				// any trigger intersections since the change cell stack only contains
				// intersections for the current plan version. If a version is 
				// specified then its almost certainly not the plan version, but
				// we should check just to be sure. If it is the plan version,
				// don't bother to include in filter.
				String planVersion = evalState.getPlanningVersionName();
				if (!member.equals(planVersion)) {
					return new HashSet<Intersection>();
				}
			} else if (dim.equals(yearDim) && lockedYearSet.contains(member)) {
				// If year dimension and not a plannable Year, then there won't be
				// any trigger intersections since the change cell stack should
				// only contain plannable intersections. 
				return new HashSet<Intersection>();
			} else {
				// For all other dimensions (and plannable year), add member to filter
				// and specify that dimension should be exploded for cloning process.
				members.add(member);
				filterMap.put(dim, members);				
				explodedDims.add(dim);
			}				
		}


		// Find the intersections that match the overridden dimensions passed to this function
		Set<Intersection> changedCells = evalState.getChangedCellsByMsr().get(triggerMeasure);				// TTN-2056
		Set<Intersection> matchingChangedCells = findIntersections(filterMap, changedCells, evalState); 	// TTN-2056


		// Get the potential impact list for each matching intersection and add to trigger set
		Set<Intersection> triggerSet = null;
		if (explodedDims.isEmpty()) {
			// No exploded dimensions found, just send back matching changed cells
			triggerSet = matchingChangedCells;
		} else {
			// Clone each matching changed intersection across each exploded dimension
			triggerSet = new THashSet<Intersection>(matchingChangedCells.size() * 10);
			for (Intersection matchingChangedCell : matchingChangedCells) {
				List<Intersection> clonedIntersections = cloneIntersection(matchingChangedCell, explodedDims, currentMeasure, evalState);
				triggerSet.addAll(clonedIntersections);
			}
		}

				
		// Return set of trigger intersections
		return triggerSet;
	}

	public boolean changeTriggersFormula(Intersection is, IPafEvalState evalState) {
		return true;
	}
	
    /* (non-Javadoc)
     * @see com.pace.base.funcs.AbstractFunction#getMemberDependencyMap(com.pace.base.state.IPafEvalState)
     */
    public Map<String, Set<String>> getMemberDependencyMap(IPafEvalState evalState) throws PafException {
    	
    	Map<String, Set<String>> dependencyMap = new HashMap<String, Set<String>>();
    	
    	// Updated the parameter of the method below to pass 
    	// the clientState to parse the cross dim params.
		overrideParms = parseCrossDimParms(null, evalState.getClientState(), null);
		
		// Check each set of function parms
		int dimCount = parms.length / 2;
		functionDims = new ArrayList<String>();
		for (int parmInx = 0; parmInx < dimCount; parmInx++) {

			// Parse dimension and member specification string
			String dim = parms[parmInx*2];
			String memberSpec = parms[parmInx*2+1];

			// If not a token, then just return member name (original membSpec value)
			Set<String> members = null;
			if (!memberSpec.startsWith(PafBaseConstants.FUNC_TOKEN_START_CHAR)) {
				// If member is hard-coded, just add it to member map
				members = new HashSet<String>();
				members.add(memberSpec);
			} else {
				// Token is specified, add all dimension members
				PafDimTree dimTree = evalState.getEvaluationTree(dim);
				members = new HashSet<String>();
				members.addAll(Arrays.asList(dimTree.getMemberKeys()));
			}
			dependencyMap.put(dim, members);
		}

		// Return member dependency map
		return dependencyMap;
    	
    }

 	/**
	 *  Clone the specified intersection across the specified dimensions
	 *
	 * @param intersection Intersection
	 * @param explodedDims Exploded dimensions
	 * @param overrideMeasure Measure to use on cloned intersections
	 * @param evalState Evaluation state
	 * 
	 * @return List<Intersection>
	 */
	private List<Intersection> cloneIntersection(Intersection intersection, List<String> explodedDims, String overrideMeasure, IPafEvalState evalState) {

		String[] allDims = intersection.getDimensions();
		@SuppressWarnings("unchecked")
		List<String>[] memberArrays = new ArrayList[allDims.length];
		MemberTreeSet memberTrees = evalState.getClientState().getUowTrees();		// TTN-1595
		String measureDim = evalState.getMsrDim();
		
		// Build member lists for clone process. 
		// 
		// Use original time tree and year trees, versus time horizon tree
		// for intersection cloning. This is done to support the scenario 
		// where a specified cross dimension is the time dimension or 
		// the year dimension, but not necessarily both (TTN-1595)
		int i = 0;
		for (String dim:allDims) { 
			List<String> memberList = null;
			if (explodedDims.contains(dim)) {
				// Floor dimension - Member list equals dimension floor
				PafDimTree dimTree = memberTrees.getTree(dim);						// TTN-1595
				memberList = dimTree.getLowestMemberNames();
			} else {
				// Else member list equals current coordinate
				String coord;
				if (overrideMeasure != null && dim.equals(measureDim)) {
					coord = overrideMeasure;
				} else {
					coord = intersection.getCoordinate(dim);
				}
				memberList = new ArrayList<String>();;
				memberList.add(coord);
			}
			memberArrays[i++] = memberList;
		}


		// Clone Intersections
		StringOdometer odom = new StringOdometer(memberArrays);
		List<Intersection> clonedIntersections = new ArrayList<Intersection>(odom.getCount());
		Intersection clonedIntersection;
		while (odom.hasNext()) {
			clonedIntersection = new Intersection(allDims, odom.nextValue());		// TTN-1851
			// Filter out any intersections whose time coordinates are not valid in the
			// time horizon tree (TTN-1595).
			if (EvalUtil.hasValidTimeCoord(clonedIntersection, evalState)) {
				clonedIntersections.add(clonedIntersection);
			}
		}

		return clonedIntersections;
	}

	private void lockRecalcComps(Intersection targetIs, Intersection crossDimIs, IPafEvalState evalState) {
		
	    Collection<Intersection> cellsToLock = new HashSet<Intersection>(1);
	    int measureAxis = evalState.getDataCache().getMeasureAxis();
	    MeasureDef msrDef = evalState.getAppDef().getMeasureDef(crossDimIs.getCoordinate(measureAxis));	//TTN-2139
	    if (msrDef.getType() == MeasureType.Recalc && evalState.getOrigLockedCells().contains(crossDimIs)) {
            cellsToLock.add(targetIs);
    	    evalState.getCurrentLockedCells().addAll(cellsToLock); 
    	    evalState.addAllAllocations(cellsToLock);
	    }
	}
	
}
