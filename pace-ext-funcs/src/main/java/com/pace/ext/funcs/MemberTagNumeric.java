/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.funcs;

import java.util.Set;

import com.pace.base.PafException;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.funcs.AbstractMemberTagFunction;
import com.pace.base.state.IPafEvalState;

/**
 * 	Retrieves the value of the member tag and cast it to a Double.   
 *	Returns: A double value if the member tag value is numeric, 0 if no data value is found or the data value is not numeric.  
 *	Required Parms
 *		memberTagName (Required)
 *			Name of the member tag as defined in the paf_member_tags.xml file
 *	Optional Parms
 *	Note: These values will override the values of the existing intersection
 *		DimName1 (Optional)
 *			Name of dimension for which the member tag is defined (MemberTagDef.dims[0])
 *		MbrName1  (Optional)
 *			Name of the member to use to build the partial intersection
 *		DimNameX  (Optional)
 *			Name of dimension for which the member tag is defined (MemberTagDef.dims[x])
 *		MbrNameX (Optional)
 *			Name of the member to use to build the partial intersection
 * @author kmoos
 *
 */
public class MemberTagNumeric extends AbstractMemberTagFunction {

	@Override
	public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException {
		
		//Validate the set of parms
		validateParms(evalState);
		
		Intersection mbrTagIs = getMemberTagIntersection(sourceIs, evalState, 1);
		
		//Return the data (if available) for the current member tag intersection.
		Double d = getMemberTagData(mbrTagIs);
		
		if(d != null && logger.isDebugEnabled()){
			logger.debug(String.format("Intersection: %s, Base Intersection:  %s, found member tag value: %s", mbrTagIs, sourceIs, d.doubleValue()));
		}
		
		return d == null ? 0.00 : d.doubleValue();
	}

	@Override
	public Set<Intersection> getTriggerIntersections(IPafEvalState evalState)
			throws PafException {
		
		return evalState.getCurrentChangedCells();
	}
}
