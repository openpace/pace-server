/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.funcs;

import com.pace.base.PafException;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.funcs.AbstractMemberTagComparisonFunction;
import com.pace.base.state.IPafEvalState;

/**
 * Tests if the specified value (string) does not match the value in the member tag. 
 *		Returns: true if the member tag value matches the compareTo text value, false if not
 *	Required Parms
 *		memberTagName (Required)
 *			Name of the member tag as defined in the paf_member_tags.xml file
 *		compareTo (Required)
 *			Text to compare to the member tag value.
 *	Optional Parms
 *	Note: These values will override the values of the existing intersection
 *		DimName1 (Optional)
 *			Name of dimension for which the member tag is defined (MemberTagDef.dims[0])
 *		MbrName1  (Optional)
 *			Name of the member to use to build the partial intersection
 *		DimNameX  (Optional)
 *			Name of dimension for which the member tag is defined (MemberTagDef.dims[x])
 *		MbrNameX (Optional)
 *			Name of the member to use to build the partial intersection
 * @author kmoos
 *
 */
public class IsMemberTagNotEquals extends AbstractMemberTagComparisonFunction {

	@Override
	public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException {
		this.operator = compOpEnum.notEqual;
		return super.calculate(sourceIs, dataCache, evalState);
	}
}
