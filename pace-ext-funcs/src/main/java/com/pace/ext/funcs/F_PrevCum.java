/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.funcs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.app.MdbDef;
import com.pace.base.data.EvalUtil;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.data.MemberTreeSet;
import com.pace.base.data.TimeSlice;
import com.pace.base.funcs.AbstractFunction;
import com.pace.base.funcs.ParsedLevelGen;
import com.pace.base.mdb.PafDimTree;
import com.pace.base.state.IPafEvalState;
import com.pace.base.state.PafClientState;


/**
 * Combines the behavior of a previous function and a cumulative function into one function
 * so as to avoid the "nesting of custom functions". It returns the running cumulative value
 * of a measure, if added up along a particular dimension (default is time). However, it 
 * stops and the "previous" point in time to the current position in the time hierarchy.

 * Function Signature: @PREV_CUM(MEASURE, TIME DIMENSION, OFFSET, GEN/LEVEL, YEAR)
 * Example: @PREV_CUM(SLS_DLR, Time, 1, G3, FY2007)
 *
 * @version	2.8.2.0
 * @author jwatkins
 *
 */


// TODO add support for multiple offset increments in next function
public class F_PrevCum extends AbstractFunction {

   	protected static int MEASURE_ARGS = 1, REQUIRED_ARGS = 1, MAX_ARGS = 3;

   	// parameter variables
	private String offsetDim, levelGenParm, yearParm, yearMbr;
	private int offset;
	private ParsedLevelGen parsedLG;
	
	PafDimTree offsetTree;
	Map<String, Set<String>> filterMap = new HashMap<String, Set<String>>();    
	
	private static Logger logger = Logger.getLogger(F_PrevCum.class);	
	
	
    public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException, PafInvalidIntersectionException {

    	
    	// convenience strings
    	String msrDim = evalState.getMsrDim();

    	validateParms(evalState);
    	
    	// create testing intersection and assign measure from parameter string
    	Intersection dataIs = sourceIs.clone();
		dataIs.setCoordinate( msrDim, this.getMeasureName() );

		// accumulate values
		double result;
		if (parms.length < 4) {
			result = dataCache.getCumTotal(dataIs, offsetDim, offset);
		} else {
			result = dataCache.getCumTotal(dataIs, offsetDim, offset, parsedLG.getLevelGenType(), parsedLG.getLevelGen(), yearMbr);
		}

		return result;
    }

    /**
     *  Parse and validate function parameters 
     *
     * @param evalState Evaluation state object
     * @throws PafException
     */
    protected void validateParms(IPafEvalState evalState) throws PafException {

    	// quick check to get out if it looks like these have been validated already
    	if (this.isValidated) return;
    	
    	String errMsg = "Error in [" + this.getClass().getName() + "] - ";
    	String measureDim = evalState.getAppDef().getMdbDef().getMeasureDim();
    	
    	
     	// Check for existence of arguments
    	if (parms == null) {
    		errMsg += "[" + REQUIRED_ARGS + "] arguments are required, but none were provided.";
    		logger.error(errMsg);
    		throw new PafException(errMsg, PafErrSeverity.Error);
    	}
    	
    	// Check for the correct number of arguments
    	if (parms.length < REQUIRED_ARGS) {
    		errMsg += "[" + REQUIRED_ARGS + "] arguments are required, but [" + parms.length + "] were provided.";
    		logger.error(errMsg);
    		throw new PafException(errMsg, PafErrSeverity.Error);
    	}
   	
    	
    	// Check validity of measure arguments for existence in measures dimension
    	PafDimTree measureTree = evalState.getEvaluationTree(measureDim);			// TTN-1595
    	if (!measureTree.hasMember(this.measureName)){
    		errMsg += "[" + this.measureName + "] is not a valid member of the [" + measureDim + "] dimension.";
    		logger.error(errMsg);
    		throw new PafException(errMsg, PafErrSeverity.Error);

    	}
    	
    	
		// by default this accumulates values along the time dimension, however
		// the dimension of accumulation can be altered by passing a 2nd parameter
		if (parms.length > 1) {
			offsetDim = parms[1];
			// Check validity of dimension argument for existing dimension
			offsetTree = evalState.getEvaluationTree(offsetDim);		// TTN-1595
			if (offsetTree == null) {
				errMsg += "[" + this.measureName
						+ "] is not a valid member of the [" + measureDim
						+ "] dimension.";
				logger.error(errMsg);
				throw new PafException(errMsg, PafErrSeverity.Error);
			}
		} else {
			offsetDim = evalState.getTimeDim();
			offsetTree = evalState.getEvaluationTree(offsetDim);		// TTN-1595
		}
		
		// be default the offset amount is 1, however if a 3rd parameter is
		// specified it may by overridden
		if (parms.length > 2) {
			try {
				offset = Integer.parseInt(parms[2]);
			}
			catch (NumberFormatException ex) {
				errMsg += "[" + this.parms[2] + "] is not a valid integer";
				logger.error(errMsg);
				throw new PafException(errMsg, PafErrSeverity.Error);
			}
		}
		else { 
			offset = 1;
		}

		// Gen/Level parm
		if (parms.length > 3) {
			levelGenParm = parms[3];
			try {
				parsedLG = parseLevelGenParm(levelGenParm);
			} catch (IllegalArgumentException e) {
				errMsg += "[" + levelGenParm + "] is not a valid level/gen specification";
				logger.error(errMsg);
				throw new PafException(errMsg, PafErrSeverity.Error);
			}
		}

		// Year member parm
		if (parms.length > 4) {
			yearParm = parms[4];
			Properties tokenCatalog = evalState.getClientState().getTokenCatalog();
			String yearDim = evalState.getAppDef().getMdbDef().getYearDim();
			PafDimTree yearTree = evalState.getEvaluationTree(yearDim);
			try {
				yearMbr = parseYearParm(yearParm, yearTree, tokenCatalog, true);
			} catch (IllegalArgumentException e) {
				errMsg += "[" + yearParm + "] is not a valid year specification";
				logger.error(errMsg);
				throw new PafException(errMsg, PafErrSeverity.Error);
			}
		}

		this.isValidated = true;
    }
    

	@Override
	public Set<Intersection> getTriggerIntersections(IPafEvalState evalState) throws PafException {
        // this function forces the recalculation of all subsequent intersections involving
        // the measure on the left hand side of this rule and time periods subsequent to the 
        // current time period
        
        String measureName = this.getMeasureName(); 
        String timeDim = evalState.getAppDef().getMdbDef().getTimeDim();


        
        // if their has been a receipt change prior to this time slice, also evaluate
        // the left hand measure in the intersection = (curent msr, cur ts, rcptchg1, rcptchg2...)
        // or pretend there is a change in the current time period for the measure so that the
        // cumulative recalcs in this period
        Set<Intersection> chngBaseMsrs = evalState.getChangedCellsByMsr().get(measureName);
        Set<Intersection> iSet = null;      
        Intersection currentChange;
        if (chngBaseMsrs != null) {
        	iSet = new HashSet<Intersection>(chngBaseMsrs.size() * 2);   
            for (Intersection is : chngBaseMsrs) {
                currentChange = is.clone();
                
             // TTN-2519 When using the custom function in a non-perpetual rulegroup, we need to convert the 
             // intersection's period coordinate to a valid time horizon coordinate since the timeSlice property 
             // is only set within a perpetual rulegroup. However, to provide accurate results, 
             // this function should only be used in a perpetual rulegroup.
                String timeHorizCoord = evalState.getCurrentTimeSlice();
				if ( timeHorizCoord == null ) {
					timeHorizCoord = TimeSlice.buildTimeHorizonCoord(is, evalState.getDataCache());
				}
				EvalUtil.setIsCoord(currentChange, timeDim, timeHorizCoord, evalState);		// TTN-1595 /
				
                iSet.add(currentChange);
            }
        }
        else {
        	 iSet = new HashSet<Intersection>(0);        
        }

        return iSet;            
	}
	
    /* (non-Javadoc)
     * Method to get the dependency map . 
     * @see com.pace.base.funcs.AbstractFunction#getMemberDependencyMap(com.pace.base.state.IPafEvalState)
     */
    @Override
    public Map<String, Set<String>> getMemberDependencyMap(IPafEvalState evalState) throws PafException{
        
        Map<String, Set<String>> memberDependencyMap = new HashMap<String,Set<String>>();
        PafClientState clientState = evalState.getClientState();
        MdbDef mdbDef = clientState.getApp().getMdbDef();
        String[] hierDims = mdbDef.getHierDims();
        String timeDim = mdbDef.getTimeDim();
        MemberTreeSet clientTrees = clientState.getUowTrees();
        
        // For the measure dimension, select just the measure name.
        memberDependencyMap = super.getMemberDependencyMap(evalState);
        
        // Select all time dimension floor members.
        PafDimTree timeTree = clientTrees.getTree(timeDim);
        Set<String> floorMemberSet = new HashSet<String>(timeTree.getLowestMemberNames()) ;
        memberDependencyMap.put(timeDim, floorMemberSet);
        
        //Select floor members for all remaining hierarchy dimensions
        for( String hierDim : hierDims){
            PafDimTree hierTree = clientTrees.getTree(hierDim);
            floorMemberSet = new HashSet<String>(hierTree.getLowestMemberNames());
            memberDependencyMap.put(hierDim, floorMemberSet);
            
        }
        
        return memberDependencyMap;
        
    }


}
