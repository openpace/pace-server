/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.funcs;

import java.util.HashSet;
import java.util.Set;

import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.funcs.AbstractFunction;
import com.pace.base.state.IPafEvalState;

/**
 * This intersection just acts as a flag for evaluation logic in many ways. It takes a particular
 * intersection and substitutes the measure passed in as a parameter and replaces it in the source
 * intersection in order to retrieve a value. However it's primary value is to indicate in the
 * evaluation logic, that the "triggering intersection's coordinates are to be used here rather
 * than normal evaluation intersection. See ES_EvalDelayedInvMeasures
 *
 * @version	x.xx
 * @author jim
 *
 */


// TODO add support for multiple offset increments in next function
public class F_TriggerIntersection extends AbstractFunction {

    /* (non-Javadoc)
     * @see com.pace.base.funcs.IPafFunction#calculate(com.pace.base.data.Intersection, com.pace.base.data.IPafRoDataCache, com.pace.base.data.IPafEvalState, java.lang.String[])
     */
    public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException, PafInvalidIntersectionException {
    	double result = 0;
    	PafApplicationDef app = evalState.getAppDef();

    	Intersection dataIs = sourceIs.clone();
    	
        // usual use case is to provide a measure parameter this operation will apply to
    	if ( parms.length > 0 )
    		dataIs.setCoordinate(app.getMdbDef().getMeasureDim(), parms[0]);
    	
        result = dataCache.getCellValue(dataIs);

    	return result;
    }

	@Override
	public Set<Intersection> getTriggerIntersections(IPafEvalState evalState) throws PafException {
		// TODO Auto-generated method stub
		return new HashSet<Intersection>(0);
	}

}
