/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.funcs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.app.MdbDef;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.data.EvalUtil;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.data.MemberTreeSet;
import com.pace.base.data.TimeSlice;
import com.pace.base.funcs.AbstractFunction;
import com.pace.base.funcs.ParsedLevelGen;
import com.pace.base.mdb.PafDimTree;
import com.pace.base.state.IPafEvalState;
import com.pace.base.state.PafClientState;

/**
 * Implements a cumulative function. It totals up all the intersections along a particular
 * dimensional axis. The default axis is the time dimension (ie. @CUM(SLS_DLR, Time))
 *
 * Function Signature: @CUMCOUNT(DIMENSION, GEN/LEVEL, YEAR)
 * Example: @CUMCOUNT(Time, G3, FY2007)
 * 
 * @version	x.xx
 * @author jim
 *
 */


public class F_Cum extends AbstractFunction {
	Map<String, Set<String>> filterMap = new HashMap<String, Set<String>>(); 
	private static Logger logger = Logger.getLogger(F_Cum.class);	
	
    public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException, PafInvalidIntersectionException {
    	double result = 0;
    	PafApplicationDef app = evalState.getAppDef();
		String yearDim = app.getMdbDef().getYearDim();
		PafDimTree yearTree = evalState.getEvaluationTree(yearDim);
		String timeDim, levelGenParm = null, yearParm = null, yearMbr = null;
    	String errMsg = "Error in [" + this.getClass().getName() + "] - ";
    	ParsedLevelGen parsedLG = null;
    	Intersection dataIs = sourceIs.clone();
    	
        // usual use case is to provide a measure parameter this operation will apply to
    	if ( parms.length > 0 )
    		dataIs.setCoordinate(app.getMdbDef().getMeasureDim(), parms[0]);
    	
        // by default this accumulates values along the time dimension, however the dimension
        // of accumulation can be altered.
    	if (parms.length > 1 ) {
			timeDim = parms[1];
			//validate the time dim name sent in, if one is specified TTN-2036
			if(!timeDim.equalsIgnoreCase(app.getMdbDef().getTimeDim())) {
				String timeErrMsg = "Illegal time dimension name: [" + timeDim + "] used in @CUM function";
				throw new IllegalArgumentException(timeErrMsg);
			}
    	}
		else
			timeDim = app.getMdbDef().getTimeDim();

		// Gen/Level parm
		if (parms.length <= 2) {
			// No Gen/Level parm
		    result  = dataCache.getCumTotal(dataIs, timeDim);
			return result;
		} else {
			levelGenParm = parms[2];
			try {
				parsedLG = parseLevelGenParm(levelGenParm);
			} catch (IllegalArgumentException e) {
				errMsg += "[" + levelGenParm + "] is not a valid level/gen specification";
				logger.error(errMsg);
				throw new PafException(errMsg, PafErrSeverity.Error);
			}
		}
		
		// Year member parm
		if (parms.length > 3) {
			yearParm = parms[3];
			Properties tokenCatalog = evalState.getClientState().getTokenCatalog();
			try {
				yearMbr = parseYearParm(yearParm, yearTree, tokenCatalog, true);
			} catch (IllegalArgumentException e) {
				errMsg += "[" + yearParm + "] is not a valid year specification";
				logger.error(errMsg);
				throw new PafException(errMsg, PafErrSeverity.Error);
			}
		}

		result = dataCache.getCumTotal(dataIs,  timeDim, 0, parsedLG.getLevelGenType(), parsedLG.getLevelGen(), yearMbr);
		return result;
    }
    

	/**
	 *	This method augments the normal dependency logic in a formula. In this case a dependency being changed
	 *  causes all intersections corresponding to the result term in the rule rolling forward to also be
	 *  recalculated.
	 * @param evalState
	 *
	 * @return Set of intersection objects
	 */
	public Set<Intersection> getTriggerIntersections(IPafEvalState evalState) {	
		
		// this function forces the recalculation of all subsequent intersections involving
		// the measure on the left hand side of this rule and time periods subsequent to the 
		// current time period
		
		String measureName = this.getMeasureName();	
        String timeDim = evalState.getAppDef().getMdbDef().getTimeDim();


		
		// if their has been a receipt change prior to this time slice, also evaluate
		// the left hand measure in the intersection = (curent msr, cur ts, rcptchg1, rcptchg2...)
		// .or pretend their is a change in the current time period for the measure so that the
		// cumulative recalcs in this period
		Set<Intersection> chngBaseMsrs = evalState.getChangedCellsByMsr().get(measureName);
		Set<Intersection> iSet = null;
		
		Intersection currentChange;
		if (chngBaseMsrs != null) {
			iSet = new HashSet<Intersection>( chngBaseMsrs.size() * 2);
			for (Intersection is : chngBaseMsrs) {
				currentChange = is.clone();

				// TTN-2519 When using the custom function in a non-perpetual rulegroup, we need to convert the 
	            // intersection's period coordinate to a valid time horizon coordinate since the timeSlice property 
	            // is only set within a perpetual rulegroup. However, to provide accurate results, 
	            // this function should only be used in a perpetual rulegroup.
				String timeHorizCoord = evalState.getCurrentTimeSlice();
				if ( timeHorizCoord == null ) {
					timeHorizCoord = TimeSlice.buildTimeHorizonCoord(is, evalState.getDataCache());
				}
				EvalUtil.setIsCoord(currentChange, timeDim, timeHorizCoord, evalState);		// TTN-1595 /
				
			
				iSet.add(currentChange);
			}
		}
		else {
			iSet = new HashSet<Intersection>(0);
		}

		return iSet;                           
		
	}

    /* (non-Javadoc)
     * Method to get the dependency map . 
     * @see com.pace.base.funcs.AbstractFunction#getMemberDependencyMap(com.pace.base.state.IPafEvalState)
     */
    @Override
    public Map<String, Set<String>> getMemberDependencyMap(IPafEvalState evalState) throws PafException{
        
        Map<String, Set<String>> memberDependencyMap = new HashMap<String,Set<String>>();
        PafClientState clientState = evalState.getClientState();
        MdbDef mdbDef = clientState.getApp().getMdbDef();
        String[] hierDims = mdbDef.getHierDims();
        String timeDim = mdbDef.getTimeDim();
        MemberTreeSet clientTrees = clientState.getUowTrees();
        
        // For the measure dimension, select just the measure name.
        memberDependencyMap = super.getMemberDependencyMap(evalState);
        
        // Select all time dimension floor members.
        PafDimTree timeTree = clientTrees.getTree(timeDim);
        Set<String> floorMemberSet = new HashSet<String>(timeTree.getLowestMemberNames()) ;
        memberDependencyMap.put(timeDim, floorMemberSet);
        
        //Select floor members for all remaining hierarchy dimensions
        for( String hierDim : hierDims){
            PafDimTree hierTree = clientTrees.getTree(hierDim);
            floorMemberSet = new HashSet<String>(hierTree.getLowestMemberNames());
            memberDependencyMap.put(hierDim, floorMemberSet);
            
        }
        
        return memberDependencyMap;
        
    }


}
