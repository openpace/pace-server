/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.funcs;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.funcs.AbstractFunction;
import com.pace.base.state.IPafEvalState;

/**
 * "Abs" Custom Function - Returns the absolute value of the supplied version member 
* 
 * The calling signature of this function is '@Abs(version)'.
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class F_Abs extends AbstractFunction {

	private static Logger logger = Logger.getLogger(F_Abs.class);
		
	public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException, PafInvalidIntersectionException {

		// Get parms
		String dimension = dataCache.getVersionDim();
		String member = parms[0];
		
		// Clone current intersection
		Intersection cloneIs = sourceIs.clone();

		// Get value
		int dimCount = 1;
		for (int i = 0; i < dimCount; i++) {
			cloneIs.setCoordinate(dimension, member);
		}
				
		// Return value of cross dimensional intersection
		double result = dataCache.getCellValue(cloneIs);
		return Math.abs(result);
	}

		
	/* 
	 * Return the set of intersections that might cause this function to need to re-evaulate
	 * TODO Re-arrange parsing logic and trigger logic so that this function can be called without a current member
	 */
	public Set<Intersection> getTriggerIntersections(IPafEvalState evalState) throws PafException {
		
		Set<Intersection> triggerSet = new HashSet<Intersection>();
		
		// Return set of trigger intersections
		return triggerSet;
	}

}
