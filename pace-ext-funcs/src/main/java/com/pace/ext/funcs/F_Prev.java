/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.ext.funcs;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.app.MeasureDef;
import com.pace.base.app.MeasureType;
import com.pace.base.app.PafApplicationDef;
import com.pace.base.data.IPafDataCache;
import com.pace.base.data.Intersection;
import com.pace.base.funcs.AbstractFunction;
import com.pace.base.mdb.PafDimMember;
import com.pace.base.mdb.PafDimTree;
import com.pace.base.state.IPafEvalState;

/**
 * Implements a previous function. It looks up the value of the a particular intersection in the datacache
 * 
 *
 * Usage: @Prev(Measure Name, optional dimension, optional offset, bCrossYears)
 * 
 * Example @Prev(BOP_DLR, Time, 1, true)
 * would return the value of at the intersection, that starts at the source intersection, but is invoked
 * for the measure BOP_DLR, and is offset in the Time dimension - 1, and will cross years
 *
 * @version	2.8.2.0
 * @author jwatkins
 *
 */

// TODO add support for multiple offset increments in next function
public class F_Prev extends AbstractFunction {
	
	private int offset = -1;	
   	private String offsetDim = null;	
	PafDimTree offsetTree = null;       	
   	PafApplicationDef app = null; 
	String measureDim = null;
	boolean bCrossYears = true;
  	
	
    public double calculate(Intersection sourceIs, IPafDataCache dataCache, IPafEvalState evalState) throws PafException, PafInvalidIntersectionException {

		if (!this.isInitialized) initialize(evalState);
    	
    	double result = 0;	
    	Intersection dataIs = sourceIs.clone();
    	
    	if ( this.measureName != null ) {
    		dataIs.setCoordinate(measureDim, this.measureName);
    	}

    	try {
    		dataIs = dataCache.shiftIntersection(dataIs, offsetDim, offset, bCrossYears);
    		if (dataIs != null) {
    			result = dataCache.getCellValue(dataIs);
    		}

    	} catch (RuntimeException e) {

    		// Check for absence of parameters
    		if (parms.length == 0) {
    			String errMsg = "@Prev function requires at least one parameter";
    			throw new IllegalArgumentException(errMsg);
    		}

    		// Check for invalid measure
    		PafDimTree measureTree = evalState.getEvaluationTree(measureDim);		// TTN-1595
    		if (!measureTree.hasMember(measureName)) {
    			String errMsg = "Illegal measure name: [" + measureName + "] used in @Prev function";
    			throw new IllegalArgumentException(errMsg);
    		}

    		// Check for invalid dimension parm
    		if (parms.length > 1) {
    			if (offsetTree == null) {
    				String errMsg = "Illegal dimension name: [" + offsetDim + "] used in @Prev function";
    				throw new IllegalArgumentException(errMsg);
    			}
    		}

    		// Unforeseen error - just throw original exception
    		throw(e);
    	}
	
//		lockRecalcComponent(sourceIs, evalState);
    	return result;
    }

	@Override
	public Set<Intersection> getTriggerIntersections(IPafEvalState evalState) {
		PafDimMember offsetMember;  
		Set<Intersection> dirty = new HashSet<Intersection>(0);
		
		if (!this.isInitialized) initialize(evalState);
		
		offsetMember = offsetTree.getPeer(evalState.getCurrentTimeMember().getKey(), offset);
		
		if (offsetMember != null) {
			if (evalState.getChangedCellsByTime().get(offsetMember.getKey()) != null)
				dirty = evalState.getChangedCellsByTime().get(offsetMember.getKey());
		} 
			
//		return dirty;
		
		// prune down to intersections involving the measure parameters
		if (dirty.size() > 0) {
			Map<String, Set<String>> filterMap = new HashMap<String, Set<String>>();
			Set<String> mbrList = new HashSet<String>(1);
			mbrList.add(this.measureName);
			filterMap.put(this.measureDim, mbrList);
			Set<Intersection> filteredDirty;
			
			filteredDirty = this.findIntersections(filterMap, dirty, evalState);
			
			return filteredDirty;
		}
		else {
			return dirty;			
		}
	}
	
	protected void lockRecalcComponent(Intersection is, IPafEvalState evalState) {
	
	// String isMsrName = targetIs.getCoordinate(evalState.getAppDef().getMdbDef().getMeasureDim());
	MeasureDef msrDef = evalState.getAppDef().getMeasureDef(this.measureName);
    
    
	if (msrDef != null && msrDef.getType() == MeasureType.Recalc) {
		Collection<Intersection> cellsToLock = new HashSet<Intersection>(1);
		cellsToLock.add(is);
		evalState.addAllAllocations(cellsToLock);
		evalState.getCurrentLockedCells().addAll(cellsToLock);
	}

}	
	
	private void initialize(IPafEvalState evalState) {

    	app = evalState.getAppDef();
    	measureDim = app.getMdbDef().getMeasureDim();
		
		String index, crossYears;
				
     	if (parms.length > 1 ) 
    		offsetDim = parms[1];
    	else 
    		offsetDim = app.getMdbDef().getTimeDim();
     	
		//Check for invalid dimension parm if specified TTN-2035
		if(!offsetDim.equalsIgnoreCase(app.getMdbDef().getTimeDim())){
			String errMsg = "Illegal dimension name: [" + offsetDim + "] used in @Prev function";
			throw new IllegalArgumentException(errMsg);
		}     	
    	
		offsetTree = evalState.getEvaluationTree(offsetDim);
	
		// Check for invalid dimension parm if specified TTN-2035
		if (parms.length > 1) {
			if (offsetTree == null) {
				String errMsg = "Illegal dimension name: [" + offsetDim + "] used in @Prev function";
				throw new IllegalArgumentException(errMsg);
			}
		}
		
		if (parms.length > 2 )  {
			index = parms[2];
    		try {
        		offset = Integer.valueOf(index) * -1;
			} catch (NumberFormatException e) {
				// Check for invalid integer
				String errMsg = "Illegal integer value of: [" + index + "] supplied for index parm in @Prev function";
				throw new IllegalArgumentException(errMsg);
			}
    	}
    	
    	// Parse cross years parm (TTN-1597)
    	if (parms.length > 3 )  {
    		crossYears = parms[3];
    		bCrossYears = Boolean.parseBoolean(crossYears);
    	}

    	this.isInitialized = true;
	}
    
}
