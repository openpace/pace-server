@rem ***************************************************************************
@rem Copyright (c) 2017-2019 Contributors to Open Pace and others.
@rem  
@rem The OPEN PACE product suite is free software: you can redistribute it and/or
@rem modify it under the terms of the GNU General Public License as published by
@rem the Free Software Foundation, either version 3 of the License, or (at your
@rem option) any later version.
@rem  
@rem This software is distributed in the hope that it will be useful, but WITHOUT
@rem ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
@rem or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
@rem License for more details.
@rem  
@rem You should have received a copy of the GNU General Public License along with
@rem this  software. If not, see <http://www.gnu.org/licenses/>.
@rem ***************************************************************************
set proj_root=%1

set class_dir=%proj_root%\WebRoot\WEB-INF\classes
set lib_dir=%proj_root%\WebRoot\WEB-INF\lib
set client_dir=%proj_root%\client\classes

\proj_wrksp\userlib\apache-cxf-2.4.6\bin\wsdl2java.bat -d %client_dir% -compile -client -p "com.pace.server.client" http://localhost:8080/pace/app?wsdl

