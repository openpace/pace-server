@rem ***************************************************************************
@rem Copyright (c) 2017-2019 Contributors to Open Pace and others.
@rem  
@rem The OPEN PACE product suite is free software: you can redistribute it and/or
@rem modify it under the terms of the GNU General Public License as published by
@rem the Free Software Foundation, either version 3 of the License, or (at your
@rem option) any later version.
@rem  
@rem This software is distributed in the hope that it will be useful, but WITHOUT
@rem ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
@rem or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
@rem License for more details.
@rem  
@rem You should have received a copy of the GNU General Public License along with
@rem this  software. If not, see <http://www.gnu.org/licenses/>.
@rem ***************************************************************************
set proj_root=%~1
set JAVA_HOME=C:\Program Files (x86)\Java\jdk1.7.0_80
REM set user_lib=C:\java\userlib
set user_lib=%USERPROFILE%\userlib
set class_dir=%proj_root%\WebRoot\WEB-INF\classes
set dist_dir=%proj_root%\client\dist
set mavenlib_dir=%USERPROFILE%\.m2\repository
set classpath="%dist_dir%\pace-base.jar";"%mavenlib_dir%\log4j\log4j\1.2.16\log4j-1.2.16.jar";"%mavenlib_dir%\org\nfunk\jep\jep-main\2.4\jep-main-2.4.jar";%class_dir%

"%user_lib%\apache-cxf-2.4.6\bin\java2ws.bat" -wsdl -o "%proj_root%\WebRoot\WEB-INF\PafServiceProviderService.wsdl" -address http://localhost:8080/pace/PafServiceProvider -cp %classpath% com.pace.server.PafServiceProvider


