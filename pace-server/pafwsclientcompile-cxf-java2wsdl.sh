#!/bin/bash
#*******************************************************************************
# Copyright (c) 2017-2019 Contributors to Open Pace and others.
#  
# The OPEN PACE product suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#  
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#  
# You should have received a copy of the GNU General Public License along with
# this  software. If not, see <http://www.gnu.org/licenses/>.
#*******************************************************************************

clear
#proj_root="$PWD"
proj_root=$1
user_lib=$HOME/userlib
class_dir=$proj_root/WebRoot/WEB-INF/classes
dist_dir=$proj_root/client/dist
mavenlib_dir=$HOME/.m2/repository
classpath="$dist_dir/pace-base.jar:$class_dir:$mavenlib_dir/log4j/log4j/1.2.16/log4j-1.2.16.jar:$mavenlib_dir/org/nfunk/jep/jep-main/2.4/jep-main-2.4.jar"
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-i386

echo "proj_root=$proj_root"
echo "user_lib=$user_lib"
echo "class_dir=$class_dir"
echo "dist_dir=$dist_dir"
echo "mavenlib_dir=$mavenlib_dir"
echo "classpath=$classpath"
echo "JAVA_HOME=$JAVA_HOME"

#echo
#echo $user_lib/apache-cxf-2.4.6/bin/java2ws -wsdl -o $proj_root/WebRoot/WEB-INF/PafServiceProviderService.wsdl -address http://localhost:8080/pace/PafServiceProvider -cp $classpath com.pace.server.PafServiceProvider
echo
$user_lib/apache-cxf-2.4.6/bin/java2ws -verbose -wsdl -o $proj_root/WebRoot/WEB-INF/PafServiceProviderService.wsdl -address http://localhost:8080/pace/PafServiceProvider -cp $classpath com.pace.server.PafServiceProvider

