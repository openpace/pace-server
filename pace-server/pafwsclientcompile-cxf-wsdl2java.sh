#!/bin/bash
#*******************************************************************************
# Copyright (c) 2017-2019 Contributors to Open Pace and others.
#  
# The OPEN PACE product suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#  
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#  
# You should have received a copy of the GNU General Public License along with
# this  software. If not, see <http://www.gnu.org/licenses/>.
#*******************************************************************************

clear
#proj_root="$PWD"
proj_root=$1
user_lib=$HOME/userlib
class_dir=$proj_root/WebRoot/WEB-INF/classes
dist_dir=$proj_root/client/dist
client_dir=$proj_root/client/classes
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-i386

echo "proj_root=$proj_root"
echo "user_lib=$user_lib"
echo "class_dir=$class_dir"
echo "dist_dir=$dist_dir"
echo "JAVA_HOME=$JAVA_HOME"


#%user_lib%\apache-cxf-2.4.6\bin\wsdl2java.bat -d %client_dir% -compile -client -p "com.pace.server.client" %proj_root%\WebRoot\WEB-INF\PafServiceProviderService.wsdl
echo
$user_lib/apache-cxf-2.4.6/bin/wsdl2java -verbose -d $client_dir -compile -client -p "com.pace.server.client" $proj_root/WebRoot/WEB-INF/PafServiceProviderService.wsdl

