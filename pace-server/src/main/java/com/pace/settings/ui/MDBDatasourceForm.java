/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import java.util.HashMap;

import com.pace.base.mdb.IPafConnectionProps;
import com.pace.base.mdb.PafConnectionProps;
import com.pace.base.mdb.PafMdbConnectionProps;
import com.pace.settings.PaceSettingsApplication;
import com.pace.settings.PaceSettingsConstants;
import com.pace.settings.data.PaceSettingsDataService;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroupFieldFactory;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;

/**
 * MDB Datasource form
 * 
 * @author JMilliron
 * 
 */
public class MDBDatasourceForm extends PaceSettingsFormDB  {

	private static final long serialVersionUID = -679608093955614805L;

	private boolean newMdbDatasource = false;

	private PafMdbConnectionProps pafConnectionProps = null;

	public MDBDatasourceForm(PaceSettingsApplication serverSettingsApplication) {
		super(serverSettingsApplication);
	//	getFooter().setVisible(false);
	}

	@Override
	protected void setupForm() {

	//	setWriteThrough(false);

		setPaceSettingsDefaultFactory(serverSettingsApplication
				.getMdbDatasourceFieldFactory());

		setFormFieldFactory(getPaceSettingsDefaultFactory());
		
		
		    
		setImmediate(true);

	}

	@Override
	protected void saveForm() {

		if (newMdbDatasource) {

			serverSettingsApplication.getMdbDatasourceContainer().addItem(
					pafConnectionProps);

			setItemDataSource(null);

			serverSettingsApplication.getMdbDatasourceTable().select(
					pafConnectionProps);
			
			pafConnectionProps = null;
			
			newMdbDatasource = false;

		} else {

			setItemDataSource(null);
			newMdbDatasource = false;
			pafConnectionProps = null;
		}

		
		serverSettingsApplication.getMdbDatasourceContainer().saveData();		
			
		serverSettingsApplication.getMdbDatasourceTable().resizeTable();	
		
		
	}

	@Override
	protected void cancelForm() {

		if (newMdbDatasource) {

			newMdbDatasource = false;

		}
		pafConnectionProps = null;
		setItemDataSource(null);

	}

	@Override
	protected void loadSettings() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pace.settings.ui.PaceSettingsForm#setItemDataSource(com.vaadin.data
	 * .Item)
	 */
	@Override
	public void setItemDataSource(Item newDataSource) {
		newMdbDatasource = false;
		if (newDataSource != null) {
			super.setItemDataSource(newDataSource, serverSettingsApplication
				.getMdbDatasourceFieldFactory().getFormOrderList());
		//	super.setItemDataSource(newDataSource);
			getFooter().setVisible(true);

			if (newDataSource instanceof BeanItem) {
				pafConnectionProps = (PafMdbConnectionProps) ((BeanItem) newDataSource)
						.getBean();
			}

			serverSettingsApplication.getMdbDatasourceTable().getValue();

			save.setCaption(PaceSettingsConstants.UPDATE_BUTTON_LABEL);
			
			

		} else {
			super.setItemDataSource(newDataSource);
			getFooter().setVisible(false);
			pafConnectionProps = null;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.Form#isValid()
	 */
	@Override
	public boolean isValid() {

		boolean isValid = super.isValid();

		if (isValid) {

			String newName = (String) getField(MDBDatasourceFieldFactory.NAME)
					.getValue();

			boolean nameExists = false;

			for (PafConnectionProps pafConnectionProps : serverSettingsApplication
					.getMdbDatasourceContainer().getItemIds()) {

				if (pafConnectionProps.getName() != null
						&& pafConnectionProps.getName().trim().equals(newName)) {

					nameExists = true;

					break;

				}

			}

			if (nameExists && newMdbDatasource) {

				isValidNotificationOverrideError = "Name " + newName
						+ " already exists.  Please choose a different name.";
				isValid = false;

			} else if (nameExists && !newMdbDatasource) {

				if (!pafConnectionProps.getName().equals(newName)) {

					isValidNotificationOverrideError = "Name "
							+ newName
							+ " already exists.  Please choose a different name.";
					isValid = false;

				}

			}
			
			String edsurl = (String) getField(MDBDatasourceFieldFactory.EDSURL).getValue();
			if(edsurl.equals(null) || edsurl.equals(" "))
			{
				isValidNotificationOverrideError = getField(
						MDBDatasourceFieldFactory.EDSURL)
						.getCaption()
						+ " content is invalid.  Content needs to be a text value.";
				isValid = false;
			}
			
			String edsdomain = (String) getField(MDBDatasourceFieldFactory.EDSDOMAIN).getValue();
			if(edsdomain.equals(null) || edsdomain.equals(" "))
			{
				isValidNotificationOverrideError = getField(
						MDBDatasourceFieldFactory.EDSDOMAIN)
						.getCaption()
						+ " content is invalid.  Content needs to be a text value.";
				isValid = false;
			}
			
			String server = (String) getField(MDBDatasourceFieldFactory.SERVER).getValue();
			if(server.equals(null) || server.equals(" "))
			{
				isValidNotificationOverrideError = getField(
						MDBDatasourceFieldFactory.SERVER)
						.getCaption()
						+ " content is invalid.  Content needs to be a text value.";
				isValid = false;
			}
			
			String user = (String) getField(MDBDatasourceFieldFactory.USER).getValue();
			if(user.equals(null) || user.equals(" "))
			{
				isValidNotificationOverrideError = getField(
						MDBDatasourceFieldFactory.USER)
						.getCaption()
						+ " content is invalid.  Content needs to be a text value.";
				isValid = false;
			}
			
			
			String password = (String) getField(MDBDatasourceFieldFactory.PASSWORD).getValue();
			if(password.equals(null) || password.equals(" "))
			{
				isValidNotificationOverrideError = getField(
						MDBDatasourceFieldFactory.PASSWORD)
						.getCaption()
						+ " content is invalid.  Content needs to be a text value.";
				isValid = false;
			}
			

			String application = (String) getField(MDBDatasourceFieldFactory.APPLICATION).getValue();
			if(application.equals(null) || application.equals(" "))
			{
				isValidNotificationOverrideError = getField(
						MDBDatasourceFieldFactory.APPLICATION)
						.getCaption()
						+ " content is invalid.  Content needs to be a text value.";
				isValid = false;
			}
			

			String database = (String) getField(MDBDatasourceFieldFactory.DATABASE).getValue();
			if(database.equals(null) || database.equals(" "))
			{
				isValidNotificationOverrideError = getField(
						MDBDatasourceFieldFactory.DATABASE)
						.getCaption()
						+ " content is invalid.  Content needs to be a text value.";
				isValid = false;
			}
			
		/*	String connectionString = (String) getField(
					MDBDatasourceFieldFactory.CONNECTION_STRING).getValue();

			try {

				new PafConnectionProps().setConnectionString(connectionString);

			} catch (IllegalArgumentException iae) {

				isValidNotificationOverrideError = getField(
						MDBDatasourceFieldFactory.CONNECTION_STRING)
						.getCaption()
						+ " content is invalid.  Content needs to be in key=value form seperated by ';'.";
				isValid = false;

			}  */
           
		}

		return isValid;
	}

	public void addMDBDatasource() {

		pafConnectionProps = new PafMdbConnectionProps();
		setItemDataSource(new BeanItem<PafMdbConnectionProps>(pafConnectionProps));
		newMdbDatasource = true;
		save.setCaption(PaceSettingsConstants.ADD_BUTTON_LABEL);
	}

	public void copyMDBDatasource() {

		if (serverSettingsApplication.getMdbDatasourceTable().getValue() != null) {

			PafMdbConnectionProps selectedConnectionProps = (PafMdbConnectionProps)serverSettingsApplication.getMdbDatasourceContainer()
					.getItem(serverSettingsApplication.getMdbDatasourceTable().getValue()).getBean();

			try {
				pafConnectionProps = (PafMdbConnectionProps) selectedConnectionProps.clone();
				pafConnectionProps.setName("Copy of "
						+ selectedConnectionProps.getName());

				setItemDataSource(new BeanItem<PafConnectionProps>(
						pafConnectionProps));
				save.setCaption(PaceSettingsConstants.ADD_BUTTON_LABEL);
				newMdbDatasource = true;
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}

		}

	}

	

}