/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import com.pace.settings.PaceSettingsApplication;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Tree;

/**
 * @author JMilliron
 * 
 */
public class SettingsTree extends Tree {

	public static final Object SERVER_INFO = "Server Information";
	public static final Object SERVER_SETTINGS = "Server Settings";
	public static final Object LDAP_SETTINGS = "LDAP Settings";
	public static final Object RELATIONAL_DATABASE_DATASOURCES = "Relational DB Datasources";
	public static final Object MULTIDIMENSIONAL_DATABASE_DATASOURCES = "Multidimensional DB Datasources";

	public SettingsTree(PaceSettingsApplication app) {

		super("Pace Settings");
		
		//setItemCaptionPropertyId("Pace Settings");
		
		setImmediate(true);
		/*Item parent = addItem("Pace Settings");
		
		setChildrenAllowed(parent, true);*/
		
		addItem(SERVER_INFO);
		addItem(SERVER_SETTINGS);
		addItem(LDAP_SETTINGS);
		addItem(RELATIONAL_DATABASE_DATASOURCES);
		addItem(MULTIDIMENSIONAL_DATABASE_DATASOURCES);

		setSelectable(true);
		setNullSelectionAllowed(false);

		addListener((ItemClickListener) app);

	}
	
}
