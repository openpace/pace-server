/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.Reindeer;

/**
 * LDAP Settings View. Used to hold label and form. 
 * 
 * @author JMilliron
 *
 */
public class LDAPSettingsView extends FormLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6009807502444532956L;

	/**
	 * Constructor
	 * 
	 * @param form form to use in view.
	 */
	public LDAPSettingsView(LDAPSettingsForm form) {
		
		Label heading = new Label(SettingsTree.LDAP_SETTINGS + " - please refer to the LDAP Integration Overview document");
		
		heading.setStyleName(Reindeer.LABEL_H2);
	
		this.addComponent(heading);
		this.addComponent(form.form);
		this.addComponent(form.createButtonBar());
	
		setHeight("100%");
		
		setSizeFull();
		
	}
	
}
