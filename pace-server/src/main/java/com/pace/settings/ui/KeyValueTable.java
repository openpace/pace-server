/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

//import org.vaadin.addon.customfield.CustomField;


import com.vaadin.ui.CustomField ;

import com.pace.base.misc.KeyValue;
import com.pace.settings.PaceSettingsApplication;
import com.pace.settings.PaceSettingsConstants;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;

import com.vaadin.server.ThemeResource;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;

import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * Custom Key Value UI Component
 * 
 * @author JMilliron
 *
 */
@SuppressWarnings("unchecked")
public class KeyValueTable extends CustomField implements Button.ClickListener {
	
	private static final long serialVersionUID = -3018010772417558367L;

	public Table table = new Table();
	
	private Button addButton = new Button(PaceSettingsConstants.ADD_BUTTON_LABEL, (ClickListener) this);
	
	private TextField keyInput = new TextField();
	private TextField valueInput = new TextField();
	private Label keyInputLabel;
	private Label valueInputLabel;
	
//	private CheckBox editableCheckBox = new CheckBox("Editable", (ClickListener) this);
	
	private CheckBox editableCheckBox = new CheckBox();
	private Set<Button> deleteButtonSet = java.util.Collections.synchronizedSet(new java.util.HashSet<Button>());
	
	public BeanItemContainer<KeyValue> keyValueContainer = new BeanItemContainer<KeyValue>(KeyValue.class);
	
	private KeyValueTableFieldFactory tableFieldFactory = new KeyValueTableFieldFactory();

//	private String keyCaption;
	
	private String valueCaption;
	
	VerticalLayout vl = new VerticalLayout();
	
	public KeyValueTable(final String keyCaption, final String valueCaption) {
		
//		this.keyCaption = keyCaption;
		this.valueCaption = valueCaption;
		
		
		table.setContainerDataSource(keyValueContainer);
		table.setTableFieldFactory(tableFieldFactory);
		
		
		final KeyValueTable keyValueTable = this;
		
		table.addGeneratedColumn(((Object) "Delete"), new Table.ColumnGenerator() {
			
			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				
	
				if ( columnId.equals("Delete") ) {
			
					Button deleteButton = new Button();
					
					deleteButton.setData(itemId);
					
					deleteButton.setIcon(new ThemeResource("icons/32/remove.png"));
					
					deleteButton.addListener((ClickListener) keyValueTable);
					
					deleteButtonSet.add(deleteButton);
					
					return deleteButton;					
					
				}
				
				return null;
			}
		});
		
		table.addGeneratedColumn(((Object) "value"), new Table.ColumnGenerator() {
			
			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
												
				if ( columnId.equals("value") ) {
			
					if( ((KeyValue)itemId).getKey().toUpperCase().indexOf("PASSWORD") >= 0 ) {
						PasswordField credField = new PasswordField();
						credField.setValue(((KeyValue)itemId).getValue());
						credField.setReadOnly(true);
						credField.setData(itemId);
						((PasswordField) credField).setNullRepresentation("");
						((PasswordField) credField).setWidth(PaceSettingsConstants.COMMON_FIELD_WIDTH_20_EM);
						
						return credField;	
					}
					return ((KeyValue)itemId).getValue();
				}
				return null;
			}
		});
		
	//	table.setVisibleColumns(new Object[] { "key", "value", "Delete" });
		table.setColumnHeaders(new String[] { keyCaption, valueCaption, "Delete" });
		table.setWidth(PaceSettingsConstants.COMMON_TABLE_WIDTH);
		table.setSelectable(true);
		table.setBuffered(true);
		table.setImmediate(true);
	
		vl.addComponent(table);
		
		HorizontalLayout hy = new HorizontalLayout();
		keyInputLabel = new Label(keyCaption);
		hy.addComponent(keyInputLabel);		
		hy.addComponent(keyInput);
		
		valueInputLabel = new Label(valueCaption);
		hy.addComponent(valueInputLabel);
		hy.addComponent(valueInput);
		
		addButton.setIcon(new ThemeResource("icons/32/add.png"));
	
		hy.addComponent(addButton);
		
		editableCheckBox.setImmediate(true);
		
		hy.addComponent(editableCheckBox);
				
		hy.setSpacing(true);
		
		vl.addComponent(hy);
		
		vl.setStyleName("keyValueTable");
		
		editableCheckBox.setCaption("Editable");
		editableCheckBox.addListener(new ValueChangeListener() {
		  

		@Override
		public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
			setTableEditable(editableCheckBox.booleanValue());
			
		}});
		
	//	setCompositionRoot(vl);
		
	}
	
	@Override
    public Class<?> getType() {
        return ArrayList.class;
    }
	
	@Override
    public void setPropertyDataSource(Property newDataSource) {
       
		Object value = newDataSource.getValue();
     
        if (value instanceof List<?>) {
            @SuppressWarnings("unchecked")
            List<KeyValue> beans = (List<KeyValue>) value;
           
           keyValueContainer.removeAllItems();
           keyValueContainer.addAll(beans);
          table.setPageLength(beans.size());
          
        } else {
        //  throw new Exception("Invalid type");
        }

        super.setPropertyDataSource(newDataSource);
    }
    
    @Override
    public Object getValue() {
    	
    	
        ArrayList<KeyValue> beans = new ArrayList<KeyValue>(); 
        for (Object itemId: keyValueContainer.getItemIds()) {
            beans.add(keyValueContainer.getItem(itemId).getBean());
        }
        
       
        return beans;
    }

	@Override
	public void buttonClick(ClickEvent event) {
		
		if ( event.getButton() == addButton ) {
									
			if ( keyInput.getValue().toString().trim().equals("") || valueInput.getValue().toString().trim().equals("")) {
				
				((PaceSettingsApplication)getUI()).showNotification(keyInputLabel.getValue() + " and " + valueInputLabel.getValue() + " are required.");
				
			} else {
				
				KeyValue kv = new KeyValue(keyInput.getValue().toString().trim(), valueInput.getValue().toString().trim());
				
				if ( table.containsId(kv) ) {
					
					((PaceSettingsApplication)getUI()).showNotification("Key already present in table.");
					
				} else {
				
					Item it = table.addItem(kv);
					
					int pageLength = table.getPageLength();
					
					
					table.setPageLength(pageLength + 1);
					
					table.select(kv);
									
					keyInput.setValue("");
					valueInput.setValue("");
					
					requestRepaintAll();
					
					setTableEditable(false);
					
			        // Adding the item to container and setting the value. 
					keyValueContainer.addItem(kv);
					setValue(getValue());
					
				}
				 				
			}
									
						
		} else if ( deleteButtonSet.contains(event.getButton())) {
			
			Button deleteButton = event.getButton();
			
			Object data = deleteButton.getData();
			
			int pageLength = table.getPageLength();
									
			table.removeItem(data);
			
			if ( pageLength > 0 ) {
				
				table.setPageLength(pageLength - 1);
				
			}
			
			deleteButtonSet.remove(deleteButton);
			
			setValue(getValue());		
			setTableEditable(false);
			
		}  /* else if ( event.getButton() == editableCheckBox ) {
			
			//tableFieldFactory.clearFields();
						
			//table.setEditable(editableCheckBox.booleanValue());
			
			setTableEditable(editableCheckBox.booleanValue());
			
		} */
		
	}

	
	private void setTableEditable(boolean editable) {

		tableFieldFactory.clearFields();
				
		editableCheckBox.setValue(editable);
		
		table.setEditable(editable);
		
	}

	/* (non-Javadoc)
	 * @see org.vaadin.addon.customfield.CustomField#isValid()
	 */
	@Override
	public boolean isValid() {
	
		
		for (Field field : tableFieldFactory.getAllFields() ) {
			
			if ( ! field.isValid()) {
				
				//this.getApplication().get
				
				if ( this.getParent().getParent() instanceof PaceSettingsForm ){
					
					PaceSettingsForm form = (PaceSettingsForm) this.getParent().getParent();
					
					form.isValidNotificationOverrideError = valueCaption + " column fields can not be blank.";
					
				}
				
				return false;
				
			}
			
		}
		
		
		return true;
		
	}

	@Override
	protected Component initContent() {
		// return the component
		return vl;
	}

	
}
