/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.settings.ui;

import java.util.Iterator;

import com.pace.settings.PaceSettingsApplication;
import com.pace.settings.PaceSettingsConstants;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.Reindeer;

/**
 * 
 * @author JMilliron
 * 
 */
public abstract class PaceSettingsForm extends FieldGroup implements ClickListener {

	private static final long serialVersionUID = 6004832755149813646L;

	private PaceSettingsDefaultFieldFactory paceSettingsDefaultFactory;

	protected Button save = new Button(PaceSettingsConstants.SAVE_BUTTON_LABEL, (ClickListener) this);

	protected Button cancel = new Button(PaceSettingsConstants.CANCEL_BUTTON_LABEL, (ClickListener) this);

	protected PaceSettingsApplication serverSettingsApplication;

	protected Object inputObject;
	
	protected String isValidNotificationOverrideError = null;
//	public FieldGroup group ;
 	public FormLayout form = new FormLayout();

	public PaceSettingsForm(PaceSettingsApplication serverSettingsApplication) {
	
		this.serverSettingsApplication = serverSettingsApplication;

		createButtonBar();
		setupForm();
		

	}

	protected abstract void setupForm();

	protected abstract void saveForm();

	protected abstract void cancelForm();

	protected abstract void loadSettings();

	protected HorizontalLayout createButtonBar() {

		HorizontalLayout footer = new HorizontalLayout();

		footer.setSpacing(true);
		footer.addComponent(save);
		footer.addComponent(cancel);

		//this.setFooter(footer);
		
		return footer;
	}

	@Override
	public void setItemDataSource(Item newDataSource) {

		if (newDataSource != null) {

			if (paceSettingsDefaultFactory == null) {

				super.setItemDataSource(newDataSource);
			

			} else {
				super.setItemDataSource(newDataSource);
	
			}

		} else {
		
			super.setItemDataSource(null);
		}

	}

	@Override
	public void buttonClick(ClickEvent event) {

		Button source = event.getButton();

		// on save
		if (source == save) {

			 if (!isValid()) {

				if ( isValidNotificationOverrideError == null ) {
					serverSettingsApplication.showNotification(	"Errors on the form.  Check required fields.");
				} else {
					serverSettingsApplication.showNotification(isValidNotificationOverrideError);
				}
				isValidNotificationOverrideError = null;
				return;
			} 

			// commit changes to data source
			
				try {
					commit();
				} catch (CommitException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			

			saveForm();

			StringBuilder messageBuilder = new StringBuilder(save.getCaption());
			
			if ( save.getCaption().equals(PaceSettingsConstants.ADD_BUTTON_LABEL)) {
				
				//e.g. Added
				messageBuilder.append("ed");
				
			} else {
				
				//e.g. Saved, Updated, etc
				messageBuilder.append('d');
				
			}
			
						serverSettingsApplication.showNotification(messageBuilder.toString());

			// on cancel
		} else if (source == cancel) {

			// discard changes
			discard();

			cancelForm();

			serverSettingsApplication.showNotification(
					"Canceled");
			
			

		}

	}

	/**
	 * @return the paceSettingsDefaultFactory
	 */
	public PaceSettingsDefaultFieldFactory getPaceSettingsDefaultFactory() {
		return paceSettingsDefaultFactory;
	}

	/**
	 * @param paceSettingsDefaultFactory
	 *            the paceSettingsDefaultFactory to set
	 */
	public void setPaceSettingsDefaultFactory(
			PaceSettingsDefaultFieldFactory paceSettingsDefaultFactory) {
		this.paceSettingsDefaultFactory = paceSettingsDefaultFactory;
	}

	
	
}
