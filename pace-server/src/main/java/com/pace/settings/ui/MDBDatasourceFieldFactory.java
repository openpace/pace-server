/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.settings.ui;

import com.pace.settings.PaceSettingsConstants;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroupFieldFactory;
//import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.ui.themes.*;
import com.vaadin.server.ThemeResource;
/**
 * MDB Datasource Field Factory
 * 
 * @author JMilliron
 *
 */
public class MDBDatasourceFieldFactory extends PaceSettingsDefaultFieldFactory {

	private static final String CONNECTION_STRING_INPUT_PROMPT = "EDSDomain=Essbase;EDSUrl=http://localhost:13080/aps/JAPI;Server=?;User=?;Password=?;Application=?;Database=?";

	private static final long serialVersionUID = 2885762876258153110L;
	
	public static final String NAME = "name";
	public static final String CONNECTION_STRING = "connectionString";
	public static final String META_DATA_SERVICE_PROVIDER = "metaDataServiceProvider";
	public static final String DATA_SERVICE_PROVIDER = "dataServiceProvider";
	public static final String TOOL_TIP = "connectionToolTip";

	//new fields added to display Key Value pairs for connection string
	public static final String APPLICATION = "application";
	public static final String DATABASE = "database";
	public static final String EDSURL = "edsurl";
	public static final String EDSDOMAIN = "edsdomain";
	public static final String SERVER = "server";
	public static final String USER = "user";
	public static final String PASSWORD = "password";
	
	private ComboBox metaDataServiceProviderComboBox = new ComboBox();
	
	private ComboBox dataServiceProviderComboBox = new ComboBox();
	
	private String connectionStringTooltip = null;
	
	public String getConnectionStringTooltip() {
		return connectionStringTooltip;
	}

	public MDBDatasourceFieldFactory() {
		
		formOrderList.add(NAME);
		formOrderList.add(TOOL_TIP);
		formOrderList.add(EDSURL);
		formOrderList.add(EDSDOMAIN);
		formOrderList.add(SERVER);
		
		formOrderList.add(APPLICATION);
		formOrderList.add(DATABASE);
		formOrderList.add(USER);
		formOrderList.add(PASSWORD);
	//	formOrderList.add(CONNECTION_STRING);
		
		formOrderList.add(META_DATA_SERVICE_PROVIDER);
		formOrderList.add(DATA_SERVICE_PROVIDER);		
			
		requiredFieldSet.add(NAME);
		requiredFieldSet.add(EDSURL);
		requiredFieldSet.add(EDSDOMAIN);
		requiredFieldSet.add(SERVER);
		requiredFieldSet.add(APPLICATION);
		requiredFieldSet.add(DATABASE);
		requiredFieldSet.add(USER);
		requiredFieldSet.add(PASSWORD);
		requiredFieldSet.add(META_DATA_SERVICE_PROVIDER);
	   //requiredFieldSet.add(TOOL_TIP);
		requiredFieldSet.add(DATA_SERVICE_PROVIDER);
	//	requiredFieldSet.add(CONNECTION_STRING);
		
		captionMap.put(NAME, "Datasource ID");
		captionMap.put(EDSURL, "EDSURL");
		captionMap.put(EDSDOMAIN, "EDSDOMAIN");
		captionMap.put(SERVER, "SERVER");
		captionMap.put(USER, "USER");
		captionMap.put(PASSWORD, "PASSWORD");
		captionMap.put(APPLICATION, "APPLICATION");
		captionMap.put(DATABASE, "DATABASE");
		captionMap.put(META_DATA_SERVICE_PROVIDER, "Meta Data Service Provider");
		captionMap.put(TOOL_TIP, "");
		captionMap.put(DATA_SERVICE_PROVIDER, "Data Service Provider");
	//	captionMap.put(CONNECTION_STRING, "Connection String");
		
			
		metaDataServiceProviderComboBox.setNewItemsAllowed(false);
		metaDataServiceProviderComboBox.setNullSelectionAllowed(false);
		metaDataServiceProviderComboBox.addItem("com.pace.mdb.essbase.EsbMetaData");
		
		dataServiceProviderComboBox.setNewItemsAllowed(false);
		dataServiceProviderComboBox.setNullSelectionAllowed(false);
		dataServiceProviderComboBox.addItem("com.pace.mdb.essbase.EsbData");
		
	
		String tab = "&nbsp;&nbsp;&nbsp;&nbsp;";
		
		//TTN-2133:	Update MDB Settings pop-up help window in AC
	/*	StringBuilder connectionStringTooltipStringBuilder = new StringBuilder("==================================================<p>");
		connectionStringTooltipStringBuilder.append("Multidimensional DB Datasource Connection String Reference<p>");
		connectionStringTooltipStringBuilder.append("==================================================<p><p>");
		connectionStringTooltipStringBuilder.append("Default Connection String:<p>");
		connectionStringTooltipStringBuilder.append(tab + "EDSDomain=Essbase;EDSUrl=Embedded;Server=localhost;User=admin;Password=password;Application=Titan;Database=Titan<p><P>");
		connectionStringTooltipStringBuilder.append("APS 11.1.2.2 and above:<p>");
		connectionStringTooltipStringBuilder.append(tab + "EDSUrl=Embedded<p>");
		connectionStringTooltipStringBuilder.append(tab + tab +"EDSUrl=http://localhost:9000/aps/JAPI<p><p>");
		connectionStringTooltipStringBuilder.append("APS 11.1.x thru 11.1.2.1:<p>");
		connectionStringTooltipStringBuilder.append(tab + "EDSUrl=Embedded<p>");
		connectionStringTooltipStringBuilder.append(tab + tab +"EDSUrl=http://localhost:13080/aps/JAPI<p><p>");
		connectionStringTooltipStringBuilder.append("APS APS 9.3.x:<p>");
		connectionStringTooltipStringBuilder.append(tab + "EDSUrl=http://localhost:13080/aps/JAPI<p><p>"); */
		
		StringBuilder connectionStringTooltipStringBuilder = new StringBuilder("==================================================<p>");
		connectionStringTooltipStringBuilder.append("Multidimensional DB Datasource Connection String Reference<p>");
		connectionStringTooltipStringBuilder.append("==================================================<p><p>");
		connectionStringTooltipStringBuilder.append("Essbase Connection String:<p>");
		connectionStringTooltipStringBuilder.append(tab + "EDSDomain=Essbase;EDSUrl=Embedded;Server=localhost;User=admin;Password=password;Application=Titan;Database=Titan<p><P>");
		connectionStringTooltipStringBuilder.append("APS 11.1.2.2 and above:<p>");
		connectionStringTooltipStringBuilder.append(tab + "EDSUrl=Embedded<p>");
		connectionStringTooltipStringBuilder.append(tab + tab +"EDSUrl=http://localhost:9000/aps/JAPI<p><p>");
		connectionStringTooltipStringBuilder.append("APS 11.1.x thru 11.1.2.1:<p>");
		connectionStringTooltipStringBuilder.append(tab + "EDSUrl=Embedded<p>");
		connectionStringTooltipStringBuilder.append(tab + tab +"EDSUrl=http://localhost:13080/aps/JAPI<p><p>");
		connectionStringTooltipStringBuilder.append("APS APS 9.3.x:<p>");
		connectionStringTooltipStringBuilder.append(tab + "EDSUrl=http://localhost:13080/aps/JAPI<p><p>");
		connectionStringTooltip = connectionStringTooltipStringBuilder.toString();
		
	}
	
	
	public Field createField(Item item, Object propertyId, Component uiContext) {
		
		Field field = super.createField(item, propertyId,uiContext);
	
		if ( propertyId.equals(CONNECTION_STRING)) {
			
			field.setHeight("3em");
			field.setWidth("95%");
			
			TextField tf = (TextField) field;
			
			tf.setInputPrompt(CONNECTION_STRING_INPUT_PROMPT);
			
		} else if ( propertyId.equals(META_DATA_SERVICE_PROVIDER) ) {
			
			metaDataServiceProviderComboBox.setCaption(field.getCaption());
			metaDataServiceProviderComboBox.setRequired(requiredFieldSet.contains(propertyId));
			metaDataServiceProviderComboBox.setWidth(PaceSettingsConstants.COMMON_FIELD_WIDTH_25_EM);
			
			return metaDataServiceProviderComboBox;
			
		} else if ( propertyId.equals(DATA_SERVICE_PROVIDER) ) {
			
			dataServiceProviderComboBox.setCaption(field.getCaption());
			dataServiceProviderComboBox.setRequired(requiredFieldSet.contains(propertyId));
			dataServiceProviderComboBox.setWidth(PaceSettingsConstants.COMMON_FIELD_WIDTH_25_EM);
			
			return dataServiceProviderComboBox;
			
		} else if (propertyId.equals(TOOL_TIP))	{
		
			TextField tf = (TextField) field;
			tf.setReadOnly(true);
			tf.setIcon(new ThemeResource("icons/32/questionmark1.png"));
			tf.setValue("");
		    tf.setData("");
		    ((TextField) tf).setNullRepresentation("");
			tf.setDescription(getConnectionStringTooltip());
			return tf;
			
		// mask the password textbox
		}else if (propertyId.equals(PASSWORD))	{
		
			PasswordField credField = new PasswordField();
			credField.setCaption("Password");
			credField.setData(item);
			((PasswordField) credField).setNullRepresentation("");
			((PasswordField) credField).setWidth(PaceSettingsConstants.COMMON_FIELD_WIDTH_20_EM);
			credField.setRequired(true);
			return credField;
		
		}
		
		field.setRequired(true);
		return field;
	}

	


}
