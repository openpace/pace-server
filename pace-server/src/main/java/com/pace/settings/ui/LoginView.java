/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import com.pace.base.AuthMode;
import com.pace.base.server.PafMetaData;
import com.pace.base.server.ServerSettings;
import com.pace.settings.PaceSettingsApplication;
import com.pace.settings.PaceSettingsConstants;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * Login View
 * 
 * @author JMilliron
 * 
 */
public class LoginView extends Panel {

	private static final String _5EM = "5em";

	private static final long serialVersionUID = 5696629523350276705L;

	private TextField username = new TextField();

	private PasswordField password = new PasswordField();
	
	private Button loginButton = new Button("Login");
	
	private CheckBox checkBox = new CheckBox();
	
	private static final String Checkbox_Label_Width = "11em";
	
	// TTN-1718 - storing LDAP properties
	private String lDAPUserName = "";
	private String lDAPDomainName = "";
	private String sid = "";
	private String serverSessionUserName = "";
	private String previousUserName = "";

	/**
	 * Constructor 
	 * 
	 * @param app used to add click listener
	 */
	public LoginView(PaceSettingsApplication app) {

		VerticalLayout mainLayout = new VerticalLayout();

		Label loginLabel = new Label("Login");

		loginLabel.setStyleName(Reindeer.LABEL_H2);

		mainLayout.addComponent(loginLabel);

		HorizontalLayout userNameHL = new HorizontalLayout();

		Label userNameLabel = new Label(PaceSettingsConstants.USERNAME_BUTTON_LABEL);
		
		userNameLabel.setWidth(_5EM);
		
		userNameHL.addComponent(userNameLabel);

		userNameHL.addComponent(username);
			
		userNameHL.setSpacing(true);
		
		mainLayout.addComponent(userNameHL);
		
		HorizontalLayout passwordHL = new HorizontalLayout();
		
		Label passwordLabel = new Label(PaceSettingsConstants.PASSWORD_BUTTON_LABEL);
		
		passwordLabel.setWidth(_5EM);
		
		passwordHL.addComponent(passwordLabel);
		
		passwordHL.addComponent(password);
		
		passwordHL.setSpacing(true);
		
		mainLayout.addComponent(passwordHL);	
		
		/**
		 * TTN-1718 - Only display different user check box if sever settings is in mixed mode
		 */
		if(PafMetaData.getServerSettings().getAuthMode().equalsIgnoreCase(AuthMode.mixedMode.toString())) {
			createDifferentUserCheckBox(mainLayout);
			
		}
		
		mainLayout.addComponent(loginButton);
		
		loginButton.setClickShortcut(KeyCode.ENTER);
		loginButton.addListener((ClickListener) app);
		
		mainLayout.setSpacing(true);
		
		mainLayout.setSizeFull();

		setContent(mainLayout);
		
	//	setSizeFull();

	}

	/**
	 * @return the loginButton
	 */
	public Button getLoginButton() {
		return loginButton;
	}

	/**
	 * @return the username
	 */
	public TextField getUsername() {
		return username;
	}
	
	/**
	 * @param username the username to set
	 */
	public void setUsernameText(String username) {
		this.username.setValue(username); 
	}

	/**
	 * @return the password
	 */
	public PasswordField getPassword() {
		return password;
	}
	
	public CheckBox getCheckBox() {
		return this.checkBox;
	}
	
	/**
	 * @return the lDAPUserName
	 */
	public String getLDAPUserName() {
		return lDAPUserName;
	}

	/**
	 * @param lDAPUserName the lDAPUserName to set
	 */
	public void setLDAPUserName(String lDAPUserName) {
		this.lDAPUserName = lDAPUserName;
	}
	
	private void createDifferentUserCheckBox(VerticalLayout layout) {
		
		HorizontalLayout differentUserHL = new HorizontalLayout();
		
		checkBox.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				
				boolean allowDifferentLogin = checkBox.getValue(); 
				
				// populate LDAP user name values 
				if (allowDifferentLogin) {
					if (!getPreviousUserName().isEmpty()) {
						username.setValue(getPreviousUserName());
						password.setValue("");
					}
					username.setEnabled(true);
					password.setEnabled(true);
				} else {
					String userNameText = lDAPDomainName + "\\" + lDAPUserName; 
					username.setValue(userNameText);
					password.setValue("");
					username.setEnabled(false);
					password.setEnabled(false);
				}
				
			}
		});
		
		differentUserHL.addComponent(checkBox);
		
		Label differentUserLabel = new Label(PaceSettingsConstants.CHECK_BOX_LABEL);
	
		differentUserLabel.setWidth(Checkbox_Label_Width);
	
		differentUserHL.addComponent(differentUserLabel);
	
		differentUserHL.setSpacing(false);
	
		layout.addComponent(differentUserHL);
	
	}
	
	/**
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}

	/**
	 * @param sid the sid to set
	 */
	public void setSid(String sid) {
		this.sid = sid;
	}
	
	/**
	 * @return the lDAPUserName
	 */
	public String getlDAPUserName() {
		return lDAPUserName;
	}

	/**
	 * @param lDAPUserName the lDAPUserName to set
	 */
	public void setlDAPUserName(String lDAPUserName) {
		this.lDAPUserName = lDAPUserName;
	}
	
	/**
	 * @return the lDAPDomainName
	 */
	public String getlDAPDomainName() {
		return lDAPDomainName;
	}
	
	/**
	 * @param lDAPDomainName the lDAPDomainName to set
	 */
	public void setlDAPDomainName(String lDAPDomainName) {
		this.lDAPDomainName = lDAPDomainName;
	}
	
	public void setLDAPProperties(String userName, String domainName) {
		String userNameText = domainName + "\\" + userName; 
		setLDAPUserName(userNameText);
	}
	
	/**
	 * @return the previousUserName
	 */
	public String getPreviousUserName() {
		return previousUserName;
	}

	/**
	 * @param previousUserName the previousUserName to set
	 */
	public void setPreviousUserName(String previousUserName) {
		this.previousUserName = previousUserName;
	}
}
