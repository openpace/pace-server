/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
/**
 * 
 */
package com.pace.settings.ui;

import java.util.ArrayList;
import java.util.List;

import com.pace.settings.PaceSettingsApplication;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Table;

/**
 * Relational Datasource Table
 * 
 * @author JMilliron
 *
 */
public class RDBDatasourceTable extends Table {

	private static final long serialVersionUID = -8136604842128189919L;
	
	
	public RDBDatasourceTable(PaceSettingsApplication paceSettingsApplication) {
		
		//this.paceSettingsApplication = paceSettingsApplication;
		
		setSizeFull();
		
		setContainerDataSource(paceSettingsApplication.getRdbDatasourceContainer());
		setNewItemsAllowed(false);
		setPageLength(paceSettingsApplication.getRdbDatasourceContainer().size());
        setVisibleColumns(paceSettingsApplication.getRdbDatasourceFieldFactory().getFormOrderList().toArray());
        
        List<String> captionHeaderList = new ArrayList<String>();
        
        for (Object formOrder : paceSettingsApplication.getRdbDatasourceFieldFactory().getFormOrderList()) {
        	
        	String caption = formOrder.toString();
        	
        	if ( paceSettingsApplication.getRdbDatasourceFieldFactory().getCaptionMap().containsKey(caption)) {
        		
        		caption = paceSettingsApplication.getRdbDatasourceFieldFactory().getCaptionMap().get(caption);
        	}
        	
        	captionHeaderList.add(caption);
        	
        }
        
        setColumnHeaders(captionHeaderList.toArray(new String[0]));
		
        setSelectable(true);
        setImmediate(true);

        addItemClickListener((ItemClickListener) paceSettingsApplication);
        
        setNullSelectionAllowed(false);
               
        setColumnExpandRatio(RDBDatasourceFieldFactory.NAME, 10);
        setColumnExpandRatio(RDBDatasourceFieldFactory.HIBERNATE_PROPERTY_LIST, 90);
        
	}

	

}