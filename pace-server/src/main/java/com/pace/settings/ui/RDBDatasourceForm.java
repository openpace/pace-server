/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import com.pace.settings.PaceSettingsApplication;
import com.pace.settings.PaceSettingsConstants;
import com.vaadin.data.Item;

/**
 * RDB Datasources form
 * 
 * @author JMilliron
 *
 */
// Extending the Form class
public class RDBDatasourceForm extends PaceSettingsFormDB {

	private static final long serialVersionUID = -6315525313032264430L;
		
	/**
	 * Constructor 
	 * 
	 * @param serverSettingsApplication application
	 */
	public RDBDatasourceForm(PaceSettingsApplication serverSettingsApplication) {
		super(serverSettingsApplication);
		getFooter().setVisible(false);
		save.setCaption(PaceSettingsConstants.UPDATE_BUTTON_LABEL);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void setupForm() {

		//setWriteThrough(false);
		
		setBuffered(true);
		
		setPaceSettingsDefaultFactory(serverSettingsApplication.getRdbDatasourceFieldFactory());
        
        setFormFieldFactory(getPaceSettingsDefaultFactory());
            
  		setImmediate(true);
  		  		
  		
	}

	@Override
	protected void saveForm() {
				
		
		serverSettingsApplication.getRdbDatasourceContainer().addItem(serverSettingsApplication.getRdbDatasourceFieldFactory().hibernatePropertiesTable.keyValueContainer.getItemIds());
		serverSettingsApplication.getRdbDatasourceContainer().saveData(this);
		
		setItemDataSource(null);
		
	}

	@Override
	protected void cancelForm() {

		setItemDataSource(null);
 	    
	}

	@Override
	protected void loadSettings() {
	}	
	
	/* (non-Javadoc)
	 * @see com.pace.settings.ui.PaceSettingsForm#setItemDataSource(com.vaadin.data.Item)
	 */
	@Override
	public void setItemDataSource(Item newDataSource) {
		
		if ( newDataSource != null ) {
			super.setItemDataSource(newDataSource, serverSettingsApplication.getRdbDatasourceFieldFactory().getFormOrderList());			
			getFooter().setVisible(true);
		
		} else {
			super.setItemDataSource(newDataSource);
			getFooter().setVisible(false);
		
		}
		
		
	}	
}
