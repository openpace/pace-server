/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import java.util.ArrayList;
import java.util.List;

import com.pace.settings.PaceSettingsApplication;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Table;

/**
 * @author JMilliron
 *
 */
public class MDBDatasourceTable extends Table {

	private PaceSettingsApplication paceSettingsApplication = null;

	private static final long serialVersionUID = 5420191120805247205L;
	
	public MDBDatasourceTable(PaceSettingsApplication paceSettingsApplication) {
		
		this.paceSettingsApplication = paceSettingsApplication;
		
		setSizeFull();
		
		setContainerDataSource(paceSettingsApplication.getMdbDatasourceContainer());
   //     setVisibleColumns(paceSettingsApplication.getMdbDatasourceFieldFactory().getFormOrderList().toArray());
		 setVisibleColumns(new Object[] {MDBDatasourceFieldFactory.NAME});
        setPageLength(paceSettingsApplication.getMdbDatasourceContainer().size());
        List<String> captionHeaderList = new ArrayList<String>();
        
  /*      for (Object formOrder : paceSettingsApplication.getMdbDatasourceFieldFactory().getFormOrderList()) {
        	
        	String caption = formOrder.toString();
        	
        	if ( paceSettingsApplication.getMdbDatasourceFieldFactory().getCaptionMap().containsKey(caption)) {
        		
        		caption = paceSettingsApplication.getMdbDatasourceFieldFactory().getCaptionMap().get(caption);
        	}
        	
        	captionHeaderList.add(caption);
        	
        }*/
    	captionHeaderList.add(paceSettingsApplication.getMdbDatasourceFieldFactory().getCaptionMap().get(MDBDatasourceFieldFactory.NAME));
        
        setColumnHeaders(captionHeaderList.toArray(new String[0]));
        
        this.setColumnAlignment("connectionToolTip", Table.ALIGN_RIGHT);
        this.setItemIcon("connectionToolTip",new ThemeResource("icons/32/questionmark1.png"));
		
        setSelectable(true);
        setImmediate(true);
        addListener((ItemClickListener) paceSettingsApplication);
        setNullSelectionAllowed(false);
     
        
        
        
  
   /*     setColumnExpandRatio(MDBDatasourceFieldFactory.NAME, 10);
        setColumnExpandRatio(MDBDatasourceFieldFactory.CONNECTION_STRING, 45);
       
        setColumnExpandRatio(MDBDatasourceFieldFactory.META_DATA_SERVICE_PROVIDER, 25);
        setColumnExpandRatio(MDBDatasourceFieldFactory.DATA_SERVICE_PROVIDER, 20);
     */   
      
        
	}
	
	public void resizeTable() {
		
		if ( getContainerDataSource() != null ) {
			
			setPageLength(getContainerDataSource().size());
			
		}
		
	}

	/* (non-Javadoc)
	 * @see com.vaadin.ui.Table#setPageLength(int)
	 */
	@Override
	public void setPageLength(int pageLength) {
	
		int maxSize = 8;
		
		if ( pageLength <= maxSize ) {
		
			super.setPageLength(pageLength);
			
		} else {
			
			super.setPageLength(maxSize);
			
		}
		
		
	}

	

}