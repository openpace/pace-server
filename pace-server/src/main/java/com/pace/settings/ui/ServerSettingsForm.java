/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.server.ServerSettings;
import com.pace.settings.PaceSettingsApplication;
import com.pace.settings.data.PaceSettingsDataService;
import com.vaadin.data.util.BeanItem;

/**
 * 
 * Server Settings form
 * 
 * @author JMilliron
 *
 */
public class ServerSettingsForm extends PaceSettingsFormDB {
		

	private static final long serialVersionUID = -1856367965196923345L;
	
	public ServerSettingsForm(PaceSettingsApplication serverSettingsApplication) {
    	super(serverSettingsApplication);    	    	                
    }
   
	@Override
	protected void setupForm() {

	//	setWriteThrough(false);
		
		setPaceSettingsDefaultFactory(new ServerSettingsFieldFactory());
        
        setFormFieldFactory(getPaceSettingsDefaultFactory());
        
        loadSettings();
      
  		setItemDataSource(new BeanItem<ServerSettings>((ServerSettings) inputObject));
    
  		setImmediate(true);
  		
	}

	@Override
	protected void saveForm() {

		
		PaceSettingsDataService.setServerSettings((ServerSettings) inputObject);
		
	}

	@Override
	protected void cancelForm() {

		loadSettings();
		
 	    setItemDataSource(new BeanItem<ServerSettings>((ServerSettings) inputObject));
		
	}

	@Override
	protected void loadSettings() {

		try {
        	
			inputObject = PaceSettingsDataService.getServerSettings();
					    	
		} catch (PafConfigFileNotFoundException e) {
			
			inputObject = new ServerSettings();
			
		}
		
	}	
	
}
