/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.pace.settings.PaceSettingsConstants;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import com.vaadin.data.fieldgroup.*;

/**
 * Abstract Pace Settings Default Field Factory
 * 
 * @author JMilliron
 * 
 */
public abstract class PaceSettingsDefaultFieldFactory extends DefaultFieldFactory {

	private static final long serialVersionUID = -1442191615737978281L;

	// form order
	protected List<Object> formOrderList = new ArrayList<Object>();

	// override caption map
	protected Map<String, String> captionMap = new HashMap<String, String>();

	// required field set
	protected Set<String> requiredFieldSet = new HashSet<String>();
	
	 public FieldGroup group = new FieldGroup();
	 public FormLayout layout = new FormLayout();
	 private List<Field> allFields;
	
	 public Field createField(Item item, Object propertyId) {

		//	Field field = super.createField(item, propertyId);
		 
		
		 Field field = new TextField();

			// add catpion
			if (captionMap.containsKey(propertyId)) {

				field.setCaption(captionMap.get(propertyId));

			}

			// add if required and set error message
			if (requiredFieldSet.contains(propertyId)) {

				// add required field
				field.setRequired(true);
				field.setRequiredError(field.getCaption() + " is a required field.");

			}
			
			// if text field, change nulls to empty strings and set the common width
			if (field instanceof TextField) {

				((TextField) field).setNullRepresentation("");
				((TextField) field)
						.setWidth(PaceSettingsConstants.COMMON_FIELD_WIDTH_20_EM);

			}
			 getAllFields().add(field);
			
			return field;
		}
	
	/**
	 * @return the formOrderList
	 */
	public List<Object> getFormOrderList() {

		return new ArrayList<Object>(formOrderList);

	}

	/**
	 * @return the captionMap
	 */
	public Map<String, String> getCaptionMap() {
		return new HashMap<String, String>(captionMap);
	}

	/**
	 * @return the requiredFieldSet
	 */
	public Set<String> getRequiredFieldSet() {
		return new HashSet<String>(requiredFieldSet);
	}
	
	/**
	 * @return the allFields
	 */
	public List<Field> getAllFields() {
		
		if ( allFields == null ) {
			allFields = new LinkedList<Field>();
		}
		return allFields;
	}
	
}
