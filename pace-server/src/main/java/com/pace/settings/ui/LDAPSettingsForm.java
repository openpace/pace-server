/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import java.util.HashMap;
import java.util.Set;

import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.misc.KeyValue;
import com.pace.base.server.PafLDAPSettings;
import com.pace.settings.PaceSettingsApplication;
import com.pace.settings.PaceSettingsConstants;
import com.pace.settings.data.PaceSettingsDataService;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroupFieldFactory;
/**
 * LDAP Settings form
 * Class extends the FieldGroup class
 * @author JMilliron
 *
 */
public class LDAPSettingsForm extends PaceSettingsForm {

	private static final long serialVersionUID = -3760996903039086588L;
	VerticalLayout vl = new VerticalLayout();
	
	// Array of properties used to create an ordered BeanItem
	String[] properties ;
	
	/**
	 * Constructor 
	 * 
	 * @param serverSettingsApplication application
	 */
	public LDAPSettingsForm(PaceSettingsApplication serverSettingsApplication) {
		super(serverSettingsApplication);
	}
	
	@Override
	protected void setupForm() {

	//	setWriteThrough(false);
		
		// Properties array required to add captions to the field group fields.
	  properties = new String[]{"ldapProviderName","securityPrincipal","securityCredentials","providerURLGC","securityAuthentication","securityProtocol","searchBase","domainController","ldapPagesize","kerberosRealm","kerberosKDC","sslKeyStore","connectTimeout","version","ignoreBuiltInGroups","ignoreExpiredAccounts","ignoreLockedOutAccounts","ignoreExpiredPWAccounts","ignoreDisabledAccounts","netBiosNames","netBiosNamesList"};
	  
		 this.setFieldFactory(new FieldGroupFieldFactory() {
	            private static final long serialVersionUID = -7503214153547969773L;

				@Override
				public <T extends Field> T createField(Class<?> dataType,
						Class<T> fieldType) {
					 TextField field = new TextField();
		             
					  // create field group fields.
		              
		                if (Integer.class == dataType)
		                    field.setConverter(dataType);
		                
		              
		                if (dataType.isAssignableFrom(java.util.Map.class) && fieldType.isAssignableFrom(com.pace.settings.ui.KeyValueTable.class))
		                {   
		                	
		                	return fieldType.cast(field);
		                }
		                if (dataType.isAssignableFrom(java.util.ArrayList.class) && fieldType.isAssignableFrom(com.pace.settings.ui.KeyValueTable.class))
		                {   
		                	
		             		KeyValueTable netBiosNameTable = new KeyValueTable("Net Bios Key", "Net Bios Value");
		                
		                	return (T) netBiosNameTable;
		                }
		                if(dataType.isAssignableFrom(java.lang.Boolean.class))
		                {
		                	
		                	CheckBox cBox = new CheckBox();
		                	return (T) cBox;
		                }
		              return fieldType.cast(field);
				}

	           
	        });
		
        
        loadSettings();
      
   	 this.setItemDataSource(new BeanItem<PafLDAPSettings>((PafLDAPSettings) inputObject, properties));
  		
  	// Loop through the properties, build fields for them and add the fields
    // to this UI
   	 
        for (Object propertyId : this.getUnboundPropertyIds()) {
        	
			int a;
			if(propertyId.equals("netBiosNames"))
				a = 5;
        	else if(propertyId.equals("netBiosNamesList"))
        	{
        	   form.addComponent(this.buildAndBind("netBiosNamesList", propertyId, KeyValueTable.class));
        	
        	}
        	else if(propertyId.equals("securityCredentials"))
        	{
        		Field fld = this.build("Security Credentials", java.lang.String.class, com.vaadin.ui.Field.class);
        		 fld =	new PasswordField();
        		if(fld.getValue().equals("")){
        			// Bind issue with empty fields
        			fld.setValue(" ");
        			
        		}
        		
        		fld.setCaption("Security Credentials");
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	
        	}
        	else if(propertyId.equals("securityPrincipal"))
        	{
        		Field fld = this.build("Security Principal", java.lang.String.class, com.vaadin.ui.Field.class);
        		fld.setRequired(true);
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	
        	else if(propertyId.equals("ignoreExpiredPWAccounts"))
        	{
        		Field fld = this.build("Ignore Expired Password Accounts", java.lang.Boolean.class, com.vaadin.ui.Field.class);
        		
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("ldapProviderName"))
        	{
        		Field fld = this.build("LDAP Provider Name", java.lang.String.class, com.vaadin.ui.Field.class);
        		fld.setRequired(true);
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("providerURLGC"))
        	{
        		Field fld = this.build("Provider URL GC", java.lang.String.class, com.vaadin.ui.Field.class);
        		fld.setRequired(true);
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("securityProtocol"))
        	{
        		Field fld = this.build("Security Protocol", java.lang.String.class, com.vaadin.ui.Field.class);
        		fld.setRequired(true);
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("securityAuthentication"))
        	{
        		Field fld = this.build("Security Authentication", java.lang.String.class, com.vaadin.ui.Field.class);
        		fld.setRequired(true);
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("domainController"))
        	{
        		Field fld = this.build("Domain Controller", java.lang.String.class, com.vaadin.ui.Field.class);
        		fld.setRequired(true);
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("ldapPagesize"))
        	{
        		Field fld = this.build("LDAP Pagesize", java.lang.String.class, com.vaadin.ui.Field.class);
        		fld.setRequired(true);
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("kerberosKDC"))
        	{
        		Field fld = this.build("Kerberos KDC", java.lang.String.class, com.vaadin.ui.Field.class);
        		
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("sslKeyStore"))
        	{
        		Field fld = this.build("SSL Key Store", java.lang.String.class, com.vaadin.ui.Field.class);
        		fld.setRequired(true);
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("connectTimeout"))
        	{
        		Field fld = this.build("Connect Timeout", java.lang.String.class, com.vaadin.ui.Field.class);
        		fld.setRequired(true);
        		this.bind(fld, propertyId);
        		form.addComponent(fld);
        	}
        	else if(propertyId.equals("version"))
        	{
        		Field fld = this.build("Version", java.lang.String.class, com.vaadin.ui.Field.class);
        		
        	//	this.bind(fld, propertyId);
        	//	form.addComponent(fld);
        	}
        	else
        		form.addComponent(this.buildAndBind(propertyId));
        }
        
       // bind the newly created fields to the object. This completes the build and bind process for FieldGroup.
        bindMemberFields(inputObject);
  		
   
      //  setBuffered(false);
  		
	}

	@Override
	protected void saveForm() {
		try {
			this.commit();
		} catch (CommitException e) {
			// TODO Auto-generated catch block
			serverSettingsApplication.showNotification("Error:"+e.getMessage());
		}
		
		setItemDataSource(new BeanItem<PafLDAPSettings>((PafLDAPSettings) inputObject));
		
		PaceSettingsDataService.setLDAPSettings((PafLDAPSettings) inputObject);
		
		
	}

	@Override
	protected void cancelForm() {

		loadSettings();
		
 	    setItemDataSource(new BeanItem<PafLDAPSettings>((PafLDAPSettings) inputObject));
 	    
 	    //fixes scroll issue
 	    serverSettingsApplication.showBlankView();
 	    serverSettingsApplication.showLDAPSettingsView();
 	    
	}

	@Override
	protected void loadSettings() {

		try {
        	
			inputObject = PaceSettingsDataService.getLDAPSettings();
					    	
		} catch (PafConfigFileNotFoundException e) {
			
			inputObject = new PafLDAPSettings();
			
		}
		
	}
	

}
