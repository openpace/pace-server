/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.settings.ui;

import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;

/**
 * RDB Datasource Field Factory
 * 
 * @author JMilliron
 *
 */
public class RDBDatasourceFieldFactory extends PaceSettingsDefaultFieldFactory {

	
	private static final long serialVersionUID = 2885762876258153110L;
	
	public static final String NAME = "name";
	public static final String HIBERNATE_PROPERTY_LIST = "hibernatePropertyList";
		
	public KeyValueTable hibernatePropertiesTable = new KeyValueTable("Property Key", "Property Value");
	
	public RDBDatasourceFieldFactory() {
		
		formOrderList.add(NAME);
		formOrderList.add(HIBERNATE_PROPERTY_LIST);
		
		captionMap.put(NAME, "Database Name");
		captionMap.put(HIBERNATE_PROPERTY_LIST, "Properties");
		
		
			
	}
	
	@Override
	public Field createField(Item item, Object propertyId, Component uiContext) {
		
		Field field = super.createField(item, propertyId, uiContext);
	
		if ( propertyId.equals(NAME)) {
			
		/*	field.setHeight("3em");
			field.setWidth("95%");*/
				
			field.setCaption(captionMap.get(NAME));
			field.setReadOnly(true);
			
		} else if ( propertyId.equals(HIBERNATE_PROPERTY_LIST)) {
			
			
			hibernatePropertiesTable.setCaption(captionMap.get(HIBERNATE_PROPERTY_LIST));
			
			return hibernatePropertiesTable;
			
		}
		
		
		return field;
	}

}
