/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.ui;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.themes.Reindeer;

/**
 * Server Settings View. Used to hold label and form. 
 * 
 * @author JMilliron
 *
 */
public class ServerSettingsView extends Panel {

	private static final long serialVersionUID = 4697527910145885915L;

	/**
	 * Constructor
	 * 
	 * @param form form to use in view.
	 */
	public ServerSettingsView(ServerSettingsForm form) {
		
		Label heading = new Label(SettingsTree.SERVER_SETTINGS.toString());
		
		heading.setStyleName(Reindeer.LABEL_H2);
		
		setContent(heading);
		
		setContent(form);
		
		setSizeFull();
		
	}
	
}
