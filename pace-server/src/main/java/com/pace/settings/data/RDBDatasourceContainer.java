/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.settings.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.PafKeystoreEncryptionException;
import com.pace.base.db.RdbProps;
import com.pace.settings.ui.RDBDatasourceForm;
import com.vaadin.data.util.BeanItemContainer;

/**
 * Relational Datasource Container.
 * 
 * @author JMilliron
 *
 */
public class RDBDatasourceContainer extends BeanItemContainer<RdbProps>{
	
	private static final long serialVersionUID = -2992769974346112813L;
	
	private static final Logger logger = Logger.getLogger(RDBDatasourceContainer.class);

	public RDBDatasourceContainer() throws IllegalArgumentException {
		super(RdbProps.class);
		readData();
		
	}
		
	/**
	 * Saves data
	 * 
	 * @return true if successful
	 */
	public boolean saveData(RDBDatasourceForm form) {
		
		boolean isSaved = false;
		
		List<RdbProps> rdbPropsList = new ArrayList<RdbProps>();
		
		
		for ( RdbProps rdbProps : getAllItemIds()) {
			
			rdbPropsList.add(rdbProps);
			
		}
				
		try {
			PaceSettingsDataService.setRDBDatasources(rdbPropsList);
		} catch (PafKeystoreEncryptionException e) {
			logger.info(e);
		}
		
		isSaved = true;
		
		return isSaved;
		
	}
	
	/**
	 * Reads data
	 */
	public void readData() {
		
		removeAllItems();
		
		List<RdbProps> data = null;
		
		try {
			
			data = PaceSettingsDataService.getRDBDatasources();

		} catch (PafConfigFileNotFoundException e) {
			logger.info(e);
		} catch (PafKeystoreDecryptionException e) {
			logger.info(e);
		}
		
		if ( data == null ) {
			
			data = new ArrayList<RdbProps>();
			
		}
		
		addAll(data);
	}


	
}
