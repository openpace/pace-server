/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.settings.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.PafKeystoreEncryptionException;
import com.pace.base.db.RdbProps;
import com.pace.base.mdb.PafConnectionProps;
import com.pace.base.mdb.PafMdbConnectionProps;
import com.pace.base.misc.KeyValue;
import com.pace.base.server.PafLDAPSettings;
import com.pace.base.server.ServerSettings;
import com.pace.base.utility.PafXStream;
import com.pace.server.PafServerConstants;

/**
 * Pace Settings Data Service.  Wraps XStream functionality.  Read/writes setting file to/from disk.
 * 
 * @author JMilliron
 *
 */
public class PaceSettingsDataService {

	/**
	 * @return the serverSettingsFile
	 */
	
	public static String getServerSettingsFile() {
		return PafServerConstants.SERVER_SETTINGS_FILE;
	}

	public static ServerSettings getServerSettings() throws PafConfigFileNotFoundException {
		
		return (ServerSettings) PafXStream.importObjectFromXml(PafServerConstants.SERVER_SETTINGS_FILE);
		
	}
	
	public static void setServerSettings(ServerSettings ss) {
				
		if ( ss != null ) {
			
			PafXStream.exportObjectToXml(ss, PafServerConstants.SERVER_SETTINGS_FILE);
			
		}
		
	}
	
	public static PafLDAPSettings getLDAPSettings() throws PafConfigFileNotFoundException {
		
		PafLDAPSettings ldapSettings = (PafLDAPSettings) PafXStream.importObjectFromXml(PafServerConstants.LDAP_SETTINGS_FILE);
		
		if ( ldapSettings != null && ldapSettings.getNetBiosNames() != null ) {
			
			List<KeyValue> keyValueList = new ArrayList<KeyValue>();
			
			for (String key : ldapSettings.getNetBiosNames().keySet()) {
											
				keyValueList.add(new KeyValue(key, ldapSettings.getNetBiosNames().get(key)));
				
			}
		
			ldapSettings.setNetBiosNamesList(keyValueList);
			
		}
				
		return ldapSettings;
		
	}
	
	public static void setLDAPSettings(PafLDAPSettings ldapSettings) {
		
		if ( ldapSettings != null ) {

			if ( ldapSettings.getNetBiosNamesList() != null && ldapSettings.getNetBiosNamesList().size() > 0 ) {
				
				Map<String, String> netBiosNameMap = new HashMap<String, String>();
				
				for (KeyValue kv : ldapSettings.getNetBiosNamesList() ) {
					
					netBiosNameMap.put(kv.getKey(), kv.getValue());
					
					
				}
				
				ldapSettings.setNetBiosNames(netBiosNameMap);
				ldapSettings.setNetBiosNamesList(null);
				
			} else {
				
				ldapSettings.setNetBiosNames(null);
			}
			
			PafXStream.exportObjectToXml(ldapSettings, PafServerConstants.LDAP_SETTINGS_FILE);
		
		}
		
	}
	
	/**
	 * MDB Datasources file
	 * 
	 * @param mdbDataSources
	 * @throws PafKeystoreEncryptionException 
	 */
	public static void setMDBDatasources(List<PafMdbConnectionProps> mdbDataSources) throws PafKeystoreEncryptionException {
		
		if ( mdbDataSources != null ) {

			if ( mdbDataSources != null && mdbDataSources.size() > 0 ) {
				//Convert KeyValueList to String

				for (int k=0;k< mdbDataSources.size();k++) {
					PafMdbConnectionProps conProp = mdbDataSources.get(k);

					
					//Convert the key value pairs to a connection String
					if( conProp != null ) {

						String conStr = "";
						if(conProp.getEdsurl()!=null)
							conStr = conStr.concat("EDSURL="+conProp.getEdsurl()+ ";");
						if(conProp.getEdsdomain()!=null)
							conStr = conStr.concat("EDSDOMAIN="+conProp.getEdsdomain()+ ";");
						if(conProp.getServer()!=null)
							conStr = conStr.concat("SERVER="+conProp.getServer()+ ";");
						if(conProp.getUser()!=null)
							conStr = conStr.concat("USER="+conProp.getUser()+ ";");
						if(conProp.getPassword()!=null)
							conStr = conStr.concat("PASSWORD="+conProp.getPassword()+ ";");
						if(conProp.getApplication()!=null)
							conStr = conStr.concat("APPLICATION="+conProp.getApplication()+ ";");
						if(conProp.getDatabase()!=null)
							conStr = conStr.concat("DATABASE="+conProp.getDatabase());
						
						conProp.setConnectionString(conStr);
						
						
						
					}
				}
			} 
									
			
			PafXStream.exportObjectToXml(mdbDataSources, PafServerConstants.MDB_DATASOURCES_FILE);
			
		}
		
	}
	
	/**
	 * Reads MDB Datasources file
	 * 
	 * @return List of mdb connection properties
	 * @throws PafConfigFileNotFoundException
	 * @throws PafKeystoreDecryptionException 
	 */
	public static List<PafMdbConnectionProps> getMDBDatasources() throws PafConfigFileNotFoundException {
		
		List<PafMdbConnectionProps> mdbDataSources = (List<PafMdbConnectionProps>) PafXStream.importObjectFromXml(PafServerConstants.MDB_DATASOURCES_FILE);

		

		if ( mdbDataSources != null && mdbDataSources.size() > 0 ) {


			//Convert String to Properties then to KeyValueList
			for (PafMdbConnectionProps conProp : mdbDataSources) {
			
				List<KeyValue> keyValueList = new ArrayList<KeyValue>();
				
				try{
					
				String conStr = conProp.decryptConnectionString();	
				
			
				Properties esbCon = conProp.getProperties();
				
				// set the properties to the object so they can be shown in separate text boxes on the UI.
				for( Object conKey: esbCon.keySet() ) {
					keyValueList.add(new KeyValue(conKey.toString(), esbCon.get(conKey).toString()));
					if(conKey.toString().equalsIgnoreCase("APPLICATION"))
						conProp.setApplication(esbCon.get(conKey).toString());
					if(conKey.toString().equalsIgnoreCase("DATABASE"))
						conProp.setDatabase(esbCon.get(conKey).toString());
					if(conKey.toString().equalsIgnoreCase("EDSURL"))
						conProp.setEdsurl(esbCon.get(conKey).toString());
					if(conKey.toString().equalsIgnoreCase("EDSDOMAIN"))
						conProp.setEdsdomain(esbCon.get(conKey).toString());
					if(conKey.toString().equalsIgnoreCase("SERVER"))
						conProp.setServer(esbCon.get(conKey).toString());
					if(conKey.toString().equalsIgnoreCase("USER"))
						conProp.setUser(esbCon.get(conKey).toString());
					if(conKey.toString().equalsIgnoreCase("PASSWORD"))
						conProp.setPassword(esbCon.get(conKey).toString());  
				}
				}catch(PafKeystoreDecryptionException e){
					// If password decryption fails due to keys being missing, then set the other properties.
					String connString = conProp.getConnectionString();
				
					try{
					Properties props = conProp.parseConnString(connString, true, false);
					for( Object conKey: props.keySet() ) {
						
						if(conKey.toString().equalsIgnoreCase("APPLICATION"))
							conProp.setApplication(props.get(conKey).toString());
						if(conKey.toString().equalsIgnoreCase("DATABASE"))
							conProp.setDatabase(props.get(conKey).toString());
						if(conKey.toString().equalsIgnoreCase("EDSURL"))
							conProp.setEdsurl(props.get(conKey).toString());
						if(conKey.toString().equalsIgnoreCase("EDSDOMAIN"))
							conProp.setEdsdomain(props.get(conKey).toString());
						if(conKey.toString().equalsIgnoreCase("SERVER"))
							conProp.setServer(props.get(conKey).toString());
						if(conKey.toString().equalsIgnoreCase("USER"))
							conProp.setUser(props.get(conKey).toString());
					}
					}
					catch(PafKeystoreDecryptionException ex){
						
					}
					
				}
			}

		}
		
		return mdbDataSources;
	}
	
	/**
	 * Writes RDB Datasources file
	 * 
	 * @param rdbDataSources
	 * @throws PafKeystoreEncryptionException 
	 */
	public static void setRDBDatasources(List<RdbProps> rdbDataSources) throws PafKeystoreEncryptionException {
		
		if ( rdbDataSources != null ) {
			
			for (RdbProps rdbProps : rdbDataSources ) {
				
				Properties props = new Properties();
				
				if ( rdbProps.getHibernatePropertyList() != null ) {
					
					for ( KeyValue kv : rdbProps.getHibernatePropertyList()) {
						
						props.put(kv.getKey(), kv.getValue());
						
					}
					
					//rdbProps.setHibernatePropertyList(null);
					rdbProps.setHibernateProperties(props);
					
				}
				
			}
									
			PafXStream.exportObjectToXml(rdbDataSources, PafServerConstants.RDB_DATASOURCES_FILE);
		}
		
	}
	
	/**
	 * Reads RDB Datasources file
	 * 
	 * @return List of rdb connection properties
	 * @throws PafConfigFileNotFoundException
	 * @throws PafKeystoreEncryptionException 
	 * @throws PafKeystoreDecryptionException 
	 */
	public static List<RdbProps> getRDBDatasources() throws PafConfigFileNotFoundException, PafKeystoreDecryptionException {
		
		List<RdbProps> rdbDatasourceList = (List<RdbProps>) PafXStream.importObjectFromXml(PafServerConstants.RDB_DATASOURCES_FILE);
		
		if ( rdbDatasourceList != null ) {
			
			for ( RdbProps rdbProps : rdbDatasourceList ) {
				
				if ( rdbProps != null && rdbProps.getHibernateProperties() != null ) {
					
					for ( Object objKey : rdbProps.getHibernateProperties().keySet()) {
						
						Object objValue = rdbProps.getHibernateProperties().get(objKey);
						
						rdbProps.getHibernatePropertyList().add(new KeyValue(objKey.toString(), objValue.toString()));
						
					}
					
										
				}
				
				
			}
			
			
		}
		
		return rdbDatasourceList;
		
	}
	
	
	public static void main(String[] args) {
		
		//ServerSettings ss = PafMetaData.getServerSettings();
		
		//Map<String, PafConnectionProps> mdbConnectionPropMap = PafMetaData.getMDBSettings();
	
		
		/*List<PafConnectionProps> list = new ArrayList<PafConnectionProps>();
		
		PafConnectionProps prop1 = new PafConnectionProps();
		prop1.setName("Titan");
		prop1.setConnectionString("EDSDomain=Essbase;EDSUrl=http://localhost:13080/aps/JAPI;Server=localhost;User=admin;Password=password;Application=Titan;Database=Titan");
		prop1.setDataServiceProvider("com.pace.mdb.essbase.EsbData");
		prop1.setMetaDataServiceProvider("com.pace.mdb.essbase.EsbMetaData");
		prop1.setProperties(null);
		
		list.add(prop1);
		
		PafXStream.exportObjectToXml(list, mdbDataSourcesFile);
		*/
		//setServerSettings(ss);
		
		
		//PafXStream.exportObjectToXml(ss.getLdapSettings(), ldapSettingsFile);
		
		//setRDBDatasources(PafMetaData.getRDBProps());
		
	}
	
	
}
