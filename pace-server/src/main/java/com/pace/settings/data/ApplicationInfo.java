/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.settings.data;

import com.pace.base.RunningState;

public class ApplicationInfo {
	
	private String appId;
	private String datasourceId;
	private RunningState appState;
	
	/**
	 * 
	 * @param pafAppDef
	 * @return
	 */
	public ApplicationInfo(String appId, String datasourceId, RunningState appState) {
		this.appId = appId;
		this.datasourceId = datasourceId;
		this.appState = appState;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the datasourceId
	 */
	public String getDatasourceId() {
		return datasourceId;
	}

	/**
	 * @param datasourceId the datasourceId to set
	 */
	public void setDatasourceId(String datasourceId) {
		this.datasourceId = datasourceId;
	}

	/**
	 * @return the appState
	 */
	public RunningState getAppState() {
		return appState;
	}

	/**
	 * @param appState the appState to set
	 */
	public void setAppState(RunningState appState) {
		this.appState = appState;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ApplicationInfo [appId=" + appId + ", datasourceId="
				+ datasourceId + ", appState=" + appState + "]";
	}
	

}
