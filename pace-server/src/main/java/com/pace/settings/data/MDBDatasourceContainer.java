/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.settings.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.pace.base.PafConfigFileNotFoundException;
import com.pace.base.PafKeystoreDecryptionException;
import com.pace.base.PafKeystoreEncryptionException;
import com.pace.base.mdb.PafConnectionProps;
import com.pace.base.mdb.PafMdbConnectionProps;
import com.vaadin.data.util.BeanItemContainer;

/**
 * @author JMilliron
 *
 */
public class MDBDatasourceContainer extends BeanItemContainer<PafMdbConnectionProps> {

	private static final long serialVersionUID = -1411854235540811035L;
	
	private static final Logger logger = Logger.getLogger(MDBDatasourceContainer.class);
	
	/**
	 * Creates container
	 * 
	 * @throws IllegalArgumentException
	 */
	public MDBDatasourceContainer() throws IllegalArgumentException {
		super(PafMdbConnectionProps.class);
		readData();
		
	}
		
	public boolean saveData() {
		
		boolean isSaved = false;
		
		List<PafMdbConnectionProps> pafConnectionPropsList = new ArrayList<PafMdbConnectionProps>();
		
		for ( PafMdbConnectionProps pafConnectionProps : getAllItemIds()) {

			
			pafConnectionPropsList.add(pafConnectionProps);
			
		}
	
		
		try {
			PaceSettingsDataService.setMDBDatasources(pafConnectionPropsList);
		} catch (PafKeystoreEncryptionException e) {
			logger.info(e);
		}
		
		isSaved = true;
		
		return isSaved;
		
	}
	
	public void readData() {
		
		removeAllItems();
		
		List<PafMdbConnectionProps> data = null;
		
		try {
			
			data = PaceSettingsDataService.getMDBDatasources();
			
			

		} catch (PafConfigFileNotFoundException e) {
			logger.info(e);
		} 
		
		if ( data == null ) {
			
			data = new ArrayList<PafMdbConnectionProps>();
			
		}
		
		addAll(data);
	}

}
