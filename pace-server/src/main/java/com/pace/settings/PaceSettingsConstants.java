/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.settings;

/**
 * Constants class
 * 
 * @author JMilliron
 *
 */
public class PaceSettingsConstants {

	public static final String PACE_APP_HEADING = "Pace Server Administration Application";
	
	public static final String COMMON_BUTTON_WIDTH = "5em";
	public static final String COMMON_FIELD_WIDTH_20_EM = "20em";
	public static final String COMMON_FIELD_WIDTH_25_EM = "25em";
	public static final String COMMON_TABLE_WIDTH = "50em";
	
	//BUTTON LABELS
	public static final String UPDATE_BUTTON_LABEL = "Update";
	public static final String ADD_BUTTON_LABEL = "Add";
	public static final String CANCEL_BUTTON_LABEL = "Cancel";
	public static final String SAVE_BUTTON_LABEL = "Save";
	public static final String DELETE_BUTTON_LABEL = "Delete";
	public static final String COPY_BUTTON_LABEL = "Copy";
	public static final String NEW_BUTTON_LABEL = "New";	
	public static final String LOGOUT_BUTTON_LABEL = "Logout";
	public static final String USERNAME_BUTTON_LABEL = "Username";
	public static final String PASSWORD_BUTTON_LABEL = "Password";
	public static final String CHECK_BOX_LABEL = "Login as different user";
	
	//error messages
	public static final String INVALID_USERNAME_OR_PASSWORD = "Invalid " + USERNAME_BUTTON_LABEL + " or " + PASSWORD_BUTTON_LABEL;
	public static final String USERNAME_IS_A_REQUIRED_FIELD = USERNAME_BUTTON_LABEL + " is a required field";
	
}
