/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.eval;

import com.pace.base.data.Intersection;
import com.pace.base.rules.Formula;

/**
 * An item that requires evaluation, and all the pieces
 * necessary to perform that evaluation. This is used in
 * conjunction with a dataCache object. The intersection
 * represents a point in the datacache that needs to be evaluated
 * by the formula provided. The axis identifies which
 * axis of the intersection/dataCache the formula is to ba applied
 * along.
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class EvalItem {
    Formula formula = null;
    Intersection intersection = null;
    String axis = null;
    
    public EvalItem( Intersection intersect, Formula formula, String axis) {
        super();
        this.axis = axis;
        this.formula = formula;
        this.intersection = intersect;
    }
    public String getAxis() {
        return axis;
    }
    public void setAxis(String axis) {
        this.axis = axis;
    }
    public Formula getFormula() {
        return formula;
    }
    public void setFormula(Formula formula) {
        this.formula = formula;
    }
    public Intersection getIntersection() {
        return intersection;
    }
    public void setIntersection(Intersection intersect) {
        this.intersection = intersect;
    }

}
