/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.eval;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.pace.base.PafErrSeverity;
import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.app.MdbDef;
import com.pace.base.data.EvalUtil;
import com.pace.base.data.Intersection;
import com.pace.base.data.TimeSlice;
import com.pace.base.funcs.IPafFunction;
import com.pace.base.mdb.PafDataCache;
import com.pace.base.mdb.PafDimMember;
import com.pace.base.mdb.PafDimTree;
import com.pace.base.rules.Formula;
import com.pace.base.rules.Rule;
import com.pace.base.state.EvalState;
import com.pace.base.utility.LogUtil;
import com.pace.server.PafDataService;
import com.pace.server.RuleMngr;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class ES_EvalPepetualRulegroup extends ES_EvalBase implements IEvalStep {
	
	private static Logger logger = Logger.getLogger(ES_EvalPepetualRulegroup.class);
	PafDataService dataService = PafDataService.getInstance();
	
	public void performEvaluation(EvalState evalState) throws PafException, PafInvalidIntersectionException {
		
		long startTime = System.currentTimeMillis();
		long stepTime;
		PafDataCache dataCache = evalState.getDataCache();
		MdbDef mdbDef = evalState.getAppDef().getMdbDef();
        String measureDim = mdbDef.getMeasureDim();
        
		HashSet<Intersection> newChngCells = new HashSet<Intersection>(50000);
		HashMap<Intersection, Formula> cellsToCalc = new HashMap<Intersection, Formula>(50000);
		Intersection calcIntersection;

		IPafFunction measFunc = null;
        if (evalState.getRule().getFormula().isResultFunction())
                measFunc = evalState.getRule().getFormula().extractResultFunction();

		stepTime = System.currentTimeMillis();       
		Set<Intersection> chngSet = new HashSet<Intersection>(5000);
//        chngSet.addAll(evalState.getOrigLockedCells()); 
        
		Rule leadingRule;
		
		// if the rule currently being examined isn't a measure function, then only changes within the current time slice need to be
		// examined
		if (measFunc == null) {
			
			// if any lookback function is used on the right side of the equation then that time slice must also
			// be added to the pool
			
            chngSet = impactingChangeList(evalState.getRule(), evalState);
            
            
			for (Intersection is : chngSet) {
				// does this intersection change force the current rule to evaluate?
				// 1st criteria, it has to be a component of this rule.
//                if ( EvalUtil.changeTriggersFormula( is, evalState.getRule(), evalState) ) { // this function is currently off because the logic is refactored into impactingChangeList()
					// it will if it is the highest priority rule within this group that can execute
					// this is true if the left hand term for every other rule above this rule is
					// locked.
                	leadingRule = RuleMngr.findLeadingRule(evalState.getRuleGroup(), evalState, is);

					if (leadingRule != null && leadingRule.equals(evalState.getRule())) {
						calcIntersection = is.clone();
						calcIntersection.setCoordinate(measureDim, leadingRule.getFormula().getResultTerm());
//						calcIntersection.setCoordinate(timeDim, evalState.getCurrentTimeSlice());
						TimeSlice.applyTimeHorizonCoord(calcIntersection, evalState.getCurrentTimeSlice(), dataCache);		// TTN-1595
                        
                        // handle lockAllPrior tag.
                        // this tag indicates that when an intersection is calculated, all prior periods level 0
                        // intersections are locked. This preserves their value as as subsequent allocations are
                        // resolved.
                        
                        if ( evalState.getRule().isLockAllPriorTime() ) {
                            Set<Intersection> priorLocks = calculatePriorTimeIntersections(calcIntersection, evalState);
                            if (logger.isTraceEnabled()) {
                            	logger.trace("Locking prior time periods for Intersection: " + calcIntersection);
                            	logger.trace(Arrays.toString(priorLocks.toArray(new Intersection[0])));
                            }
                            evalState.getCurrentLockedCells().addAll(priorLocks);
                        }
                        

                        if ( // override locks set or not locked to begin with and not already specified as having a formula
                                (evalState.getRule().getEvalLockedIntersections() || !evalState.getCurrentLockedCells().contains(calcIntersection))
                                && !cellsToCalc.containsKey(calcIntersection)
                            ) {
							cellsToCalc.put(calcIntersection, leadingRule.getFormula());
						}
					}
//				}		
			}
		}
		//now we must consider changes in other locations depending on the rule we're processing
		else {
			// figure out which time slice to examine for changes
			// a little custom for the current next previous stuff...should be encapsulated better

			// FIXME This will need to work with default parameters like the function on the right hand
            // side of the equation
			
			PafDimMember offsetTimeMember = null;  
			
			if (measFunc.getOpCode().equalsIgnoreCase("@NEXT")) {       		
				offsetTimeMember = evalState.getTimeSubTree().getPrevSibling(evalState.getCurrentTimeMember(), false);
			}
			else if  (measFunc.getOpCode().equalsIgnoreCase("@PREV")){
				offsetTimeMember = evalState.getTimeSubTree().getNextSibling(evalState.getCurrentTimeMember(), false);        		
			}
            else {
                throw new PafException("Illegal function used on left side of formula expression. Only Next() and Prev() operations are allowed. [" + evalState.getRule().toString() + "]", PafErrSeverity.Error);
            }
			if (offsetTimeMember != null) {
				if (evalState.getChangedCellsByTime().get(offsetTimeMember.getKey()) != null)
					chngSet.addAll(evalState.getChangedCellsByTime().get(offsetTimeMember.getKey()));
			} 
			
			
//			chngCells.addAll(measFunc.getDirtyIntersections(evalState));
			
			for (Intersection is : chngSet) {
				// does this intersection change force the current rule to evaluate?
				// 1st criteria, it has to be a component of this rule.
                if ( EvalUtil.changeTriggersFormula( is, evalState.getRule(), evalState) ) {
					// it will if it is the highest priority rule within this group that can execute
					// this is true if the left hand term for every other rule above this rule is
					// locked.
					leadingRule = evalState.getRule();
					calcIntersection = EvalUtil.translocateIntersection(is, measFunc, evalState);
					// emergency exit, not sure I need this anymore
					// TODO Make time evaluation boundary check more generic
//					if ((calcIntersection != null) && (evalState.getClientState().getUowTrees().getTree(timeDim).hasMember(calcIntersection.getCoordinate(timeDim)) == true)) {
					if ((calcIntersection != null) && (dataCache.hasValidTimeHorizonCoord(calcIntersection))) {		// TTN-1595
						calcIntersection.setCoordinate(measureDim, measFunc.getMeasureName());                        
                        if ( // override locks set or not locked to begin with and not already specified as having a formula
                                (evalState.getRule().getEvalLockedIntersections() || !evalState.getCurrentLockedCells().contains(calcIntersection))
                                && !cellsToCalc.containsKey(calcIntersection)
                            ) {
							cellsToCalc.put(calcIntersection, leadingRule.getFormula());
						}
					}		
				}
			}    		
		}
		
        if (logger.isTraceEnabled())
        	logger.trace(LogUtil.timedStep("Found relevant changed cells out of " + evalState.getCurrentChangedCells().size() + " cells", stepTime));          			          
		
		stepTime = System.currentTimeMillis();

		for (Intersection is : cellsToCalc.keySet()) {
			if (measFunc == null) {
				EvalUtil.evalFormula(cellsToCalc.get(is), measureDim, is, dataCache, evalState);
			}
			else {
				Formula f = cellsToCalc.get(is);
                      
				Intersection srcIs = EvalUtil.inverseTranslocateIntersection(is, measFunc, evalState);
				srcIs.setCoordinate(measureDim, f.getExpression().trim());
                EvalUtil.evalFormula(cellsToCalc.get(is), measureDim, srcIs, is, dataCache, evalState);				
			}

			newChngCells.add(is);
		}
		
        if (logger.isTraceEnabled())
        	logger.trace(LogUtil.timedStep("Calculating " + newChngCells.size() + " newly changed cells", stepTime));
		
		stepTime = System.currentTimeMillis();		
		if (newChngCells.size() > 0) {
			
			// any cell that is changed from formula evaluation is a potential target for allocation
			// Normally evaluation results are not locked or allocated to preserve shape
			// However this can be overridden by a rule flag
			if (evalState.getRule().isLockSystemEvaluationResult()  ) {
				evalState.getCurrentLockedCells().addAll(newChngCells);
				evalState.addAllAllocations(newChngCells);
			}
	
			evalState.addAllChangedCells(newChngCells);
			evalState.setStateChanged(true);
		}
        if (logger.isTraceEnabled())
        	logger.trace(LogUtil.timedStep("Adding " + newChngCells.size() + " newly changed cells to buckets", stepTime));
        
		if (logger.isTraceEnabled())
        	logger.trace(LogUtil.timedStep("Evaluate measures step complete", startTime));

	}

    private Set<Intersection> calculatePriorTimeIntersections(Intersection calcIntersection, EvalState evalState) {
        Set<Intersection> locks = new HashSet<Intersection>(500);
        
        // setup some local variables
        PafDimTree timeTree = evalState.getTimeSubTree();
        String timeDim = evalState.getAppDef().getMdbDef().getTimeDim();
//        String isTime = calcIntersection.getCoordinate(timeDim);
        String isTime = EvalUtil.getIsCoord(calcIntersection, timeDim, evalState); 	// TTN-1595
        
        // dump out of no previous sibling
        PafDimMember prevSibling = timeTree.getPrevSibling(timeTree.getMember(isTime)); 
        if (prevSibling == null) return locks;
        
        
        // get cumulative level 0 members leading up to and including the sibling
        List<PafDimMember> prevMembers = timeTree.getLPeers(isTime);
        
        // add to list holding all other dimensions constant
        Intersection isCopy;
        for (PafDimMember m : prevMembers) {
            isCopy = calcIntersection.clone();
//            isCopy.setCoordinate(timeDim, m.getKey());
            EvalUtil.setIsCoord(isCopy, timeDim, m.getKey(), evalState);		// TTN-1595
            locks.addAll(EvalUtil.buildFloorIntersections(isCopy, evalState));
        }
                
        return locks;
    }
    
     
 
    
}
