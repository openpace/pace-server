/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.server.eval;

import java.util.List;
import java.util.Random;

import org.apache.commons.math3.stat.clustering.Cluster;
import org.apache.commons.math3.stat.clustering.EuclideanIntegerPoint;
import org.apache.commons.math3.stat.clustering.KMeansPlusPlusClusterer;

/**
 * @author jim
 *
 */
public class MathOp {
	
	/**
	 * Performs a cluster on a List of DoublePoints
	 * @param dataPoints
	 * @param numOfClusters
	 * @param maxIterations
	 * @return
	 */
	static public List<Cluster<DoublePoint>> clusterDataDouble(List<DoublePoint> dataPoints, int numOfClusters, int maxIterations) {
		KMeansPlusPlusClusterer<DoublePoint> transformer =
	            new KMeansPlusPlusClusterer<DoublePoint>(new Random(1746432956321l), KMeansPlusPlusClusterer.EmptyClusterStrategy.FARTHEST_POINT);

	        List<Cluster<DoublePoint>> clusters = transformer.cluster(dataPoints, numOfClusters, maxIterations);
	        
	        return clusters;
	}
	
	static public List<Cluster<EuclideanIntegerPoint>> clusterData(List<EuclideanIntegerPoint> dataPoints, int numOfClusters, int maxIterations) {
		
		        KMeansPlusPlusClusterer<EuclideanIntegerPoint> transformer =
		            new KMeansPlusPlusClusterer<EuclideanIntegerPoint>(new Random(1746432956321l));

		        List<Cluster<EuclideanIntegerPoint>> clusters = transformer.cluster(dataPoints, numOfClusters, maxIterations);


		return clusters;
	}

}
