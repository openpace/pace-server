/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.eval;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.pace.base.PafException;
import com.pace.base.PafInvalidIntersectionException;
import com.pace.base.app.MeasureDef;
import com.pace.base.app.MeasureType;
import com.pace.base.data.EvalUtil;
import com.pace.base.data.Intersection;
import com.pace.base.mdb.PafDataCache;
import com.pace.base.state.EvalState;
import com.pace.base.utility.LogUtil;
import com.pace.server.PafDataService;


public class ES_RecalcMeasures extends ES_EvalBase implements IEvalStep {
    
    PafDataService dataService = PafDataService.getInstance();    
    private static Logger logger = Logger.getLogger(ES_RecalcMeasures.class);
    
    //no locks but adds to change stack
    public void performEvaluation(EvalState evalState) throws PafException, PafInvalidIntersectionException {
        long startTime = System.currentTimeMillis();
        long stepTime;
        PafDataCache dataCache = evalState.getDataCache();        
        String measureDim = evalState.getClientState().getApp().getMdbDef().getMeasureDim();
		int measureAxis = dataCache.getMeasureAxis();
        String measure = evalState.getMeasureName();
		int versionAxis = dataCache.getVersionAxis();


	    MeasureDef msrDef = evalState.getAppDef().getMeasureDef(measure);	//TTN-2139
        if (msrDef.getType() != MeasureType.Recalc) return;
        
        
        stepTime = System.currentTimeMillis();

        Set<Intersection> targets = new HashSet<Intersection>(evalState.getLoadFactor());
        Intersection is;
 
        
        // 1st find any changed or locked intersection by measure that could impact this rule
        //Set<Intersection> chngSet = EvalUtil.getChangeSet(evalState.getRule(), evalState);
        Set<Intersection> chngSet = this.impactingChangeList(evalState.getRule(), evalState);       
        chngSet.addAll(evalState.getOrigLockedCells()); // only consider original locks
            
        
        // Now put on the stack any measure that needs to be calculated
        for (Intersection changeIs: chngSet) {
            if ( EvalUtil.changeTriggersFormula( changeIs, evalState.getRule(), evalState) ) {        
        
                is = changeIs.clone();
                is.setCoordinate(measureDim, evalState.getRule().getFormula().getResultTerm());
  
            	// skip over elapsed time periods if there are any
                if (EvalUtil.isElapsedIs(is, evalState)) {
            		continue;
            	}

                if (evalState.isRoundingResourcePass()){
                	if (!EvalUtil.isLevel0(is, evalState)){
                		targets.add(is); 
                	}
                }
                else{
                	targets.add(is);  
                }
            }
        }
        
        //TTN-718 - Need to add any replicated recalc measures to recalc stack.
        if(!evalState.isDefaultEvalStep()){
        	
        	// Replicate all
        	this.addReplicatedIntersections(targets, evalState.getSliceState().getReplicateAllCells(),
        			measureAxis, measure, evalState.getVarVersNames(), versionAxis);

            // Replicate existing
        	this.addReplicatedIntersections(targets, evalState.getSliceState().getReplicateExistingCells(), 
        			measureAxis, measure, evalState.getVarVersNames(), versionAxis);

        	// Lift all - Client (TTN-1793)
        	this.addReplicatedIntersections(targets, evalState.getSliceState().getLiftAllCells(),
        			measureAxis, measure, evalState.getVarVersNames(), versionAxis);

        	// Lift existing - Client (TTN-1793)
        	this.addReplicatedIntersections(targets, evalState.getSliceState().getLiftExistingCells(),
        			measureAxis, measure, evalState.getVarVersNames(), versionAxis);

        	// Lift all - Rule Set (TTN-1793)
        	this.addReplicatedIntersections(targets, evalState.getSliceState().getRuleSetLiftAllCells(),
        			measureAxis, measure, evalState.getVarVersNames(), versionAxis);

        	// Lift existing - Rule Set (TTN-1793)
        	this.addReplicatedIntersections(targets, evalState.getSliceState().getRuleSetLiftExistingCells(),
        			measureAxis, measure, evalState.getVarVersNames(), versionAxis);

    	}
    	
         
        
        if (logger.isTraceEnabled())
        	logger.trace(LogUtil.timedStep("Building intersections for calculated measure: " + evalState.getMeasureName(), stepTime));       
        
        stepTime = System.currentTimeMillis();
        Set<Intersection> updatedIntersections = EvalUtil.calcAndDiffIntersections(targets, measureDim, evalState.getRule().getFormula(), dataCache, evalState);
        
        if (logger.isTraceEnabled())
        	logger.trace(LogUtil.timedStep("Calculating " + updatedIntersections.size() + " intersections", stepTime));   
        
        logEvalDetail(this, evalState, dataCache);
        
        evalState.addAllChangedCells(updatedIntersections);
		
        if (logger.isTraceEnabled())
        	logger.trace(LogUtil.timedStep("Recalc measures calculation step", startTime));
    }   
    
    /**
     * add any replicated recalc measures to recalc stack
     * @param targets The set of intersections to be recalculated.
     * @param replicatedIntersections the array of intersections to be replicated.
     * @param measureDim the name of the measure dimension name.
     * @param measure the measure coordinate.
     * @parm vvNames set containg the names of the variance version 
     * @parm versionDim name of the version dimension
     */
    private void addReplicatedIntersections(Set<Intersection> targets,  
    		Intersection[] replicatedIntersections, int measureAxis, 
    		String measure, List<String> vvNames, int versionAxis){
    	
    	//TTN-870
    	// Only add intersections from "replicateAllCells" or "replicateExistingCells" to targets that aren't in variance versions.
    	// They naturally get recalculated dynamically for screen presentation and variance version 
    	// dimensionality doesn't exist in this data cache.
    	
    	//TTN-718
        if(replicatedIntersections != null){
	    	for(Intersection ix : replicatedIntersections){
	    		if(ix.getCoordinate(measureAxis).equals(measure)){
	    			if(!targets.contains(ix)){
	    				//TTN-870
	    				if (!vvNames.contains(ix.getCoordinate(versionAxis))) {
	    					targets.add(ix);
	    				}
	    			}
	    		}
	    	}
    	}
    }
}
