/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import com.pace.base.PafInvalidLogonInformation;
import com.pace.base.PafNotAbletoGetLDAPContext;
import com.pace.base.SystemStatus;
import com.pace.base.app.PafSecurityDomainUserNames;
import com.pace.base.app.PafUserDef;
import com.pace.base.server.ServerSettings;


public interface IPAFAuthentication {

	public PafUserDef authenticateUserByUpn(ServerSettings serverSettings, String userPrincipalName, String password, String domain) throws PafInvalidLogonInformation, PafNotAbletoGetLDAPContext;

	public PafUserDef authenticateUserBySid(ServerSettings serverSettings, String userName, String sid) throws PafInvalidLogonInformation, PafNotAbletoGetLDAPContext;
	
	public PafUserDef getUser(ServerSettings serverSettings, String userName, String domain) throws PafNotAbletoGetLDAPContext;

	public TreeSet<PafSecurityDomainUserNames> getUserNamesforSecurityGroups(
			ServerSettings serverSettings, Map<String, List<String>> paceGroups) throws PafNotAbletoGetLDAPContext;

	public Map<String, TreeSet<String>> getSecurityGroups(
			ServerSettings serverSettings) throws PafNotAbletoGetLDAPContext;
	
	public SystemStatus testProviderContext(ServerSettings serverSettings);

}