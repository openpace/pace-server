/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;

/**
 * This class is used to execute a custom menu command. It indicates
 * the id of the menu command selected by the user, and any parameter
 * key/values pairs
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PafCustomCommandRequest extends PafRequest {

    String menuCommandKey;
    String[] parameterValues;
    String[] parameterKeys;
    /**
     * @return Returns the menuCommandId.
     */
    public String getMenuCommandKey() {
        return menuCommandKey;
    }
    /**
     * @param menuCommandKey The menuCommandKey to set.
     */
    public void setMenuCommandKey(String menuCommandKey) {
        this.menuCommandKey = menuCommandKey;
    }
    /**
     * @return Returns the parameterValues.
     */
    public String[] getParameterValues() {
        return parameterValues;
    }
    /**
     * @param parameterValues The parameterValues to set.
     */
    public void setParameterValues(String[] parameterValues) {
        this.parameterValues = parameterValues;
    }
    /**
     * @return Returns the parameterKeys.
     */
    public String[] getParameterKeys() {
        return parameterKeys;
    }
    /**
     * @param parameterKeys The parameterKeys to set.
     */
    public void setParameterKeys(String[] parameterKeys) {
        this.parameterKeys = parameterKeys;
    }
    
    
}
