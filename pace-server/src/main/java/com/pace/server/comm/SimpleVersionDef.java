/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.app.VersionDef;

public class SimpleVersionDef {

	private String name;
	private String type;
	private String numericFormat;
	
	//used in WS
	public SimpleVersionDef() {}
	
	public SimpleVersionDef(VersionDef versionDef) {
		
		this.name = versionDef.getName();
		this.type = versionDef.getType().name();
		this.numericFormat = versionDef.getNumericFormatName();
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumericFormat() {
		return numericFormat;
	}
	public void setNumericFormat(String numericFormat) {
		this.numericFormat = numericFormat;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
	
	
}
