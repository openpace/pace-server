/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

/**
 * Clear imported attributes response
 *
 * @version	x.xx
 * @author kmoos
 *
 */

import com.pace.base.comm.PafRequest;

public class PafClearImportedAttrRequest extends PafRequest {

	private boolean clearAllDimensions;
	private String[] dimensionsToClear = null;
	
	/**
	 * Gets the clear all dimensions flag.
	 * @return true if all dimensions will be cleared, 
	 * 	false if only certain dimensions will be cleared.
	 */
	public boolean isClearAllDimensions() {
		return clearAllDimensions;
	}
	
	/**
	 * Set to true to clear all dimension in the cache.  If false, then
	 * only the dimensions set will be cleared. 
	 * @param clearAllDimensions
	 */
	public void setClearAllDimensions(boolean clearAllDimensions) {
		this.clearAllDimensions = clearAllDimensions;
	}
	
	/**
	 * Gets the array of dimensions to clear.  If clearAllDimensions is true, then
	 * the property is ignored.
	 * @return an array of dimension names.
	 */
	public String[] getDimensionsToClear() {
		return dimensionsToClear;
	}
	
	/**
	 * Sets the array of dimensions to clear. If clearAllDimensions is true, then
	 * the property is ignored.
	 * @param dimensionsToClear The array of dimensions to clear.
	 */
	public void setDimensionsToClear(String[] dimensionsToClear) {
		this.dimensionsToClear = dimensionsToClear;
	}
}