/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import java.util.ArrayList;

import com.pace.base.app.PafDimSpec;
import com.pace.base.comm.PafRequest;

public class PaceQueryRequest extends PafRequest {


	PafDimSpec members;
	ArrayList<PafDimSpec> attributes;
	int level;
	
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}


	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}


	/**
	 * @return the members
	 */
	public PafDimSpec getMembers() {
		return members;
	}


	/**
	 * @param members the members to set
	 */
	public void setMembers(PafDimSpec members) {
		this.members = members;
	}


	/**
	 * @return the attributes
	 */
	public ArrayList<PafDimSpec> getAttributes() {
		return attributes;
	}


	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(ArrayList<PafDimSpec> attributes) {
		this.attributes = attributes;
	}

	
	public PaceQueryRequest(PafDimSpec members, ArrayList<PafDimSpec> attributes) {
		this.members = members;
		this.attributes = attributes;
	}
	
	public PaceQueryRequest() {}

}
