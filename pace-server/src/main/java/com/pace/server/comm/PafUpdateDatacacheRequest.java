/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;

/**
 * Update datacache request
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafUpdateDatacacheRequest extends PafRequest {

    private String[] versionFilter;
	private String viewName;

	/**
	 * @param versionFilter the versionFilter to set
	 */
	public void setVersionFilter(String[] versionFilter) {
		this.versionFilter = versionFilter;
	}

	/**
	 * @return the versionFilter
	 */
	public String[] getVersionFilter() {
		return versionFilter;
	}
    
    /**
     * @return Returns the viewName.
     */
    public String getViewName() {
        return viewName;
    }

    /**
     * @param viewName The viewName to set.
     */
    public void setViewName(String viewName) {
        this.viewName = viewName;
    }
    
}
