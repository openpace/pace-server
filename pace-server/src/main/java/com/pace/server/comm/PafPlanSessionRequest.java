/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;


/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PafPlanSessionRequest extends PafRequest {

	private String selectedRole = null;
    private String seasonId = null;
    private boolean isInvalidIntersectionSuppressionSelected = false;
    private boolean isCompressResponse = false;
    private boolean isFilteredSubtotalsSelected = false;

    /**
     * @return Returns the selectedRole.
     */
    public String getSelectedRole() {
        return selectedRole;
    }

    /**
     * @param selectedRole The selectedRole to set.
     */
    public void setSelectedRole(String selectedRole) {
        this.selectedRole = selectedRole;
    }

    /**
     * @return Returns the seasonId.
     */
    public String getSeasonId() {
        return seasonId;
    }

    /**
     * @param seasonId The seasonId to set.
     */
    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public void setIsInvalidIntersectionSuppressionSelected(boolean isInvalidIntersectionSuppressionSelected) {
		this.isInvalidIntersectionSuppressionSelected = isInvalidIntersectionSuppressionSelected;
	}
    /**
     * @return Returns whether or not Invalid Intersection Suppression is selected.
     */
	public boolean getIsInvalidIntersectionSuppressionSelected() {
		return isInvalidIntersectionSuppressionSelected;
	}

	public boolean isCompressResponse() {
		return isCompressResponse;
	}

	public void setCompressResponse(boolean isCompressResponse) {
		this.isCompressResponse = isCompressResponse;
	}

	/**
	 * @return the isFilteredSubtotalsSelected
	 */
	public boolean isFilteredSubtotalsSelected() {
		return isFilteredSubtotalsSelected;
	}

	/**
	 * @param isFilteredSubtotalsSelected the isFilteredSubtotalsSelected to set
	 */
	public void setFilteredSubtotalsSelected(boolean isFilteredSubtotalsSelected) {
		this.isFilteredSubtotalsSelected = isFilteredSubtotalsSelected;
	}
}
