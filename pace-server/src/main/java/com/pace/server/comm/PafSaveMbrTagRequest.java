/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;
import com.pace.base.db.membertags.SimpleMemberTagData;

/**
 * Defines updates to the member tag database
 *
 * @version	x.xx
 * @author Alan
 *
 */
public class PafSaveMbrTagRequest extends PafRequest {

	private SimpleMemberTagData[] addMemberTags;
	private SimpleMemberTagData[] updateMemberTags;
	private SimpleMemberTagData[] deleteMemberTags;
	
	/**
	 * @return the addMemberTags
	 */
	public SimpleMemberTagData[] getAddMemberTags() {
		return addMemberTags;
	}
	/**
	 * @param addMemberTags the addMemberTags to set
	 */
	public void setAddMemberTags(SimpleMemberTagData[] addMemberTags) {
		this.addMemberTags = addMemberTags;
	}
	/**
	 * @return the deleteMemberTags
	 */
	public SimpleMemberTagData[] getDeleteMemberTags() {
		return deleteMemberTags;
	}
	/**
	 * @param deleteMemberTags the deleteMemberTags to set
	 */
	public void setDeleteMemberTags(SimpleMemberTagData[] deleteMemberTags) {
		this.deleteMemberTags = deleteMemberTags;
	}
	/**
	 * @return the updateMemberTags
	 */
	public SimpleMemberTagData[] getUpdateMemberTags() {
		return updateMemberTags;
	}
	/**
	 * @param updateMemberTags the updateMemberTags to set
	 */
	public void setUpdateMemberTags(SimpleMemberTagData[] updateMemberTags) {
		this.updateMemberTags = updateMemberTags;
	}	
}
