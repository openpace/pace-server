/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;
import com.pace.base.db.membertags.PafMbrTagFilter;

/**
 * Filtered Member Tag Request
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafFilteredMbrTagRequest extends PafRequest {
	
	private PafMbrTagFilter[] memberTagFilters = new PafMbrTagFilter[0];

 	/**
 	 * default constructor
 	 */
 	public PafFilteredMbrTagRequest() {}
 	
 	/**
 	 * @param memberTagFilters Array of application / member tag filters
 	 */
 	public PafFilteredMbrTagRequest(PafMbrTagFilter[] memberTagFilters) {
 		this.memberTagFilters = memberTagFilters;
 	}
 	
	/**
	 * @return the memberTagFilters
	 */
	public PafMbrTagFilter[] getMemberTagFilters() {
		return memberTagFilters;
	}

	/**
	 * @param memberTagFilters the memberTagFilters to set
	 */
	public void setMemberTagFilters(PafMbrTagFilter[] memberTagFilters) {
		this.memberTagFilters = memberTagFilters;
	}
	
}
