/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;

/**
 * Request object for a support request.
 * @author themoosman
 *
 */
public class SupportRequest extends PafRequest {

	private boolean derbyOnly;
	private boolean verbose;
	
	/**
	 * 
	 * @return isDerbyOnly
	 */
	public boolean isDerbyOnly() {
		return derbyOnly;
	}

	/**
	 * Sets the derbyOnly parm
	 * @param derbyOnly
	 */
	public void setDerbyOnly(boolean derbyOnly) {
		this.derbyOnly = derbyOnly;
	}

	/**
	 * 
	 * @return isVerbose
	 */
	public boolean isVerbose() {
		return verbose;
	}

	/**
	 * Sets the verbose 
	 * @param verbose
	 */
	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	@Override
	public String toString() {
		return "SupportRequest [derbyOnly=" + derbyOnly + ", verbose=" + verbose + "]";
	}
}
