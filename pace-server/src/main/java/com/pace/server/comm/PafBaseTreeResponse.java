/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.mdb.PafSimpleBaseMember;
import com.pace.base.mdb.PafSimpleBaseMemberProps;
import com.pace.base.mdb.PafSimpleBaseTree;

// Adding the PafSimpleBaseMember and PafSimpleBaseMemberProps to have these objects visible in the pace-client.jar
public class PafBaseTreeResponse {

	
	private PafSimpleBaseTree pafSimpleBaseTree;
	private PafSimpleBaseMember pafSimpleBaseMember;
	public PafSimpleBaseMember getPafSimpleBaseMember() {
		return pafSimpleBaseMember;
	}

	public void setPafSimpleBaseMember(PafSimpleBaseMember pafSimpleBaseMember) {
		this.pafSimpleBaseMember = pafSimpleBaseMember;
	}

	private PafSimpleBaseMemberProps pafSimpleBaseMemberProps;

	public PafSimpleBaseMemberProps getPafSimpleBaseMemberProps() {
		return pafSimpleBaseMemberProps;
	}

	public void setPafSimpleBaseMemberProps(
			PafSimpleBaseMemberProps pafSimpleBaseMemberProps) {
		this.pafSimpleBaseMemberProps = pafSimpleBaseMemberProps;
	}

	/**
	 * @return the pafSimpleDimTree
	 */
	public PafSimpleBaseTree getPafSimpleBaseTree() {
		return pafSimpleBaseTree;
	}

	/**
	 * @param pafSimpleDimTree the pafSimpleDimTree to set
	 */
	public void setPafSimpleBaseTree(PafSimpleBaseTree pafSimpleBaseTree) {
		this.pafSimpleBaseTree = pafSimpleBaseTree;
	}
	
	
}
