/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

package com.pace.server.comm;


import java.util.Map;

import com.pace.base.app.MdbDef;
import com.pace.base.db.membertags.MemberTagDef;
import com.pace.base.format.ColorScale;
import com.pace.base.format.ConditionalFormat;
import com.pace.base.format.DataBars;
import com.pace.base.format.IconStyle;
import com.pace.base.rules.RuleSet;
import com.pace.base.view.PafStyle;

public class PafClientCacheBlock {

	MdbDef mdbDef;
    String[] axisSequence;
    RuleSet[] ruleSets;
    SimpleMeasureDef[] measureDefs = null;
    PafStyle[] globalStyles;
    SimpleVersionDef[] versionDefs = null;  
    MemberTagDef[] memberTagDefs;
    String lastPeriod;	
    String currentYear;	

	private ColorScale[] colorScaleConditionalStyles;
	private DataBars[] dataBarConditionalStyles;
	private IconStyle[] iconStyleConditionalStyles;
	private ConditionalFormat[] conditionalFormats;
	private Map<String, String[]> ruleSetMemberTagFuncs;
	
	public IconStyle[] getIconStyleConditionalStyles(){
		return this.iconStyleConditionalStyles;
	}
	
	public void setIconStyleConditionalStyles(IconStyle[] iconStyleConditionalStyles){
		this.iconStyleConditionalStyles = iconStyleConditionalStyles;
	}
	
	public DataBars[] getDataBarConditionalStyles(){
		return this.dataBarConditionalStyles;
	}
	
	public void setDataBarConditionalStyles(DataBars[] dataBarConditionalStyles){
		this.dataBarConditionalStyles = dataBarConditionalStyles;
	}
	
	public ColorScale[] getColorScaleConditionalStyles(){
		return this.colorScaleConditionalStyles;
	}
	
	public void setColorScaleConditionalStyles(ColorScale[] colorScaleConditionalStyles){
		this.colorScaleConditionalStyles = colorScaleConditionalStyles;
	}

    /**
	 * @return Returns the mdbDef.
	 */
	public MdbDef getMdbDef() {
		return mdbDef;
	}
	/**
	 * @param mdbDef The mdbDef to set.
	 */
	public void setMdbDef(MdbDef mdbDef) {
		this.mdbDef = mdbDef;
	}
	
	public RuleSet[] getRuleSets() {
        return ruleSets;
    }
    public void setRuleSets(RuleSet[] ruleSets) {
        this.ruleSets = ruleSets;
    }

    public String[] getAxisSequence() {
        return axisSequence;
    }
    public void setAxisSequence(String[] axisSequence) {
        this.axisSequence = axisSequence;
    }
    
	public PafStyle[] getGlobalStyles() {
		return globalStyles;
	}
	public void setGlobalStyles(PafStyle[] globalStyles) {
		this.globalStyles = globalStyles;
	}
	
    /**
     * @return Returns the measureDefs.
     */
    public SimpleMeasureDef[] getMeasureDefs() {
        return measureDefs;
    }
    /**
     * @param measureDefs The measureDefs to set.
     */
    public void setMeasureDefs(SimpleMeasureDef[] measureDefs) {
        this.measureDefs = measureDefs;
    }
    
	public SimpleVersionDef[] getVersionDefs() {
		return versionDefs;
	}
	public void setVersionDefs(SimpleVersionDef[] versionDefs) {
		this.versionDefs = versionDefs;
	}
	
	/**
	 * @return Returns the currentYear.
	 */
	public String getCurrentYear() {
		return currentYear;
	}
	/**
	 * @param currentYear The currentYear to set.
	 */
	public void setCurrentYear(String currentYear) {
		this.currentYear = currentYear;
	}
	
	/**
	 * @return Returns the lastPeriod.
	 */
	public String getLastPeriod() {
		return lastPeriod;
	}
	/**
	 * @param lastPeriod The lastPeriod to set.
	 */
	public void setLastPeriod(String lastPeriod) {
		this.lastPeriod = lastPeriod;
	}
	/**
	 * @return the memberTagDefs
	 */
	public MemberTagDef[] getMemberTagDefs() {
		return memberTagDefs;
	}
	/**
	 * @param memberTagDefs the memberTagDefs to set
	 */
	public void setMemberTagDefs(MemberTagDef[] memberTagDefs) {
		this.memberTagDefs = memberTagDefs;
	}

	public ConditionalFormat[] getConditionalFormats() {
		return conditionalFormats;
	}

	public void setConditionalFormats(ConditionalFormat[] conditionalFormats) {
		this.conditionalFormats = conditionalFormats;
	}

	/**
	 * RuleSetName, List of MemberTagDef names
	 * This allows the caller to know which rulsets have member tag functions
	 * @return
	 */
	public Map<String, String[]> getRuleSetMemberTagFuncs() {
		return ruleSetMemberTagFuncs;
	}

	public void setRuleSetMemberTagFuncs(
			Map<String, String[]> ruleSetMemberTagFuncs) {
		this.ruleSetMemberTagFuncs = ruleSetMemberTagFuncs;
	}

}
