/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.app.PafDimSpec;
import com.pace.base.comm.PafRequest;

/**
 * Essbase Server Request
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafValidAttrRequest extends PafRequest {
	
	private String reqAttrDim = null;
	private String selBaseDim = null;
	private String[] selBaseMembers = null;
//	private String[] selAttrDims = null;
	private PafDimSpec[] selAttrSpecs = null;
	
	/**
	 * @return the reqAttrDim
	 */
	public String getReqAttrDim() {
		return reqAttrDim;
	}
	/**
	 * @param reqAttrDim the reqAttrDim to set
	 */
	public void setReqAttrDim(String reqAttrDim) {
		this.reqAttrDim = reqAttrDim;
	}
	/**
	 * @return the selBaseDim
	 */
	public String getSelBaseDim() {
		return selBaseDim;
	}
	/**
	 * @param selBaseDim the selBaseDim to set
	 */
	public void setSelBaseDim(String selBaseDim) {
		this.selBaseDim = selBaseDim;
	}
	/**
	 * @return the selBaseMembers
	 */
	public String[] getSelBaseMembers() {
		return selBaseMembers;
	}
	/**
	 * @param selBaseMembers the selBaseMembers to set
	 */
	public void setSelBaseMembers(String[] selBaseMembers) {
		this.selBaseMembers = selBaseMembers;
	}
	public PafDimSpec[] getSelAttrSpecs() {
		return selAttrSpecs;
	}
	public void setSelAttrSpecs(PafDimSpec[] selAttrSpecs) {
		this.selAttrSpecs = selAttrSpecs;
	}

}
