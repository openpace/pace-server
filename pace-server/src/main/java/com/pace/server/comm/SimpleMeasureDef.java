/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.app.MeasureDef;
/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class SimpleMeasureDef {
    private String name;
    private String type;
    private String recalcTBOveride;
    private String[] aliases;
    private String numericFormatName;
  
    
    public SimpleMeasureDef() {}
    public SimpleMeasureDef(MeasureDef measDef) {
        this.name = measDef.getName();
        this.type = measDef.getType().name();
        this.numericFormatName = measDef.getNumericFormatName();
        this.recalcTBOveride = measDef.getRecalcTBOveride().name();
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return Returns the numericFormatName.
     */
    public String getNumericFormatName() {
        return numericFormatName;
    }
    /**
     * @param numericFormatName The numericFormatName to set.
     */
    public void setNumericFormatName(String numericFormatName) {
        this.numericFormatName = numericFormatName;
    }
    /**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }
    /**
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return Returns the recalcTBOveride.
     */
    public String getRecalcTBOveride() {
        return recalcTBOveride;
    }
    /**
     * @param recalcTBOveride The recalcTBOveride to set.
     */
    public void setRecalcTBOveride(String recalcTBOveride) {
        this.recalcTBOveride = recalcTBOveride;
    }
	/**
	 * @return the aliases
	 */
	public String[] getAliases() {
		return aliases;
	}
	/**
	 * @param aliases the aliases to set
	 */
	public void setAliases(String[] aliases) {
		this.aliases = aliases;
	}
	
}
