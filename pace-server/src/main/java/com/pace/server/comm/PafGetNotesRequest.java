/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;
import com.pace.base.comm.SimpleCoordList;

/**
 * @author fskrgic
 *
 */
public class PafGetNotesRequest extends PafRequest {
	
	private SimpleCoordList simpleCoordList;
	
	private boolean refreshFromDatabase;

	/**
	 * @return the refreshFromDatabase
	 */
	public boolean isRefreshFromDatabase() {
		return refreshFromDatabase;
	}

	/**
	 * @param refreshFromDatabase the refreshFromDatabase to set
	 */
	public void setRefreshFromDatabase(boolean refreshFromDatabase) {
		this.refreshFromDatabase = refreshFromDatabase;
	}

	/**
	 * @return the simpleCoordLists
	 */
	public SimpleCoordList getSimpleCoordLists() {
		return simpleCoordList;
	}

	/**
	 * @param simpleCoordLists the simpleCoordLists to set
	 */
	public void setSimpleCoordLists(SimpleCoordList simpleCoordList) {
		this.simpleCoordList = simpleCoordList;
	}

	/**
	 * 
	 */
	public PafGetNotesRequest() {
		// TODO Auto-generated constructor stub
	}

}
