/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import java.util.Arrays;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlMimeType;

import com.pace.base.comm.PafResponse;

/**
 * Response object for a support request.
 * @author themoosman
 *
 */
public class SupportResponse extends PafResponse {

	private DataHandler dataHandler;
	private String serverPlatform;
	private String serverPlatformVersion;
	private String serverBitness;
	private String serverPaceVersion;
	private String serverJdkVersion;
	private String essbaseVersion;
	private boolean verboseOutput;
	private String[] verboseFileNames;

	/**
	 * Gets the DataHandler 
	 * @return
	 */
	public DataHandler getDataHandler() {
		return dataHandler;
	}

	/**
	 * Sets the DataHandler
	 * @param dataHandler
	 */
	@XmlMimeType("application/octet-stream") 
	public void setDataHandler(DataHandler dataHandler) {
		this.dataHandler = dataHandler;
	}

	/**
	 * Gets the Server OS (JDK Platform)
	 * @return
	 */
	public String getServerPlatform() {
		return serverPlatform;
	}

	/**
	 * Gets the bitness of the server OS.
	 * @return
	 */
	public String getServerBitness() {
		return serverBitness;
	}

	/**
	 * Gets the version of Pace currently running.
	 * @return
	 */
	public String getServerPaceVersion() {
		return serverPaceVersion;
	}

	/**
	 * Gets the JDK version.
	 * @return
	 */
	public String getServerJdkVersion() {
		return serverJdkVersion;
	}

	/**
	 * Sets the Server OS (JDK Platform)
	 * @param serverPlatform
	 */
	public void setServerPlatform(String serverPlatform) {
		this.serverPlatform = serverPlatform;
	}

	/**
	 * Sets the version of Pace currently running.
	 * @param serverBitness
	 */
	public void setServerBitness(String serverBitness) {
		this.serverBitness = serverBitness;
	}

	/**
	 * 
	 * @param serverPaceVersion
	 */
	public void setServerPaceVersion(String serverPaceVersion) {
		this.serverPaceVersion = serverPaceVersion;
	}

	/**
	 * Gets the Server OS (JDK Platform)
	 * @param serverJdkVersion
	 */
	public void setServerJdkVersion(String serverJdkVersion) {
		this.serverJdkVersion = serverJdkVersion;
	}

	/**
	 * Gets the current server platform version (OS Version)
	 * @return
	 */
	public String getServerPlatformVersion() {
		return serverPlatformVersion;
	}

	/**
	 * Sets the current server platform version (OS Version)
	 * @param serverPlatformVersion
	 */
	public void setServerPlatformVersion(String serverPlatformVersion) {
		this.serverPlatformVersion = serverPlatformVersion;
	}

	/**
	 * Gets the Essbase version
	 * @return
	 */
	public String getEssbaseVersion() {
		return essbaseVersion;
	}

	/**
	 * Sets the Essbase version
	 * @param essbaseVersion
	 */
	public void setEssbaseVersion(String essbaseVersion) {
		this.essbaseVersion = essbaseVersion;
	}

	/**
	 * Gets the verbose output flag.
	 * @return
	 */
	public boolean isVerboseOutput() {
		return verboseOutput;
	}

	/**
	 * Sets the verbose output flag.
	 * @param verboseOutput
	 */
	public void setVerboseOutput(boolean verboseOutput) {
		this.verboseOutput = verboseOutput;
	}

	/**
	 * Gets the list of files that will be returned if verbose is true.
	 * @return
	 */
	public String[] getVerboseFileNames() {
		return verboseFileNames;
	}

	/**
	 * Sets the list of files that will be returned if verbose is true.
	 * @param verboseFileNames
	 */
	public void setVerboseFileNames(String[] verboseFileNames) {
		this.verboseFileNames = verboseFileNames;
	}

	@Override
	public String toString() {
		return "SupportResponse [dataHandler=" + dataHandler
				+ ", serverPlatform=" + serverPlatform
				+ ", serverPlatformVersion=" + serverPlatformVersion
				+ ", serverBitness=" + serverBitness + ", serverPaceVersion="
				+ serverPaceVersion + ", serverJdkVersion=" + serverJdkVersion
				+ ", essbaseVersion=" + essbaseVersion + ", verboseOutput="
				+ verboseOutput + ", verboseFileNames="
				+ Arrays.toString(verboseFileNames) + "]";
	}
}