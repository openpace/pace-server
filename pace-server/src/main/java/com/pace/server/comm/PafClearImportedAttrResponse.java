/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

/**
 * Used to send back response information from an clear attribute import process.
 *
 * @version	x.xx
 * @author kmoos
 *
 */

public class PafClearImportedAttrResponse {
	
	private boolean isSuccess = false;
	
    String[] errors = null;

    public PafClearImportedAttrResponse() {}

    /**
     * @return Returns the errors.
     */
    public String[] Errors() {
        return errors;
    }

    /**
     * @param errors errors
     */
    public void setErrors(String[] errors) {
        this.errors = errors;
    }

	/**
	 * @return Returns the isSuccess.
	 */
	public boolean isSuccess() {
		return isSuccess;
	}

	/**
	 * @param isSuccess The isSuccess to set.
	 */
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
}
