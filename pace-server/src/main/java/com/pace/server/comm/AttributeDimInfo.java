/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

/**
 * Attribute dimension information
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class AttributeDimInfo {
	
	private String dimName;
	private String baseDimName;
	private int baseDimMappingLevel;
	
	/**
	 * @return the associated base dimension
	 */
	public String getBaseDimName() {
		return baseDimName;
	}
	/**
	 * @param baseDimension the associated base dimension to set
	 */
	public void setBaseDimName(String baseDimension) {
		this.baseDimName = baseDimension;
	}
	/**
	 * @return the baseDimensionMappingLevel
	 */
	public int getBaseDimMappingLevel() {
		return baseDimMappingLevel;
	}
	/**
	 * @param baseDimensionMappingLevel the baseDimensionMappingLevel to set
	 */
	public void setBaseDimMappingLevel(int baseDimensionMappingLevel) {
		this.baseDimMappingLevel = baseDimensionMappingLevel;
	}
	/**
	 * @return the dimName
	 */
	public String getDimName() {
		return dimName;
	}
	/**
	 * @param dimName the dimName to set
	 */
	public void setDimName(String dimName) {
		this.dimName = dimName;
	}
}
