/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.IPafViewRequest;
import com.pace.base.comm.PafRequest;
import com.pace.base.comm.SimpleCoordList;
import com.pace.base.utility.CompressionUtil;
import com.pace.base.view.PafUserSelection;


/**
 * Holds paramters necessary for retrieving a view request
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class ViewRequest extends PafRequest implements IPafViewRequest{
    private String viewName;
    private PafUserSelection[] userSelections;
    private boolean isCompressResponse = false;
    private boolean rowsSuppressed;
    private boolean columnsSuppressed;
	private SimpleCoordList[] sessionLockedCells;

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    /**
     * @return Returns the userSelections.
     */
    public PafUserSelection[] getUserSelections() {
        return userSelections;
    }

    /**
     * @param userSelections The userSelections to set.
     */
    public void setUserSelections(PafUserSelection[] userSelections) {
        this.userSelections = userSelections;
    }
    
	public boolean isCompressResponse() {
		return isCompressResponse;
	}

	public void setCompressResponse(boolean isCompressResponse) {
		this.isCompressResponse = isCompressResponse;
	}

	public void setRowsSuppressed(boolean rowsSuppressed) {
		this.rowsSuppressed = rowsSuppressed;
	}

	public boolean getRowsSuppressed() {
		return rowsSuppressed;
	}

	public void setColumnsSuppressed(boolean columnsSuppressed) {
		this.columnsSuppressed = columnsSuppressed;
	}

	public boolean getColumnsSuppressed() {
		return columnsSuppressed;
	}

	/**
	 * @return the sessionLockedCells
	 */
	public SimpleCoordList[] getSessionLockedCells() {
		return CompressionUtil.tryToUncompress(sessionLockedCells);
	}

	/**
	 * @param sessionLockedCells the sessionLockedCells to set
	 */
	public void setSessionLockedCells(SimpleCoordList[] sessionLockedCells) {
		this.sessionLockedCells = sessionLockedCells;
	}
}
