/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;


public class PafGetFilteredUOWSizeResponse {
	private Long uowCellCount = null;
	private String[] emptyDimensions = null;

	public void setUowCellCount(Long uowCellCount) {
		this.uowCellCount = uowCellCount;
	}

	public Long getUowCellCount() {
		return uowCellCount;
	}

	public void setEmptyDimensions(String[] emptyDimensions) {
		this.emptyDimensions = emptyDimensions;
	}

	public String[] getEmptyDimensions() {
		return emptyDimensions;
	}
	
}
