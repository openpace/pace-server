/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;
import com.pace.base.comm.SimpleCoordList;
import com.pace.base.db.cellnotes.SimpleCellNote;

/**
 * @author fskrgic
 *
 */
public class PafSaveNotesRequest extends PafRequest {

	private SimpleCellNote [] addNotes;
	private SimpleCellNote [] updateNotes;
	private SimpleCoordList deleteNoteIntersections;
	
	/**
	 * 
	 */
	public PafSaveNotesRequest() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the addnotes
	 */
	public SimpleCellNote [] getAddNotes() {
		return addNotes;
	}

	/**
	 * @param addnotes the addnotes to set
	 */
	public void setAddNotes(SimpleCellNote [] addnotes) {
		this.addNotes = addnotes;
	}

	/**
	 * @return the updateNotes
	 */
	public SimpleCellNote [] getUpdateNotes() {
		return updateNotes;
	}

	/**
	 * @param updateNotes the updateNotes to set
	 */
	public void setUpdateNotes(SimpleCellNote [] updateNotes) {
		this.updateNotes = updateNotes;
	}

	public SimpleCoordList getDeleteNoteIntersections() {
		return deleteNoteIntersections;
	}

	public void setDeleteNoteIntersections(SimpleCoordList deleteNoteIntersections) {
		this.deleteNoteIntersections = deleteNoteIntersections;
	}

}
