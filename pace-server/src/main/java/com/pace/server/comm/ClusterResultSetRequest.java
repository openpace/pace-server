/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.server.comm;

import java.util.Map;

import com.pace.base.app.PafDimSpec;
import com.pace.base.comm.PafRequest;

/**
 * @author jim
 *
 * Just a stub to kick off the clustering behavior
 */
public class ClusterResultSetRequest extends PafRequest {
	
	private String label;
	private PafDimSpec timeDimSpec;
	private PafDimSpec yearsDimSpec;
	private PafDimSpec measuresDimSpec;
	private PafDimSpec versionDimSpec;
	private int numOfClusters;
	private int maxIterations;
	private String ruleSetName;
	private Map<String, Integer> customClusterMap;	// Optional parm - Location, Cluster No.
	
	public ClusterResultSetRequest(){super();}

	public ClusterResultSetRequest(String label, PafDimSpec timeDimSpec, PafDimSpec yearsDimSpec, PafDimSpec measuresDimSpec, PafDimSpec versionDimSpec,
			int numOfClusters, int maxIterations){
		super();
		this.label = label;
		this.timeDimSpec = timeDimSpec;
		this.yearsDimSpec = yearsDimSpec;
		this.measuresDimSpec = measuresDimSpec;
		this.versionDimSpec = versionDimSpec;
		this.numOfClusters = numOfClusters;
		this.maxIterations = maxIterations;
	}
	
	public PafDimSpec getTimeDimSpec() {
		return timeDimSpec;
	}

	public void setTimeDimSpec(PafDimSpec timeDimSpec) {
		this.timeDimSpec = timeDimSpec;
	}

	public PafDimSpec getYearsDimSpec() {
		return yearsDimSpec;
	}

	public void setYearsDimSpec(PafDimSpec yearsDimSpec) {
		this.yearsDimSpec = yearsDimSpec;
	}

	public PafDimSpec getMeasuresDimSpec() {
		return measuresDimSpec;
	}

	public void setMeasuresDimSpec(PafDimSpec measuresDimSpec) {
		this.measuresDimSpec = measuresDimSpec;
	}

	public PafDimSpec getVersionDimSpec() {
		return versionDimSpec;
	}

	public void setVersionDimSpec(PafDimSpec versionDimSpec) {
		this.versionDimSpec = versionDimSpec;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public int getNumOfClusters() {
		return numOfClusters;
	}

	public void setNumOfClusters(int numOfClusters) {
		this.numOfClusters = numOfClusters;
	}

	public int getMaxIterations() {
		return maxIterations;
	}

	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}

	/**
	 * @return the ruleSetName
	 */
	public String getRuleSetName() {
		return ruleSetName;
	}

	/**
	 * @param ruleSetName the ruleSetName to set
	 */
	public void setRuleSetName(String ruleSetName) {
		this.ruleSetName = ruleSetName;
	}

	/**
	 * @return the customClusterMap
	 */
	public Map<String, Integer> getCustomClusterMap() {
		return customClusterMap;
	}

	/**
	 * @param customClusterMap the customClusterMap to set
	 */
	public void setCustomClusterMap(Map<String, Integer> customClusterMap) {
		this.customClusterMap = customClusterMap;
	}

}
