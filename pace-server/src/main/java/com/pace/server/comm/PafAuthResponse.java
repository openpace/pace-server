/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.PafSecurityToken;
import com.pace.base.app.AppSettings;
import com.pace.base.app.PafPlannerRole;
import com.pace.base.app.PafWorkSpec;
import com.pace.base.comm.PafPlannerConfig;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PafAuthResponse {
	
    private PafSecurityToken securityToken;
    private PafWorkSpec workSpec;
    private PafPlannerRole[] plannerRoles;
    private PafPlannerConfig[] plannerConfigs;
    private String[] discontigRoles;				// TTN-2530 - Add Filtered Intermediate Totals
    private AppSettings appSettings;
    private AttributeDimInfo[] attrDimInfo;

    //db admin flag
    private boolean admin;
    
    //change password
    private boolean changePassword;

    /**
     * @return Returns the securityToken.
     */
    public PafSecurityToken getSecurityToken() {
        return securityToken;
    }
    /**
     * @param securityToken The securityToken to set.
     */
    public void setSecurityToken(PafSecurityToken securityToken) {
        this.securityToken = securityToken;
    }
    /**
     * @return Returns the workSpec.
     */
    public PafWorkSpec getWorkSpec() {
        return workSpec;
    }
    /**
     * @param workSpec The workSpec to set.
     */
    public void setWorkSpec(PafWorkSpec workSpec) {
        this.workSpec = workSpec;
    }
    /**
     * @return Returns the plannerRoles.
     */
    public PafPlannerRole[] getPlannerRoles() {
        return plannerRoles;
    }
    /**
     * @param plannerRoles The plannerRoles to set.
     */
    public void setPlannerRoles(PafPlannerRole[] plannerRoles) {
        this.plannerRoles = plannerRoles;
    }
	/**
	 * @return the admin
	 */
	public boolean getAdmin() {
		return admin;
	}
	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	/**
	 * @return Returns the changePassword.
	 */
	public boolean getChangePassword() {
		return changePassword;
	}
	/**
	 * @param changePassword The changePassword to set.
	 */
	public void setChangePassword(boolean changePassword) {
		this.changePassword = changePassword;
	}
	/**
	 * @return an array of PafPlannerConfigs available to the user.
	 */
	public PafPlannerConfig[] getPlannerConfigs() {
		return this.plannerConfigs;
	}
	/**
	 * @param an array of role names available to the user.
	 */
	public void setPlannerConfigs(PafPlannerConfig[] plannerConfigs){
		this.plannerConfigs = plannerConfigs;
	}
	/**
	 * @return the discontigRoles
	 */
	public String[] getDiscontigRoles() {
		return discontigRoles;
	}
	/**
	 * @param discontigRoles the discontigRoles to set
	 */
	public void setDiscontigRoles(String[] discontigRoles) {
		this.discontigRoles = discontigRoles;
	}
	/**
	 * @return The application specific settings.
	 */
	public AppSettings getAppSettings(){
		return this.appSettings;
	}
	
	/**
	 * @param The application specific settings.
	 */
	public void setAppSettings(AppSettings appSettings){
		this.appSettings = appSettings;
	}
	/**
	 * @return the attrDimInfo
	 */
	public AttributeDimInfo[] getAttrDimInfo() {
		return attrDimInfo;
	}
	/**
	 * @param attrDimInfo the attrDimInfo to set
	 */
	public void setAttrDimInfo(AttributeDimInfo[] attrDimInfo) {
		this.attrDimInfo = attrDimInfo;
	}
	
}
