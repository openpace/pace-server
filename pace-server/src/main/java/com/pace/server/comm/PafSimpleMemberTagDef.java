/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.db.membertags.MemberTagDef;


/**
 * Simple member tag definition suitable for soap layer
 *
 * @version	x.xx
 * @author AFarkas
 *
 */
public class PafSimpleMemberTagDef {

	String name;
	String type;	/* "TX" = TEXT, "FO" = FORMULA, "HY" = HYPERLINK */
	String label;
	String[] dims;
	boolean isEditable;
	boolean isCommentVisible;

	public PafSimpleMemberTagDef() {}
	
	/**
	 * @param memberTagDef Member tag definition
	 */
	public PafSimpleMemberTagDef(MemberTagDef memberTagDef) {
		
		this.name = memberTagDef.getName();
		this.type = memberTagDef.getType().getCode();
		this.label = memberTagDef.getLabel();
		this.dims = memberTagDef.getDims();
		this.isEditable = memberTagDef.isEditable();
		this.isCommentVisible = memberTagDef.isCommentVisible();	
	}

	/**
	 * @return the dims
	 */
	public String[] getDims() {
		return dims;
	}

	/**
	 * @param dims the dims to set
	 */
	public void setDims(String[] dims) {
		this.dims = dims;
	}

	/**
	 * @return the isCommentVisible
	 */
	public boolean  isCommentVisible() {
		return isCommentVisible;
	}

	/**
	 * @param isCommentVisible the isCommentVisible to set
	 */
	public void setCommentVisible(boolean isCommentVisible) {
		this.isCommentVisible = isCommentVisible;
	}

	/**
	 * @return the isEditable
	 */
	public boolean isEditable() {
		return isEditable;
	}

	/**
	 * @param isEditable the isEditable to set
	 */
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
}
