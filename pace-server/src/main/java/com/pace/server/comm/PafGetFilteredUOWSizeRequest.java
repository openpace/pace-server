/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.app.PafDimSpec;
import com.pace.base.comm.PafRequest;


/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author PMack
 *
 */
public class PafGetFilteredUOWSizeRequest extends PafRequest{
    private String selectedRole = null;
    private String seasonId = null;
    private boolean isInvalidIntersectionSuppressionSelected = false;
    private PafDimSpec[] pafUserSelections = null;
    private boolean isFilteredSubtotalsSelected = false;

    
	public void setSelectedRole(String selectedRole) {
		this.selectedRole = selectedRole;
	}
    /**
     * @return Returns the selectedRole.
     */
	public String getSelectedRole() {
		return selectedRole;
	}
	public void setSeasonId(String seasonId) {
		this.seasonId = seasonId;
	}
    /**
     * @return Returns the selectedSeasonID.
     */
	public String getSeasonId() {
		return seasonId;
	}
	public void setIsInvalidIntersectionSuppressionSelected(boolean isInvalidIntersectionSuppressionSelected) {
		this.isInvalidIntersectionSuppressionSelected = isInvalidIntersectionSuppressionSelected;
	}
    /**
     * @return Returns whether or not Invalid Intersection Suppression is selected.
     */
	public boolean getIsInvalidIntersectionSuppressionSelected() {
		return isInvalidIntersectionSuppressionSelected;
	}
	public void setPafUserSelections(PafDimSpec[] userSelections) {
		this.pafUserSelections = userSelections;
	}
    /**
     * @return Returns an array of member selection made by the user
     */
	public PafDimSpec[] getPafUserSelections() {
		return pafUserSelections;
	}
	/**
	 * @return the isFilteredSubtotalsSelected
	 */
	public boolean isFilteredSubtotalsSelected() {
		return isFilteredSubtotalsSelected;
	}
	/**
	 * @param isFilteredSubtotalsSelected the isFilteredSubtotalsSelected to set
	 */
	public void setFilteredSubtotalsSelected(boolean isFilteredSubtotalsSelected) {
		this.isFilteredSubtotalsSelected = isFilteredSubtotalsSelected;
	}
	/**
	 * @param isInvalidIntersectionSuppressionSelected the isInvalidIntersectionSuppressionSelected to set
	 */
	public void setInvalidIntersectionSuppressionSelected(
			boolean isInvalidIntersectionSuppressionSelected) {
		this.isInvalidIntersectionSuppressionSelected = isInvalidIntersectionSuppressionSelected;
	}
    
    
}
