/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

/**
 * Returned after a password has been reset.
 *
 * @author jmilliron
 * @version	x.xx
 *
 */
public class PafClientSecurityPasswordResetResponse {

	private boolean successful;
	
	private String userName;
	
	private String userEmailAddress;
			
	private boolean invalidUserName;
	
	private boolean invalidEmailAddress;

	/**
	 * @return Returns the successful.
	 */
	public boolean isSuccessful() {
		return successful;
	}

	/**
	 * @param successful The successful to set.
	 */
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

	/**
	 * @return Returns the userEmailAddress.
	 */
	public String getUserEmailAddress() {
		return userEmailAddress;
	}

	/**
	 * @param userEmailAddress The userEmailAddress to set.
	 */
	public void setUserEmailAddress(String userEmailAddress) {
		this.userEmailAddress = userEmailAddress;
	}

	/**
	 * @return Returns the userName.
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName The userName to set.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return Returns the invalidEmailAddress.
	 */
	public boolean isInvalidEmailAddress() {
		return invalidEmailAddress;
	}

	/**
	 * @param invalidEmailAddress The invalidEmailAddress to set.
	 */
	public void setInvalidEmailAddress(boolean invalidEmailAddress) {
		this.invalidEmailAddress = invalidEmailAddress;
	}

	/**
	 * @return Returns the invalidUserName.
	 */
	public boolean isInvalidUserName() {
		return invalidUserName;
	}

	/**
	 * @param invalidUserName The invalidUserName to set.
	 */
	public void setInvalidUserName(boolean invalidUserName) {
		this.invalidUserName = invalidUserName;
	}
	
	
	
}
