/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;
import com.pace.base.utility.*;
import com.pace.server.PafServiceProvider;

import org.apache.log4j.Logger;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PafAuthRequest extends PafRequest {
    private String username;
    private String passwordHash;
    private String password;
    private String IV;
    private String domain;
    private String sid;
    private static Logger logger = Logger.getLogger(PafServiceProvider.class);
    
    public PafAuthRequest() {}
    public PafAuthRequest(String hash, String username) {
        super();
        passwordHash = hash;
        this.username = username;
    }
    public String getPasswordHash() {
        return passwordHash;
    }
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
    @SuppressWarnings("finally")
	public String getUsername() {
    	if(IV != null && username != null){
    		String decryptedUsername = null;
    		try{
    			decryptedUsername = AESEncryptionUtil.decrypt(username, IV);
    		}
    		catch(Exception e){
    			logger.error(e.getMessage());
    		}
    		finally{
    			return decryptedUsername;
    		}
    	}
    	
    	if (username != null){
    		username = username.trim();
    	}
    	else{
    		username = "";
    	}
    	
        return username.trim();
    }
    public void setUsername(String username) {
        this.username = username;
    }
	public void setPassword(String password) {
		this.password = password;
	}
	
	@SuppressWarnings("finally")
	public String getPassword(){
    	if(IV != null && password != null){
    		String decryptedPassword = null;
    		try{
    			decryptedPassword = AESEncryptionUtil.decrypt(password, IV);
    		}
    		catch(Exception e){
    			logger.error(e.getMessage());
    		}
    		finally{
    			return decryptedPassword.trim();
    		}
    	}
    	
    	if (password != null){
    		password = password.trim();
    	}
        return password;
    }
	
	public void setIV(String iV) {
		IV = iV;
	}
	public String getIV() {
		return IV;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	@SuppressWarnings("finally")
	public String getDomain() {
    	if(IV != null && domain != null){
    		String decryptedDomain = null;
    		try{
    			decryptedDomain = AESEncryptionUtil.decrypt(domain, IV);
    		}
    		catch(Exception e){
    			logger.error(e.getMessage());
    		}
    		finally{
    			return decryptedDomain.trim() ;
    		}
    	}
    	
    	if (domain != null){
    		domain = domain.trim();
    	}else{
    		domain = "";
    	}
    	
		return domain;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	
	@SuppressWarnings("finally")
	public String getSid() {
    	if(IV != null && sid != null){
    		String decryptedSid = null;
    		try{
    			decryptedSid = AESEncryptionUtil.decrypt(sid, IV);
    		}
    		catch(Exception e){
    			logger.error(e.getMessage());
    		}
    		finally{
    			return decryptedSid.trim() ;
    		}
    	}
    	
    	if (sid != null){
    		sid = sid.trim();
    	}
    	
    	//Non Null sid indicates SSO
    	
		return sid;
	}
}
