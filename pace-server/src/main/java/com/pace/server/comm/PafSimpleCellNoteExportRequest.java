/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;
import com.pace.base.db.cellnotes.CellNotesInformation;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author jmilliron
 *
 */
public class PafSimpleCellNoteExportRequest extends PafRequest {

	private CellNotesInformation[] cellNoteInformationAr = null;

	/**
	 * @return the cellNoteInformationAr
	 */
	public CellNotesInformation[] getCellNoteInformationAr() {
		return cellNoteInformationAr;
	}

	/**
	 * @param cellNoteInformationAr the cellNoteInformationAr to set
	 */
	public void setCellNoteInformationAr(
			CellNotesInformation[] cellNoteInformationAr) {
		this.cellNoteInformationAr = cellNoteInformationAr;
	}
	
}
