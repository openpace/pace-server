/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.server.comm;

import java.util.List;
import java.util.Map;

import com.pace.base.comm.PafResponse;
import com.pace.base.comm.SimpleCoordList;
import com.pace.base.data.PafDataSlice;

/**
 * @author jim
 *
 */
public class ClusteredResultSetResponse extends PafResponse {

	private StringRow header;
	@Deprecated private StringRow[] data;
	private SimpleCoordList level0Coords;
	private PafDataSlice level0Data;
	private SimpleCoordList aggregateCoords;
	private PafDataSlice aggregateData;
	private List<String> measures;
	private List<String> dimToCluster;
	private List<String> dimToMeasure;
	private List<String> years;
	private List<String> time;
	private List<String> version;
	private Map<String, Integer> clusters;
	
	public ClusteredResultSetResponse(){super();}
	
	@Deprecated public StringRow[] getData() {
		return data;
	}
	@Deprecated public void setData(StringRow[] data) {
		this.data = data;
	}

	/**
	 * @return the aggregateCoords
	 */
	public SimpleCoordList getAggregateCoords() {
		return aggregateCoords;
	}
	/**
	 * @param aggregateCoords the aggregateCoords to set
	 */
	public void setAggregateCoords(SimpleCoordList aggregateCoords) {
		this.aggregateCoords = aggregateCoords;
	}

	/**
	 * @return the aggregateData
	 */
	public PafDataSlice getAggregateData() {
		return aggregateData;
	}
	/**
	 * @param aggregateData the aggregateData to set
	 */
	public void setAggregateData(PafDataSlice aggregateData) {
		this.aggregateData = aggregateData;
	}

	/**
	 * @return the level0Coords
	 */
	public SimpleCoordList getLevel0Coords() {
		return level0Coords;
	}
	/**
	 * @param level0Coords the level0Coords to set
	 */
	public void setLevel0Coords(SimpleCoordList level0Coords) {
		this.level0Coords = level0Coords;
	}

	/**
	 * @return the level0Data
	 */
	public PafDataSlice getLevel0Data() {
		return level0Data;
	}
	/**
	 * @param level0Data the level0Data to set
	 */
	public void setLevel0Data(PafDataSlice level0Data) {
		this.level0Data = level0Data;
	}

	public StringRow getHeader() {
		return header;
	}
	public void setHeader(StringRow header) {
		this.header = header;
	}
	public List<String> getMeasures() {
		return measures;
	}
	public void setMeasures(List<String> measures) {
		this.measures = measures;
	}
	public List<String> getDimToCluster() {
		return dimToCluster;
	}
	public void setDimToCluster(List<String> dimToCluster) {
		this.dimToCluster = dimToCluster;
	}
	public List<String> getDimToMeasure() {
		return dimToMeasure;
	}
	public void setDimToMeasure(List<String> dimToMeasure) {
		this.dimToMeasure = dimToMeasure;
	}
	public List<String> getYears() {
		return years;
	}
	public void setYears(List<String> years) {
		this.years = years;
	}
	public List<String> getTime() {
		return time;
	}
	public void setTime(List<String> time) {
		this.time = time;
	}
	public List<String> getVersion() {
		return version;
	}
	public void setVersion(List<String> version) {
		this.version = version;
	}

	public Map<String, Integer> getClusters() {
		return clusters;
	}

	public void setClusters(Map<String, Integer> clusters) {
		this.clusters = clusters;
	}

}
