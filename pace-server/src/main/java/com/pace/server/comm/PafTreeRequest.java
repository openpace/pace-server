/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.server.comm;

import com.pace.base.app.PafDimSpec;
import com.pace.base.comm.PafRequest;


/**
 * @author jmilliron
 *
 */
public class PafTreeRequest extends PafRequest {
	
	private String treeName;
	
	private boolean isCompressResponse = false;

	private String datasourceID ;
	
	private PafDimSpec filterSpecification;
	
	public String getDatasourceID() {
		return datasourceID;
	}

	public void setDatasourceID(String datasourceID) {
		this.datasourceID = datasourceID;
	}

	/**
	 * @return the treeName
	 */
	public String getTreeName() {
		return treeName;
	}

	/**
	 * @param treeName the treeName to set
	 */
	public void setTreeName(String treeName) {
		this.treeName = treeName;
	}

	/**
	 * @return the isCompressResponse
	 */
	public boolean isCompressResponse() {
		return isCompressResponse;
	}

	/**
	 * @param isCompressResponse the isCompressResponse to set
	 */
	public void setCompressResponse(boolean isCompressResponse) {
		this.isCompressResponse = isCompressResponse;
	}	

	/**
	 * 
	 * @return
	 */
	public PafDimSpec getFilterSpecification() {
		return filterSpecification;
	}

	/**
	 * 
	 * @param filterSpecification
	 */
	public void setFilterSpecification(PafDimSpec filterSpecification) {
		this.filterSpecification = filterSpecification;
	}
	
}
