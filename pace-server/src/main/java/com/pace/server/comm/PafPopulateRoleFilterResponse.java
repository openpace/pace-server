/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.comm.PafRequest;
import com.pace.base.comm.UserFilterSpec;
import com.pace.base.mdb.PafSimpleDimTree;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PafPopulateRoleFilterResponse extends PafRequest {
	private PafSimpleDimTree[] simpleDimTrees = null;
	private String[] baseTreeNames = null;
	private String[] attributeTreeNames = null;
	private UserFilterSpec userFilterSpec = null;
	
	/**
	 * @return Returns the PafSimpleDimTrees
	 */
	public PafSimpleDimTree[] getDimTrees(){
		return this.simpleDimTrees;
	}
	
	public void setDimTrees(PafSimpleDimTree[] simpleDimTrees){
		this.simpleDimTrees = simpleDimTrees;
	}

	public void setBaseTreeNames(String[] baseTreeNames) {
		this.baseTreeNames = baseTreeNames;
	}

	public String[] getBaseTreeNames() {
		return baseTreeNames;
	}

	public void setAttributeTreeNames(String[] attributeTreeNames) {
		this.attributeTreeNames = attributeTreeNames;
	}

	public String[] getAttributeTreeNames() {
		return attributeTreeNames;
	}

	public void setUserFilterSpec(UserFilterSpec userFilterSpec) {
		this.userFilterSpec = userFilterSpec;
	}

	public UserFilterSpec getUserFilterSpec() {
		return userFilterSpec;
	}
	
}
