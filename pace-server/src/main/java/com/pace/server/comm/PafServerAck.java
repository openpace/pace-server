/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server.comm;

import com.pace.base.AuthMode;

/**
 * Class_description_goes_here
 *
 * @version	x.xx
 * @author JWatkins
 *
 */
public class PafServerAck {
    
	private String serverVersion;
    private String platform;
    private String clientId;
    private String applicationId;
    private String dataSourceId;
    private boolean serverPasswordResetEnabled;
    private boolean clientPasswordResetEnabled;
    private int minPassowordLength;
    private int maxPassowordLength;
    private AuthMode authMode;
    private boolean clientUpgradeRequired = false;
    private String clientUpgradeUrl;
    
	/**
	 * @return Returns the clientPasswordResetEnabled.
	 */
	public boolean isClientPasswordResetEnabled() {
		return clientPasswordResetEnabled;
	}

	/**
	 * @param clientPasswordResetEnabled The clientPasswordResetEnabled to set.
	 */
	public void setClientPasswordResetEnabled(boolean clientPasswordResetEnabled) {
		this.clientPasswordResetEnabled = clientPasswordResetEnabled;
	}

	/**
	 * @return Returns the serverPasswordResetEnabled.
	 */
	public boolean isServerPasswordResetEnabled() {
		return serverPasswordResetEnabled;
	}

	/**
	 * @param serverPasswordResetEnabled The serverPasswordResetEnabled to set.
	 */
	public void setServerPasswordResetEnabled(boolean serverPasswordResetEnabled) {
		this.serverPasswordResetEnabled = serverPasswordResetEnabled;
	}

	public PafServerAck() {}
    
    public PafServerAck(String clientId, String platform, String serverVersion) {
        super();

        this.clientId = clientId;
        this.platform = platform;
        this.serverVersion = serverVersion;
    }
    
    public PafServerAck(String clientId, String platform, String serverVersion, String applicationId, String dataSourceId) {
    	
    	this(clientId, platform, serverVersion);
    	this.applicationId = applicationId;
    	this.dataSourceId = dataSourceId;
    	
    }
    public String getClientId() {
        return clientId;
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public String getPlatform() {
        return platform;
    }
    public void setPlatform(String platform) {
        this.platform = platform;
    }
    public String getServerVersion() {
        return serverVersion;
    }
    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }

    public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(String dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	/**
	 * @return Returns the maxPassowordLength.
	 */
	public int getMaxPassowordLength() {
		return maxPassowordLength;
	}

	/**
	 * @param maxPassowordLength The maxPassowordLength to set.
	 */
	public void setMaxPassowordLength(int maxPassowordLength) {
		this.maxPassowordLength = maxPassowordLength;
	}

	/**
	 * @return Returns the minPassowordLength.
	 */
	public int getMinPassowordLength() {
		return minPassowordLength;
	}

	/**
	 * @param minPassowordLength The minPassowordLength to set.
	 */
	public void setMinPassowordLength(int minPassowordLength) {
		this.minPassowordLength = minPassowordLength;
	}

	public boolean isClientUpgradeRequired() {
		return clientUpgradeRequired;
	}

	public void setClientUpgradeRequired(boolean clientUpgradeRequired) {
		this.clientUpgradeRequired = clientUpgradeRequired;
	}

	public String getClientUpgradeUrl() {
		return clientUpgradeUrl;
	}

	public void setClientUpgradeUrl(String clientUpgradeUrl) {
		this.clientUpgradeUrl = clientUpgradeUrl;
	}

	/**
	 * @return the authMode
	 */
	public AuthMode getAuthMode() {
		return authMode;
	}

	/**
	 * @param authMode the authMode to set
	 */
	public void setAuthMode(AuthMode authMode) {
		this.authMode = authMode;
	}
}
