/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 * 
 */
package com.pace.server.comm;

import com.pace.base.app.PafUserDef;

/**
 * @author jmilliron
 *
 */
public class PafClientSecurityResponse {
	
	private boolean successful;
	
	private PafUserDef pafUserDef;
	
	private PafUserDef[] pafUserDefs;
	
	private String[] pafUserNames;
	
 	private String iV;

	/**
	 * @return the successful
	 */
	public boolean isSuccessful() {
		return successful;
	}

	/**
	 * @param successful the successful to set
	 */
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

	/**
	 * @return the pafUserDefs
	 */
	public PafUserDef[] getPafUserDefs() {
		return pafUserDefs;
	}

	/**
	 * @param pafUserDefs the pafUserDefs to set
	 */
	public void setPafUserDefs(PafUserDef[] pafUserDefs) {
		this.pafUserDefs = pafUserDefs;
	}

	/**
	 * @return the pafUserDef
	 */
	public PafUserDef getPafUserDef() {
		return pafUserDef;
	}

	/**
	 * @param pafUserDef the pafUserDef to set
	 */
	public void setPafUserDef(PafUserDef pafUserDef) {
		this.pafUserDef = pafUserDef;
	}

	/**
	 * @return the pafUserNames
	 */
	public String[] getPafUserNames() {
		return pafUserNames;
	}

	/**
	 * @param pafUserNames the pafUserNames to set
	 */
	public void setPafUserNames(String[] pafUserNames) {
		this.pafUserNames = pafUserNames;
	}

	public void setIV(String iV) {
		this.iV = iV;
	}

	public String getIV() {
		return iV;
	}

}
