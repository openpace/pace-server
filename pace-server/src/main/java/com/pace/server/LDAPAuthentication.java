/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server;

//import javax.naming.*;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang3.StringUtils;

import com.pace.base.PafException;
import com.pace.base.PafInvalidLogonInformation;
import com.pace.base.PafNotAbletoGetLDAPContext;
import com.pace.base.SystemStatus;
import com.pace.base.app.PafSecurityDomainUserNames;
import com.pace.base.app.PafUserDef;
import com.pace.base.server.ServerSettings;

/**
 * @author PMack
 *
 */
public class LDAPAuthentication implements IPAFAuthentication {
	
	
	/* (non-Javadoc)
	 * @see com.pace.base.server.IPAFAuthentication#authenticateUser(java.lang.String, com.pace.base.server.ServerSettings, java.lang.String, java.lang.String)
	 */
	public PafUserDef authenticateUserBySid(ServerSettings serverSettings, String userName, String sid) throws PafInvalidLogonInformation, PafNotAbletoGetLDAPContext{
		IPafLDAPProvider ldapProvider = null;
		LdapContext ctx = null;
		PafUserDef userDef = null;
		PafInvalidLogonInformation exceptionToThrow = null;
		
		try{
			//We just have the Ad Provider right now so...
			ldapProvider = new ActiveDirectoryProvider();
			
			ctx = ldapProvider.getLdapContext(serverSettings);
			
			userDef = ldapProvider.authenticateUserBySid(ctx, serverSettings, userName, sid);
			
			if (userDef == null){
				//set exception to be thrown as invalid user name exc
				exceptionToThrow = new PafInvalidLogonInformation();
			}
		}
		finally{
			ldapProvider.closeLDAPContext(ctx);
		}
		
		//if an exception needs to be thrown, throw
		if ( exceptionToThrow != null ) {
			throw exceptionToThrow;
		}
		
		return userDef;
	}
	
	public PafUserDef authenticateUserByUpn(ServerSettings serverSettings, String userPrincipalName, String password, String domain) throws PafInvalidLogonInformation, PafNotAbletoGetLDAPContext{
			IPafLDAPProvider ldapProvider;
		
		//We just have the Ad Provider right now so...
		ldapProvider = new ActiveDirectoryProvider();
		
		PafUserDef userDef = null;
		
		userDef = ldapProvider.authenticateUserByUpn(serverSettings, userPrincipalName, password, domain);
		
		PafInvalidLogonInformation exceptionToThrow = null;
		if (userDef == null){
			//set exception to be thrown as invalid user name exc
			exceptionToThrow = new PafInvalidLogonInformation();
		}
		
		//if an exception needs to be thrown, throw
		if ( exceptionToThrow != null ) {
			
			throw exceptionToThrow;
		}
		
		return userDef;
	}
	

	
	/* (non-Javadoc)
	 * @see com.pace.base.server.IPAFAuthentication#getUser(java.lang.String, com.pace.base.server.ServerSettings, java.lang.String, java.lang.String)
	 */
	public PafUserDef getUser(ServerSettings serverSettings, String userName, String domain)throws PafNotAbletoGetLDAPContext{
		IPafLDAPProvider ldapProvider = null;
		LdapContext ctx = null;
		PafUserDef userDef = null;
		
		try{
			//We just have the Ad Provider right now so...
			ldapProvider = new ActiveDirectoryProvider();
			
			ctx = ldapProvider.getLdapContext(serverSettings);
		
			userDef = new PafUserDef();
			
			userDef = ldapProvider.getUser(ctx, serverSettings, userName, domain);
		 }finally{
			 ldapProvider.closeLDAPContext(ctx);
		 }
		
		return userDef;
	}
	
	/* (non-Javadoc)
	 * @see com.pace.base.server.IPAFAuthentication#getUserNamesforSecurityGroups(com.pace.base.server.ServerSettings, java.util.Map)
	 */
	public TreeSet<PafSecurityDomainUserNames> getUserNamesforSecurityGroups  (ServerSettings serverSettings, Map<String, List<String>> paceGroups)throws PafNotAbletoGetLDAPContext{
		IPafLDAPProvider ldapProvider = null;
		TreeSet<PafSecurityDomainUserNames> userNamesforSecurityGroups = new TreeSet<PafSecurityDomainUserNames>();
		LdapContext ctx = null;

		try{
			//We just have the Ad Provider right now so...
			ldapProvider = new ActiveDirectoryProvider();
			
			ctx = ldapProvider.getLdapContext(serverSettings);
			
			if (ctx != null){
				userNamesforSecurityGroups = ldapProvider.getUserNamesforSecurityGroups(ctx, serverSettings, paceGroups);
			}
		} finally{
			ldapProvider.closeLDAPContext(ctx);
		}
		
		return userNamesforSecurityGroups;
	}
	
	/* (non-Javadoc)
	 * @see com.pace.base.server.IPAFAuthentication#getSecurityGroups(com.pace.base.server.ServerSettings)
	 */
	public Map<String, TreeSet<String>> getSecurityGroups(ServerSettings serverSettings) throws PafNotAbletoGetLDAPContext{
		IPafLDAPProvider ldapProvider = null;
		LdapContext ctx =  null;
		Map<String, TreeSet<String>> securityGroups = null;

		try{
			//We just have the Ad Provider right now so...
			ldapProvider = new ActiveDirectoryProvider();
			
			ctx = ldapProvider.getLdapContext(serverSettings);
			
			securityGroups = ldapProvider.getSecurityGroups(ctx, serverSettings);
		}finally{
			ldapProvider.closeLDAPContext(ctx);
		}
		
		return securityGroups;
	}
	
	public Map<String, List<String>> validateUsers(ServerSettings serverSettings, Map<String, List<String>> users) throws PafNotAbletoGetLDAPContext{
		IPafLDAPProvider ldapProvider = null;
		LdapContext ctx =  null;
		Map<String, List<String>> invalidUsers = null;
		
		try{
			//We just have the Ad Provider right now so...
			ldapProvider = new ActiveDirectoryProvider();
			
			ctx = ldapProvider.getLdapContext(serverSettings);
			
			invalidUsers = ldapProvider.validateUsers(ctx, serverSettings, users);
		}finally{
			ldapProvider.closeLDAPContext(ctx);
		}
		
		return invalidUsers;
	}

	@Override
	public SystemStatus testProviderContext(ServerSettings serverSettings) {
		IPafLDAPProvider ldapProvider = null;
		LdapContext ctx = null;
		SystemStatus results = new SystemStatus();
		String resultsString = null;
		boolean success = false;
		StringBuilder environment = new StringBuilder();
		
		try{
			//We just have the Ad Provider right now so...
			ldapProvider = new ActiveDirectoryProvider();
			
			ctx = ldapProvider.getLdapContext(serverSettings, serverSettings.getLdapSettings().getSecurityPrincipal(), serverSettings.getLdapSettings().getSecurityCredentials());
			
			success = true;

			@SuppressWarnings("unchecked")
			Hashtable<String, String> enviro = (Hashtable<String, String>) ctx.getEnvironment();
			Set<String> keys = enviro.keySet();
	        for(String key: keys){
	        	//Don't add the password 
	        	String value = enviro.get(key);
	        	if(!key.equalsIgnoreCase("java.naming.security.credentials")){
	        		value = "xxxxxxxxxxx";
	        	}
	        	environment.append(key).append("=").append(value).append("\r\n");
	        }
			
			resultsString = String.format("Test: Passed.  \r\nCurrent Environment: %s", environment.toString());
			
		} catch(NamingException ex){
			resultsString = String.format("Test: Failed.  \r\nError: %s", ex.getMessage());
		} catch(PafException ex){
			if(ex.getMessageDetails() != null && ex.getMessageDetails().size() > 0){
				resultsString = String.format("Test: Failed.  \r\nError: %s\r\nExtended Details: %s", ex.getMessage(), StringUtils.join(ex.getMessageDetails(), ","));
			}else{
				resultsString = String.format("Test: Failed.  \r\nError: %s", ex.getMessage());
			}
		} 
		
		results.setCanConnect(success);
		results.setTestResults(resultsString);
		
		return results;
	}
}

