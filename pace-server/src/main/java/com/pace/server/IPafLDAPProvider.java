/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.naming.ldap.LdapContext;

import com.pace.base.PafException;
import com.pace.base.PafNotAbletoGetLDAPContext;
import com.pace.base.app.PafSecurityDomainUserNames;
import com.pace.base.app.PafUserDef;
import com.pace.base.server.ServerSettings;

public interface IPafLDAPProvider {

	public LdapContext getLdapContext(ServerSettings serverSettings) throws PafNotAbletoGetLDAPContext;
	
	public LdapContext getLdapContext(ServerSettings serverSettings, String securityPrincipal, String securityCredentials) throws PafException;

	public void closeLDAPContext(LdapContext ctx);

	public PafUserDef getUser(LdapContext ctx,	ServerSettings serverSettings, String userName, String domain);
	
	public Map<String, TreeSet<String>> getSecurityGroups(
			LdapContext ctx, ServerSettings serverSettings);

	public TreeSet<PafSecurityDomainUserNames> getUserNamesforSecurityGroups(
			LdapContext ctx, ServerSettings serverSettings, Map<String, List<String>> paceGroups);

	public PafUserDef authenticateUserBySid (LdapContext ctx, ServerSettings serverSettings, String securityPrincipal, String sid);
	
	public PafUserDef authenticateUserByUpn (ServerSettings serverSettings, String securityPrincipal, String domain, String password) throws PafNotAbletoGetLDAPContext;
	
	public Map<String, List<String>> validateUsers(LdapContext ctx, ServerSettings serverSettings, Map<String, List<String>> users);
	

}