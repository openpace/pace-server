/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server;

import java.util.Map;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;

import com.pace.db.PacePOJO;

/**
 * @author jim
 * Just a wrapper class for the data matrix classes, meant to carry grids of data from mathematical operations
 *
 */
public class PaceDataSet extends PacePOJO {
	
	private DoubleMatrix2D data;
	transient private Map<Integer, String>  clusterRowMap;
	
	
	public PaceDataSet() {
	}

	public PaceDataSet(double[][] inData) {
		this.data = DoubleFactory2D.dense.make(inData);
	}
	
	public PaceDataSet(double[][] inData, Map<Integer, String> clusterRowMap) {
		this.data = DoubleFactory2D.dense.make(inData);
		this.clusterRowMap = clusterRowMap;
	}
	
	public double[] getRow(int index) {
		return data.viewRow(index).toArray();
	}

	public int getRowCount() {
		return data.rows();
	}
	
	public int getColCount() {
		return data.columns();
	}
	
	public double[][] getData() {
		return data.toArray();
	}

	public void setData(double[][] inData) {
		data.assign(inData);
	}

	public Map<Integer, String>  getClusterRowMap() {
		return clusterRowMap;
	}

	public void setClusterRowMap(Map<Integer, String>  clusterRowMap) {
		this.clusterRowMap = clusterRowMap;
	}
}
