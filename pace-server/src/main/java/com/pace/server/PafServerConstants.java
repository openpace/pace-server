/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server;

import com.pace.base.PafBaseConstants;
import com.pace.base.server.PafMetaData;

/**
 * Application server constants
 *
 * @version	x.xx
 * @author Jim Watkins
 *
 */
public class PafServerConstants {

	public static final String SERVER_VERSION = "3.0.2 GA";

	public static String SERVER_SETTINGS_FILE = PafMetaData.getConfigServerDirPath()  + PafBaseConstants.FN_ServerSettings;
	public static String LDAP_SETTINGS_FILE = PafMetaData.getConfigServerDirPath()  + PafBaseConstants.FN_LdapSettings;
	public static String RDB_DATASOURCES_FILE = PafMetaData.getConfigServerDirPath()  + PafBaseConstants.FN_RdbClientDataSources;
	public static String MDB_DATASOURCES_FILE = PafMetaData.getConfigServerDirPath()  + PafBaseConstants.FN_MdbDataSources;

}
