/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server;

import java.util.ArrayList;
import java.util.List;

import com.pace.base.rules.MemberSet;
import com.pace.base.rules.RoundingRule;
import com.pace.base.utility.PafXStream;
import com.thoughtworks.xstream.XStream;



public class tempExport {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
		MemberSet ms = new MemberSet();
		ms.setMember("SLS_DLR");
		ms.setDimension("Measures");
		List<MemberSet> memberSets = new ArrayList<MemberSet>();
		memberSets.add(ms);
		
		RoundingRule r1 = new RoundingRule();
		r1.setMemberList(memberSets);
		r1.setRoundingFunction("Round");
		r1.setDigits(2);
		
		RoundingRule r2 = new RoundingRule();
		r2.setMemberList(memberSets);
		r2.setRoundingFunction("Round");
		r2.setDigits(2);
		
		RoundingRule[] rr = new RoundingRule[2];
		rr[0] = r1;
		rr[1] = r2;
		PafXStream.setMode(XStream.NO_REFERENCES);
		
		//PafMetaData.exportRoundingRules(rr);
		PafXStream.exportObjectToXml(rr, "C:\\ProgramData\\Open Pace\\Pace Server\\SVR\\conf\\paf_rounding_rules.xml");
	}

}
