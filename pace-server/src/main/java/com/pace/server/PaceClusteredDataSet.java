/*******************************************************************************
 * Copyright (c) 2017-2019 Contributors to Open Pace and others.
 *  
 * The OPEN PACE product suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *  
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *  
 * You should have received a copy of the GNU General Public License along with
 * this  software. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.pace.server;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.clustering.Cluster;

import com.pace.server.eval.DoublePoint;

public class PaceClusteredDataSet {
	private PaceDataSet clusterData;
	
	private Map<String, Integer>clusterKeys = new HashMap<String, Integer>();
	private Map<Integer, String> clusterRowMap = new HashMap<Integer, String>();
	private List<Cluster<DoublePoint>> clusters;

	public List<Cluster<DoublePoint>> getClusters() {
		return clusters;
	}

	public void setClusters(List<Cluster<DoublePoint>> clusters) {
		this.clusters = clusters;
	}

	public PaceDataSet getClusterData() {
		return clusterData;
	}

	public void setClusterData(PaceDataSet clusterData) {
		this.clusterData = clusterData;
	}

	public Map<String, Integer> getClusterKeys() {
		return clusterKeys;
	}

	public void setClusterKeys(Map<String, Integer> clusterKeys) {
		this.clusterKeys = clusterKeys;
	}

	public Map<Integer, String> getClusterRowMap() {
		return clusterRowMap;
	}

	public void setClusterRowMap(Map<Integer, String> clusterRowMap) {
		this.clusterRowMap = clusterRowMap;
	}
	
}
