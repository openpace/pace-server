@rem ***************************************************************************
@rem Copyright (c) 2017-2019 Contributors to Open Pace and others.
@rem  
@rem The OPEN PACE product suite is free software: you can redistribute it and/or
@rem modify it under the terms of the GNU General Public License as published by
@rem the Free Software Foundation, either version 3 of the License, or (at your
@rem option) any later version.
@rem  
@rem This software is distributed in the hope that it will be useful, but WITHOUT
@rem ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
@rem or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
@rem License for more details.
@rem  
@rem You should have received a copy of the GNU General Public License along with
@rem this  software. If not, see <http://www.gnu.org/licenses/>.
@rem ***************************************************************************
set proj_root=\proj_wrksp\eclipse\PafServer
set class_dir=%proj_root%\WebRoot\WEB-INF\classes
set lib_dir=%proj_root%\WebRoot\WEB-INF\lib

\java\sun\jwsdp-1.6\jaxrpc\bin\wscompile.bat -cp "%class_dir%;%lib_dir%\esbprv.jar;%lib_dir%\paf_base.jar;%lib_dir%\jep-2.4.0.jar" -d %class_dir% -gen:server -f:documentliteral -mapping %proj_root%\WebRoot\WEB-INF\jaxrpc-mapping.xml %proj_root%\config.xml